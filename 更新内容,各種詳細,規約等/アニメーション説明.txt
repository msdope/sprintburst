アニメーション説明

・サバイバー(ハッカー側)
00	Walk・・・歩く
01	Run・・・走る
02	Wait・・・立っているときの待機モーション
03	SitDown・・・しゃがむ
04	SitDownWait・・・しゃがんだ時の待機モーション
05	SitDownWalk・・・しゃがみ歩き
06	Stand・・・立つ
07	Stand from Down・・・立ちからダウンモーションへ
08	SitDown from Down・・・しゃがみからダウンモーションへ
09	Crawls around・・・這いずりモーション
10	Crawls around from Stand・・・這いずりモーションから立ちモーションへ

・キラー(セキュリティ側)
00	Movement・・・移動
01	Wait・・・待機モーション
02	Attack(Sink)・・・攻撃時の溜めモーション
03	Attack・・・溜めモーションから攻撃モーション
04	Attack(NoHit)・・・攻撃が外れたとき
05	Attack(Hit)・・・攻撃が当たったとき




