#include "Game/Application.h"
#include "Game/System/Scene/Hk/HkScene.h"
#include "Game/System/Scene/TestScene/TestScene.h"


LRESULT CALLBACK WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EzLib::MainFrame_WindowProc(hWnd, msg, wParam, lParam);
	return DefWindowProc(hWnd, msg, wParam, lParam);
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpszArgs, int nWinMode)
{

#ifdef _DEBUG
	// メモリリーク検知
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	// COM初期化
	CoInitializeEx(nullptr, COINIT_MULTITHREADED);

	// mbstorwsc_s関数で日本語対応にするため
	setlocale(LC_ALL, "japan");

	ZWindowProperties properties{ hInstance,1280,720,false,false,&WindowProc};

	if (IDYES == MessageBox(NULL, "フルスクリーンにしますか?", "確認", MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2))
		properties.UseFullScreen = true;

	Application app("Sprint Burst", properties);
	app.SetFrameRate(60);
	app.m_SceneMgr.SubmitSceneGenerater<TestScene>("Test");
	app.m_SceneMgr.ChangeScene("Test");
	app.Start();

	// COM開放
	CoUninitialize();


	return 0;
}

