#include "PCH/pch.h"

namespace EzLib
{
#pragma region ESModelLoader

	ZEffectManager::ESModelLoader::ESModelLoader(Effekseer::ModelLoader* modelLoader)
	{
		m_ModelLoader = modelLoader;
	}

	ZEffectManager::ESModelLoader::~ESModelLoader()
	{
		ES_SAFE_DELETE(m_ModelLoader);
	}

	void* ZEffectManager::ESModelLoader::Load(const EFK_CHAR* path)
	{
		auto key = EFKString(path);

		auto it = m_MpdelCache.find(key);

		if (it != m_MpdelCache.end())
		{
			it->second.Count++;
			return it->second.pData;
		}

		CachedModel v;
		v.pData = m_ModelLoader->Load(path);

		if (v.pData == nullptr)
			return nullptr;

		m_MpdelCache[key] = v;
		m_KeyMap[v.pData] = key;
		
		return v.pData;
	}

	void ZEffectManager::ESModelLoader::Unload(void* data)
	{
		if (data == nullptr) return;
		auto key = m_KeyMap[data];

		auto it = m_MpdelCache.find(key);

		if (it == m_MpdelCache.end())
			return;

		it->second.Count--;
		if (it->second.Count > 0)
			return;

		m_ModelLoader->Unload(it->second.pData);
		m_KeyMap.erase(data);
		m_MpdelCache.erase(key);
		
	}

#pragma endregion

#pragma region ESTextureLoader

	ZEffectManager::ESTextureLoader::ESTextureLoader(Effekseer::TextureLoader* textureLoader)
	{
		m_TextureLoader = textureLoader;
	}

	ZEffectManager::ESTextureLoader::~ESTextureLoader()
	{
		ES_SAFE_DELETE(m_TextureLoader);
	}

	Effekseer::TextureData* ZEffectManager::ESTextureLoader::Load(const EFK_CHAR* path, Effekseer::TextureType textureType)
	{
		auto key = EFKString(path);

		auto it = m_TextureCache.find(key);

		if (it != m_TextureCache.end())
		{
			it->second.Count++;
			return it->second.pData;
		}

		CachedTexture v;
		v.pData = m_TextureLoader->Load(path, textureType);

		if (v.pData == nullptr)
			return nullptr;

		m_TextureCache[key] = v;
		m_KeyMap[v.pData] = key;

		return v.pData;
	}

	void ZEffectManager::ESTextureLoader::Unload(Effekseer::TextureData* data)
	{
		if (data == nullptr) return;
		auto key = m_KeyMap[data];

		auto it = m_TextureCache.find(key);

		if (it == m_TextureCache.end())
			return;

		it->second.Count--;
		if (it->second.Count > 0)
			return;

		m_TextureLoader->Unload(it->second.pData);
		m_KeyMap.erase(data);
		m_TextureCache.erase(key);
	}

#pragma endregion

	ZEffectManager* ZEffectManager::s_pInstance = nullptr;

	ZEffectManager::ZEffectManager()
	{
		s_pInstance = this;
	}

	ZEffectManager::~ZEffectManager()
	{
		Release();
	}
	
	bool ZEffectManager::InitManager(ID3D11Device* device, ID3D11DeviceContext* context, size_t maxEffectInst,size_t maxSprite)
	{
		if (device == nullptr || context == nullptr)
			return false;
		if (maxEffectInst <= 0)
			maxEffectInst = DefaultMaxEffectInst;

		m_ESRenderer = EffekseerRendererDX11::Renderer::Create(device, context, maxSprite);

		m_ESManager = Effekseer::Manager::Create(maxEffectInst);
		m_ESManager->SetCoordinateSystem(Effekseer::CoordinateSystem::LH);
		
		// 描画用インスタンスから描画機能を設定
		m_ESManager->SetSpriteRenderer(m_ESRenderer->CreateSpriteRenderer());
		m_ESManager->SetRibbonRenderer(m_ESRenderer->CreateRibbonRenderer());
		m_ESManager->SetRingRenderer(m_ESRenderer->CreateRingRenderer());
		m_ESManager->SetTrackRenderer(m_ESRenderer->CreateTrackRenderer());
		m_ESManager->SetModelRenderer(m_ESRenderer->CreateModelRenderer());

		// 描画用インスタンスからテクスチャの読込機能を設定
		m_ESManager->SetTextureLoader(new ESTextureLoader(m_ESRenderer->CreateTextureLoader()));
		m_ESManager->SetModelLoader(new ESModelLoader(m_ESRenderer->CreateModelLoader()));

		// XAudio2の初期化
		XAudio2Create(&m_XA2);
		m_XA2->CreateMasteringVoice(&m_XA2_Master);

		return true;
	}

	bool ZEffectManager::InitSound(size_t numMonoVoice, size_t numStereoVoice)
	{
		if (m_ESManager == nullptr || m_XA2 == nullptr)
			return false;

		if (numMonoVoice <= 0)
			numMonoVoice = DefaultNumMonoVoice;
		if (numStereoVoice <= 0)
			numStereoVoice = DefaultNumStereoVoice;
		
		// 音再生用インスタンス作成
		m_ESSound = EffekseerSound::Sound::Create(m_XA2, numMonoVoice, numStereoVoice);
		
		// 音再生用インスタンス作成
		m_ESManager->SetSoundPlayer(m_ESSound->CreateSoundPlayer());
		m_ESManager->SetSoundLoader(m_ESSound->CreateSoundLoader());

		return true;
	}

	void ZEffectManager::StartUpServer(uint16 port)
	{
		if (m_ESServer != nullptr)return;
		m_ESServer = Effekseer::Server::Create();
		m_ESServer->Start(port);
		m_ServerPort = port;
	}

	void ZEffectManager::ReStartServer(uint16 port)
	{
		if (m_ESServer == nullptr) return;
		m_ESServer->Stop();
		m_ESServer->Start(port);
		m_ServerPort = port;
	}

	void ZEffectManager::StopServer()
	{
		if (m_ESServer == nullptr) return;
		m_ESServer->Stop();
	}

	bool ZEffectManager::RegistToServer(Effekseer::Effect* effect)
	{
		if (m_ESServer == nullptr) return false;
		if (effect == nullptr) return false;

		try
		{
			auto& effectName = m_EffectNames.at(effect);
			ZWString wsEffectName = ConvertStringToWString(effectName);
			m_ESServer->Regist((const EFK_CHAR*)wsEffectName.c_str(), effect);
		}
		catch(std::out_of_range&)
		{
			return false;
		}
		
		return true;
	}

	void ZEffectManager::RegistAllEffectsToServer()
	{
		if (m_EffectNames.empty())
			return;

		// 全エフェクト登録
		for (auto& effectNamePair : m_EffectNames)
		{
			ZWString wsEffectName = ConvertStringToWString(effectNamePair.second);
			m_ESServer->Regist((const EFK_CHAR*)wsEffectName.c_str(), effectNamePair.first);
		}

	}

	bool ZEffectManager::UnregistFromServer(Effekseer::Effect* effect)
	{
		if (m_ESServer == nullptr) return false;
		if (effect == nullptr) return false;

		m_ESServer->Unregist(effect);
		return true;
	}

	void ZEffectManager::SetCullingWorld(const ZVec3& worldSize, int32 layerCount)
	{
		if (m_ESManager == nullptr)return;
		m_ESManager->CreateCullingWorld(worldSize.x, worldSize.y, worldSize.z, layerCount);
	}

	void ZEffectManager::Release()
	{
		StopServer();
		ES_SAFE_DELETE(m_ESServer);

		m_PlayRequests.clear();
		m_OnRemoveEffects.clear();
		m_OnStopedFunctionMap.clear();

		// 再生中の全エフェクト停止
		m_ESManager->StopAllEffects();

		// 全Effect破棄
		ReleaseAllEffects();

	#define SAFE_DESTROY(x) if(x){x->Destroy();x = nullptr;}

		SAFE_DESTROY(m_ESManager);
		SAFE_DESTROY(m_ESSound);
		SAFE_DESTROY(m_ESRenderer);

	#undef SAFE_DESTROY

		if (m_XA2_Master != nullptr)
		{
			m_XA2_Master->DestroyVoice();
			m_XA2_Master = nullptr;
		}
		ES_SAFE_RELEASE(m_XA2);

	}

	void ZEffectManager::ReleaseAllEffects()
	{
		// Effect破棄
		if (m_Effects.empty() == false)
		{
			for_each(m_Effects.begin(), m_Effects.end(),
				[](auto& effectPair)
			{
				ES_SAFE_RELEASE(effectPair.second);
			});
		}

		m_Effects.clear();
		m_EffectNames.clear();
		m_ESHandleMap.clear();
		m_EffectHandleMap.clear();

		m_PlayEffectCounter = 0;
	}

	void ZEffectManager::ReleaseEffect(Effekseer::Effect* effect)
	{
		auto& it = std::find_if(m_Effects.begin(), m_Effects.end(),
			[&effect](auto& effectPair)
		{
			return effectPair.second == effect;
		});
		
		// Effect破棄
		ES_SAFE_RELEASE(it->second);
		
		m_Effects.erase(it);
		m_EffectNames.erase(effect);

	}

	Effekseer::Effect* ZEffectManager::LoadEffect(const ZString& filepath)
	{
		if (m_ESManager == nullptr)return nullptr;

		auto& it = m_Effects.find(filepath);
		if (it != m_Effects.end())
			return it->second; // ロード済み
		
		
		ZWString wsFilepath = ConvertStringToWString(filepath); // shift-jis -> utf-16
		auto* effect = Effekseer::Effect::Create(m_ESManager, (const EFK_CHAR*)wsFilepath.c_str());
		
		if (effect == nullptr)
			return nullptr;

		m_Effects[filepath] = effect;
		{
			auto effectName = ZPathUtil::GetFileName_NonExt(filepath);
			m_EffectNames[effect] = std::move(effectName);
		}
		return effect;
	}

	ZEffectManager::EffectHandle ZEffectManager::SubmitPlayEffect(Effekseer::Effect* effect)
	{
		std::lock_guard<std::mutex> lg(m_PlayRequestMtx);
		return _SubmitPlayEffect(effect,ZVec3::Zero,OnPlayEffectFunction());
	}

	ZEffectManager::EffectHandle ZEffectManager::SubmitPlayEffect(Effekseer::Effect* effect, const OnPlayEffectFunction& onPlayEffectFunc)
	{
		std::lock_guard<std::mutex> lg(m_PlayRequestMtx);
		return _SubmitPlayEffect(effect, ZVec3::Zero, onPlayEffectFunc);
	}

	ZEffectManager::EffectHandle ZEffectManager::SubmitPlayEffect(Effekseer::Effect* effect, const ZVec3& pos, const OnPlayEffectFunction & onPlayEffectFunc)
	{
		std::lock_guard<std::mutex> lg(m_PlayRequestMtx);
		return _SubmitPlayEffect(effect, pos, onPlayEffectFunc);
	}

	bool ZEffectManager::PauseEffect(const EffectHandle& handle)
	{
		if (m_ESManager == nullptr)
			return false;

		if (handle == NullEffectHandle)
			return false;

		Effekseer::Handle esHandle = EffectHandleToEffekseerHandle(handle);

		if (esHandle == NullEffectHandle)
			return false;

		if (m_ESManager->GetPaused(esHandle) == true)
			return false;

		m_ESManager->SetPaused(esHandle, false);

		return true;
	}

	bool ZEffectManager::UnpauseEffect(const EffectHandle& handle)
	{
		if (m_ESManager == nullptr)
			return false;

		if (handle == NullEffectHandle)
			return false;

		Effekseer::Handle esHandle = EffectHandleToEffekseerHandle(handle);

		if (esHandle == NullEffectHandle)
			return false;

		if (m_ESManager->GetPaused(esHandle) == false)
			return false;

		m_ESManager->SetPaused(esHandle, true);

		return true;
	}

	bool ZEffectManager::StopEffect(const EffectHandle& handle)
	{
		if (m_ESManager == nullptr)
			return false;

		if (handle == NullEffectHandle)
			return false;

		Effekseer::Handle esHandle = EffectHandleToEffekseerHandle(handle);

		if (esHandle == NullEffectHandle)
			return false;

		// 表示されていない -> 停止済み or 再生されていない
		if (m_ESManager->GetShown(esHandle) == false)
			return false;

		// 停止
		m_ESManager->StopEffect(esHandle);
		
		// 停止したEffectのHandleは管理下から除外
		m_EffectHandleMap.erase(m_ESHandleMap[handle]);
		m_ESHandleMap.erase(handle);

		// コールバック関数削除
		{
			auto& it = m_OnStopedFunctionMap.find(handle);
			if (it != m_OnStopedFunctionMap.end())
				m_OnStopedFunctionMap.erase(it);
		}

		return true;
	}

	void ZEffectManager::RegistOnStopedEffectFunction(const EffectHandle& handle, const OnStopedEffectFunction& onStopedFunc)
	{
		if (handle == NullEffectHandle)return;
		// 無効な関数オブジェクトは無視
		if (!onStopedFunc)return;

		// handleが登録されていない -> 再生されていない or 停止済みなら無視
		{
			auto& it = m_ESHandleMap.find(handle);
			if (it == m_ESHandleMap.end())return;
		}

		m_OnStopedFunctionMap[handle].emplace_back(std::move(onStopedFunc));
	}

	void ZEffectManager::UpdateAudioListener(const ZMatrix& mat)
	{
		// Sound Listenerセット
		ZVec3 listenerPos = mat.GetPos();
		Effekseer::Vector3D pos{ listenerPos.x,listenerPos.y,listenerPos.z };
		Effekseer::Vector3D at;
		Effekseer::Vector3D up;
		{
			ZVec3 camUp = mat.GetYAxis();
			ZVec3 camAt = mat.GetZAxis();
			up = { camUp.x,camUp.y,camUp.z };
			at = { camAt.x,camAt.y,camAt.z };
		}
		m_ESSound->SetListener(pos, at, up);

	}

	void ZEffectManager::Update()
	{
		// Server更新
		if (m_ESServer) m_ESServer->Update();

		// Effekseer更新
#define USE_THREAD 0
#if !USE_THREAD
		m_ESManager->Update();
#else
		auto fut = std::async(std::launch::async, [this]() { m_ESManager->Update(); });
#endif	
		// Start処理
		PlayAllRequestEffects();

#if USE_THREAD
		fut.wait();
#endif
		// Remove(停止)されたEffectの処理
		for (auto& removeEffectPair : m_OnRemoveEffects)
		{
			// 停止したEffectのHandleを管理下から除外
			{
				auto& it = m_ESHandleMap.find(removeEffectPair.first);
				if (it == m_ESHandleMap.end())
					goto CallBackProc;
				
				m_EffectHandleMap.erase(it->second);
				m_ESHandleMap.erase(it);
			}

		CallBackProc:
			// コールバック関数処理
			{
				auto& it = m_OnStopedFunctionMap.find(removeEffectPair.first);
				if (it == m_OnStopedFunctionMap.end())
					continue;
				if (it->second.empty())
					continue;
				
				for (auto& onStopedFunc : it->second)
					onStopedFunc(removeEffectPair.second);
				m_OnStopedFunctionMap.erase(it);
			}
		}
		
			// カメラ関係の行列はZCameraクラスを継承したクラスで最後に更新した行列を使う
		{
			ZMatrix mView = ZCamera::LastCam.mView;
			ZMatrix mProj = ZCamera::LastCam.mProj;
			Effekseer::Matrix44 mESCam;
			Effekseer::Matrix44 mESProj;
			// 行列コピー
			memcpy(mESCam.Values, mView.m, sizeof Effekseer::Matrix44);
			memcpy(mESProj.Values, mProj.m, sizeof Effekseer::Matrix44);

			// 投影行列セット
			m_ESRenderer->SetProjectionMatrix(mESProj);
			// カメラ行列セット
			m_ESRenderer->SetCameraMatrix(mESCam);
		}

	}

	void ZEffectManager::Draw()const
	{
		m_ESRenderer->BeginRendering();
		// カリング計算
		m_ESManager->CalcCulling(m_ESRenderer->GetCameraProjectionMatrix(), false);
		
		// 描画
		m_ESManager->Draw();

		m_ESRenderer->EndRendering();
	}

	void ZEffectManager::PlayAllRequestEffects()
	{
		if (m_PlayRequests.empty()) return;

		std::lock_guard<std::mutex> lg(m_PlayRequestMtx);
		auto it = m_PlayRequests.begin();
		while (it != m_PlayRequests.end())
		{
			// Effect再生
			auto handle = m_ESManager->Play(it->pEffect, it->Pos.x, it->Pos.y, it->Pos.z);
			m_ESHandleMap[it->handle] = handle;
			m_EffectHandleMap[handle] = it->handle;
			// コールバック設定
			m_ESManager->SetRemovingCallback(handle, &ZEffectManager::OnRemoveEffectInstance);
			if (it->OnPlayEffectFunc)
				it->OnPlayEffectFunc(handle, m_ESManager);

			it = m_PlayRequests.erase(it);
		}
	}

	void EFK_STDCALL ZEffectManager::OnRemoveEffectInstance(Effekseer::Manager* manager, Effekseer::Handle ESHandle, bool isRemovingManager)
	{
		if (s_pInstance == nullptr)
			return;

		EffectHandle handle = s_pInstance->EffekseerHandleToEffectHandle(ESHandle);
		if (handle == NullEffectHandle) return;
		
		// isRemovingManager == true
		//	-> Effekseer::ManagerのDestroy関数が呼ばれた時で
		//	m_OnRemoveEffectsに貯めずにその場でEffect停止時のコールバック呼び出し
		if (isRemovingManager)
		{
			// コールバック関数処理
			auto& it = s_pInstance->m_OnStopedFunctionMap.find(handle);
			if (it == s_pInstance->m_OnStopedFunctionMap.end())return;
			if (it->second.empty())return;

			for (auto& onStopedFunc : it->second)
				onStopedFunc(isRemovingManager);
		}
		else
			s_pInstance->m_OnRemoveEffects.emplace_back(handle,isRemovingManager);
	}

}

