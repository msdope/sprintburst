#ifndef __ZEFFECT_MANAGER_H__
#define __ZEFFECT_MANAGER_H__

namespace EzLib
{
	class ZEffectManager
	{
	public:
		using EffectHandle = long;
		using OnPlayEffectFunction = std::function<void(const Effekseer::Handle, Effekseer::Manager*)>;
		// 引数 isRemovingManager : Manager破棄時にエフェクト破棄されたか
		using OnStopedEffectFunction = std::function<void(bool isRemovingManager)>;

		// デフォルトの最大エフェクトインスタンス数
		static constexpr size_t DefaultMaxEffectInst = 4000;
		// デフォルトの最大スプライト数
		static constexpr size_t DefaultMaxSprite = DefaultMaxEffectInst * 5;
		// デフォルトのモノラルボイス数
		static constexpr size_t DefaultNumMonoVoice = 16;
		// デフォルトのステレオボイス数
		static constexpr size_t DefaultNumStereoVoice = 16;

		static constexpr EffectHandle NullEffectHandle = -1;

	private:
		using EFKString = std::basic_string<EFK_CHAR, std::char_traits<EFK_CHAR>, ZSTLAllocator<EFK_CHAR>>;
		
		// モデルローダー(参照カウント有り)
		class ESModelLoader : public ::Effekseer::ModelLoader
		{
		private:
			struct CachedModel
			{
				void* pData{ nullptr };
				int32 Count{ 1 };
			};

		public:
			ESModelLoader(Effekseer::ModelLoader* modelLoader);

			virtual ~ESModelLoader();

			virtual void* Load(const EFK_CHAR* path) override;

			virtual void Unload(void* data) override;

		private:
			Effekseer::ModelLoader*		m_ModelLoader;
			ZMap<EFKString, CachedModel>	m_MpdelCache;
			ZMap<void*, EFKString>	m_KeyMap;
		};

		// テクスチャローダー(参照カウント有り)
		class ESTextureLoader : public ::Effekseer::TextureLoader
		{
		private:
			struct CachedTexture
			{
				Effekseer::TextureData*	pData{ nullptr };
				int32 Count{ 1 };
			};

		public:
			ESTextureLoader(Effekseer::TextureLoader* textureLoader);
			virtual ~ESTextureLoader();

			virtual ::Effekseer::TextureData* Load(const EFK_CHAR* path, ::Effekseer::TextureType textureType) override;

			virtual void Unload(::Effekseer::TextureData* data) override;

		private:
			Effekseer::TextureLoader* m_TextureLoader;
			ZMap<EFKString, CachedTexture> m_TextureCache;
			ZMap<Effekseer::TextureData*, EFKString> m_KeyMap;
		};

		struct PlayRequestData
		{
			EffectHandle handle;
			Effekseer::Effect* pEffect;				// 再生するEffect
			ZVec3 Pos;
			OnPlayEffectFunction OnPlayEffectFunc;	// 再生時に実行する関数(座標,角度調整など)
		};

	public:
		ZEffectManager();
		// コピー禁止
		ZEffectManager(const ZEffectManager&) = delete;
		void operator=(const ZEffectManager&) = delete;
		void operator=(const ZEffectManager&&) = delete;

		~ZEffectManager();

#pragma region 初期化

		// EffekseerのManager初期化
		bool InitManager(ID3D11Device* device, ID3D11DeviceContext* context, size_t maxEffectInst = DefaultMaxEffectInst,size_t maxSprite = DefaultMaxSprite);
		// EffekseerのSound初期化
		bool InitSound(size_t numMonoVoice = DefaultNumMonoVoice, size_t numStereoVoice = DefaultNumStereoVoice);
		
#pragma endregion

#pragma region Effekseer::Server関係

		// Effekseerのサーバー起動
		void StartUpServer(uint16 port);
		// Effekseerのサーバーをポート番号を変更し再起動
		void ReStartServer(uint16 port);
		// Effekseerのサーバー停止
		void StopServer();
		// リロード対象エフェクトの登録
		// Effekseerでの編集をリモートで反映させたいエフェクトをこの関数で登録できる
		bool RegistToServer(Effekseer::Effect* effect);
		// 現時点でロードしている全エフェクトをリロード対象に登録
		void RegistAllEffectsToServer();
		// リロード対象エフェクトの登録解除
		bool UnregistFromServer(Effekseer::Effect* effect);

#pragma endregion

		// カリングを行う範囲の設定
		// layerCountが大きいほど高速にカリングを行えるがメモリも消費する 最大6程度
		void SetCullingWorld(const ZVec3& worldSize, int32 layerCount);

		// 解放
		void Release();

		// 全Effect破棄
		void ReleaseAllEffects();

		// 任意のEffect破棄
		void ReleaseEffect(Effekseer::Effect* effect);

		// Effect読み込み(Effectは参照カウンタを持っているため取得先でもRelease関数を呼ぶように)
		Effekseer::Effect* LoadEffect(const ZString& filepath);

#pragma region Effect操作系

		// 再生するエフェクトを登録(Update関数内でEffectの再生処理を再生中のEffectの更新処理と並列処理する)
		ZEffectManager::EffectHandle SubmitPlayEffect(Effekseer::Effect* effect);
		ZEffectManager::EffectHandle SubmitPlayEffect(Effekseer::Effect* effect, const OnPlayEffectFunction& onPlayEffectFunc);
		ZEffectManager::EffectHandle SubmitPlayEffect(Effekseer::Effect* effect, const ZVec3& pos, const OnPlayEffectFunction& onPlayEffectFunc);

		// Effect一時停止
		// 一時停止実行時 : true
		// すでに一時停止状態 or handleが不正な値 or etc... : false を返す
		bool PauseEffect(const EffectHandle& handle);

		// Effect一時停止状態解除
		// 再生再開処理実行時 : true
		// すでに再生状態 or handleが不正な値 or etc... : false を返す
		bool UnpauseEffect(const EffectHandle& handle);

		// Effect停止
		// 停止処理実行時 : true
		// 再生されていない or handleが不正な値 or etc... : false を返す
		bool StopEffect(const EffectHandle& handle);

#pragma endregion

		// Effect停止時に実行するコールバック関数の登録
		//	※ StopEffect関数で停止した際は呼ばれない(明示的に停止したならコールバックによる処理は不要と判断)
		void RegistOnStopedEffectFunction(const EffectHandle& handle, const OnStopedEffectFunction& onStopedFunc);

		// AudioListenerの位置,向き更新
		void UpdateAudioListener(const ZMatrix& mat);

		// Effectの更新と再生の並列実行
		void Update();

		// Effectの描画
		void Draw()const;

		// ZEffectManagerのEffect管理用EffectHandleからEffekseerのHandleを取得
		Effekseer::Handle EffectHandleToEffekseerHandle(const EffectHandle& handle)const;

#pragma region 取得系

		Effekseer::Manager* GetManager()const;
		EffekseerRenderer::Renderer* GetRenderer()const;
		EffekseerSound::Sound* GetSound()const;
		uint16 GetServerPort()const;

#pragma endregion
		
		// エフェクトインスタンス削除時(停止時)のコールバック関数
		// Effekseer::ManagerのUpdate関数実行時に呼ばれる(はず)だがUpdate関数自体、
		// 別スレッドで実行しているため削除されたエフェクトインスタンスの情報は一時的にm_OnRemoveEffectsに貯め込む
		static void EFK_STDCALL OnRemoveEffectInstance(Effekseer::Manager* manager, Effekseer::Handle ESHandle, bool isRemovingManager);
		
	private:
		void PlayAllRequestEffects();
		// SubmitPlayEffect関数の実装
		ZEffectManager::EffectHandle _SubmitPlayEffect(Effekseer::Effect* effect, const ZVec3& pos, const OnPlayEffectFunction& onPlayEffectFunc);

		// EffekseerのHandleからZEffectManagerのEffect管理用EffectHandleを取得
		EffectHandle EffekseerHandleToEffectHandle(const Effekseer::Handle& esHandle)const;

	private:
		static ZEffectManager* s_pInstance;

		IXAudio2* m_XA2{ nullptr };
		IXAudio2MasteringVoice* m_XA2_Master{ nullptr };
		Effekseer::Manager* m_ESManager{ nullptr };
		EffekseerRenderer::Renderer* m_ESRenderer{ nullptr };
		EffekseerSound::Sound* m_ESSound{ nullptr };
		Effekseer::Server* m_ESServer{ nullptr };		// Effekseerでの編集をリモートで反映させるために使う
		uint16 m_ServerPort{ 0 };						// ↑のサーバーで使うポート番号記憶用
		ZVector<PlayRequestData> m_PlayRequests;
		ZUnorderedMap<ZString, Effekseer::Effect*> m_Effects;
		ZUnorderedMap<Effekseer::Effect*, ZString> m_EffectNames;
		ZUnorderedMap<EffectHandle, Effekseer::Handle> m_ESHandleMap;		// EffectHandleをkey値としたHandleMap
		ZUnorderedMap<Effekseer::Handle, EffectHandle> m_EffectHandleMap;	// Effekseer::Handleをkey値としたHandleMap
		ZUnorderedMap<EffectHandle, ZVector<OnStopedEffectFunction>> m_OnStopedFunctionMap;	// Effect停止時のコールバック保持用
		ZVector<std::pair<EffectHandle, bool>> m_OnRemoveEffects;			// 破棄されたEffectのHandle一時保管用
		long m_PlayEffectCounter{ 0 };
		std::mutex m_PlayRequestMtx;

	};

}

namespace EzLib
{
#include "ZEffectManager.inl"
}

#endif