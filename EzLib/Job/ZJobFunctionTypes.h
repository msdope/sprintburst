#ifndef __ZJOB_FUNCTION_TYPES_H__
#define __ZJOB_FUNCTION_TYPES_H__

namespace EzLib
{
	// Jobによって実行される関数

	template<typename T>
	using JobFunction = std::function<T()>;

	// 2つの関数オブジェクトを使って中止可能
	template<typename T>
	using JobAbortableFunction = std::function<T(std::function<bool()>, std::function<bool()>)>;

	// Job実行結果を取得する関数
	// Jobが例外を投げた場合この関数によって再度スローされる
	template<typename T>
	using JobResultFunction = std::function<T()>;

	// Job実行完了後に呼び出されるコールバック関数
	// 引数で渡された関数オブジェクトでJobの結果を取得するのに使用
	template<typename T>
	using JobCallBackFunction = std::function<void(JobResultFunction<T>)>;

	// Jobを中止するかを返す関数
	using JobShouldAbortFunction = std::function<bool()>;

	// Jobを中止する関数
	using JobAbortFunction = std::function<void()>;
}

#endif
