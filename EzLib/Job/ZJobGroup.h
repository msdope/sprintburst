#ifndef __ZJOB_GROUP_H__
#define __ZJOB_GROUP_H__

#include "ZWorker.h"

namespace EzLib
{

	class ZJobManager;
	
	class ZJobGroup
	{
	private:
		friend class ZJobManager;

	public:
		ZJobGroup();

		template<typename T>
		ZJob<T> PushJob(JobFunction<T> func, JobCallBackFunction<T> callback = {})
		{
			assert(m_ParentWorker);
			auto state = Make_Shared(ZJobState<T>, sysnew, func, callback);
			m_ParentWorker->PushJob(state);
			return ZJob<T>(state);
		}

		template<typename T>
		ZJob<T> PushJob(JobAbortableFunction<T> func, JobCallBackFunction<T> callback = {})
		{
			assert(m_ParentWorker);
			auto state = MakeState(AbortableJobState<T>, sysnew, func, callback);
			m_ParentWorker->PushJob(state);
			return ZJob<T>(state);
		}

	private:
		ZJobGroup(ZWorker* parantWorker);

	private:
		ZWorker* m_ParentWorker;

	};
}

#endif