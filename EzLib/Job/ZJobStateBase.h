#ifndef __ZJOB_STATE_BASE_H__
#define __ZJOB_STATE_BASE_H__

namespace EzLib
{
	class ZJobStateBase
	{
	public:
		virtual ~ZJobStateBase() = default;

		virtual bool Run(JobShouldAbortFunction ShouldAbort) = 0;

		virtual void RunCallBack() = 0;

		virtual size_t GetThreadID() = 0;
	};
}

#endif