#include "PCH/pch.h"

namespace EzLib
{
	ZJobGroup::ZJobGroup()
		: ZJobGroup(nullptr)
	{
	}

	ZJobGroup::ZJobGroup(ZWorker* parentWorker)
		: m_ParentWorker(parentWorker)
	{
	}

}