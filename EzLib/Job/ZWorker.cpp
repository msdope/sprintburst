#include "PCH/pch.h"

namespace EzLib
{
	ZWorker::ZWorker(ZJobManager* manager)
		: m_pJobManager(manager),m_IsRunning(false)
	{
	}

	void ZWorker::Start()
	{
		m_IsRunning = true;
		m_WorkerThread = sysnew(std::thread,&ZWorker::WorkerThreadFunc, this);
	}

	void ZWorker::Stop()
	{
		{
			std::unique_lock<std::mutex> ul(m_JobQueueMtx);
			m_IsRunning = false;
		}
		Notify();
	}

	void ZWorker::Join()
	{
		m_WorkerThread->join();
	}

	void ZWorker::PushJob(ZSP<ZJobStateBase> job)
	{
		{
			std::unique_lock<std::mutex> ul(m_JobQueueMtx);
			m_JobQueue.push(job);
		}
		Notify();
	}

	void ZWorker::Notify()
	{
		m_CV.notify_all();
	}

	void ZWorker::RunJob(ZSP<ZJobStateBase>& job)
	{
		auto jobAbort = [this]() { return !m_IsRunning; };
		bool aborted = job->Run(jobAbort);
		if (aborted == false)
			m_pJobManager->FinishJob(job);
	}

	void ZWorker::WorkerThreadFunc()
	{
		while (true)
		{
			std::unique_lock<std::mutex> ul(m_JobQueueMtx);
			
			// WorkerのStop関数が呼ばれていればスレッド終了
			if (m_IsRunning == false)
				return;

			// Jobがなければ待機ループ
			while(m_JobQueue.empty() && m_pJobManager->HasJob() == false)
			{
				m_CV.wait(ul);
			
				// スリープ復帰時にStop関数が呼ばれていればスレッド終了
				if (m_IsRunning == false)
					return;
			}

			// 処理するJobがあるなら
			if (m_JobQueue.empty() == false)
			{
				// Job取得
				auto job = m_JobQueue.front();
				m_JobQueue.pop();
				ul.unlock();
				// Job実行
				RunJob(job);
			}
			else
				ul.unlock();

			// Job実行後にStop関数が呼ばれていればスレッド終了
			if (m_IsRunning == false)
				return;

			// JobManagerからJob取得
			auto job = m_pJobManager->FetchJob();
			if (job.GetPtr() == nullptr) // Jobが空 -> Managerには実行可能なJobなし
				continue;
			// 取得したJobが空でなければ実行
			RunJob(job);
		}

	}

}
