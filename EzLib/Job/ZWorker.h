#ifndef __ZWORKER_H__
#define __ZWORKER_H__

#include "ZJobStateBase.h"

namespace EzLib
{
	class ZJobManager;

	class ZWorker
	{
	public:
		ZWorker(ZJobManager* manager);
		~ZWorker() = default;

		void Start();

		void Stop();

		void Join();

		void PushJob(ZSP<ZJobStateBase> job);

		void Notify();

	private:
		void RunJob(ZSP<ZJobStateBase>& job);

		void WorkerThreadFunc();

	private:
		ZJobManager* m_pJobManager;
		bool m_IsRunning;
		ZUP<std::thread> m_WorkerThread;
		std::mutex m_JobQueueMtx;
		ZQueue<ZSP<ZJobStateBase>> m_JobQueue;
		std::condition_variable m_CV;
	};
}


#endif