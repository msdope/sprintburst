#ifndef __ZJOB_STATE_H__
#define __ZJOB_STATE_H__

namespace EzLib
{
	template<typename T>
	class ZJobState : public ZTemplateJobStateBase<T>
	{
	public:
		ZJobState(JobFunction<T> function,JobCallBackFunction<T> callback)
			: ZTemplateJobStateBase<T>(callback), m_Func(function)
		{
		}

		virtual ~ZJobState() = default;

	protected:
		T RunAndGetResult(JobShouldAbortFunction)override
		{
			return m_Func();
		}

	public:
		JobFunction<T> m_Func;

	};


}

#endif