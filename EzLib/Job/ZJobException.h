#ifndef __ZJOB_EXCEPTION_H__
#define __ZJOB_EXCEPTION_H__

namespace EzLib
{
	// Jobが自身の処理を中止したいときに投げる例外
	class ZJobAbortedException : public std::exception
	{
	public:
		const char* what() const noexcept override
		{
			return "Job Aborted";
		}
	};
}


#endif