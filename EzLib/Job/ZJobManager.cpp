#include "PCH/pch.h"

namespace EzLib
{
	ZJobManager::ZJobManager(size_t numWorkers)
		: m_NumWorkers(0),m_GroupIndex(0),m_IsRunning(false)
	{
		if(numWorkers <= 0)
			numWorkers = std::thread::hardware_concurrency() - 1; // CPU�R�A��-1
		for (size_t i = 0; i < numWorkers; i++)
			m_Workers.emplace_back(sysnew(ZWorker, this));
		m_NumWorkers = numWorkers;
	}

	ZJobManager::~ZJobManager()
	{
		Stop();
	}

	void ZJobManager::Start()
	{
		if (m_IsRunning.load())
			return;

		m_IsRunning.store(true);

		for (auto& worker : m_Workers)
			worker->Start();
	}

	void ZJobManager::Stop()
	{
		if (m_IsRunning.load() == false)
			return;
		m_IsRunning.store(false);
		for (auto& worker : m_Workers)
			worker->Stop();

		for (auto& worker : m_Workers)
			worker->Join();
	}

	void ZJobManager::RunCallBacks()
	{
		size_t id = GetCurrentThreadID();
		std::unique_lock<std::mutex> ul(m_FinishedJobQueueMtx);
		auto it = m_FinishedJobQueue.find(id);

		if (it == m_FinishedJobQueue.end())
			return;

		ZVector<ZSP<ZJobStateBase>> jobs;
		std::swap(jobs, it->second);

		it->second.clear();
		ul.unlock();

		for (auto& job : jobs)
			job->RunCallBack();
	}

	size_t ZJobManager::GetNumWorkers()
	{
		return m_NumWorkers;
	}

	ZJobGroup ZJobManager::CreateJobGroup()
	{
		size_t index = m_GroupIndex;
		m_GroupIndex = (m_GroupIndex + 1) % m_NumWorkers;
		return ZJobGroup(m_Workers[index].GetPtr());
	}

	void ZJobManager::PushState(ZSP<ZJobStateBase> state)
	{
		std::lock_guard<std::mutex> lg(m_JobQueueMtx);
		m_JobQueue.push(state);
		for (auto& worker : m_Workers)
			worker->Notify();
	}

	ZSP<ZJobStateBase> ZJobManager::FetchJob()
	{
		std::lock_guard<std::mutex> lg(m_JobQueueMtx);
		if (m_JobQueue.empty())
			return ZSP<ZJobStateBase>();

		auto job = m_JobQueue.front();
		m_JobQueue.pop();
		return job;
	}

	bool ZJobManager::HasJob()
	{
		std::lock_guard<std::mutex> lg(m_JobQueueMtx);
		return !m_JobQueue.empty();
	}

	void ZJobManager::FinishJob(ZSP<ZJobStateBase> job)
	{
		std::lock_guard<std::mutex> lg(m_FinishedJobQueueMtx);
		auto it = m_FinishedJobQueue.find(job->GetThreadID());
		if (it == m_FinishedJobQueue.end())
			m_FinishedJobQueue.insert({ job->GetThreadID(),{job} });
		else
			it->second.push_back(job);
	}
}