#ifndef __ZTEMPLATE_JOB_STATE_BASE_H__
#define __ZTEMPLATE_JOB_STATE_BASE_H__

#include "Utils/ZThreadID.h"

namespace EzLib
{
	template<typename T>
	class ZTemplateJobStateBase : public ZJobStateBase
	{
	public:
		ZTemplateJobStateBase(JobCallBackFunction<T> callback)
			: m_ThreadID(GetCurrentThreadID()), m_CallBackFunc(callback), m_IsFinished(false)
		{
		}
		virtual ~ZTemplateJobStateBase() = default;

		// Jobを実行し結果を格納
		// 戻り値... Jobの実行が中止されたか
		bool Run(JobShouldAbortFunction shouldAbort)override
		{
			try
			{
				m_Result = RunAndGetResult(shouldAbort);
			}
			catch (ZJobAbortedException& e)
			{
				return true;
			}
			catch (...)
			{
				m_Exception = std::current_exception();
			}
			m_IsFinished.store(true);
			return false;
		}

		void RunCallBack()override
		{
			// 実行済み
			assert(m_IsFinished.load());
			if (!m_CallBackFunc)
				return;

			auto getResult = [this]
			{
				if (m_Exception != nullptr)
					std::rethrow_exception(m_Exception);
				else
					return std::move(m_Result);
			};

			m_CallBackFunc(getResult);
		}

		size_t GetThreadID()override
		{
			return m_ThreadID;
		}

	protected:
		// Jobを実行し結果を返す
		virtual T RunAndGetResult(JobShouldAbortFunction shouldAbort) = 0;

	public:
		// このJobを作成したThreadのID
		size_t m_ThreadID;

		// Job実行完了時のコールバック関数
		JobCallBackFunction<T> m_CallBackFunc;

		// Jobの実行が完了したか
		std::atomic<bool> m_IsFinished;
		
		std::exception_ptr m_Exception;

		// Job実行結果
		T m_Result;
	};
}

#endif