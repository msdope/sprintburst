#ifndef __ZABORTABLE_JOB_STATE_H__
#define __ZABORTABLE_JOB_STATE_H__

namespace EzLib
{
	template<typename T>
	class ZAbortableJobState : ZTemplateJobStateBase<T>
	{
	public:
		ZAbortableJobState(JobAbortableFunction<T> func, JobCallBackFunction<T> callback)
			: ZTemplateJobStateBase<T>(callback), m_Function(func)
		{
		}

		virtual ~ZAbortableJobState() = default;

	protected:
		virtual T RunAndGet(JobShouldAbortFunction shouldAbort)
		{
			static auto abortFunc = []
			{
				throw ZJobAbortedException();
			};

			return std::move(m_Function(shouldAbort, abortFunc));
		}

	public:
		JobAbortableFunction<T> m_Function;

	};
}

#endif