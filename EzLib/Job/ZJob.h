#ifndef __ZJOB_H__
#define __ZJOB_H__

namespace EzLib
{
	class ZJobGroup;
	class ZJobManager;

	template<typename T>
	class ZJob
	{
	private:
		friend class ZJobGroup;
		friend class ZJobManager;

	public:
		ZJob() = default;

		bool IsFinished()const
		{
			if (m_State)
				return m_State->m_IsFinished.load();
			return true;
		}

		void Wait()const
		{
			while (IsFinished() == false) {}
		}

		T GetResult()
		{
			assert(m_State);
			assert(m_State->m_IsFinished.load());
			if (m_State->m_Exception != nullptr)
				std::rethrow_exception(m_State->m_Exception);
			else
				return std::move(m_State->m_Result);
		}


	private:

		ZJob(ZSP<ZTemplateJobStateBase<T>> state)
			: m_State(state)
		{
		}

	private:
		ZSP<ZTemplateJobStateBase<T>> m_State;

	};


}


#endif