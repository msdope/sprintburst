#ifndef __ZJOB_MANAGER_H__
#define __ZJOB_MANAGER_H__

namespace EzLib
{
	class ZWorker;

	class ZJobManager
	{
	private:
		friend class ZWorker;

	public:
		ZJobManager(size_t numWorkers = 0);
		// �R�s�[�֎~
		ZJobManager(const ZJobManager&) = delete;
		ZJobManager(ZJobManager&&) = delete;
		ZJobManager& operator=(const ZJobManager&) = delete;
		ZJobManager& operator=(ZJobManager&&) = delete;

		~ZJobManager();

		void Start();

		void Stop();

		template<typename T>
		ZJob<T> PushJob(JobFunction<T> func, JobCallBackFunction<T> callback = {})
		{
			auto state = Make_Shared(ZJobState<T>, sysnew, func, callback);
			PushState(state);
			return ZJob<T>(state);
		}

		template<typename T>
		ZJob<T> PushJob(JobAbortableFunction<T> func, JobCallBackFunction<T> callback = {})
		{
			auto state = Make_Shared(ZAbortableJobState<T>, sysnew, func, callback);
			PushState(state);
			return ZJob<T>(state);
		}



		ZJobGroup CreateJobGroup();
		
		void RunCallBacks();

		size_t GetNumWorkers();

	private:
		void PushState(ZSP<ZJobStateBase> state);

		ZSP<ZJobStateBase> FetchJob();

		bool HasJob();

		void FinishJob(ZSP<ZJobStateBase> job);

	private:
		size_t m_NumWorkers;
		size_t m_GroupIndex;
		std::mutex m_JobQueueMtx;
		std::mutex m_FinishedJobQueueMtx;
		std::atomic<bool> m_IsRunning;
		ZVector<ZUP<ZWorker>> m_Workers;
		ZQueue<ZSP<ZJobStateBase>> m_JobQueue;
		ZUnorderedMap<size_t, ZVector<ZSP<ZJobStateBase>>> m_FinishedJobQueue;
	};

}

#endif