#include "MainFrame/ZMainFrame.h"

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

namespace EzLib
{
	ZMainFrame* ZMainFrame::m_sInstance = nullptr;

	LRESULT MainFrame_WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		auto* window = EzLib::ZWindow::GetWindowClass(hWnd);
		if (window == nullptr)
			return 0;

		ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam);

		// メッセージ処理
		switch (msg)
		{
			case WM_ACTIVATE:
			{
				bool isActive;

				// 非アクティブ
				if (LOWORD(wParam) == WA_INACTIVE)
					isActive = false;
				else
					isActive = true;

				window->SetWindowActive(isActive);
			}
			break;

			case WM_MOUSEWHEEL:
				INPUT.SetMouseWheelValue((short)HIWORD(wParam));
			break;

			case WM_DESTROY:
				PostQuitMessage(0);
			return 0;

			case WM_DPICHANGED:
				if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_DpiEnableScaleViewports)
				{
					const RECT* suggested_rect = (RECT*)lParam;
					SetWindowPos(hWnd, NULL,
								 suggested_rect->left,
								 suggested_rect->top,
								 suggested_rect->right - suggested_rect->left,
								 suggested_rect->bottom - suggested_rect->top,
								 SWP_NOZORDER | SWP_NOACTIVATE);
				}
			break;
		}

		return 0;
	}

	#ifdef SHOW_DEBUG_WINDOW

	void DebugCommandProc(ZString cmdText)
	{

	}

	#endif

	ZMainFrame::ZMainFrame(const char * wndTitle, const ZWindowProperties& properties)
		: m_FrameRate(60), m_IsExitGameLoop(false), m_WndTitle(wndTitle), m_WndProperties(properties)
	{
		{
			using namespace std::chrono;
			m_OneFrameParMilli = (1000.0ms / (float)m_FrameRate);
		}
		m_DeltaTime = 1.0f / (float)m_FrameRate;
		m_sInstance = this;

		SubSystemDetails::Init();
		// 乱数生成用
		AddSubSystem<ZRand>(timeGetTime());
		// Physicsクラス追加
		m_pPhysicsWorld = &AddSubSystem<ZPhysicsWorld>();
		m_pPhysicsWorld->Init();
		
		m_pThreadPool = &AddSubSystem<ZThreadPool>();
		m_pThreadPool->Init(0); // 0以下でCPU数 - 1のスレッド作成

		// サウンド初期化
		ZSndMgr.Init();

		PlatformInit();
	}

	ZMainFrame::~ZMainFrame()
	{
		if (m_IsFullScreen)
			ChangeDisplaySettings(NULL, 0);
		RemoveInstance();
	}

	void ZMainFrame::Start()
	{
		if (Init() == false)
			return;

		Run();
	}

	bool ZMainFrame::Init()
	{
		// エフェクト初期化
		AddSubSystem<ZEffectManager>();
		EFFECT.InitManager(ZDx.GetDev(), ZDx.GetDevContext());
		EFFECT.InitSound();
		EFFECT.SetCullingWorld(ZVec3(1000), 5);
		// Effekseerエディタ連携用
		EFFECT.StartUpServer(60000);

		// フォント管理設定
		ZFontSpriteManager::CreateInstance();
		ZFontMgr.Init(m_Window->GetWindowHandle());
		// フォント追加
		ZFontMgr.AddFont(0, "ＭＳ ゴシック", 12);
		ZFontMgr.AddFont(1, "ＭＳ ゴシック", 24);
		ZFontMgr.AddFont(2, "ＭＳ ゴシック", 36);

		if (m_WndProperties.UseFullScreen)
			ZDx.SetFullScreen(true);

	#ifdef SHOW_DEBUG_WINDOW
		DEBUG_WINDOW.Init(m_Window->GetWindowHandle(), 4, DebugCommandProc);
	#endif

		return true;
	}

	void ZMainFrame::FrameUpdate()
	{
		// サウンド処理
		ZSndMgr.Update();

		// デバッグウィンドウ表示切り替え
		if (INPUT.KeyStay(VK_CONTROL) && INPUT.KeyEnter('D'))
			DEBUG_WINDOW.ToggleWindow();

		// Update~
		{
			m_SceneMgr.Update();
			EFFECT.Update();
#ifdef SHOW_DEBUG_WINDOW
			DEBUG_WINDOW.Update();
#endif
			INPUT.SetMouseWheelValue(0);

			// メモリ詳細出力
			constexpr int beginMemoryScrollIndex = 10;
			ZAllocator::DisplayAllMemoryInfo(beginMemoryScrollIndex);
		}

		// Draw~
		if (m_FrameCnt % 2 == 0)
		{
			// 描画前準備(バックバッファ& Zバッファクリア)
			// バックバッファをクリアする。
			m_pGraphics->Begin(ZVec4(0, 0, 0, 1.0f), true, true);

			m_SceneMgr.Draw();
			EFFECT.Draw(); //仮置き

#ifdef SHOW_DEBUG_WINDOW
			m_SceneMgr.ImGuiUpdate();
			DEBUG_WINDOW.Draw();
#endif
			m_pGraphics->End();
		}

		// FPS描画
		ShowFps();
	}

	void ZMainFrame::PlatformInit()
	{
		ZString errorMsg;

		m_Window = std::make_unique<ZWindow>(m_WndTitle.c_str(), m_WndProperties);
		if (m_Window->IsInitialized() == false)
		{
			errorMsg = "Window初期化失敗";
			MessageBox(m_Window->GetWindowHandle(), errorMsg.c_str(), "Error", MB_OK);
			return;
		}

		// Graphicsクラス追加
		m_pGraphics = &AddSubSystem<ZDirectXGraphics>();
		
		// DirectX初期化
		bool result = m_pGraphics->Init
		(
			m_Window->GetWindowHandle(),
			m_WndProperties.Width, m_WndProperties.Height,
			&m_ResStg,
			ZDirectXGraphics::MSAA::MSAA_NONE,
			&errorMsg
		);

		if (result == false)
		{
			errorMsg = "DirectX初期化失敗 : " + errorMsg;
			MessageBox(m_Window->GetWindowHandle(), errorMsg.c_str(), "Error", MB_OK);
			return;
		}
		m_pGraphics->SetVSync(m_WndProperties.UseVSync);
		
	}

	void ZMainFrame::Run()
	{
		// スリープ時間計測用
		std::chrono::duration<double> sleepDurationTime;
	
		ZTimer timer(m_DulationTime);
		ZTimer stimer(sleepDurationTime); // スリープ時間計測

		// ケームループ
		while (m_IsExitGameLoop == false)
		{
			// ウィンドウメッセージ処理,入力情報取得
			m_Window->Update();
			if (m_Window->IsClosed())break; // ウィンドウが閉じられていたら即終了

			// ゲーム処理
			timer.Start();
			{
				// 1フレーム分の処理
				FrameUpdate();
			}
			timer.Stop();

			m_FrameCnt++;
			m_FrameCnt %= m_FrameRate;

			#pragma region FPS Control
			
			FpsControll(stimer, sleepDurationTime);

			// 経過時間保持
			m_DeltaTime = (float)(m_DulationTime.count());

			#pragma endregion

		}


#if EffectTest
		testEffect->Release();
#endif

		Release();
	}

	void ZMainFrame::Release()
	{
		// フォント解放
		ZFontSpriteManager::DeleteInstance();
		ZFontSpriteManager::sRemoveAllFontResource(); // 読み込んだttfファイル削除
		
		// シーン解放
		m_SceneMgr.Release();
		// リソース解放
		m_ResStg.Release();
		// サウンド解放
		ZSndMgr.Release();

		// デバッグ用GUI解放処理
	#ifdef SHOW_DEBUG_WINDOW
		DEBUG_WINDOW.Release();
	#endif

		// サブシステム解放
		SubSystemDetails::Release();
	}

	void ZMainFrame::FpsControll(ZTimer& stimer, std::chrono::duration<double>& sleepDurationTime)
	{
		if (m_DulationTime >= m_OneFrameParMilli)
			return;

		// 処理時間が指定フレームレートの1フレームあたりの時間より小さければ
		
		// ミリ秒単位のスリープ
		stimer.Start();
		timeBeginPeriod(1);
		DWORD t = (DWORD)((m_OneFrameParMilli - m_DulationTime).count() * 1000);
		Sleep(t);
		timeEndPeriod(1);
		stimer.Stop();
		m_DulationTime += sleepDurationTime;

		// ↑のマイクロ秒単位の端数の時間分Sleep(0)をはさんでの待機ループ
		while (m_DulationTime < m_OneFrameParMilli)
		{
			stimer.Start();
			Sleep(0);
			stimer.Stop();
			m_DulationTime += sleepDurationTime;
		}
		
	}

	bool ZMainFrame::ShowFps()
	{
		using namespace std::chrono;
		static std::chrono::duration<double> fpsTimeCounter = 0ms;
		static float frameCnt = 0;

		frameCnt += 1;
		fpsTimeCounter += std::chrono::duration<double>(m_DeltaTime);

		if (fpsTimeCounter >= 1s)
		{
			#ifdef SHOW_DEBUG_WINDOW
			DW_STATIC(2, "%.3f\n", 1.0f / (fpsTimeCounter.count() / frameCnt));
			DW_STATIC(3, "%3.0f Frame/s\n", frameCnt);

			#else
			DebugLog("%.3f\n", 1.0f / (fpsTimeCounter.count() / frameCnt));
			#endif

			fpsTimeCounter = 0ms;
			frameCnt = 0;
			return true;
		}

		return false;

	}


}