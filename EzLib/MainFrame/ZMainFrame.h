#ifndef __ZMAIN_FRAME_H__
#define __ZMAIN_FRAME_H__

#include "PCH/pch.h"
#include "DebugWindow/DebugWindow.h"
#include "Window/ZWindow.h"

namespace EzLib
{
#ifdef SHOW_DEBUG_WINDOW
	static void DebugCommandProc(ZString cmtText);
#endif

	LRESULT MainFrame_WindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	
	class ZMainFrame
	{
	public:
		ZMainFrame(const char* wndTitle, const ZWindowProperties& properties);
		virtual ~ZMainFrame();

		// コピー禁止
		ZMainFrame(ZMainFrame&) = delete;
		ZMainFrame& operator=(ZMainFrame&) = delete;

		void Start();

		void ExitGameLoop()
		{
			m_IsExitGameLoop = true;
		}

		// フレームレート設定
		void constexpr SetFrameRate(float rate)
		{
			using namespace std::chrono;
			m_FrameRate = (size_t)rate;
			m_OneFrameParMilli = (1000.0ms / m_FrameRate);
			m_DeltaTime = 1.0f / m_FrameRate;
		}
		
		// フレームレート取得
		const size_t GetFrameRate()const
		{
			return m_FrameRate;
		}

	protected:
		virtual bool Init();
		virtual void FrameUpdate();
		virtual void Release();

	private:
		void PlatformInit();
		void Run();	// メインループ開始
		void FpsControll(ZTimer& stimer, std::chrono::duration<double>& sleepDurationTime);
		bool ShowFps();
		

	#pragma region Singleton

	public:
		inline static ZMainFrame& GetInstance()
		{
			assert(m_sInstance);
			return *m_sInstance;
		}

		static void RemoveInstance()
		{
			m_sInstance = nullptr;
		}

	#pragma endregion

	public:
		// 前フレームからの経過時間
		float m_DeltaTime;
		
		uptr<ZWindow>		m_Window;		// ウィンドウクラス
		ZDirectXGraphics*	m_pGraphics;	// SubSystemに追加したGraphicsクラスへのポインタ
		ZSceneManager		m_SceneMgr;		// シーン管理
		ZResourceStorage	m_ResStg;		// リソース管理庫

		// SubSystemに追加したPhysicsWorldクラスへのポインタ
		ZPhysicsWorld* m_pPhysicsWorld;

		// SubSystemに追加したThreadPoolクラス
		ZThreadPool* m_pThreadPool;

	protected:
		// フレームカウンター
		size_t m_FrameCnt;
		ZString				m_WndTitle;
		bool				m_IsFullScreen;
		size_t				m_FrameRate;
		bool				m_IsExitGameLoop;
		
		// FSP制御用
		std::chrono::duration<double> m_OneFrameParMilli;
		std::chrono::duration<double> m_DulationTime;
	
	private:
		ZWindowProperties m_WndProperties;

		static ZMainFrame* m_sInstance;
	};

}

using namespace EzLib;

#define APP EzLib::ZMainFrame::GetInstance()

extern LRESULT EzLib::MainFrame_WindowProc(HWND, UINT, WPARAM, LPARAM);

#endif