#include "ZECS.h"
#include "MainFrame/ZMainFrame.h"
#include "DebugWindow/DebugWindow.h"

namespace EzLib
{
namespace ZECS
{

	void Example()
	{
		struct test : public ECSComponentBase<test>
		{
			ZString dtext = "ecsTest\n";
		};
	
		struct test2 : public ECSComponentBase<test2>
		{
			ZString dtext = "ecsTest2\n";
		};
	
		class testSystem : public ECSSystemBase
		{
			DefUseComponentType(test,test2)
		
		public:
			testSystem()
			{
				Init();
				m_UseMultiThread = true;
			}
	
			virtual void UpdateComponents(float delta, UpdateCompParams components)override
			{
				auto* t = GetCompFromUpdateParam(test,components);
				
			#ifdef SHOW_DEBUG_WINDOW
				DW_SCROLL(0, t->dtext.c_str());
			#else
				DebugLog(t->dtext);
			#endif
			}

			virtual void LateUpdateComponents(float delta, UpdateCompParams components)override
			{
				auto* t = GetCompFromUpdateParam(test,components);
			#ifdef SHOW_DEBUG_WINDOW
				DW_SCROLL(1, "Late %s",t->dtext.c_str());
			#else
				DebugLog(t->dtext);
			#endif
			}

		};
	
		class testSystem2 : public ECSSystemBase
		{

			DefUseComponentType(test2)

		public:
			testSystem2()
			{
				Init();
				m_UseMultiThread = true;
			}
	
			virtual void UpdateComponents(float delta, UpdateCompParams components)override
			{
				auto* t = GetCompFromUpdateParam(test2,components);
				
			#ifdef SHOW_DEBUG_WINDOW
				DW_SCROLL(2, t->dtext.c_str());
			#else
				DebugLog(t->dtext);
			#endif
			}

			virtual void LateUpdateComponents(float delta, UpdateCompParams components)override
			{
				auto* t = GetCompFromUpdateParam(test2,components);
#ifdef SHOW_DEBUG_WINDOW
				DW_SCROLL(3, "Late %s", t->dtext.c_str());
#else
				DebugLog(t->dtext);
#endif
			}

		};
	
		const size_t numEntitys = 1000;
		ZAVector<ZSP<ECSEntity>> entities;
		entities.reserve(numEntitys);
		for (int i = 0; i < numEntitys; i++)
			entities.emplace_back(ECS.MakeEntity(ECS.MakeComponent<test>(),ECS.MakeComponent<test2>()));
	
		ECSSystemList systemList;
		systemList.AddSystem<testSystem>();
		systemList.AddSystem<testSystem2>();

		ECS.EnableUseMultiThread();
		std::chrono::duration<double> dulationTime;
		{
			ZTimer timer(dulationTime);
			timer.Start();
			ECS.UpdateSystems(systemList,0);
			timer.Stop();
		}
		ECS.DisableUseMultiThread();

#ifdef SHOW_DEBUG_WINDOW
		DW_STATIC(20, "Time : %f", dulationTime.count());
#else
		DebugLog_fmt("Time : %f", dulationTime.count());
#endif

		ECSEntity::RemoveAllEntity(entities);
	}

}
}