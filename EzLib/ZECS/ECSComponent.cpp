#include "ECSComponent.h"

namespace EzLib
{
namespace ZECS
{

	std::vector<std::tuple<ECSComponentCreateFunction, ECSComponentFreeFunction, size_t>>* __ECSComponentBase::ComponentTypes = nullptr;
	
	uint32 __ECSComponentBase::RegisterComponentType(ECSComponentCreateFunction crateFunc, ECSComponentFreeFunction freeFunc,size_t size)
	{
		if (ComponentTypes == nullptr)
			ComponentTypes = new std::vector<std::tuple<ECSComponentCreateFunction, ECSComponentFreeFunction, size_t>>;
		uint32 componentID = ComponentTypes->size();
		ComponentTypes->push_back(std::tuple<ECSComponentCreateFunction, ECSComponentFreeFunction, size_t>(crateFunc, freeFunc,size));

		return componentID;
	}

	void TransformComponent::InitFromJson(const json11::Json& jsonObj)
	{
		// 座標
		ZVec3 pos;
		ZVec3 rotate;
		ZVec3 scale(1, 1, 1);
		pos.Set(jsonObj["Position"]);
		rotate.Set(jsonObj["Rotation"]);
		scale.Set(jsonObj["Scaling"]);

		// 行列作成
		Transform.CreateMove(pos);
		Transform.Scale_Local(scale);
		Transform.RotateX_Local(rotate.x);
		Transform.RotateY_Local(rotate.y);
		Transform.RotateZ_Local(rotate.z);

		// 直接行列が指定されている時
		Transform.Set(jsonObj["Matrix"]);

	}

}
}