#ifndef __ECS_COMMON_H__
#define __ECS_COMMON_H__

namespace EzLib
{
namespace ZECS
{
	class ECSListener;
	class ECSEntity;
	class ECSSystemBase;
	class ECSSystemList;
	struct __ECSComponentBase;

	static constexpr size_t MaxComponentTypeNum = 64; // 最大コンポーネントタイプ数 ※要調整

	#pragma region EntityComponentSystemDefines

	using ComponentMemory = ZSVector<uint8>;							// 1バイトの動的メモリ配列
	using ComponentMemoriesByID = ZSMap<uint32, ComponentMemory>;		// ID別のコンポーネントメモリ
	using EntityComps =  ZSMap<uint32, uint32>;							// コンポーネントID& コンポーネント用メモリのIndex
	using EntityCompsPair = std::pair<ZSP<ECSEntity>, EntityComps>;		// EntityIndex & EntityComps
	using ComponentArray = ZSVector<ComponentMemory*>;					// 各コンポーネントごとの専用メモリ(混同させると管理が面倒)
	using UpdateComponentPackage = ZSVector<__ECSComponentBase*>;		// Systemの渡す前のコンポーネント収集用コンテナ

	#pragma endregion

	#pragma region EntityDefunes

	using ComponentBitSet = std::bitset<MaxComponentTypeNum>;

	#pragma endregion

	#ifndef NULL_ENTITY_HANDLE
	
	static constexpr size_t NULL_ENTITY_HANDLE = (size_t)-1;
	
	#endif

	#pragma region ComponentDefines

	using EntityHandle = size_t;
	using ECSComponentCreateFunction = __ECSComponentBase*(*)(ComponentMemory&,ZSP<ECSEntity>);
	using ECSComponentFreeFunction = void(*)(__ECSComponentBase*);
	
	#pragma endregion

	#pragma region SystemDefines

	using UpdateCompParams = __ECSComponentBase**;

	#pragma endregion
}
}


#endif