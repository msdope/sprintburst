#ifndef __ECS_COMPONENT_H__
#define __ECS_COMPONENT_H__

#include "PCH/pch.h"
#include "ECSCommon.h"

#include "ECSEntity.h"

namespace EzLib
{
namespace ZECS
{

	// 全てのコンポーネントクラスの基底クラス
	// ※新規でコンポーネントを定義する際はこのクラスではなく,
	// ↓のECSConponentBaseを継承すること ( ZECS.hで定義しているDefComponentマクロを使うと良い )
	// でないとECSシステム内でIDやSIZEなどのメンバ変数にアクセスした際にエラー落ちする
	struct __ECSComponentBase
	{
	public:
		__ECSComponentBase() : m_UUID(EzLib::GetUUID())
		{
		}
		virtual ~__ECSComponentBase()
		{
		}

		static uint32 RegisterComponentType(ECSComponentCreateFunction crateFunc, ECSComponentFreeFunction freeFunc, size_t size);
		static ECSComponentCreateFunction GetTypeCreateFunction(uint32 id);
		static ECSComponentFreeFunction GetTypeFreeFunction(uint32 id);
		static size_t GetTypeSize(uint32 id);
		static bool IsTypeValid(uint32 id);

		static void Release()
		{
			delete ComponentTypes;
		}

		virtual void InitFromJson(const json11::Json& jsonObj)
		{

		}

		// UUID取得
		const UUID& GetUUID()const;
		// UUID設定
		void SetUUID(const UUID& uuid);

	public:
		ZSP<ECSEntity> m_Entity;
		uint32 m_MemoryIndex;
		
	private:
		static std::vector<std::tuple<ECSComponentCreateFunction, ECSComponentFreeFunction, size_t>>* ComponentTypes;
		UUID m_UUID;
	};

	// コンポーネント作成用
	template<typename Component>
	__ECSComponentBase* CreateECSComponent(ComponentMemory& memory,ZSP<ECSEntity> entity)
	{
		static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");
		uint32 index = memory.size();
		memory.resize(index + Component::SIZE);
		Component* component = new(&memory[index])Component();
		component->m_Entity = entity;
		component->m_MemoryIndex = index;
		return static_cast<__ECSComponentBase*>(component);
	}

	// コンポーネント解放用
	template<class Component>
	void FreeECSComponent(__ECSComponentBase* comp)
	{
		static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");
		Component* component = static_cast<Component*>(comp);
		component->m_Entity = nullptr;
		component->~Component();
	}

	// 継承前提のコンポーネントの基底クラス
	template<typename T>
	struct ECSComponentBase : public __ECSComponentBase
	{
		virtual ~ECSComponentBase()
		{
		}

		static const ECSComponentCreateFunction CREATE_FUNCTION;
		static const ECSComponentFreeFunction FREE_FUNCTION;
		static const size_t SIZE;
		static const uint32 ID;

	};

	

	template<typename T>
	const uint32 ECSComponentBase<T>::ID(__ECSComponentBase::RegisterComponentType(CreateECSComponent<T>,FreeECSComponent<T>, sizeof(T)));

	template<typename T>
	const size_t ECSComponentBase<T>::SIZE(sizeof(T));

	template<typename T>
	const ECSComponentCreateFunction ECSComponentBase<T>::CREATE_FUNCTION(CreateECSComponent<T>);

	template<typename T>
	const ECSComponentFreeFunction ECSComponentBase<T>::FREE_FUNCTION(FreeECSComponent<T>);

	#include "ECSComponent.inl"

	// 行列コンポーネント(エンティティを生成したら必ず追加される)
	struct TransformComponent : public ECSComponentBase<TransformComponent>
	{
		ZMatrix Transform;
		virtual void InitFromJson(const json11::Json& jsonObj)override;
#if _DEBUG
		ZVec3	Pos;
		ZVec3	Rot;
		ZVec3	Scale;
#endif
	};

}
}

#endif