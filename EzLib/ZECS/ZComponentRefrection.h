#ifndef __ZCOMPONENT_REFRECTION_H__
#define __ZCOMPONENT_REFRECTION_H__

#include "ECSCommon.h"

namespace EzLib
{
namespace ZECS
{
	struct RefrectData
	{
		ECSComponentCreateFunction CreateFunction;
		size_t ComponentID;
	};

	struct InstanceData
	{
		__ECSComponentBase* Instance = nullptr;
		size_t ComponentID;
	};

	// クラスリフレクション
	// 指定した名前(文字列)から特定のクラスを生成する
	// ClassBase ... ベースの型 このクラスを継承したクラスのみを生成出来る
	class ZComponentRefrection
	{
	public:
		ZComponentRefrection(ComponentMemoriesByID* pCompMemories)
		{
			m_pCompMemories = pCompMemories;
		}

		// クラス登録
		template<class Component>
		void Register(const ZString& name)
		{
			static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");

			if (m_pCompMemories == nullptr)
				return;
			//if (m_pCompPairMap == nullptr)
			//	return;

			// 生成関数作成
			RefrectData data;
			data.CreateFunction = __ECSComponentBase::GetTypeCreateFunction(Component::ID);
			data.ComponentID = Component::ID;
		
			// 登録
			m_ReflectionMap.insert(std::pair<ZString, RefrectData>(name, std::move(data)));

		}

		// インスタンス生成

		InstanceData New(const ZString& name)const
		{
			InstanceData data;

			if (m_pCompMemories == nullptr)
				return data;

			try
			{
				auto& refrectData = m_ReflectionMap.at(name);
				data.ComponentID = refrectData.ComponentID;
				data.Instance = refrectData.CreateFunction((*m_pCompMemories)[refrectData.ComponentID],nullptr);
			}
			catch (std::out_of_range&)
			{
				return data;
			}

			// インスタンス生成関数実行
			return std::move(data);
		}

		// 解放
		void Release()
		{
			m_pCompMemories = nullptr;
			m_ReflectionMap.clear();
		}

	private:
		// クラス登録用 連想配列
		//  <クラス名,生成関数オブジェクト>で登録
		ZSUnorderedMap<ZString, RefrectData> m_ReflectionMap;

		ComponentMemoriesByID* m_pCompMemories;
	};

}
}
#endif