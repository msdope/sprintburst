#ifndef __ECS_LISTENER_H__
#define __ECS_LISTENER_H__

#include "PCH/pch.h"
#include "ECSCommon.h"

namespace EzLib
{
namespace ZECS
{
	// m_ComponentIDsに含まれるいずれかのComponentをもつEntityが生成、削除された時や
	// m_ComponentIDsに含まれるいずれかのComponentをEntityに追加、除外した際に
	// このECSListenerクラスの各コールバック関数が呼ばれる
	class ECSListener
	{
	public:
		ECSListener();
		virtual ~ECSListener();

		virtual void OnMakeEntity(EntityHandle handle);
		virtual void OnRemoveEntity(EntityHandle hadle);
		virtual void OnAddComponent(EntityHandle handle, uint32 id);
		virtual void OnRemoveComponent(EntityHandle handle, uint32 id);

		const ZSVector<uint32>& GetComponentIDs();
		const ComponentBitSet& GetComponentBitSet();

		bool NotifyOnAllComponentOperations();
		bool NotifyOnAllEntityOperations();

	protected:
		void SetNotificationSettings(bool notifyOnAllComponentOperations,bool notifyOnAllEntityOperations);
		void AddComponentID(uint32 id);

	private:
		ZSVector<uint32>  m_ComponentIDs;
		ComponentBitSet m_CompBitSet;
		bool m_IsNotifyOnAllComponentOperations;	// すべてのコンポーネント操作時に↑のコールバック関数に通知するか
		bool m_IsNotifyOnAllEntityOperations;		// すべてのエンティティ操作時に↑のコールバック関数に通知するか

	};

}
}

namespace EzLib
{
namespace ZECS
{
#include "ECSListener.inl"
}
}

#endif
