template<typename Component>
inline void ECSEntity::AddComponent()
{
	ECS.AddComponent(ZSP<ECSEntity>(this), c);
}

template<typename Component>
inline Component* ECSEntity::GetComponent()
{
	return ECS.GetComponent<Component>(m_EntityHandle);
}

template<typename Component>
inline void ECSEntity::RemoveComponent()
{
	ECS.RemoveComponent<Component>(m_EntityHandle);
}

inline bool ZECS::ECSEntity::IsActive()
{
	return m_EntityHandle != NULL_ENTITY_HANDLE;
}

inline const UUID& ECSEntity::GetUUID()const
{
	return m_UUID;
}

inline void ECSEntity::SetUUID(const UUID& uuid)
{
	m_UUID = uuid;
}
