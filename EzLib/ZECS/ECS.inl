inline void EntityComponentSystem::AddListener(ECSListener* listener)
{
	m_ECSListeners.push_back(listener);
}

inline void EntityComponentSystem::RemoveListener(ECSListener* listener)
{
	auto it = std::find(m_ECSListeners.begin(), m_ECSListeners.end(), listener);
	if (it == m_ECSListeners.end())
		return;

	m_ECSListeners.erase(it);
}

inline void EntityComponentSystem::RemoveAllListener()
{
	m_ECSListeners.clear();
}

inline ZSP<ECSEntity> EntityComponentSystem::MakeEntity(__ECSComponentBase** components, const uint32* componentIDs, size_t numComponents)
{
	std::lock_guard<std::mutex> lg(m_EntityMtx);
	ZSP<ECSEntity> entity = Make_Shared(ECSEntity,appnew);
	entity->m_EntityHandle = m_Entities.size();
	m_Entities.emplace_back();
	m_Entities.back().first = entity;
	EntityCompsPair& entityCompsPair = m_Entities.back();

	for (uint32 i = 0; i < numComponents; i++)
	{
		if (__ECSComponentBase::IsTypeValid(componentIDs[i]) == false)
		{
			std::string errorMsg = "'" + std::to_string(componentIDs[i]) + "'" + " is not a valid component type";
			MessageBox(nullptr, errorMsg.c_str(), "ECS Error", MB_OK);

			return Make_Shared(ECSEntity,appnew);
		}

		_AddComponent(entity, entityCompsPair.second, componentIDs[i], components[i]);
	}

	// 行列コンポーネントが追加されていなければ無条件で追加
	if (_GetComponent(entityCompsPair.second,m_CompMemories[TransformComponent::ID], TransformComponent::ID) == nullptr)
	{
		if (__ECSComponentBase::IsTypeValid(TransformComponent::ID) == false)
		{
			std::string errorMsg = "'" + std::to_string(TransformComponent::ID) + "'" + " is not a valid component type";
			MessageBox(nullptr, errorMsg.c_str(), "ECS Error", MB_OK);

			return Make_Shared(ECSEntity,appnew);
		}

		_AddComponent(entity, entityCompsPair.second, TransformComponent::ID, MakeComponent<TransformComponent>());
		numComponents++;
	}

	//entityHandle->first = m_EntitieCompsPairList.size();
	//m_EntitieCompsPairList.push_back(entityHandle);

	// リスナーに通知
	for (auto& listener : m_ECSListeners)
	{
		if (listener->NotifyOnAllEntityOperations())
		{
			listener->OnMakeEntity(entity->m_EntityHandle);
			continue;
		}

		ComponentBitSet tmpBit = listener->GetComponentBitSet();
		tmpBit &= entity->m_CompBitSet;
		if (tmpBit == 0)
			continue;

		listener->OnMakeEntity(entity->m_EntityHandle);

	}

	return std::move(entity);
}

template<typename ...Args>
inline ZSP<ECSEntity> EntityComponentSystem::MakeEntity(Args* ...args)
{
	// 引数で受け取ったパラメータ数を保持
	static constexpr size_t size = sizeof...(Args);
	__ECSComponentBase* Components[size] = { args... };
	const uint32 IDs[size] = { args->ID ... };

	return std::move(MakeEntity(Components, &IDs[0], size));
}

template<>
inline ZSP<ECSEntity> EntityComponentSystem::MakeEntity()
{
	return std::move(MakeEntity(nullptr, nullptr, 0));

}

inline uint32 EntityComponentSystem::GetNumEntities()const
{
	return m_Entities.size();
}

inline ZSP<ECSEntity>& EntityComponentSystem::HandleToEntitySptr(EntityHandle entityHandle)
{
	return m_Entities[entityHandle].first;
}

inline EntityComps& EntityComponentSystem::HandleToEntityComps(EntityHandle entityHandle)
{
	return m_Entities[entityHandle].second;
}

template<class Component>
inline void EntityComponentSystem::AddComponent(ZSP<ECSEntity>& entity,const Component* component)
{
	static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");
	std::lock_guard<std::mutex> lg(m_CompMtx);

	_AddComponent(entity, HandleToEntityComps(entity->m_EntityHandle), Component::ID, component);
	
	// リスナーに通知
	for (auto& listener : m_ECSListeners)
	{
		const ZSVector<uint32>& componentIDs = listener->GetComponentIDs();
		for (auto compID : componentIDs)
		{
			if (listener->NotifyOnAllComponentOperations())
			{
				listener->OnAddComponent(entity->m_EntityHandle, Component::ID);
				continue;
			}
			
			if (compID != Component::ID)
				continue;
			listener->OnAddComponent(entity->m_EntityHandle, Component::ID);
			break;
			
		}
	}

}

inline void EntityComponentSystem::AddComponent(ZSP<ECSEntity>& entity, const InstanceData& instanceData)
{
	if (instanceData.Instance == nullptr)
		return;

	std::lock_guard<std::mutex> lg(m_CompMtx);

	_AddComponent(entity, HandleToEntityComps(entity->m_EntityHandle), instanceData.ComponentID,instanceData.Instance);

}

inline void EntityComponentSystem::_AddComponent(ZSP<ECSEntity>& entity, EntityComps& entityComps, uint32 componentID, __ECSComponentBase* component)
{
	// 同じコンポーネントIDをもつコンポーネントをすでに持っているか
	__ECSComponentBase* comp = nullptr;
	if (entity->m_EntityHandle == NULL_ENTITY_HANDLE)
		comp = _GetComponent(entityComps, m_CompMemories[componentID], componentID);
	else
		comp = GetComponent(entity->m_EntityHandle, componentID);

	// 持っていなければ新規追加
	component->m_Entity = entity;
	if (comp != nullptr)
	{
		// すでに持っていれば削除し、新規で受け取ったコンポーネントを追加
		_RemoveComponent(entity->m_EntityHandle, componentID);
	}

	// ビットフラグON
	entity->m_CompBitSet.set(componentID);

	entityComps[componentID] = component->m_MemoryIndex;

	// リスナーに通知
	for (auto& listener : m_ECSListeners)
	{
		if (listener->NotifyOnAllComponentOperations())
		{
			listener->OnAddComponent(entity->m_EntityHandle,componentID);
			continue;
		}

		ComponentBitSet tmpBit = listener->GetComponentBitSet();
		tmpBit &= entity->m_CompBitSet;
		if (tmpBit == 0)
			continue;

		listener->OnAddComponent(entity->m_EntityHandle, componentID);
	}

}

template<class Component>
inline bool EntityComponentSystem::RemoveComponent(EntityHandle entityHandle)
{
	static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");

	std::lock_guard<std::mutex> lg(m_CompMtx);

	return _RemoveComponent(entityHandle, Component::ID);
}

inline bool EntityComponentSystem::_RemoveComponent(EntityHandle entityHandle, uint32 componentID)
{
	auto& entityComponents = HandleToEntityComps(entityHandle);

	ZSP<ECSEntity> entity = HandleToEntitySptr(entityHandle);

	if (entity->m_CompBitSet.test(componentID) == false)
		return false;

	// リスナーに通知
	for (auto& listener : m_ECSListeners)
	{
		if (listener->NotifyOnAllComponentOperations())
		{
			listener->OnRemoveComponent(entityHandle, componentID);
			continue;
		}

		ComponentBitSet tmpBit = listener->GetComponentBitSet();
		tmpBit &= entity->m_CompBitSet;
		if (tmpBit == 0)
			continue;

		listener->OnRemoveComponent(entityHandle, componentID);
	}
	
	// ビットフラグ Off
	HandleToEntitySptr(entityHandle)->m_CompBitSet.set(componentID, false);

	DeleteComponent(componentID, entityComponents[componentID]);
	entityComponents.erase(componentID);
	
	return true;
}

template<class Component>
inline Component* EntityComponentSystem::GetComponent(EntityHandle entity)
{
	static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");
	return (Component*)(_GetComponent(HandleToEntityComps(entity), m_CompMemories[Component::ID], Component::ID));
}

inline __ECSComponentBase* EntityComponentSystem::GetComponent(EntityHandle entity, uint32 compID)
{
	return _GetComponent(HandleToEntityComps(entity), m_CompMemories[compID], compID);
}

inline __ECSComponentBase* EntityComponentSystem::_GetComponent(EntityComps& entityComponents, ComponentMemory& compMemory, uint32 componentID)
{
	if (entityComponents.size() == 0)
		return nullptr;

	for(auto& it : entityComponents)
	{
		if(it.first == componentID)
			return reinterpret_cast<__ECSComponentBase*>(&compMemory[it.second]);
	}

	return nullptr;
}

template<class Component>
inline Component* EntityComponentSystem::MakeComponent()
{
	static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");
	std::lock_guard<std::mutex> lg(m_CompMtx);
	ECSComponentCreateFunction createFunc = __ECSComponentBase::GetTypeCreateFunction(Component::ID);
	Component* comp = static_cast<Component*>(createFunc(m_CompMemories[Component::ID], nullptr));
	return comp;
}

template<class Component>
inline void EntityComponentSystem::RegisterClassRefrection(const ZString& className)
{
	static_assert(std::is_base_of<__ECSComponentBase, Component>(), "this class is not component");
	std::lock_guard<std::mutex> lg(m_CompMtx);
	m_CompRefrection.Register<Component>(className);
}

inline InstanceData EntityComponentSystem::InstantiateComponent(const ZString& className)
{
	std::lock_guard<std::mutex> lg(m_CompMtx);
	
	InstanceData data = m_CompRefrection.New(className);
	return std::move(data);
}