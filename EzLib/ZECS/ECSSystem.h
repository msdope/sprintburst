#ifndef __ECS_SYSTEM_H__
#define __ECS_SYSTEM_H__

#include "PCH/pch.h"
#include "ECSCommon.h"

namespace EzLib
{
namespace ZECS
{
	class ECSSystemBase : public ZEnable_Shared_From_This<ECSSystemBase>
	{
	public:
		enum
		{
			FLAG_OPTIONAL = 1
		};

	public:
		ECSSystemBase() : m_UseMultiThread(false), m_IsEnable(true)
		{
		}
		virtual ~ECSSystemBase()
		{
		}

		ECSSystemBase(ECSSystemBase&) = delete;

		virtual void UpdateComponents(float delta, UpdateCompParams components)
		{
		}
		virtual void LateUpdateComponents(float delta, UpdateCompParams components)
		{
		}

		virtual void DebugImGuiRender();

		const ZSVector<uint32>& GetComponentTypes()const;

		const ComponentBitSet& GetBitSet()const;

		const ZSVector<uint32>& GetComponentFlags()const;
		
		bool IsActive()const;

		bool UseMultiThread()const;

		ZString GetSystemName()const;
		
	protected:
		virtual void Init()
		{
		}
		template<class T>
		void AddComponentType(uint32 compFlag = 0);
		template<typename ...Args>
		void AddMultiComponentType();
		void _AddMultiComponentType(size_t* IDs,size_t numIDs);

	protected:
		bool m_UseMultiThread;
		ZString m_DebugSystemName;
		bool m_IsEnable;

	private:
		ZSVector<uint32> m_ComponentTypes;
		ZSVector<uint32> m_ComponentFlags;
		ComponentBitSet m_CompBitSet;
	};

#define DefUseComponentType(...)				\
private:										\
	struct ComponentParamIndex					\
	{											\
		enum{__VA_ARGS__};						\
	};											\
	virtual void Init()override					\
	{											\
		AddMultiComponentType<__VA_ARGS__>();	\
	}

#define GetCompFromUpdateParam(compType,compParam) (compType*)compParam[ComponentParamIndex::compType]

	class ECSSystemList
	{
	public:
		~ECSSystemList()
		{
			Release();
		}

		bool AddSystem(ZSP<ECSSystemBase> system);

		template<typename T>
		ZSP<T> AddSystem();
		
		bool RemoveSystem(ZSP<ECSSystemBase> system);

		void Release()
		{
			m_Systems.clear();
		}

		size_t Size()
		{
			return m_Systems.size();
		}

		auto begin()
		{
			return m_Systems.begin();
		}

		auto end()
		{
			return m_Systems.end();
		}

		ZSP<ECSSystemBase> operator[](uint32 index)
		{
			return m_Systems[index];
		}

	private:
		ZSVector<ZSP<ECSSystemBase>> m_Systems;

	};

}
}

namespace EzLib
{
namespace ZECS
{
#include "ECSSystem.inl"
}
}

#endif