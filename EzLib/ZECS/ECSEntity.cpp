#include "ECSEntity.h"

namespace EzLib
{
namespace ZECS
{
	ECSEntity::~ECSEntity()
	{
		if (m_EntityHandle != NULL_ENTITY_HANDLE)
			ECS.RemoveEntity(*this);
	}

	void EzLib::ZECS::ECSEntity::RemoveAllEntity(ZAVector<ZSP<ECSEntity>>& entityList)
	{
		if (entityList.empty())return;

		ZSVector<ZSP<ECSEntity>> removeTargetEntities;
		removeTargetEntities.reserve(entityList.size() / 2);
	
		// Handleがnullptrの要素を削除
		for (size_t i = 0; i < entityList.size(); i++)
		{
			if (entityList[i]->m_EntityHandle != NULL_ENTITY_HANDLE)
				removeTargetEntities.push_back(entityList[i]);
		}
	
		entityList.clear();

		// 昇順ソート
		std::sort(removeTargetEntities.begin(), removeTargetEntities.end(),
			[](ZSP<ECSEntity>& l, ZSP<ECSEntity>& r)
		{
			return l->m_EntityHandle < r->m_EntityHandle;
		});
	
		for (uint32 i = 0; i < removeTargetEntities.size(); i++)
			ECS.RemoveEntity(*removeTargetEntities[i].GetPtr());
	
		removeTargetEntities.clear();
		removeTargetEntities.shrink_to_fit();
	
	}

	void ECSEntity::Remove()
	{
		if (m_EntityHandle != NULL_ENTITY_HANDLE)
			ECS.RemoveEntity(*this);
	}

	void ECSEntity::CreateEntityFromJsonFile(const ZString & fileName, ZAVector<ZSP<ECSEntity>>* list)
	{
		// json ファイル読み込み
		std::string error;
		json11::Json json = LoadJsonFromFile(fileName.c_str(), error);
		if (error.size() != 0)
		{
			DW_SCROLL(2, "json 読み込みエラー(%s)", fileName.c_str());
			return;
		}

		CreateEntityFromJson(json,list);
	}

	void ECSEntity::CreateEntityFromJson(const json11::Json & jsonObj, ZAVector<ZSP<ECSEntity>>* list)
	{
		for (auto& jsonEntity : jsonObj["Objects"].array_items())
		{
			// エンティティ作成
			auto entity = ECS.MakeEntity();

			for (auto& jsonComp : jsonEntity["CompList"].array_items())
			{
				// 文字列からクラスインスタンスを生成する（ClassReflectionクラス使用）
				auto comp = ECS.InstantiateComponent(jsonComp.string_value().c_str());

				// jsonで初期化
				comp.Instance->InitFromJson(jsonEntity[jsonComp.string_value()]);

				// コンポーネント追加
				ECS.AddComponent(entity, comp);
			}
			list->push_back(entity);
		}
	}

}
}
