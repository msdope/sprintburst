#include "PCH/pch.h"

using namespace EzLib;


//==========================================================================
// 複合形状クラス
//==========================================================================
void ZPhysicsShape_Compound::AddChildShape(ZSP<ZPhysicsShape_Base> shape, const ZMatrix& mat)
{
	// 子リストに追加
	m_ShapeList.push_back(shape);

	btTransform t;
	t.setFromOpenGLMatrix(&mat._11);
	m_Shape->addChildShape(t, shape->GetShape().get());

	//	UpdateInertia();
}

void ZPhysicsShape_Compound::Release()
{
	m_Shape = nullptr;

	// 子形状削除
	m_ShapeList.clear();
}


//==========================================================================
// メッシュ形状クラス
//==========================================================================
void ZPhysicsShape_Mesh::CreateFromMesh(const ZMesh& pMesh, const ZVec3& localScale)
{
	int vertStride = sizeof(btVector3);
	int indexStride = 3* sizeof(int);

	// 頂点数
	int totalVerts = pMesh.GetNumVerts();
	m_Verts = appnewArray(btVector3,totalVerts);
	// 頂点データ
	for (int i = 0; i < totalVerts; i++)
	{
		const ZVec3& pos0 = pMesh.GetVertex_Pos(i);
		m_Verts[i] = btVector3(pos0.x, pos0.y, pos0.z);
	}


	// インデックス 面数ｘ３
	int totalTriangles = pMesh.GetNumFaces();
	m_Idxs = appnewArray(int,totalTriangles* 3);

	// インデックスデータ作成
	for (int i = 0; i < totalTriangles; i++)
	{
		m_Idxs[i* 3] = pMesh.GetFace()[i].idx[0];// i* 3;
		m_Idxs[i* 3 + 1] = pMesh.GetFace()[i].idx[1];//i* 3 + 1;
		m_Idxs[i* 3 + 2] = pMesh.GetFace()[i].idx[2];//i* 3 + 2;
	}

	// 
	m_indexVertexArrays = appnew(btTriangleIndexVertexArray,
		totalTriangles,
		m_Idxs,
		indexStride,
		totalVerts,
		(btScalar*)m_Verts,
		vertStride);

	ZVec3 vBBMin = pMesh.GetAABB_Center() - pMesh.GetAABB_HalfSize();
	ZVec3 vBBMax = pMesh.GetAABB_Center() + pMesh.GetAABB_HalfSize();
	btVector3 aabbMin(vBBMin.x, vBBMin.y, vBBMin.z);
	btVector3 aabbMax(vBBMax.x, vBBMax.y, vBBMax.z);

	bool useQuantizedAabbCompression = true;
	// ※Bulletライブラリはmake_shared使うと失敗する時があったので、通常作成
	m_Shape = sptr<btBvhTriangleMeshShape>(new btBvhTriangleMeshShape(m_indexVertexArrays, useQuantizedAabbCompression, aabbMin, aabbMax));
	m_Shape->setLocalScaling(btVector3(localScale.x, localScale.y, localScale.z));

	//	m_Mass = 0;
	//	m_Inertia.setValue(0, 0, 0);
}

bool EzLib::CreateShape(ZPhysicsShape_Base** out, ZBP_RigidBody& rbBp)
{
	Safe_Delete(*out);

	// shapeの作成は関数化したほうがいい
	switch (rbBp.Shape)
	{
		case ZBP_RigidBody::shape::Sphere:
		{
			auto* sphere = sysnew(ZPhysicsShape_Sphere);
			sphere->Create(rbBp.ShapeSize.x);
			(*out) = sphere;
		}
		break;

		case ZBP_RigidBody::shape::Box:
		{
			auto* box = sysnew(ZPhysicsShape_Box);
			box->Create(rbBp.ShapeSize);
			(*out) = box;
		}
		break;

		case ZBP_RigidBody::shape::Capsule:
		{
			auto* capsule = sysnew(ZPhysicsShape_Capsule);
			capsule->Create(rbBp.ShapeSize.x, rbBp.ShapeSize.y);
			(*out) = capsule;
		}
		break;

		default:
			// その他 Mesh形状,Compound形状は未実装
			return false;
	}

	return true;
}



bool EzLib::CreateShape(ZSP<ZPhysicsShape_Base>& out, ZBP_RigidBody& rbBp)
{
	ZPhysicsShape_Base* shape = nullptr;

	if (CreateShape(&shape, rbBp) == false)
		return false;

	out = shape;

	return true;
}
