//===============================================================
//  @file ZBullet_RigidBody.h
//   「Bullet Physics Engine」の「形状」関係のクラス
// 
//  適当にまとめて書いてます。
// 
//  @author 鎌田
//===============================================================
#ifndef ZBullet_Shape_h
#define ZBullet_Shape_h

#pragma warning(disable:4316)

namespace EzLib
{
	// ZPhysicsShape_Base		… 形状の基本クラス
	// ZPhysicsShape_Box		… 立方体形状
	// ZPhysicsShape_Sphere	… 球形状
	// ZPhysicsShape_Capsule	… カプセル形状
	// ZPhysicsShape_Mesh		… メッシュ形状
	// ZPhysicsShape_Compound	… 複合形状

	//==========================================================================
	//   形状基本クラス
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsShape_Base : public ZPhysicsObj_Base
	{
	public:
		//   形状を取得
		virtual sptr<btCollisionShape>	GetShape() { return nullptr; }

		//   スケール値を取得
		void GetLocalScaling(ZVec3& scale)
		{
			btCollisionShape* shape = GetShape().get();
			btVector3 v = shape->getLocalScaling();
			scale.Set(v.x(), v.y(), v.z());
		}

		//   スケール設定
		void SetLocalScaling(const ZVec3& localScale)
		{
			GetShape()->setLocalScaling(btVector3(localScale.x, localScale.y, localScale.z));
		}


	};

	//==========================================================================
	//   BOX形状クラス
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsShape_Box : public ZPhysicsShape_Base
	{
	public:
		//   形状を取得
		virtual sptr<btCollisionShape>		GetShape() override { return m_Shape; }

		// @ brief BOX形状取得
		sptr<btBoxShape>					GetShape_Box() { return m_Shape; }

		//   形状を作成
		//  	halfSize	… 大きさ(ハーフサイズ)
		//  	localScale	… 内部に記憶する形状のスケール GetMatrix()時に影響します
		void Create(const ZVec3& halfSize, const ZVec3& localScale = ZVec3::One)
		{
			// 形状作成
			btVector3 shapeHarfSize(halfSize.x, halfSize.y, halfSize.z);
			m_Shape = sptr<btBoxShape>(new btBoxShape(shapeHarfSize));	// ※Bulletライブラリはmake_shared使うと失敗する時があったので、通常作成
			m_Shape->setLocalScaling(btVector3(localScale.x, localScale.y, localScale.z));
		}

		//   解放
		virtual void Release() override
		{
			m_Shape = nullptr;
		}

		// 
		ZPhysicsShape_Box()
		{
		}
		// 
		~ZPhysicsShape_Box()
		{
			Release();
		}

	protected:
		sptr<btBoxShape>		m_Shape;		// 形状

	private:
		// コピー禁止用
		ZPhysicsShape_Box(const ZPhysicsShape_Box& src) {}
		void operator=(const ZPhysicsShape_Box& src) {}

	};

	//==========================================================================
	//   カプセル形状クラス
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsShape_Capsule : public ZPhysicsShape_Base
	{
	public:
		//   形状を取得
		virtual sptr<btCollisionShape>	GetShape() override { return m_Shape; }

		// @ brief カプセル形状取得
		sptr<btCapsuleShape>			GetShape_Capsule() { return m_Shape; }

		//   形状を作成
		//  	halfSize	… 大きさ(ハーフサイズ)
		//  	localScale	… 内部に記憶する形状のスケール GetMatrix()時に影響します
		void Create(float radius, float height, const ZVec3& localScale = ZVec3::One)
		{
			// 形状作成
			m_Shape = sptr<btCapsuleShape>(new btCapsuleShape(radius, height));	// ※Bulletライブラリはmake_shared使うと失敗する時があったので、通常作成
			m_Shape->setLocalScaling(btVector3(localScale.x, localScale.y, localScale.z));
		}

		//   解放
		virtual void Release() override
		{
			m_Shape = nullptr;
		}

		// 
		ZPhysicsShape_Capsule()
		{
		}
		// 
		~ZPhysicsShape_Capsule()
		{
			Release();
		}

	protected:
		sptr<btCapsuleShape>		m_Shape;		// 形状

	private:
		// コピー禁止用
		ZPhysicsShape_Capsule(const ZPhysicsShape_Capsule& src) {}
		void operator=(const ZPhysicsShape_Capsule& src) {}

	};

	//==========================================================================
	//   球形状クラス
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsShape_Sphere : public ZPhysicsShape_Base
	{
	public:
		//   形状を取得
		virtual sptr<btCollisionShape>	GetShape() override { return m_Shape; }

		// @ brief カプセル形状取得
		sptr<btSphereShape>				GetShape_Sphere() { return m_Shape; }

		//   形状を作成
		//  	halfSize	… 大きさ(ハーフサイズ)
		//  	localScale	… 内部に記憶する形状のスケール GetMatrix()時に影響します
		void Create(float radius, const ZVec3& localScale = ZVec3::One)
		{
			// 形状作成
			m_Shape = sptr<btSphereShape>(new btSphereShape(radius));	// ※Bulletライブラリはmake_shared使うと失敗する時があったので、通常作成
			m_Shape->setLocalScaling(btVector3(localScale.x, localScale.y, localScale.z));
		}

		//   解放
		virtual void Release() override
		{
			m_Shape = nullptr;
		}

		// 
		ZPhysicsShape_Sphere()
		{
		}
		// 
		~ZPhysicsShape_Sphere()
		{
			Release();
		}

	protected:
		sptr<btSphereShape>		m_Shape;		// 形状

	private:
		// コピー禁止用
		ZPhysicsShape_Sphere(const ZPhysicsShape_Sphere& src) {}
		void operator=(const ZPhysicsShape_Sphere& src) {}

	};

	//==========================================================================
	//   複合形状クラス
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsShape_Compound : public ZPhysicsShape_Base
	{
	public:
		//   形状を取得
		virtual sptr<btCollisionShape>	GetShape() override { return m_Shape; }

		// @ brief カプセル形状取得
		sptr<btCompoundShape>			GetShape_Compound() { return m_Shape; }

		//   形状を作成
		//  	localScale	… 内部に記憶する形状のスケール GetMatrix()時に影響します
		void Create(const ZVec3& localScale = ZVec3::One)
		{
			// 形状作成
			m_Shape = sptr<btCompoundShape>(new btCompoundShape());	// ※Bulletライブラリはmake_shared使うと失敗する時があったので、通常作成
			m_Shape->setLocalScaling(btVector3(localScale.x, localScale.y, localScale.z));
		}

		//   Compound形状の子形状として登録
		void AddChildShape(ZSP<ZPhysicsShape_Base> shape, const ZMatrix& mat);

		//   登録されてる子形状数を取得
		int GetChildShapeCnt() { return m_Shape->getNumChildShapes(); }

		//   解放
		virtual void Release() override;

		// 
		ZPhysicsShape_Compound()
		{
		}
		// 
		~ZPhysicsShape_Compound()
		{
			Release();
		}

	protected:
		sptr<btCompoundShape>					m_Shape;		// 形状

		ZSVector<ZSP<ZPhysicsShape_Base> >	m_ShapeList;	// 子形状リスト(形状を削除する場合は、m_Shape内からの削除すること)

	private:
		// コピー禁止用
		ZPhysicsShape_Compound(const ZPhysicsShape_Compound& src) {}
		void operator=(const ZPhysicsShape_Compound& src) {}

	};

	//==========================================================================
	//   メッシュ形状クラス
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsShape_Mesh : public ZPhysicsShape_Base
	{
	public:
		//   形状を取得
		virtual sptr<btCollisionShape>	GetShape() override { return m_Shape; }

		// @ brief カプセル形状取得
		sptr<btBvhTriangleMeshShape>			GetShape_Compound() { return m_Shape; }

		//   形状を作成
		//  	pMesh	… 大きさ(ハーフサイズ)
		//  	localScale	… 内部に記憶する形状のスケール GetMatrix()時に影響します
		void CreateFromMesh(const ZMesh& pMesh, const ZVec3& localScale = ZVec3::One);


		//   解放
		virtual void Release() override
		{
			m_Shape = nullptr;
			SAFE_DELPTR(m_indexVertexArrays);
			SAFE_DELPTR(m_Verts);
			SAFE_DELPTR(m_Idxs);
		}

		// 
		ZPhysicsShape_Mesh()
		{
		}
		// 
		~ZPhysicsShape_Mesh()
		{
			Release();
		}

	protected:
		btVector3* m_Verts;				// 頂点配列
		int*								m_Idxs;					// インデックス配列
		btTriangleIndexVertexArray*			m_indexVertexArrays;	// 
		//	btGImpactMeshShape*				m_Shape;				// 形状
		sptr<btBvhTriangleMeshShape>		m_Shape;				// 形状


	private:
		// コピー禁止用
		ZPhysicsShape_Mesh(const ZPhysicsShape_Mesh& src) {}
		void operator=(const ZPhysicsShape_Mesh& src) {}

	};

	bool CreateShape(ZPhysicsShape_Base** out, ZBP_RigidBody& rbBp);

	bool CreateShape(ZSP<ZPhysicsShape_Base>& out, ZBP_RigidBody& rbBp);

}


#endif
