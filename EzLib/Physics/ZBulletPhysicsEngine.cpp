#include "PCH/pch.h"

using namespace EzLib;

extern ContactProcessedCallback gContactProcessedCallback;

//==========================================================================
// Bullet Physics Engine ワールド用クラス
//==========================================================================

void ZPhysicsWorld::Init()
{
	// bullet初期化
	m_collisionConfiguration = sysnew(btDefaultCollisionConfiguration);		// デフォルトのコリジョンコンフィグ作成
	m_dispatcher = sysnew(btCollisionDispatcher,m_collisionConfiguration);	// デフォルトのコリジョンディスパッチャ作成

	// ブロードフェーズ衝突判定
	m_broadphase = sysnew(btDbvtBroadphase);
	/*
	btVector3 worldAabbMin(-10000,-10000,-10000);
	btVector3 worldAabbMax(10000,10000,10000);
	m_broadphase = new btAxisSweep3 (worldAabbMin, worldAabbMax);
	*/

	// デフォルトの拘束計算（数値解析）ソルバを作成する
	m_solver = sysnew(btSequentialImpulseConstraintSolver);

	// 物理ワールドの作成
	m_dynamicsWorld = sysnew(btDiscreteDynamicsWorld,m_dispatcher, m_broadphase, m_solver, m_collisionConfiguration);
	// 重力の設定
	m_dynamicsWorld->setGravity(btVector3(0, -9.8f, 0));

	// 衝突検知用コールバック関数設定
	gContactProcessedCallback = ZPhysicsWorld::s_ContactProcessedCallback;

	btGImpactCollisionAlgorithm::registerAlgorithm(m_dispatcher);

	// デバッグ描画用クラス作成
	m_debugDraw = Make_Unique(ZBullet_DebugDraw,sysnew);

	m_dynamicsWorld->setDebugDrawer(m_debugDraw.GetPtr());
}

bool ZPhysicsWorld::s_ContactProcessedCallback(btManifoldPoint& cp, void* body0, void* body1)
{
	btRigidBody* pBody0 = (btRigidBody*)body0;
	btRigidBody* pBody1 = (btRigidBody*)body1;

	ZPhysicsRigidBody* pRigidBase0 = (ZPhysicsRigidBody*)pBody0->getUserPointer();
	ZPhysicsRigidBody* pRigidBase1 = (ZPhysicsRigidBody*)pBody1->getUserPointer();

	// Bullet Worldのアドレスを取得
	ZPhysicsWorld* world = nullptr;
	if (pRigidBase0)world = pRigidBase0->GetWorld();
	else if (pRigidBase1)world = pRigidBase1->GetWorld();

	// 仮想関数呼び出しで通知する
	if (world && world->m_ContactProcessedCallback)
	{
		//		return world->ContactProcessedCallback(cp, pRigidBase0, pRigidBase1);
		return world->m_ContactProcessedCallback(cp, pRigidBase0, pRigidBase1);
	}

	return true;
}

// 解放
void ZPhysicsWorld::Release()
{
	if (m_dynamicsWorld)
	{

		// ジョイントを解除
		while (m_dynamicsWorld->getNumConstraints() > 0)
		{
			btTypedConstraint* obj = m_dynamicsWorld->getConstraint(0);
			ZPhysicsJoint_Base* p = (ZPhysicsJoint_Base*)obj->getUserConstraintPtr();
			if (p)
			{
				//				p->Release();
				p->RemoveFromWorld();
			}
		}
		// 剛体を解除
		for (int i = m_dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
		{
			btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
			btRigidBody* body = btRigidBody::upcast(obj);
			if (body)
			{
				ZPhysicsRigidBody* p = (ZPhysicsRigidBody*)body->getUserPointer();
				if (p)
				{
					//					p->Release();
					p->RemoveFromWorld();
				}
			}
		}
	}

	SAFE_DELPTR(m_dynamicsWorld);			// ワールド削除
	SAFE_DELPTR(m_solver);
	SAFE_DELPTR(m_broadphase);
	SAFE_DELPTR(m_dispatcher);
	SAFE_DELPTR(m_collisionConfiguration);
	m_debugDraw = nullptr;
}

bool ZPhysicsWorld::AddRigidBody(ZPhysicsRigidBody& rb, short group, short mask)
{
	if (rb.GetBody() == nullptr)return false;

	// ワールドに剛体追加
	if (!rb.GetBody()->isInWorld())
	{
		rb.SetWorld(this);
		m_dynamicsWorld->addRigidBody(rb.GetBody().get(), group, mask);
		return true;
	}
	return false;
}

bool ZPhysicsWorld::AddJoint(const ZSP<ZPhysicsJoint_Base>& joint, bool disableCollisionsBetweenLinkedBodies)
{
	if (joint->GetBaseJoint() == nullptr)return false;

	// ワールドにジョイント追加
	if (joint->GetWorld() == nullptr)
	{
		joint->SetWorld(this);
		m_dynamicsWorld->addConstraint(joint->GetBaseJoint().get(), disableCollisionsBetweenLinkedBodies);
		return true;
	}
	return false;
}

void ZPhysicsWorld::SetDebugDrawMode(int drawMode)
{
	if (m_debugDraw == nullptr)
		return;
	m_debugDraw->setDebugMode(drawMode);
}

bool ZPhysicsWorld::IsEnableDebugDraw()
{
	if (m_debugDraw == nullptr)
		return false;
	return (m_debugDraw->getDebugMode() != 0);
}

void ZPhysicsWorld::DisableDebugDraw()
{
	if (m_debugDraw == nullptr)
		return;
	m_debugDraw->setDebugMode(btIDebugDraw::DBG_NoDebug);
}

void ZPhysicsWorld::DebugDraw()
{
	m_dynamicsWorld->debugDrawWorld();
}
