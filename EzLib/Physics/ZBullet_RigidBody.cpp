#include "PCH/pch.h"

using namespace EzLib;

void ZPhysicsRigidBody::Create(ZSP<ZPhysicsShape_Base> shape, const ZMatrix& startMat, float mass)
{
	Release();

	m_Shape = shape;

	// 剛体の行列は拡大成分を入れてはいけないので、拡大無しの行列と拡大率を分離する
	//  拡大無し行列 → 剛体に使用
	//  拡大率　　　 → 形状に使用
	ZMatrix mat = (ZMatrix&)startMat;
	ZVec3 scale = mat.GetScale();
	mat.NormalizeScale();

	// 行列
	btTransform startTransform;
	startTransform.setFromOpenGLMatrix(&mat._11);

	// 形状に行列の拡大率を適用する
	shape->SetLocalScaling(scale);

	// 質量が0以外なら、動的物体なので形状から慣性テンソルを算出。0なら静的物体。
	if (mass != 0.0f)
		shape->GetShape()->calculateLocalInertia(mass, m_Inertia);
	else
		m_Inertia.setValue(0, 0, 0);
	m_Mass = mass;

	// 剛体作成
	btDefaultMotionState* myMotionState = sysnew(btDefaultMotionState,startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, shape->GetShape().get(), m_Inertia);
	rbInfo.m_additionalDamping = true;
	m_Body = sptr< btRigidBody>(new btRigidBody(rbInfo));
	m_Body->setUserPointer(this);		// ユーザーポインタに自分のアドレスを記憶させておく

	// 質量0なら、キネマティックにしとこ
	if (mass == 0)
		SetKinematic(true);

}

void ZPhysicsRigidBody::Release()
{
	// 剛体削除
	if (m_Body)
	{
		// 
		while (m_Body->getNumConstraintRefs() > 0)
		{
			m_Body->removeConstraintRef(m_Body->getConstraintRef(0));
		}

		// ワールドから解除
		if (m_World && m_World->GetWorld())
		{
			m_World->GetWorld()->removeCollisionObject(m_Body.get());
		}

		// 解放
		if (m_Body->getMotionState() != m_ActiveMotionState.GetPtr() &&
			m_Body->getMotionState() != m_KinematicMotionState.GetPtr())
			delptr(m_Body->getMotionState());

		m_Body = nullptr;
		m_Inertia.setValue(0, 0, 0);
		m_Mass = 0;
		m_Shape = nullptr;
		m_World = nullptr;
		m_ActiveMotionState = nullptr;
		m_KinematicMotionState = nullptr;
	}

}

bool ZPhysicsRigidBody::AddToWorld(ZPhysicsWorld& world, short group, short mask)
{
	return world.AddRigidBody(*this, group, mask);
}

bool ZPhysicsRigidBody::RemoveFromWorld()
{
	if (m_Body->isInWorld())
	{
		if (m_World && m_World->GetWorld())
		{
			m_World->GetWorld()->removeCollisionObject(m_Body.get());
			m_World = nullptr;
		}
		return true;
	}
	return false;
}

template<class T>
T* ZPhysicsRigidBody::CreateMotionState()
{
	if (m_Body->getMotionState() != m_ActiveMotionState.GetPtr() &&
		m_Body->getMotionState() != m_KinematicMotionState.GetPtr())
	{
		delptr(m_Body->getMotionState());
		m_Body->setMotionState(nullptr);
	}

	T* ms = sysnew(T);
	m_Body->setMotionState(ms);
	return ms;
}

void ZPhysicsRigidBody::SetMotionState(btMotionState* ms)
{
	if (m_Body->getMotionState() != m_ActiveMotionState.GetPtr() &&
		m_Body->getMotionState() != m_KinematicMotionState.GetPtr())
	{
		delptr(m_Body->getMotionState());
		m_Body->setMotionState(nullptr);
	}

	m_Body->setMotionState(ms);
}

bool EzLib::CreateRigidBody(ZPhysicsRigidBody** out, ZBP_RigidBody& rbBp)
{
	Safe_Delete(*out);

	ZSP<ZPhysicsShape_Base> shape;
	
	CreateShape(shape, rbBp);

	*out = sysnew(ZPhysicsRigidBody);
	
	ZMatrix mat;
	
	mat.CreateMove(rbBp.Translate);
	mat.CreateRotateX(rbBp.Rotate.x);
	mat.RotateY_Local(rbBp.Rotate.y);
	mat.RotateZ_Local(rbBp.Rotate.z);

	(*out)->Create(shape, mat, rbBp.Mass);
	(*out)->SetLinearDamping(rbBp.TranslateDimmer);
	(*out)->SetAngularDamping(rbBp.RotateDimmer);
	(*out)->SetRestitution(rbBp.Repulsion);
	(*out)->SetFriction(rbBp.Friction);
	
	return true;
}

void ZPhysicsRigidBody::SetActivationMotionState(bool activation)
{
	if (m_Body->isKinematicObject() == false)
	{
		if (activation)
		{
			m_Body->setCollisionFlags(m_Body->getCollisionFlags() & btCollisionObject::CF_KINEMATIC_OBJECT);
			SetMotionState(m_ActiveMotionState.GetPtr());
		}
		else
		{
			m_Body->setCollisionFlags(m_Body->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
			SetMotionState(m_KinematicMotionState.GetPtr());
		}
	}
	else
		SetMotionState(m_KinematicMotionState.GetPtr());
}
