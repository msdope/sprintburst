//===============================================================
//  @file ZBullet_RigidBody.h
//   「Bullet Physics Engine」の「ジョイント」関係のクラス
// 
//  適当にまとめて書いてます。
// 
//  @author 鎌田
//===============================================================
#ifndef ZBullet_Joint_h
#define ZBullet_Joint_h

#pragma warning(disable:4316)

namespace EzLib
{

	//==========================================================================
	//   ジョイント基本クラス
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsJoint_Base : public ZPhysicsObj_Base
	{
	public:
		//   基本型でジョイント取得
		virtual sptr<btTypedConstraint> GetBaseJoint() = 0;

		//   ジョイントを物理ワールドへ追加する。 ZPhysicsWorld::AddJoint()でも可能。
		void AddToWorld(ZPhysicsWorld& world, bool disableCollisionsBetweenLinkedBodies)
		{
			world.AddJoint(ToZSP(this), disableCollisionsBetweenLinkedBodies);
		}

		//   ジョイントを物理ワールドから解除する。
		void RemoveFromWorld()
		{
			if (GetBaseJoint() && m_World)
			{
				m_World->GetWorld()->removeConstraint(GetBaseJoint().get());
				m_World = nullptr;
			}
		}

	protected:
		ZPhysicsJoint_Base() {}

	};

	//==========================================================================
	//   ボールジョイントクラス
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsJoint_Point : public ZPhysicsJoint_Base
	{
	public:
		//   ボールジョイント取得
		sptr<btPoint2PointConstraint> GetJoint() { return m_Joint; }

		//   基本型でジョイント取得
		virtual sptr<btTypedConstraint> GetBaseJoint() override { return m_Joint; }

		//   解放
		virtual void Release() override
		{
			if (m_Joint)
			{
				if (m_World)m_World->GetWorld()->removeConstraint(m_Joint.get());
				m_Joint = nullptr;
			}
			m_World = nullptr;
		}

		//   ジョイントを作成
		//  	body1		… ジョイントに接続するジョイント１
		//  	body2		… ジョイントに接続するジョイント２
		//  	pivotA		… body1への相対位置
		//  	pivotB		… body2への相対位置
		void Create(btRigidBody* body1, btRigidBody* body2, const ZVec3& pivotA, const ZVec3& pivotB)
		{
			Release();

			btVector3 vA(pivotA.x, pivotA.y, pivotA.z);
			btVector3 vB(pivotB.x, pivotB.y, pivotB.z);
			m_Joint = sptr<btPoint2PointConstraint>(new btPoint2PointConstraint(*body1, *body2, vA, vB));
			m_Joint->setUserConstraintPtr(this);
			
		}

		//   ジョイントを作成
		//  	body1		… ジョイントに接続するジョイント１
		//  	pivotA		… body1への相対位置
		void Create(btRigidBody* body1, const btVector3& pivotA)
		{
			m_Joint = sptr<btPoint2PointConstraint>(new btPoint2PointConstraint(*body1, pivotA));
			m_Joint->setUserConstraintPtr(this);
		}

		// 
		ZPhysicsJoint_Point()
		{
		}

		~ZPhysicsJoint_Point()
		{
			Release();
		}


	protected:
		sptr<btPoint2PointConstraint>		m_Joint;

	private:
		// コピー禁止用
		ZPhysicsJoint_Point(const ZPhysicsJoint_Point& src) {}
		void operator=(const ZPhysicsJoint_Point& src) {}
	};

	//==========================================================================
	//   ヒンジジョイントクラス
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsJoint_Hinge : public ZPhysicsJoint_Base
	{
	public:
		//   ヒンジジョイント取得
		sptr<btHingeConstraint> GetJoint() { return m_Joint; }

		//   基本型でジョイント取得
		virtual sptr<btTypedConstraint> GetBaseJoint() override { return m_Joint; }

		//   解放
		virtual void Release() override
		{
			if (m_Joint)
			{
				if (m_World)m_World->GetWorld()->removeConstraint(m_Joint.get());
				m_Joint = nullptr;
			}
			m_World = nullptr;
		}

		//   ジョイント作成
		void Create(btRigidBody* body1, btRigidBody* body2, const btVector3& pivotA, const btVector3& pivotB, const btVector3& axisA, const btVector3& axisB)
		{
			m_Joint = sptr<btHingeConstraint>(new btHingeConstraint(*body1, *body2, pivotA, pivotB, axisA, axisB));
			m_Joint->setUserConstraintPtr(this);
		}

		//   ジョイント作成を作成し、ワールドへ追加
		void CreateAndAddWorld(btRigidBody* body1, const btVector3& pivotA, const btVector3& axisA)
		{
			m_Joint = sptr<btHingeConstraint>(new btHingeConstraint(*body1, pivotA, axisA));
			m_Joint->setUserConstraintPtr(this);
		}

		// 
		ZPhysicsJoint_Hinge()
		{
		}

		~ZPhysicsJoint_Hinge()
		{
			Release();
		}

	protected:
		sptr<btHingeConstraint>		m_Joint;

	private:
		// コピー禁止用
		ZPhysicsJoint_Hinge(const ZPhysicsJoint_Hinge& src) {}
		void operator=(const ZPhysicsJoint_Hinge& src) {}
	};


	//==========================================================================
	//   6Dofジョイントクラス
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsJoint_6Dof : public ZPhysicsJoint_Base
	{
	public:
		//   6Dofジョイント取得
		sptr<btGeneric6DofConstraint> GetJoint() { return m_Joint; }

		//   基本型でジョイント取得
		virtual sptr<btTypedConstraint> GetBaseJoint() override { return m_Joint; }

		//   解放
		virtual void Release() override
		{
			if (m_Joint)
			{
				if (m_World)m_World->GetWorld()->removeConstraint(m_Joint.get());
				m_Joint = nullptr;
			}
			m_World = nullptr;
		}

		//   ジョイント作成を作成し、ワールドへ追加
		//  	body1		… ジョイントに接続するジョイント１
		//  	body2		… ジョイントに接続するジョイント２
		//  	mA			… body1への相対位置
		//  	mB			… body2への相対位置
		void Create(btRigidBody* body1, btRigidBody* body2, const ZMatrix& mA, const ZMatrix& mB)
		{
			Release();

			btTransform tA;
			tA.setFromOpenGLMatrix(&mA._11);
			btTransform tB;
			tB.setFromOpenGLMatrix(&mB._11);
			m_Joint = sptr<btGeneric6DofConstraint>(new btGeneric6DofConstraint(*body1, *body2, tA, tB, true));
			m_Joint->setUserConstraintPtr(this);

		}

		// 制限

		//   移動制限設定 下限
		void SetLinearLowerLimit(float limitX, float limitY, float limitZ)
		{
			m_Joint->setLinearLowerLimit(btVector3(limitX, limitY, limitZ));
		}
		void SetLinearLowerLimit(const ZVec3& limit)
		{
			m_Joint->setLinearLowerLimit(btVector3(limit.x, limit.y, limit.z));
		}
		//   移動制限設定 上限
		void SetLinearUpperLimit(float limitX, float limitY, float limitZ)
		{
			m_Joint->setLinearUpperLimit(btVector3(limitX, limitY, limitZ));
		}
		void SetLinearUpperLimit(const ZVec3& limit)
		{
			m_Joint->setLinearUpperLimit(btVector3(limit.x, limit.y, limit.z));
		}
		//   回転制限設定 下限(度)
		void SetAngularLowerLimit(float anglimitX, float anglimitY, float anglimitZ)
		{
			m_Joint->setAngularLowerLimit(btVector3(ToRadian(anglimitX), ToRadian(anglimitY), ToRadian(anglimitZ)));
		}
		void SetAngularLowerLimit(const ZVec3& anglimit)
		{
			m_Joint->setAngularLowerLimit(btVector3(ToRadian(anglimit.x), ToRadian(anglimit.y), ToRadian(anglimit.z)));
		}
		//   回転制限設定 上限(度)
		void SetAngularUpperLimit(float anglimitX, float anglimitY, float anglimitZ)
		{
			m_Joint->setAngularUpperLimit(btVector3(ToRadian(anglimitX), ToRadian(anglimitY), ToRadian(anglimitZ)));
		}
		void SetAngularUpperLimit(const ZVec3& anglimit)
		{
			m_Joint->setAngularUpperLimit(btVector3(ToRadian(anglimit.x), ToRadian(anglimit.y), ToRadian(anglimit.z)));
		}
		//   Index版 各制限設定
		void SetLimit(int Index, float limitLo, float limitHi)
		{
			m_Joint->setLimit(Index, limitLo, limitHi);
		}

		// 
		ZPhysicsJoint_6Dof()
		{
		}

		~ZPhysicsJoint_6Dof()
		{
			Release();
		}

	protected:
		sptr<btGeneric6DofConstraint>		m_Joint;

	private:
		// コピー禁止用
		ZPhysicsJoint_6Dof(const ZPhysicsJoint_6Dof& src) {}
		void operator=(const ZPhysicsJoint_6Dof& src) {}
	};

	//==========================================================================
	//   6Dofバネジョイントクラス
	// 
	//  @ingroup Physics
	//==========================================================================
	class ZPhysicsJoint_6DofSpring : public ZPhysicsJoint_Base
	{
	public:
		//   6Dofバネジョイント取得
		sptr<btGeneric6DofSpringConstraint> GetJoint() { return m_Joint; }

		//   基本型でジョイント取得
		virtual sptr<btTypedConstraint> GetBaseJoint() override { return m_Joint; }

		//   解放
		virtual void Release() override
		{
			if (m_Joint)
			{
				if (m_World)m_World->GetWorld()->removeConstraint(m_Joint.get());
				m_Joint = nullptr;
			}
			m_World = nullptr;
		}

		//   ジョイント作成
		//  	body1		… ジョイントに接続するジョイント１
		//  	body2		… ジョイントに接続するジョイント２
		//  	mA			… body1への相対位置
		//  	mB			… body2への相対位置
		void Create(btRigidBody* body1, btRigidBody* body2, const ZMatrix& mA, const ZMatrix& mB)
		{
			Release();

			btTransform tA;
			tA.setFromOpenGLMatrix(&mA._11);
			btTransform tB;
			tB.setFromOpenGLMatrix(&mB._11);
			m_Joint = sptr<btGeneric6DofSpringConstraint>(new btGeneric6DofSpringConstraint(*body1, *body2, tA, tB, true));
			m_Joint->setEquilibriumPoint();
			m_Joint->setUserConstraintPtr(this);
		}

		//   バネを有効にする
		//  Index:0〜2が平行移動x,y,z 3〜5は回転移動x,y,z
		void EnableSpring(int Index, bool enable)
		{
			m_Joint->enableSpring(Index, enable);
		}
		//   バネの柔らかさ
		void SetSpring_Stiffness(int Index, float f)
		{
			m_Joint->setStiffness(Index, f);
		}
		//   減衰率
		void SetDamping(int Index, float f)
		{
			m_Joint->setDamping(Index, f); //減衰率
		}

		// 制限

		//   移動制限設定 下限
		void SetLinearLowerLimit(float limitX, float limitY, float limitZ)
		{
			m_Joint->setLinearLowerLimit(btVector3(limitX, limitY, limitZ));
		}
		void SetLinearLowerLimit(const ZVec3& limit)
		{
			m_Joint->setLinearLowerLimit(btVector3(limit.x, limit.y, limit.z));
		}
		//   移動制限設定 上限
		void SetLinearUpperLimit(float limitX, float limitY, float limitZ)
		{
			m_Joint->setLinearUpperLimit(btVector3(limitX, limitY, limitZ));
		}
		void SetLinearUpperLimit(const ZVec3& limit)
		{
			m_Joint->setLinearUpperLimit(btVector3(limit.x, limit.y, limit.z));
		}
		//   回転制限設定 下限(度)
		void SetAngularLowerLimit(float anglimitX, float anglimitY, float anglimitZ)
		{
			m_Joint->setAngularLowerLimit(btVector3(ToRadian(anglimitX), ToRadian(anglimitY), ToRadian(anglimitZ)));
		}
		void SetAngularLowerLimit(const ZVec3& anglimit)
		{
			m_Joint->setAngularLowerLimit(btVector3(ToRadian(anglimit.x), ToRadian(anglimit.y), ToRadian(anglimit.z)));
		}
		//   回転制限設定 上限(度)
		void SetAngularUpperLimit(float anglimitX, float anglimitY, float anglimitZ)
		{
			m_Joint->setAngularUpperLimit(btVector3(ToRadian(anglimitX), ToRadian(anglimitY), ToRadian(anglimitZ)));
		}
		void SetAngularUpperLimit(const ZVec3& anglimit)
		{
			m_Joint->setAngularUpperLimit(btVector3(ToRadian(anglimit.x), ToRadian(anglimit.y), ToRadian(anglimit.z)));
		}
		//   Index版 各制限設定
		void SetLimit(int Index, float limitLo, float limitHi)
		{
			m_Joint->setLimit(Index, limitLo, limitHi);
		}

		//   これを呼んだときのjointの位置関係がバネの復元基準の位置になる
		void SetEquilibriumPoint()
		{
			m_Joint->setEquilibriumPoint();
		}

		// 
		ZPhysicsJoint_6DofSpring() :
			m_Joint(nullptr)
		{
			//			m_Type = JO_6DOFSPRING;
		}

		~ZPhysicsJoint_6DofSpring()
		{
			Release();
		}

	protected:
		sptr<btGeneric6DofSpringConstraint>		m_Joint;

	private:
		// コピー禁止用
		ZPhysicsJoint_6DofSpring(const ZPhysicsJoint_6DofSpring& src) {}
		void operator=(const ZPhysicsJoint_6DofSpring& src) {}
	};

	// ジョイントをZBP_Joint構造体をもとに作成
	bool CreateJoint(ZPhysicsJoint_Base** out, ZBP_Joint& jointBp, btRigidBody* body1, btRigidBody* body2);
	

}

#endif
