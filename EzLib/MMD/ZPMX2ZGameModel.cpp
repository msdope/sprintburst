#include "PCH/pch.h"
#include "ZPMX2ZGameModel.h"

namespace EzLib
{
	bool ZPMX2ZGameModel(ZPMX& pmx, ZGameModel& model, const ZString& filename)
	{
		ZSP<ZSingleModel> pModel = model.GetModelTbl()[0];
		pModel->SetMesh(Make_Shared(ZMesh,appnew));
		pModel->SetName(pmx.Info.ModelName);

		#pragma region メッシュ
		{
			size_t numVerts = pmx.Vertices.size();
			ZAVector<ZVertex_Pos_UV_TBN_Skin> vertTbl(numVerts);
			size_t numFace = pmx.Faces.size();
			ZAVector<ZMesh::Face> face(numFace);

			size_t numMate = pmx.Materials.size();
			ZAVector<ZMeshSubset> subset(numMate);
			ZAVector<ZMaterial> material(numMate);

			// 頂点情報作成
			for (size_t i = 0; i < numVerts; i++)
			{
				vertTbl[i].Pos = pmx.Vertices[i].Position;
				vertTbl[i].Normal = pmx.Vertices[i].Normal;
				vertTbl[i].UV = pmx.Vertices[i].UV;

				int wCnt = 4;
				for (int wi = 0; wi < 4; wi++)
				{
					vertTbl[i].BlendIndices[wi] = pmx.Vertices[i].BoneIndices[wi];
					vertTbl[i].BlendWeight[wi] = (uint8)(255 * pmx.Vertices[i].BoneWeights[wi]);
				}

				switch (pmx.Vertices[i].WeightType)
				{
					case ZPMXVertexWeight::BDEF1:
					{
						vertTbl[i].BlendWeight[0] = 255;
						for (int j = 1; j < 4; j++)
						{
							vertTbl[i].BlendIndices[j] = 0xFFFF;
							vertTbl[i].BlendWeight[j] = 0;
						}
						wCnt = 1;
					}
					break;

					case ZPMXVertexWeight::BDEF2:
					case ZPMXVertexWeight::SDEF:
					{
						vertTbl[i].BlendWeight[1] = 255 - vertTbl[i].BlendWeight[0];
						for (int j = 2; j < 4; j++)
						{
							vertTbl[i].BlendIndices[j] = 0xFFFF;
							vertTbl[i].BlendWeight[j] = 0;
						}
						wCnt = 2;
					}
					break;
				}

				int total = 0;
				for (int wi = 0; wi < 4; wi++)
					total += vertTbl[i].BlendWeight[wi];

				// 正規化
				int total2 = 0;
				for (int wi = 0; wi < 4; wi++)
				{
					vertTbl[i].BlendWeight[wi] *= 255 / total;
					if (wi == wCnt - 1)
					{
						vertTbl[i].BlendWeight[wi] = 255 - total2;
						break;
					}
					total2 += vertTbl[i].BlendWeight[wi];
				}

			}

			// 面情報作成
			for (size_t i = 0; i < numFace; i++)
			{
				for (int fi = 0; fi < 3; fi++)
					face[i].idx[fi] = pmx.Faces[i].Vertices[fi];
				UINT f1 = face[i].idx[0];
				UINT f2 = face[i].idx[1];
				UINT f3 = face[i].idx[2];

				// 頂点の接線、順法線算出
				ZVec3 tangent, binormal;
				ZVec3 vN = (vertTbl[f1].Normal + vertTbl[f2].Normal + vertTbl[f3].Normal) / 3;
				CalcTangentAndBinormal(&vertTbl[f1].Pos, &vertTbl[f1].UV,
									   &vertTbl[f2].Pos, &vertTbl[f2].UV,
									   &vertTbl[f3].Pos, &vertTbl[f3].UV,
									   &vN, &tangent, &binormal);
				vN.Normalize();

				vertTbl[f1].Tangent = tangent;
				vertTbl[f2].Tangent = tangent;
				vertTbl[f3].Tangent = tangent;
				vertTbl[f1].Binormal = binormal;
				vertTbl[f2].Binormal = binormal;
				vertTbl[f3].Binormal = binormal;
			}

			ZAVector<ZString> texPaths;
			texPaths.reserve(pmx.Textures.size());
			for (const auto& tex : pmx.Textures)
			{
				ZString texPath = ZPathUtil::Combine(ZPathUtil::GetDirName(filename), tex.TextureName);
				texPaths.emplace_back(std::move(texPath));
			}

			// マテリアル作成
			size_t start = 0;
			for (size_t i = 0; i < numMate; i++)
			{
				ZString mateName = pmx.Materials[i].Name;

				subset[i].AttribId = i;
				subset[i].FaceStart = start;
				subset[i].FaceCount = pmx.Materials[i].NumFaceVertices / 3;
				start += subset[i].FaceCount;

				material[i].Diffuse = pmx.Materials[i].Diffuse;
				material[i].Specular = ZVec4(pmx.Materials[i].Specular.x, pmx.Materials[i].Specular.y, pmx.Materials[i].Specular.z, 1);
				material[i].Power = pmx.Materials[i].SpePower;
				material[i].Emissive = ZVec4(0);
				material[i].Ambient = pmx.Materials[i].Ambient;
				material[i].DrawFlag = ((uint8)pmx.Materials[i].DrawMode & (uint8)ZPMXDrawModeFlags::BothFlag);
				material[i].UV_Tiling = ZVec2(0, 0);
				material[i].UV_Offset = ZVec2(0, 0);

				material[i].TexSet = Make_Shared(ZTextureSet,appnew);
				material[i].TexSet->LoadTextureSetFromPMXMaterial(texPaths, pmx.Materials[i]);
			}
			pModel->SetMaterials(material);

			// メッシュ作成
			if (pModel->GetMesh()->Create(&vertTbl[0], vertTbl.size(), &face[0], face.size(), subset) == false)
				return false;
		}
		#pragma endregion

		#pragma region ボーン
		{
			auto& boneTree = model.GetBoneTree();
			
			int boneNum = pmx.Bones.size();

			if (boneTree.size() != 0)
				boneTree.clear();

			boneTree.resize(boneNum);
			for (int bi = 0; bi < boneNum; bi++)
				boneTree[bi] = Make_Shared(ZGameModel::BoneNode,appnew);

			for (int bi = 0; bi < boneNum; bi++)
			{
				ZSP<ZGameModel::BoneNode>boneNode = boneTree[bi];
				boneNode->BoneName = pmx.Bones[bi].Name;
				boneNode->OffsetID = bi;
				boneNode->Level = pmx.Bones[bi].DeformDepth;

				ZVec3 parentPos(0);
				if (pmx.Bones[bi].ParentBoneIndex >= 0 && pmx.Bones[bi].ParentBoneIndex < boneNum)
				{
					boneNode->pMother = boneTree[pmx.Bones[bi].ParentBoneIndex];
					parentPos = pmx.Bones[pmx.Bones[bi].ParentBoneIndex].Position;
				}

				boneNode->DefTransMat.SetPos(pmx.Bones[bi].Position - parentPos);
				boneNode->BoneFlag = (int)pmx.Bones[bi].BoneFlag;

				if ((uint16)pmx.Bones[bi].BoneFlag & (uint16)ZPMXBoneFlags::IK)
				{
					boneNode->IK.boneIdx = pmx.Bones[bi].IKTargetBoneIndex;
					boneNode->IK.LoopCnt = pmx.Bones[bi].IKIterationCount;
					boneNode->IK.ikLimitedAng = pmx.Bones[bi].IKLimit;
					for (const auto& ikLimit : pmx.Bones[bi].IKLinks)
					{
						ZGameModel::BoneNode::IKData::LinkData data;
						data.boneIdx = ikLimit.IKBoneIndex;
						data.bLimitAng = ikLimit.EnableLimit;
						data.minLimitAng = ikLimit.LimitMin;
						data.maxLimitAng = ikLimit.LimitMax;
						boneNode->IK.LinkList.emplace_back(std::move(data));
					}
				}

				if (boneNode->pMother.IsActive())
				{
					boneNode->pMother.Lock()->Child.push_back(boneNode);
					boneNode->DefLocalMat.SetPos(pmx.Bones[bi].Position);
					boneNode->OffsetMat = boneNode->DefLocalMat.Inversed();
				}

			}

			std::sort(boneTree.begin(), boneTree.end(),
					  [](const auto& a, const auto& b)
			{
				return a->Level < b->Level;
			});

			for (auto& bone : boneTree)
				bone->Level = -1;

		}
		#pragma endregion
	

		/*	~~~~~~~~~~ 表情モーフ(頂点アニメーション)未実装 ~~~~~~~~~~	*/
	


		/*------------------------------------------------------------------------------*/
		// 物理オブジェクトはモデルが持つと同じ[モデルの敵を複数出す]というようなことが	//
		// できないので物理オブジェクトの管理はボーンコントローラに任せる				//
		//	-> ZGameModelは情報のみ保持させる											//
		/*------------------------------------------------------------------------------*/
		
		#pragma region 物理オブジェクト

		model.GetPhysicsDataSetList().clear();
		
		// 剛体
		{
			auto& rbInfos = pmx.PhysicsDataSet.RigidBodyTbl;
			if (rbInfos.size() > 0)
			{
				model.GetPhysicsDataSetList().resize(1);
			}
			auto& modelRbDataTbl = model.GetPhysicsDataSetList()[0].RigidBodyDataTbl;

			for (auto& rbInfo : rbInfos)
			{
				ZGM_RigidBodyData rbData;
				// 剛体情報抽出
				{
					rbData.RigidBodyName		= std::move(rbInfo.Name);
					rbData.BoneIndex			= rbInfo.BoneIndex;
					rbData.PhysicsCalcType		= (ZGM_RigidBodyData::CalcType)rbInfo.OP;
					rbData.UnCollisionGroup		= rbInfo.UnCollisionGroup;
					rbData.Group				= rbInfo.Group;
					rbData.Mass					= rbInfo.Mass;
					rbData.Repulsion			= rbInfo.Repulsion;
					rbData.Friction				= rbInfo.Friction;
					rbData.Shape				= (ZBP_RigidBody::shape)rbInfo.Shape;
					rbData.ShapeSize			= rbInfo.ShapeSize;
					rbData.Translate			= rbInfo.Translate;
					rbData.TranslateDimmer		= rbInfo.TranslateDimmer;
					rbData.Rotate				= rbInfo.Rotate;
					rbData.RotateDimmer			= rbInfo.RotateDimmer;
				}

				if (rbData.PhysicsCalcType == ZGM_RigidBodyData::CalcType::Static)
					rbData.Mass = 0;

				// 剛体情報保持
				modelRbDataTbl.emplace_back(std::move(rbData));
			}

		}



		/*	~~~~~~~~~~ ソフトボディ未実装 ~~~~~~~~~~	*/



		// ジョイント
		{
			auto& jointInfos = pmx.PhysicsDataSet.JointTbl;
			auto& modelJointDataTbl = model.GetPhysicsDataSetList()[0].JointDataTbl;
	
			for (auto& jointInfo : jointInfos)
			{
				ZGM_JointData jointData;
				// ジョイント情報抽出
				{
					jointData.JointName				= jointInfo.Name;
					jointData.RigidBodyAIndex		= jointInfo.RigidBodyAIndex;
					jointData.RigidBodyBIndex		= jointInfo.RigidBodyBIndex;
					jointData.Type					= (ZBP_Joint::JointType)jointInfo.Type;
					jointData.Translate				= jointInfo.Translate;
					jointData.Rotate				= jointInfo.Rotate;
					jointData.TranslateLowerLimit	= jointInfo.TranslateLowerLimit;
					jointData.TranslateUpperLimit	= jointInfo.TranslateUpperLimit;
					jointData.RotateLowerLimit		= jointInfo.RotateLowerLimit;
					jointData.RotateUpperLimit		= jointInfo.RotateUpperLimit;
					jointData.SpringTranslateFactor	= jointInfo.SpringTranslateFactor;
					jointData.SpringRotateFactor	= jointInfo.SpringRotateFactor;
				}
				
				
				// ジョイント情報保持
				modelJointDataTbl.emplace_back(std::move(jointData));
				
			}
		}

		#pragma endregion

		return true;
	}

}