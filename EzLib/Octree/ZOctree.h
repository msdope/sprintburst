#ifndef __ZOCTREE_H__
#define __ZOCTREE_H__

namespace EzLib
{
	template<typename T>
	class ZOctreeSpace;
	
	template<typename T>
	class ZObjectForTree
	{
	public:
		ZObjectForTree();
		~ZObjectForTree();
	
		// 空間から抜ける
		bool Remove();
		// 空間を登録
		void RegisterSpace(ZOctreeSpace<T>* pSpace);
		// 次のオブジェクトへのポインタ取得
		ZSP<ZObjectForTree<T>>& GetNextObj();

	public:
		ZOctreeSpace<T>* m_pSpace;
		T* m_pObject;
		ZSP<ZObjectForTree<T>> m_pPrev;
		ZSP<ZObjectForTree<T>> m_pNext;

	};

	// 8分木空間分割クラス
	template<typename T>
	class ZOctree
	{
	protected:
		static constexpr uint MaxSpaceLevel = 7;

	public:
		ZOctree();
		~ZOctree();

		void Release();

		// 線形8分木配列構築
		bool Init(uint splitLevel,const ZAABB& octreeArea);

		// 生成した全ての空間削除
		void Clear();

		// オブジェクト登録
		bool RegisterObject(const ZAABB& aabb, T* pObj);
		bool RegisterObject(const ZAABB& aabb, ZSP<ZObjectForTree<T>>& pOFT);

		// モートンオーダーから所属空間のAABBを算出
		ZAABB CalculateOctreeSpaceAABBFromMotonNumber(uint num)const;

		// 登録されている全てのオブジェクトのなかで衝突の可能性のある組み合わせを収集
		// (リストには衝突の可能性があるオブジェクトの組を並べて格納している)
		size_t GetAllCollisionPairList(ZVector<T*>& colList)const;

		// AABBが所属する空間の衝突の可能性があるオブジェクトを収集
		size_t GetCollisionList(const ZAABB& aabb, ZVector<T*>& colList)const;

		// レイが衝突する空間からオブジェクト収集
		size_t GetCollisionList(const ZRay& ray, ZVector<T*>& colList)const;

		// レイが衝突する空間からオブジェクト収集(空間内のリンクリストの先頭オブジェクトのみ)
		size_t GetCollisionList(const ZRay& ray, ZVector<ZSP<ZObjectForTree<T>>>& firistObjectsforSpaces)const;

	protected:
		// 空間内で衝突ペアリスト作成
		void GetCollisionPairList(uint elem, ZVector<T*>& colList, ZList<T*>& colStac)const;
		
		// 指定空間とその全子空間に所属するオブジェクトを収集
		void GetCollisionList(uint elem, ZVector<T*>& colList)const;

		// レイが衝突する空間のIndexをすべて取得
		void GetRayHitSpaceIndexes(const ZRay& ray, ZSet<uint>& set)const;

		// 空間作成
		void CreateNewSpace(uint elem);

		// 範囲外か
		bool IsOutOfArea(const ZVec3& pos)const;

		// min,max座標から所属する空間番号を取得
		uint GetMortonNumber(const ZAABB& aabb)const;

		uint BitSeparate(uint8 n)const;

		// 3Dモートン空間番号算出
		uint Get3DMortonNumber(uint8 x, uint8 y, uint8 z)const;

		// 座標 -> 線形8分木要素番号変換
		uint GetPointElem(const ZVec3& p)const;

		// 空間のグリッド位置取得
		ZVec3 GetGridCoordinate(const ZVec3& pos)const;

		// 空間のグリッド位置取得
		ZVec3 GetGridCoordinate(const ZVec3& pos,uint level)const;

		// 空間のグリッド座標(AABBのMin座標)取得
		ZVec3 GetGridPosition(uint number)const;

		// 座標から所属する空間のグリッド座標(AABBのMin座標)算出
		ZVec3 CalculateGridPosition(const ZVec3& pos)const;

	protected:
		ZOctreeSpace<T>** m_pSpaceArray;				// 線形空間ポインタ配列
		uint m_NumOfSpaceByLevel[MaxSpaceLevel + 1];	// 各レベルでの空間数(べき乗値テーブル)
		ZVec3 m_RgnWidth;								// 領域の幅
		ZVec3 m_RgnMin;									// 領域の最小値
		ZVec3 m_RgnMax;									// 領域の最大値
		ZVec3 m_Unit;									// 最小領域の辺の長さ
		ulong m_NumSpace;								// 空間数
		uint m_LowestLevel;								// 最下位レベル

	};

	template<typename T>
	class ZOctreeSpace
	{
	public:
		ZOctreeSpace();
		~ZOctreeSpace();

		void ResetLink(ZSP<ZObjectForTree<T>>& pOFT);

		bool Push(ZSP<ZObjectForTree<T>>& pOFT);

		ZSP<ZObjectForTree<T>>& GetFirstObj();

		void OnRemove(ZObjectForTree<T>*& pRemoveObj);

	protected:
		ZSP<ZObjectForTree<T>> m_pLatest; // 最新OFTへのポインタ

	};

}

namespace EzLib
{
#include "ZOctree.inl"
}

#endif