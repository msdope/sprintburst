#include "ZOctree.h"
template<typename T>
inline ZObjectForTree<T>::ZObjectForTree()
	: m_pSpace(nullptr),
	m_pObject(nullptr),
	m_pPrev(nullptr),
	m_pNext(nullptr)
{
}

template<typename T>
inline ZObjectForTree<T>::~ZObjectForTree()
{
}

template<typename T>
inline bool ZObjectForTree<T>::Remove()
{
	if (m_pCell == nullptr)
		return false;

	// 空間に通知
	m_pCell->OnRemove(this)

	// このオブジェクトの前後のオブジェクトの結びつけ
	if (m_pPrev != nullptr)
	{
		m_pPrev->m_pNext = m_pNext;
		m_pPrev = nullptr;
	}

	if (m_pNext != nullptr)
	{
		m_pNext->m_pPrev = m_pPrev;
		m_pNext = nullptr;
	}

	m_pCell = nullptr;
	return true;
}

template<typename T>
inline ZSP<ZObjectForTree<T>>& ZObjectForTree<T>::GetNextObj()
{
	return m_pNext;
}

template<typename T>
inline void ZObjectForTree<T>::RegisterSpace(ZOctreeSpace<T>* pSpace)
{
	m_pSpace = pSpace;
}

template<typename T>
inline ZOctree<T>::ZOctree()
			: m_pSpaceArray(nullptr),
			  m_RgnWidth(1),
			  m_RgnMin(0),
			  m_RgnMax(1),
			  m_Unit(1),
			  m_NumSpace(0),
			  m_LowestLevel(0)
{
	// 各レベルの空間数算出
	m_NumOfSpaceByLevel[0] = 1;
	for (int i = 1; i < MaxSpaceLevel + 1; i++)
		m_NumOfSpaceByLevel[i] = m_NumOfSpaceByLevel[i - 1] * 8;
}

template<typename T>
inline ZOctree<T>::~ZOctree()
{
	Release();
}

template<typename T>
inline void ZOctree<T>::Release()
{
	Clear();
	SAFE_DELPTR(m_pSpaceArray);
}

template<typename T>
inline bool ZOctree<T>::Init(uint splitLevel, const ZAABB& octreeArea)
{
	if (splitLevel >= MaxSpaceLevel)
		return false;

	Release();

	// レベル(0基点)の配列作成
	m_NumSpace = (m_NumOfSpaceByLevel[splitLevel + 1] - 1) / 7;
	m_pSpaceArray = sysnewArray(ZOctreeSpace<T>*,m_NumSpace);
	memset(m_pSpaceArray, 0, sizeof(ZOctreeSpace<T>*) * m_NumSpace);

	// 領域を登録
	m_RgnMin = octreeArea.Min;
	m_RgnMax = octreeArea.Max;
	m_RgnWidth = octreeArea.Max - octreeArea.Min;
	m_Unit = m_RgnWidth / (float)(1 << splitLevel);

	m_LowestLevel = splitLevel;

	return true;
}

template<typename T>
inline void ZOctree<T>::Clear()
{
	if (m_pSpaceArray == nullptr) return;

	for (uint i = 0; i < m_NumSpace; i++)
	{
		if (m_pSpaceArray[i] != nullptr)
			SAFE_DELPTR(m_pSpaceArray[i]);
	}
}

template<typename T>
inline void ZOctree<T>::CreateNewSpace(uint elem)
{
	// elem番号の空間が作られていなければ
	while (m_pSpaceArray[elem] == nullptr)
	{
		// 指定の要素番号に空間を作成
		m_pSpaceArray[elem] = sysnew(ZOctreeSpace<T>);

		// 親空間に移動
		// 親空間も作られていなければ空間作成
		elem = (elem - 1) >> 3;
		if (elem >= m_NumSpace)
			break;
	}
}

template<typename T>
inline bool ZOctree<T>::IsOutOfArea(const ZVec3& pos)const
{
	// 範囲外か
	if (pos.x < m_RgnMin.x || pos.y < m_RgnMin.y || pos.z < m_RgnMin.z ||
		pos.x > m_RgnMax.x || pos.y > m_RgnMax.y || pos.z > m_RgnMax.z)
		return true;

	return false;
}

template<typename T>
inline bool ZOctree<T>::RegisterObject(const ZAABB& aabb, T* pObj)
{
	ZSP<ZObjectForTree<T>> pOFT = Make_Shared(ZObjectForTree<T>, sysnew);
	pOFT->m_pObject = pObj;
	return RegisterObject(aabb, pOFT);
}

template<typename T>
inline bool ZOctree<T>::RegisterObject(const ZAABB& aabb, ZSP<ZObjectForTree<T>>& pOFT)
{
	uint elem = GetMortonNumber(aabb);
	if (elem >= m_NumSpace)
		return false;

	if (m_pSpaceArray[elem] == nullptr)
		CreateNewSpace(elem);

	return m_pSpaceArray[elem]->Push(pOFT);
}

template<typename T>
inline ZAABB ZOctree<T>::CalculateOctreeSpaceAABBFromMotonNumber(uint num)const
{
	int level = 0;
	while (num >= m_NumOfSpaceByLevel[level])
	{
		num -= m_NumOfSpaceByLevel[level];
		level++;
	}

	// 所属する空間レベルの分割サイズを算出
	ZVec3 boxSize = m_RgnWidth / (float)(1 << level);
	ZVec3 minPos = GetGridPosition(num);

	return ZAABB(minPos,minPos + boxSize);
}

template<typename T>
inline size_t ZOctree<T>::GetAllCollisionPairList(ZVector<T*>& colList)const
{
	// リスト初期化
	colList.clear();

	// ルート空間の存在チェック
	if (m_pSpaceArray[0] == nullptr)
		return 0; // 空間が存在しない

	// ルート空間を処理
	ZList<T*> colStac;
	GetCollisionList(0, colList, colStac);

	return colList.size();
}

template<typename T>
inline size_t ZOctree<T>::GetCollisionList(const ZAABB& aabb, ZVector<T*>& colList)const
{
	size_t elem = GetMortonNumber(aabb);
	// 範囲外
	if (elem >= m_NumSpace)return 0;

	colList.clear();
	if (m_pSpaceArray[elem] == nullptr)
		return 0; // 空間が存在しない

	// この空間と全子空間のオブジェクトを再回帰で取得
	GetCollisionList(elem, colList);

	// 親空間のオブジェクト収集
	while (elem > 0)
	{
		// 親空間へ
		elem = (elem - 1) / 8;
		// ルート空間までは収集しに行かない
		if (elem <= 0) break;

		if (m_pSpaceArray[elem] == nullptr)
			continue;
		auto pOFT = m_pSpaceArray[elem]->GetFirstObj();
		while (pOFT != nullptr)
		{
			colList.push_back(pOFT->m_pObject);
			pOFT = pOFT->m_pNext;
		}

	}

	return colList.size();
}

template<typename T>
inline size_t ZOctree<T>::GetCollisionList(const ZRay& ray, ZVector<T*>& colList)const
{
	colList.clear();
	
	// レイと衝突する空間格納用(被りがないようにstd::set)
	ZSet<uint> spaceIndexes;
	GetRayHitSpaceIndexes(ray, spaceIndexes);
	
	colList.reserve(spaceIndexes.size() * 1000); // 適当にメモリ確保
	for (auto index : spaceIndexes)
	{
		// 親空間のオブジェクト取得
		auto pOFT = m_pSpaceArray[index]->GetFirstObj();
		while (pOFT != nullptr)
		{
			colList.push_back(pOFT->m_pObject);
			pOFT = pOFT->m_pNext;
		}
	}

	return colList.size();
}

template<typename T>
inline size_t ZOctree<T>::GetCollisionList(const ZRay& ray, ZVector<ZSP<ZObjectForTree<T>>>& firistObjectsforSpaces)const
{
	firistObjectsforSpaces.clear();

	ZRay tmpRay = ray;
	ZAABB rootAABB = ZAABB(m_RgnMin, m_RgnMax);
	// Ray対ルート空間のAABB
	// 当たっていなければ収集なし
	if (ZCollision::RayToBox(ray, nullptr, rootAABB, nullptr, &tmpRay.Start) == false)
		return 0;

	// レイと衝突する空間格納用(被りがないようにstd::set)
	ZSet<uint> spaceIndexes;
	GetRayHitSpaceIndexes(tmpRay, spaceIndexes);
	
	for (auto index : spaceIndexes)
	{
		// 親空間のオブジェクト取得
		auto pOFT = m_pSpaceArray[index]->GetFirstObj();
		if (pOFT == nullptr)continue;
		firistObjectsforSpaces.push_back(pOFT);
	}

	return firistObjectsforSpaces.size();
}

template<typename T>
inline void ZOctree<T>::GetCollisionPairList(uint elem, ZVector<T*>& colList, ZList<T*>&colStac)const
{
	// 空間内のオブジェクト同士の衝突リスト作成
	ZSP<ZObjectForTree<T>> pOFT1 = m_pSpaceArray[elem]->GetFirstObj();
	while (pOFT1 != nullptr)
	{
		ZSP<ZObjectForTree> pOFT2 = pOFT1->pNext;
		while (pOFT2 != nullptr)
		{
			// 衝突リスト作成
			colList.push_back(pOFT1->m_pObject);
			colList.push_back(pOFT2->m_pObject);
			pOFT2 = pOFT2->m_pNext;
		}

		// 衝突スタックとの衝突リスト作成
		for (auto it = colStac.begin(); it != colStac.end(); it++)
		{
			colList.push_back(pOFT1->m_pObject);
			colList.push_back(*it);
		}

		pOFT1 = pOFT1->m_pNext;
	}

	bool childFlg = false;
	// 子空間に移動
	uint numObj = 0;
	uint nextElem;
	for (uint i = 0; i < 8; i++)
	{
		nextElem = elem * 8 + 1 + i;
		if (nextElem < m_NumSpace && m_pSpaceArray[nextElem])
		{
			if (childFlg == false)
			{
				// 登録オブジェクトをスタックに追加
				pOFT1 = m_pSpaceArray[elem]->GetFirstObj();
				while (pOFT1 != nullptr)
				{
					colStac.push_back(pOFT1->m_pObject);
					numObj++;
					pOFT1 = pOFT1->m_pNext;
				}
			}
			childFlg = true;
			GetCollisionList(nextElem, colList, colStac); // 小空間へ
		}
	}

	if (childFlg == false)
		return;

	// スタックからオブジェクトを外す
	for (uint i = 0; i < numObj; i++)
		colStac.pop_back();
}

template<typename T>
inline void ZOctree<T>::GetCollisionList(uint elem, ZVector<T*>& colList)const
{
	if (m_pSpaceArray[elem] == nullptr)
		return;

	// 空間内のオブジェクト同士の衝突リスト作成
	ZSP<ZObjectForTree<T>> pOFT = m_pSpaceArray[elem]->GetFirstObj();
	while (pOFT != nullptr)
	{
		colList.push_back(pOFT->m_pObject);
		pOFT = pOFT->m_pNext;
	}

	bool childFlg = false;
	// 子空間に移動
	uint nextElem = elem * 8 + 1;
	for (uint i = 0; i < 8; i++)
	{
		if (nextElem + i < m_NumSpace && m_pSpaceArray[nextElem + i])
		{
			childFlg = true;
			GetCollisionList(nextElem + i, colList); // 小空間へ
		}
	}

	if (childFlg == false)
		return;
}

template<typename T>
inline void ZOctree<T>::GetRayHitSpaceIndexes(const ZRay& ray, ZSet<uint>& set) const
{
	set.clear();
	auto minSize = m_Unit;
	auto rootAABB = ZAABB(m_RgnMin, m_RgnMax);
	auto rayLen = (ray.Start - ray.End).Length();
	ZVec3 grid = GetGridCoordinate(ray.Start);
	ZVec3 gridForward = ZVec3(ray.Dir.x >= 0 ? 1 : -1, ray.Dir.y >= 0 ? 1 : -1, ray.Dir.z >= 0 ? 1 : -1);

	float dist = 0; // 例の進んだ距離
	ZVec3 pos = ZVec3(grid.x * minSize.x, grid.y * minSize.y, grid.z * minSize.y) + m_RgnMin;
	ZVec3 nextPos = pos;

	// レイと衝突する空間探索
	while (true)
	{
		uint number = Get3DMortonNumber(grid.x, grid.y, grid.z);
		int exists_max_split_level = 0;
		for (int i = 0; i <= m_LowestLevel; i++)
		{
			int splitLevel = m_LowestLevel - i;
			uint index = ((number >> i * 3) + m_NumOfSpaceByLevel[splitLevel] / 7);
			if (index > m_NumSpace) continue;
			if (m_pSpaceArray[index] != nullptr)
			{
				set.insert(index);
				exists_max_split_level = max(exists_max_split_level, splitLevel);
			}
		}

		// 探索すべき空間レベルの決定
		exists_max_split_level = min(exists_max_split_level + 1, m_LowestLevel);
		// 探索空間レベルの基準の座標系で次のグリッド座標を決定する
		auto nextGrid = gridForward + GetGridCoordinate(pos, exists_max_split_level);
		auto size = m_RgnWidth / (float)(1 << exists_max_split_level);
		// 次のグリッドサイズから探索空間における座標を算出
		nextPos = ZVec3(nextGrid.x * size.x, nextGrid.y * size.y, nextGrid.z * size.y) + m_RgnMin;

		// 次のグリッド座標が8分木の範囲から出ていれば終了
		if (IsOutOfArea(nextPos)) break;

		float ax = ray.Dir.x != 0 ? abs((nextPos.x - pos.x) / ray.Dir.x) : FLT_MAX;
		float ay = ray.Dir.y != 0 ? abs((nextPos.y - pos.y) / ray.Dir.y) : FLT_MAX;
		float az = ray.Dir.z != 0 ? abs((nextPos.z - pos.z) / ray.Dir.z) : FLT_MAX;

		auto oldPos = pos;
		// レイを進める
		if (ax < ay && ax < az)
		{
			pos += ray.Dir * ax;
			grid.x = nextGrid.x;
		}
		else if (ay < ax && ay < az)
		{
			pos += ray.Dir * ay;
			grid.y = nextGrid.y;
		}
		else if (az < ax && az < ay)
		{
			pos += ray.Dir * az;
			grid.z += nextGrid.z;
		}
		else
		{
			pos += ZVec3(ray.Dir.x * ax, ray.Dir.y * ay, ray.Dir.z * az);
			grid = nextGrid;
		}

		// 進んだ位置がレイの範囲外か
		if ((pos - oldPos).Length() > rayLen)
			break;
	}


}

template<typename T>
inline uint ZOctree<T>::GetMortonNumber(const ZAABB& aabb)const
{
	// 最小レベルにおける各軸位置を算出
	uint LT = GetPointElem(aabb.Min);
	uint RB = GetPointElem(aabb.Max);

	// 空間番号を引き算し、
	// 最上位区切りから所属レベルを算出
	uint def = RB ^ LT;
	uint hiLevel = 1;
	for (uint i = 0; i < m_LowestLevel; i++)
	{
		uint chack = (def >> (i * 3)) & 0x7;
		if (chack != 0)
			hiLevel = i + 1;
	}

	uint spaceNum = RB >> (hiLevel * 3);
	uint addNum = (m_NumOfSpaceByLevel[m_LowestLevel - hiLevel] - 1) / 7;
	spaceNum += addNum;

	if (spaceNum > m_NumSpace)
		return 0xFFFFFFFF;

	return spaceNum;
}

template<typename T>
inline uint ZOctree<T>::Get3DMortonNumber(uint8 x, uint8 y, uint8 z)const
{
	return BitSeparate(x) | BitSeparate(y) << 1 | BitSeparate(z) << 2;
}

template<typename T>
inline uint ZOctree<T>::BitSeparate(uint8 n)const
{
	uint s = n;
	s = (s | s << 8) & 0x0000F00F;
	s = (s | s << 4) & 0x000C30C3;
	s = (s | s << 2) & 0x00249249;
	return s;
}

template<typename T>
inline uint ZOctree<T>::GetPointElem(const ZVec3& p)const
{
	uint elem = Get3DMortonNumber
	(
		(uint8)((p.x - m_RgnMin.x) / m_Unit.x),
		(uint8)((p.y - m_RgnMin.y) / m_Unit.y),
		(uint8)((p.z - m_RgnMin.z) / m_Unit.z)
	);

	return elem;
}

template<typename T>
inline ZVec3 ZOctree<T>::GetGridCoordinate(const ZVec3& pos)const
{
	ZVec3 p = pos;
	p.x = max(m_RgnMin.x, min(pos.x, m_RgnMax.x));
	p.y = max(m_RgnMin.y, min(pos.y, m_RgnMax.y));
	p.z = max(m_RgnMin.z, min(pos.z, m_RgnMax.z));

	uint x = (uint)((p.x - m_RgnMin.x) / m_Unit.x);
	uint y = (uint)((p.y - m_RgnMin.y) / m_Unit.y);
	uint z = (uint)((p.z - m_RgnMin.z) / m_Unit.z);

	return ZVec3(x,y,z);
}

template<typename T>
inline ZVec3 ZOctree<T>::GetGridCoordinate(const ZVec3& pos, uint level)const
{
	ZVec3 p = pos;
	p.x = max(m_RgnMin.x, min(pos.x, m_RgnMax.x));
	p.y = max(m_RgnMin.y, min(pos.y, m_RgnMax.y));
	p.z = max(m_RgnMin.z, min(pos.z, m_RgnMax.z));

	ZVec3 unit = m_RgnWidth / (float)(1 << level);

	uint x = (uint)((p.x - m_RgnMin.x) / unit.x);
	uint y = (uint)((p.y - m_RgnMin.y) / unit.y);
	uint z = (uint)((p.z - m_RgnMin.z) / unit.z);

	return ZVec3(x, y, z);
}

template<typename T>
inline ZVec3 ZOctree<T>::GetGridPosition(uint number)const
{
	int level = 0;
	while (number >= m_NumOfSpaceByLevel[level])
	{
		number -= m_NumOfSpaceByLevel[level];
		level++;
	}

	uint s = 0;
	for (int i = level; i > 0; i--)
		s = s | (number >> (3 * i - 2 - i) & (1 << i - 1));
	uint x = s;
	s = 0;
	for (int i = level; i > 0; i--)
		s = s | (number >> (3 * i - 1 - i) & (1 << i - 1));
	uint y = s;
	s = 0;
	for (int i = level; i > 0; i--)
		s = s | (number >> (3 * i - i) & (1 << i - 1));
	uint z = s;

	// 所属する空間レベルの分割サイズを算出
	ZVec3 boxSize = m_RgnWidth / (float)m_NumOfSpaceByLevel[level];
	
	return ZVec3(x * boxSize.x.y * boxSize.y, z * boxSize.z) + m_RgnMin;
}

template<typename T>
inline ZVec3 ZOctree<T>::CalculateGridPosition(const ZVec3& pos)const
{
	// モートン番号算出
	uint motonNum = GetPointElem(pos);

	return GetGridPosition(motonNum);
}

template<typename T>
inline ZOctreeSpace<T>::ZOctreeSpace()
	: m_pLatest(nullptr)
{
}

template<typename T>
inline ZOctreeSpace<T>::~ZOctreeSpace()
{
	if (m_pLatest.GetPtr() != nullptr)
		ResetLink(m_pLatest);
}

template<typename T>
inline void ZOctreeSpace<T>::ResetLink(ZSP<ZObjectForTree<T>>& pOFT)
{
	if (pOFT->m_pNext != nullptr)
		ResetLink(pOFT->m_pNext);
	pOFT = nullptr;
}

template<typename T>
inline bool ZOctreeSpace<T>::Push(ZSP<ZObjectForTree<T>>& pOFT)
{
	if (pOFT == nullptr)
		return false;
	if (pOFT->m_pSpace == this)
		return false;

	if (m_pLatest == nullptr)
		m_pLatest = pOFT;
	else
	{
		// 最新OFTオブジェクト更新
		pOFT->m_pNext = m_pLatest;
		m_pLatest->m_pPrev = pOFT;
		m_pLatest = pOFT;
	}

	pOFT->RegisterSpace(this);
	return true;
}

template<typename T>
inline ZSP<ZObjectForTree<T>>& ZOctreeSpace<T>::GetFirstObj()
{
	return m_pLatest;
}

template<typename T>
inline void ZOctreeSpace<T>::OnRemove(ZObjectForTree<T>*& pRemoveObj)
{
	if (m_pLatest != pRemoveObj)
		return;
	if (m_pLatest != nullptr)
		m_pLatest = m_pLatest->GetNextObj();
}
