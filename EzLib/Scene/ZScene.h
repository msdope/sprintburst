#ifndef __SCENE_H__
#define __SCENE_H__

// シーン基本クラス
class ZSceneBase
{
public:
	ZSceneBase()
	{
	}
	virtual ~ZSceneBase()
	{
	}

	virtual void Init() = 0;
	virtual void Update() = 0;
	virtual void ImGuiUpdate() = 0;
	virtual void Draw() = 0;

public:
	// 更新システム
	ECSSystemList m_UpdateSystems;
	// 描画システム
	ECSSystemList m_DrawSystems;
};

// シーンファクトリ
class ZSceneManager
{
public:
	// 初期化
	void Init();

	// 更新
	void Update();

	// ImGui更新
	void ImGuiUpdate();

	// 描画
	void Draw();

	// 解放
	void Release();

	template<typename T>
	void SubmitSceneGenerater(const ZString& sceneName)
	{
		m_SceneGenerateMap[sceneName] =
			[]()
		{
			return Make_Shared(T, sysnew);
		};
	}

	// シーンを変更
	//  sceneName	... あらかじめInit()で登録しておいたシーン名
	//  ※即座には変更されず次のUpdateの前に変更が実行される
	void ChangeScene(const ZString& sceneName);

	// 現在のシーンを取得
	ZSP<ZSceneBase> GetNowScene();

private:
	// 現在のシーン
	ZSP<ZSceneBase> m_NowScene;

	// シーン生成マップ
	ZSMap<ZString, std::function<ZSP<ZSceneBase>()>>	m_SceneGenerateMap;

	ZString m_NextChangeSceneName;

};


#endif