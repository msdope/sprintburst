#include "PCH/pch.h"

void ZSceneManager::Init()
{
	m_NowScene = nullptr;
	// 初期シーン作成
	if (m_NextChangeSceneName.empty() == false)
	{
		m_NowScene = m_SceneGenerateMap[m_NextChangeSceneName]();
		m_NextChangeSceneName = "";
	}

	// 初期シーン初期化
	if(m_NowScene)
		m_NowScene->Init();
}

void ZSceneManager::Update()
{
	if (m_NextChangeSceneName.empty() == false)
	{
		m_NowScene = nullptr;

		// 存在するシーン名か
		if (m_SceneGenerateMap.end() == m_SceneGenerateMap.find(m_NextChangeSceneName))
			DW_SCROLL(0, "存在シーンです : %s\n", m_NextChangeSceneName.c_str());
		else
		{
			// 存在するならシーン作成
			m_NowScene = m_SceneGenerateMap[m_NextChangeSceneName]();
			// 初期化
			m_NowScene->Init();
		}

		// シーン切り替え用文字列クリア
		m_NextChangeSceneName = "";
	}

	if (m_NowScene)
		m_NowScene->Update();
}

void ZSceneManager::ImGuiUpdate()
{
	if (m_NowScene)
		m_NowScene->ImGuiUpdate();
}

void ZSceneManager::Draw()
{
	if (m_NowScene)
		m_NowScene->Draw();
}

void ZSceneManager::Release()
{
	m_NowScene = nullptr;
	m_NextChangeSceneName = "";
	m_SceneGenerateMap.clear();
}

void ZSceneManager::ChangeScene(const ZString& sceneName)
{
	m_NextChangeSceneName = sceneName;
}

ZSP<ZSceneBase> ZSceneManager::GetNowScene()
{
	return m_NowScene;
}
