#include "PCH/pch.h"

using namespace EzLib;


const ZVec2 ZVec2::zero(0, 0);	// 全要素0ベクトル
const ZVec2 ZVec2::one(1, 1);		// 全要素1ベクトル

void ZVec2::Homing(const ZVec2& vTargetDir, float MaxAng)
{
	// 方向
	ZVec3 vWay(x, y, 0);
	vWay.Normalize();
	// 敵への方向
	ZVec3 vTar(vTargetDir.x, vTargetDir.y, 0);
	vTar.Normalize();


	// 内積で角度を求める
	float dot = ZVec3::DotClamp(vWay, vTar);
	if (dot < 1.0f)
	{
		float Deg;
		ZVec3 crs;
		// 正反対のときは適当に曲げる
		if (dot == -1.0f)
		{
			crs.Set(0, 0, 1);
			Deg = MaxAng;
		}
		else
		{
			// 角度制限
			Deg = DirectX::XMConvertToDegrees(acos(dot));
			if (Deg > MaxAng)
				Deg = MaxAng;

			// 外積
			ZVec3::Cross(crs, vWay, vTar);
			// 正規化
			crs.Normalize();
			if (crs.Length() == 0)
			{
				crs.z = 1;
			}
		}

		// 回転行列作成
		ZMatrix mRota;
		mRota.CreateRotateAxis(crs, Deg);

		TransformNormal(mRota);

	}
}

void ZVec2::CreateAABB(const ZVec2& v1, const ZVec2& v2, const ZVec2& v3, ZVec2& outMinP, ZVec2& outMaxP)
{
	outMinP = v1;
	outMaxP = v1;
	// 最小点算出
	if (outMinP.x > v2.x)outMinP.x = v2.x;
	if (outMinP.y > v2.y)outMinP.y = v2.y;
	if (outMinP.x > v3.x)outMinP.x = v3.x;
	if (outMinP.y > v3.y)outMinP.y = v3.y;
	// 最大点算出
	if (outMaxP.x < v2.x)outMaxP.x = v2.x;
	if (outMaxP.y < v2.y)outMaxP.y = v2.y;
	if (outMaxP.x < v3.x)outMaxP.x = v3.x;
	if (outMaxP.y < v3.y)outMaxP.y = v3.y;
}

void ZVec2::CreateAABBFromLineSegment(const ZVec2& v1, const ZVec2& v2, ZVec2& outMinP, ZVec2& outMaxP)
{

	// 線分のバウンディングボックス算出
	if (v1.x < v2.x)
	{
		outMinP.x = v1.x;
		outMaxP.x = v2.x;
	}
	else
	{
		outMinP.x = v2.x;
		outMaxP.x = v1.x;
	}

	if (v1.y < v2.y)
	{
		outMinP.y = v1.y;
		outMaxP.y = v2.y;
	}
	else
	{
		outMinP.y = v2.y;
		outMaxP.y = v1.y;
	}

}
