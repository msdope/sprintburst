#include "PCH/pch.h"

using namespace EzLib;

const ZVec3 ZVec3::Zero(0, 0, 0);
const ZVec3 ZVec3::One(1, 1, 1);
const ZVec3 ZVec3::Up(0, 1, 0);
const ZVec3 ZVec3::Down(0, -1, 0);
const ZVec3 ZVec3::Left(-1, 0, 0);
const ZVec3 ZVec3::Right(1, 0, 0);
const ZVec3 ZVec3::Front(0, 0, 1);
const ZVec3 ZVec3::Back(0, 0, -1);

void ZVec3::Homing(const ZVec3& vTargetDir, float MaxAng)
{

	// 方向
	ZVec3 vWay(x, y, z);
	vWay.Normalize();
	// 敵への方向
	ZVec3 vTar = vTargetDir;
	vTar.Normalize();

	// 内積で角度を求める
	float dot = ZVec3::DotClamp(vWay, vTar);
	if (dot < 1.0f)
	{
		float Deg;
		ZVec3 crs;
		// 正反対のときは適当に曲げる
		if (dot == -1.0f)
		{
			crs.Set(0, 1, 0);
			Deg = MaxAng;
		}
		else
		{
			// 角度制限
			Deg = DirectX::XMConvertToDegrees(acos(dot));
			if (Deg > MaxAng)
				Deg = MaxAng;

			// 外積
			ZVec3::Cross(crs, vWay, vTar);
			// 正規化
			crs.Normalize();
			if (crs.Length() == 0)
			{
				crs.y = 1;
			}
		}

		// 回転行列作成
		ZMatrix mRota;
		mRota.CreateRotateAxis(crs, Deg);

		TransformNormal(mRota);

	}
}
