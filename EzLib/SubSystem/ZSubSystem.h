#ifndef __ZSUB_SYSTEM_H__
#define __ZSUB_SYSTEM_H__

#include "TypeIndex.h"

namespace EzLib
{
	// サブシステム取得
	template<typename T>
	T& GetSubSystem();
	
	// サブシステム追加
	template<typename T,typename... Args>
	T& AddSubSystem(Args&&... args);

	// サブシステム削除
	template<typename T>
	void RemoveSubSystem();

	// サブシステム存在確認
	template<typename... Args>
	bool HasSubSystem();


	struct ZSubSystemContainer;
	namespace SubSystemDetails
	{
		enum class InternalStatus : uint8
		{
			IDLE,
			RUNNING,
			RELEASED
		};

		bool Init();

		InternalStatus& Status();

		void Release();

		ZSubSystemContainer& GetConteiner();
	}

	struct ZSubSystemContainer
	{

	public:
		bool Init();

		void Release();

		template<typename T,typename... Args>
		T& AddSubSystem(Args&&... args);
		
		template<typename T>
		T& GetSubSystem();

		template<typename T>
		void RemoveSubSystem();

		template<typename T>
		bool HasSubSystem()const;

		template<typename T,typename U,typename... Args>
		bool HasSubSystem()const;

	protected:
		ZVector<size_t> m_Orders;
		ZUnorderedMap<size_t, ZSP<void>> m_SubSystems;
	};

#include "ZSubSystem.inl"

}

#endif