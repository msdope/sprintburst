#ifndef __TYPE_INDEX_H__
#define __TYPE_INDEX_H__

namespace EzLib
{

#include <cstddef>
#include <type_traits>

#define __TRY_USING_RTTI__ 1

#ifndef __cpp_rtti
#define __cpp_rtti _CPPRTTI
#endif

#define __CPP_RTTI_ENABLED__ __TRY_USING_RTTI__ && __cpp_rtti
#if __CPP_RTTI_ENABLED__
#include <typeindex>
#endif

namespace rtti
{
	class TypeIndex_t;

	namespace impl
	{
		template<typename T>
		const TypeIndex_t& TypeID_Impl();
	}

	class TypeIndex_t
	{
	private:
	#if __CPP_RTTI_ENABLED__
		using construct_t = const std::type_info;
	#else
		using construct_t = const TypeIndex_t&();
	#endif

		template<typename T>
		friend const TypeIndex_t& impl::TypeID_Impl();

	public:
		size_t hash_code()const
		{
	#if __CPP_RTTI_ENABLED__
			return std::type_index(*m_Info).hash_code();
	#else
			return (size_t)m_Info;
	#endif
		}

		bool operator==(const TypeIndex_t& t)const
		{
			return hash_code() == t.hash_code();
		}

		bool operator!=(const TypeIndex_t& t)const
		{
			return hash_code() != t.hash_code();
		}

		bool operator<(const TypeIndex_t& t)const
		{
			return hash_code() < t.hash_code();
		}

		bool operator>(const TypeIndex_t& t)const
		{
			return hash_code() > t.hash_code();
		}

	private:
		explicit TypeIndex_t(construct_t* info)noexcept
			: m_Info(info)
		{
		}

	private:
		construct_t* m_Info = nullptr;
	
	};

	namespace impl
	{
		template<typename T>
		const TypeIndex_t& TypeID_Impl()
		{
	#if __CPP_RTTI_ENABLED__
			static TypeIndex_t id(&typeid(T));
	#else
			static TypeIndex_t id(&TypeID_Impl<T>);
	#endif
			return id;
		}
	}

	template<typename T>
	const TypeIndex_t& TypeID()
	{
		return impl::TypeID_Impl<typename std::remove_cv<T>::type>();
	}

}
}
#endif