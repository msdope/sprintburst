#include "PCH/pch.h"

using namespace EzLib;

// アニメーション進行
void ZUVAnimator::Animation(float fSpeed, bool bLoop)
{
	// アニメ進行
	m_fAnimePos += fSpeed;
	if (m_fAnimePos >= m_AnimeMaxNum)
	{
		if (bLoop)
		{
			m_fAnimePos = m_fAnimePos - m_AnimeMaxNum* ((int)(m_fAnimePos / m_AnimeMaxNum));
		}
		else
		{
			m_fAnimePos = (float)(m_AnimeMaxNum - 1);
		}
	}
}
