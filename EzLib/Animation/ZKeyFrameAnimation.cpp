#include "PCH/pch.h"

namespace EzLib
{
	// ベジェ曲線の指定Xの位置からt(0〜1)を求め、それを使いYを求める
	static float CalcBezierY_FromX(float x, float x1, float y1, float x2, float y2)
	{
		float t = 0.5f, s = 0.5f;
		for (int i = 0; i < 15; i++)
		{
			float ft = (3 * s * s * t * x1) + (3 * s * t * t * x2) + (t * t * t) - x;
			if (ft == 0) break;

			if (ft > 0) t -= 1.0f / float(4 << i);
			else		t += 1.0f / float(4 << i);	// ft < 0

			s = 1 - t;
		}
		return (3 * s * s * t * y1) + (3 * s * t * t * y2) + (t * t * t);

	}

	bool ZAnimeKey_Quaternion::InterpolationFromTime(const ZAVector<ZAnimeKey_Quaternion>& keyList, double time, ZQuat &outQ, UINT* nowKeyIndex)
	{
		if (keyList.empty())
			return false;

		if (keyList.size() == 1)
		{	// キーが１つのとき
			// その内容を使用
			outQ = keyList.front().Value;
			return true;
		}

		// キーが複数
		// 最初のキーより手前なら、先頭を返す
		if (time < keyList[0].Time)
		{
			outQ = keyList.front().Value;
			return true;
		}

		// 最後のキーを過ぎてるとき
		if (time >= keyList.back().Time)
		{
			// 最後のデータを返す
			outQ = keyList.back().Value;
			return true;
		}

		// 位置を検索
		UINT i = 0;
		if (nowKeyIndex == nullptr)
		{
			for (i = 0; i < keyList.size(); i++)
			{
				// 自分より上のキーを発見
				if (time < keyList[i].Time) break;
			}
		}
		else
		{
			// 前回のIndex
			UINT idx = *nowKeyIndex;
			if (idx >= keyList.size())
				idx = keyList.size() - 1;

			i = idx;
			int searchEndIndex = (int)idx - 1;
			if (searchEndIndex < 0)
				searchEndIndex = keyList.size() - 2;

			while (true)
			{
				if ((int)i >= (int)keyList.size() - 1)
					i = 0;		// 最後の1つ前まで行けば、最初に戻す
				
				if (i == searchEndIndex) break;

				// 範囲内？
				if (time >= keyList[i].Time && time < keyList[i + 1].Time)
					break;
				i++;
			}

			*nowKeyIndex = (unsigned int)i;
			i++;
		}

		// 不正
		if (i >= keyList.size())
			return false;

		// 計算
		double f = time - keyList[i - 1].Time;
		
		// ちょうどキーフレームがある(補完なし)
		if (f == 0.0)
		{
			outQ = keyList[i - 1].Value;
			return true;
		}

		// 比率算出
		float diff = (keyList[i].Time - keyList[i - 1].Time);

		float fT = (float)(f * (1.0f / diff));
		switch (keyList[i - 1].CalcType)
		{
			case 0: // 球面線形補間
			{
				ZQuat::Slerp
				(
					outQ,
					keyList[i - 1].Value,
					keyList[i].Value,
					fT
				);
			}
			break;

			case 1: // 球面2次補間
			{
				int idx[4];
				idx[0] = i - 2;
				idx[1] = i - 1;
				idx[2] = i;
				idx[3] = i + 1;
				if (idx[0] < 0)idx[0] = 0;
				
				if (idx[3] >= (int)keyList.size())
					idx[3] = i;

				// 制御ポイント算出(処理効率を考えるなら、これは事前計算してるほうが良い)
				ZQuat qABC[3];
				ZQuat::SquadSetup
				(
					qABC[0],
					qABC[1],
					qABC[2],
					keyList[idx[0]].Value,
					keyList[idx[1]].Value,
					keyList[idx[2]].Value,
					keyList[idx[3]].Value
				);

				// 補間
				ZQuat::Squad
				(
					outQ,
					keyList[i - 1].Value,
					qABC[0],
					qABC[1],
					qABC[2],
					fT
				);
			}
			break;

			case 2: // ベジェ曲線補間(かなり重いから、処理を軽くしたい場合は事前に焼き込み線形補間にするのが良い(BakeCurve関数))
			{
				auto& bezierA = keyList[i - 1].BezierA;
				auto& bezierB = keyList[i - 1].BezierB;
				float by = CalcBezierY_FromX
				(
					fT,
					bezierA[0] / 127.0f,
					bezierA[1] / 127.0f,
					bezierB[0] / 127.0f,
					bezierB[1] / 127.0f
				);

				// 補間
				ZQuat::Slerp
				(
					outQ,
					keyList[i - 1].Value,
					keyList[i].Value,
					by
				);
			}
			break;
		}

		return true;
	}

	void ZAnimeKey_Quaternion::BakeCurve(ZAVector<ZAnimeKey_Quaternion>& keyList, int bakeSpace)
	{
		// 一度コピー
		auto copyRefRotateList = keyList;
		int keyNum = (int)keyList.size();
		for (int h = 0; h < keyNum; h++)
		{
			// 補間タイプが曲線系の場合
			if (copyRefRotateList[h].CalcType == 0)
				continue;
			
			// 最後のキーなら焼き込みなし
			if (h >= (int)copyRefRotateList.size() - 1)
				goto ToLiner;

			// 総数(うち２つが、hとh+1番目のキー)
			int cnt = (int)copyRefRotateList[h + 1].Time - (int)copyRefRotateList[h].Time + 1;

			int bakeInterval = bakeSpace;
			// 最初(h)と後部(h+1)キー以外
			for (int bki = 1; bki < cnt - 1; bki++)
			{
				if (bakeInterval > 0)
				{
					bakeInterval--;
					continue;
				}

				bakeInterval = bakeSpace;

				int time = (int)copyRefRotateList[h].Time + bki;
				float sa = (copyRefRotateList[h + 1].Time - copyRefRotateList[h].Time);
				double f = time - copyRefRotateList[h].Time;
				float fT = (float)(f* (1.0f / sa));

				ZQuat outQ;
				// 補間計算
				UINT idx = h;
				ZAnimeKey_Quaternion::InterpolationFromTime(copyRefRotateList, time, outQ, &idx);

				// 結果をキーとして追加
				ZAnimeKey_Quaternion add2;// = new ZAnimeKey_Quaternion();
				add2.Time = (float)time;
				add2.Value = outQ;
				add2.CalcType = 0;
				keyList.push_back(add2);
			}

		ToLiner:
			// このキーは曲線をやめて線形に戻す
			copyRefRotateList[h].CalcType = 0;
			keyList[h].CalcType = 0;
		}

		// ソート
		std::sort(keyList.begin(), keyList.end(),
			[](const ZAnimeKey_Quaternion& v1, const ZAnimeKey_Quaternion& v2)
		{
			return v1.Time < v2.Time;
		});
	}

	bool ZAnimeKey_Vector3::InterpolationFromTime(const ZAVector<ZAnimeKey_Vector3>& keyList, double time, ZVec3 &outV, UINT* nowKeyIndex)
	{
		// キーが１つのとき
		if (keyList.empty())
			return false;

		if (keyList.size() == 1)
		{
			// その内容を使用
			outV.Set(keyList.front().Value);
			return true;
		}
		
		// キーが複数
		// 最初のキーより手前なら、先頭を返す
		if (time < keyList[0].Time)
		{
			outV.Set(keyList.front().Value);
			return true;
		}

		// 最後のキーを過ぎてるとき
		if (time >= keyList.back().Time)
		{
			// 最後のデータを返す
			outV.Set(keyList.back().Value);
			return true;
		}

		// 位置を検索
		UINT i = 0;
		if (nowKeyIndex == nullptr)
		{
			for (i = 0; i < keyList.size(); i++)
			{
				// 自分より上のキーを発見
				if (time < keyList[i].Time)
					break;
			}
		}
		else
		{
			// 前回のIndex
			UINT idx = *nowKeyIndex;
			if (idx >= keyList.size())
				idx = keyList.size() - 1;

			i = idx;
			int searchEndIndex = (int)idx - 1;
			if (searchEndIndex < 0)
				searchEndIndex = keyList.size() - 2;

			while (true)
			{
				if ((int)i >= (int)
					keyList.size() - 1)i = 0;		// 最後の1つ前まで行けば、最初に戻す
				if (i == searchEndIndex) break;

				// 範囲内？
				if (time >= keyList[i].Time && time < keyList[i + 1].Time)
					break;
				i++;
			}

			*nowKeyIndex = (unsigned int)i;
			i++;
		}

		// 不正
		if (i >= keyList.size())
			return false;

		// 比率算出
		float diff = (keyList[i].Time - keyList[i - 1].Time);
		double f = time - keyList[i - 1].Time;
		if (f == 0)
		{
			outV.Set(keyList[i - 1].Value);
			return true;
		}

		float fT = (float)(f* (1.0f / diff));
		switch (keyList[i-1].CalcType)
		{
			case 0: // 線形補間
			{
				ZVec3::Lerp
				(
					outV,
					keyList[i - 1].Value,
					keyList[i].Value,
					(float)(f* (1.0f / diff))
				);
			}
			break;

			case 1: // Catmull-Romスプライン補間(それなりに重いから、処理を軽くしたい場合は事前に焼き込み線形補間にするのが良い(BakeCurve関数))
			{
				int idx[4];
				idx[0] = i - 2;
				idx[1] = i - 1;
				idx[2] = i;
				idx[3] = i + 1;
				if (idx[0] < 0)idx[0] = 0;
				if (idx[3] >= (int)keyList.size())idx[3] = i;

				ZVec3::CatmullRom
				(
					outV,
					keyList[idx[0]].Value,
					keyList[idx[1]].Value,
					keyList[idx[2]].Value,
					keyList[idx[3]].Value,
					fT
				);
			}
			break;

			case 2: // ペジェ曲線補間
			{
				auto& bezierX_A = keyList[i - 1].BezierX_A;
				auto& bezierX_B = keyList[i - 1].BezierX_B;
				auto& bezierY_A = keyList[i - 1].BezierY_A;
				auto& bezierY_B = keyList[i - 1].BezierY_B;
				auto& bezierZ_A = keyList[i - 1].BezierZ_A;
				auto& bezierZ_B = keyList[i - 1].BezierZ_B;

				// X
				float by = CalcBezierY_FromX
				(
					fT,
					bezierX_A[0] / 127.0f,
					bezierX_A[1] / 127.0f,
					bezierX_B[0] / 127.0f,
					bezierX_B[1] / 127.0f
				);

				outV.x = keyList[i - 1].Value.x + (keyList[i].Value.x - keyList[i - 1].Value.x)* by;

				// Y
				by = CalcBezierY_FromX
				(
					fT,
					bezierY_A[0] / 127.0f,
					bezierY_A[1] / 127.0f,
					bezierY_B[0] / 127.0f,
					bezierY_B[1] / 127.0f
				);

				outV.y = keyList[i - 1].Value.y + (keyList[i].Value.y - keyList[i - 1].Value.y)* by;

				// Z
				by = CalcBezierY_FromX
				(
					fT,
					bezierZ_A[0] / 127.0f,
					bezierZ_A[1] / 127.0f,
					bezierZ_B[0] / 127.0f,
					bezierZ_B[1] / 127.0f
				);

				outV.z = keyList[i - 1].Value.z + (keyList[i].Value.z - keyList[i - 1].Value.z)* by;
			}
			break;

			default:
			break;
		}

		return true;
	}

	void ZAnimeKey_Vector3::BakeCurve(ZAVector<ZAnimeKey_Vector3>& keyList, int bakeSpace)
	{
		// 一度コピー
		auto copyRefPosList = keyList;
		int keyNum = (int)keyList.size();
		for (int h = 0; h < keyNum; h++)
		{
			// 補間タイプが曲線系の場合
			if (copyRefPosList[h].CalcType == 0)
				continue;
			
			// 最後のキーなら焼き込みなし
			if (h >= (int)copyRefPosList.size() - 1)
				goto ToLiner;
			
			// 総数(うち２つが、hとh+1番目のキー)
			int cnt = (int)copyRefPosList[h + 1].Time - (int)copyRefPosList[h].Time + 1;

			int bakeInterval = bakeSpace;
			// 最初(h)と後部(h+1)キー以外
			for (int bki = 1; bki < cnt - 1; bki++)
			{
				if (bakeInterval > 0)
				{
					bakeInterval--;
					continue;
				}

				bakeInterval = bakeSpace;

				int time = (int)copyRefPosList[h].Time + bki;
				float sa = (copyRefPosList[h + 1].Time - copyRefPosList[h].Time);
				double f = time - copyRefPosList[h].Time;
				float fT = (float)(f* (1.0f / sa));

				ZVec3 outV;
				// 補間計算
				UINT idx = h;
				ZAnimeKey_Vector3::InterpolationFromTime(copyRefPosList, time, outV, &idx);

				// 結果をキーとして追加
				ZAnimeKey_Vector3 add2;
				add2.Time = (float)time;
				add2.Value = outV;
				add2.CalcType = 0;
				keyList.push_back(add2);
			}

		ToLiner:
			// このキーは曲線をやめて線形にする
			copyRefPosList[h].CalcType = 0;
			keyList[h].CalcType = 0;
		}

		// ソート
		std::sort(keyList.begin(), keyList.end(),
			[](const ZAnimeKey_Vector3& v1, const ZAnimeKey_Vector3& v2)
		{
			return v1.Time < v2.Time;
		});
	}

	//=============================================================================
	//
	// ZKeyframeAnime
	//
	//=============================================================================

	void ZKeyframeAnime::Release()
	{
		m_Rotate.clear();
		m_Scale.clear();
		m_Pos.clear();
	}

	//=============================================================================
	//
	// ZAnimationSet
	//
	//=============================================================================
	void ZAnimationSet::Release()
	{
		m_AnimeName = "";
		m_TicksPerSecond = 60;
		m_AnimeLen = 0;

		for (UINT i = 0; i < m_FrameAnimeList.size(); i++)
			SAFE_DELPTR(m_FrameAnimeList[i]);
		
		m_FrameAnimeList.clear();
		m_Script.clear();
	}

	void ZAnimationSet::CreateFrameAnimeList(UINT size)
	{
		for (UINT i = 0; i < m_FrameAnimeList.size(); i++)
			Safe_Delete(m_FrameAnimeList[i]);
		
		m_FrameAnimeList.resize(size);

		for (UINT i = 0; i < m_FrameAnimeList.size(); i++)
			m_FrameAnimeList[i] = appnew(ZKeyframeAnime);
	}

	bool ZAnimationSet::LoadXMD(const ZString& filename, const char* registerAnimeName, bool bakeCurve)
	{
		// 
		FILE* fp;
		fopen_s(&fp, filename.c_str(), "rb");
		if (!fp)return FALSE;
		char strHead[256];

		// 汎用
#pragma region 便利関数
		auto RD = [fp](void* p, int size) { fread(p, size, 1, fp); };
		auto RDchar = [fp](char* c) { fread(c, 1, 1, fp); };
		auto RDbyte = [fp](BYTE* c) { fread(c, 1, 1, fp); };
		auto RDword = [fp](WORD* w) { fread(w, 2, 1, fp); };
		auto RDint = [fp](int* n) { fread(n, 4, 1, fp); };
		auto RDfloat = [fp](float* f) { fread(f, 4, 1, fp); };
		auto RDfloat3 = [fp](ZVec3* v) { fread(v, 12, 1, fp); };
		auto RDdouble = [fp](double* d) { fread(d, 8, 1, fp); };
		// サイズ(int)＋文字列
		auto RDstr = [fp](ZString& str)
		{
			int n;
			fread(&n, 4, 1, fp);	// size
			str.resize(n);
			//		str[n] = '\0';
			if (n <= 0) return;

			fread(&str[0], n, 1, fp);
		};

		BYTE lastReadSize = 0;
		auto RHead = [fp, &strHead, &lastReadSize]() mutable
		{
			while (true)
			{
				BYTE len;
				while (true)
				{
					fread(&len, 1, 1, fp);		// 文字数
												// 開始文字
					BYTE st = 0;
					fread(&st, 1, 1, fp);
					// ヘッダ確認　続行
					if (st == 0x8C)
						break;

					// 不正なヘッダ。巻き戻し、ヘッダまで進める
					fseek(fp, -1, SEEK_CUR);
				}
				// 文字列
				fread(strHead, len, 1, fp);
				strHead[len] = '\0';
				// 終端文字
				BYTE term[2];
				fread(term, 2, 1, fp);

				// 終端確認
				if (term[0] == 0x60 && term[1] == 0x99) break;
				
				// 終端文字ではない。最初(文字数取得後)まで巻き戻す
				fseek(fp, -(1 + len + 2), SEEK_CUR);
			}
		};
#pragma endregion

		//===========================================
		// Main Header
		//===========================================
		char head[4];
		RD(head, 3);
		head[3] = '\0';

		if (strcmp(head, "XMD") != 0)
		{
			fclose(fp);
			return false;
		}

		while (true)
		{
			// 項目読み込み
			RHead();
			// 終了
			if (strcmp(strHead, "EndXMD") == 0) break;

			//===========================================
			// アニメデータ
			//===========================================
			if (strcmp(strHead, "AnimeName") == 0)
			{
#pragma region アニメ
				// アニメ追加
				//			ZSP<XEDAnimation> anime(new XEDAnimation());
				//			m_AnimeList.push_back(anime);
				ZAnimationSet* anime = this;

				// ★アニメ名
				RDstr(anime->m_AnimeName);
				// アニメ名指定がある場合は、差し替え
				if (registerAnimeName)
				{
					anime->m_AnimeName = registerAnimeName;
				}

				// ★アニメ長
				int animeLen;
				RDint(&animeLen);
				anime->m_TicksPerSecond = 60;
				anime->m_AnimeLen = animeLen;

				while (1)
				{
					// 項目読み込み
					RHead();
					// 終了
					if (strcmp(strHead, "End") == 0)
					{
						break;
					}
					else if (strcmp(strHead, "BlendAnimeMode") == 0)
					{
						int nn;
						RDint(&nn);
						//					anime->m_bBlendAnimeMode = (nn == 1 ? true : false);
					}
					else if (strcmp(strHead, "BlendAnimeWeight") == 0)
					{
						//					RDfloat(&anime->m_fBlendAnimeWeight);
					}
					// スクリプトキー
					else if (strcmp(strHead, "ScriptKey") == 0)
					{
						// キー数
						int KeyNum;
						RDint(&KeyNum);
						// データ
						for (int i = 0; i < KeyNum; i++)
						{
							// キー追加
							ZAnimeKey_Script lpScr;

							while (1)
							{
								RHead();
								if (strcmp(strHead, "End") == 0)
								{
									anime->m_Script.push_back(lpScr);
									break;
								}
								else if (strcmp(strHead, "Time") == 0)
								{
									int n;
									RDint(&n);
									lpScr.Time = (float)n;
								}
								else if (strcmp(strHead, "ScriptText") == 0)
								{
									RDstr(lpScr.Value);
								}
							}
						}
					}
					// TicksPerSecound
					else if (strcmp(strHead, "TicksPerSecond") == 0)
					{
						RDdouble(&anime->m_TicksPerSecond);
					}
					// ボーンデータ
					else if (strcmp(strHead, "BoneData") == 0)
					{
						//					std::string sBoneName = "";
						//					int boneIdx = -1;

						ZKeyframeAnime* aniSet;
						// �A
						while (1)
						{
							RHead();
							if (strcmp(strHead, "End") == 0)
							{
								break;
							}
							// ボーン名
							else if (strcmp(strHead, "BoneName") == 0)
							{
								// 追加
								aniSet = appnew(ZKeyframeAnime);
								anime->m_FrameAnimeList.push_back(aniSet);

								// 
								ZString boneName;
								RDstr(boneName);
							}
							// 回転キー
							else if (strcmp(strHead, "RotateKey") == 0)
							{
								// キー数
								int keyNum;
								RDint(&keyNum);
								for (int h = 0; h < keyNum; h++)
								{
									// キー追加
									ZAnimeKey_Quaternion key;// = new ZAnimeKey_Quaternion();
									while (1)
									{
										RHead();
										if (strcmp(strHead, "End") == 0)
										{
											aniSet->m_Rotate.push_back(key);
											break;
										}
										else if (strcmp(strHead, "Time") == 0)
										{
											int n;
											RDint(&n);
											key.Time = (float)n;
										}
										else if (strcmp(strHead, "Rota") == 0)
										{
											RD(&key.Value, 4 * 4);
										}
										else if (strcmp(strHead, "CalcType") == 0)
										{
											RDchar(&key.CalcType);
										}
										else if (strcmp(strHead, "PowVal") == 0)
										{	// ※未使用
											float ff;
											RDfloat(&ff);
										}
										else if (strcmp(strHead, "SplineValBefore") == 0)
										{	// ※未使用
											float ff;
											RDfloat(&ff);
										}
										else if (strcmp(strHead, "SplineValAfter") == 0)
										{	// ※未使用
											float ff;
											RDfloat(&ff);
										}
										else if (strcmp(strHead, "Bezier") == 0)
										{
											RDchar(&key.BezierA[0]);
											RDchar(&key.BezierA[1]);
											RDchar(&key.BezierB[0]);
											RDchar(&key.BezierB[1]);
										}

									}
								}

								// 曲線系の補間は処理が重いので、曲線になるような線形補間として焼き込む
								if (bakeCurve)
								{
									ZAnimeKey_Quaternion::BakeCurve(aniSet->m_Rotate, 0);
								}
							}
							// 拡大キー
							else if (strcmp(strHead, "ScaleKey") == 0)
							{
								// キー数
								int keyNum;
								RDint(&keyNum);
								for (int h = 0; h < keyNum; h++)
								{
									// キー追加
									ZAnimeKey_Vector3 key;
									while (true)
									{
										RHead();
										if (strcmp(strHead, "End") == 0)
										{
											aniSet->m_Scale.push_back(key);
											break;
										}
										else if (strcmp(strHead, "Time") == 0)
										{
											int n;
											RDint(&n);
											key.Time = (float)n;
										}
										else if (strcmp(strHead, "Scale") == 0)
										{
											RD(&key.Value, 4 * 3);
										}
										else if (strcmp(strHead, "CalcType") == 0)
										{
											RDchar(&key.CalcType);
										}
										else if (strcmp(strHead, "PowVal") == 0)
										{			// ※未使用
											float ff;
											RDfloat(&ff);
										}
										else if (strcmp(strHead, "SplineValBefore") == 0)
										{	// ※未使用
											float ff;
											RDfloat(&ff);
										}
										else if (strcmp(strHead, "SplineValAfter") == 0)
										{	// ※未使用
											float ff;
											RDfloat(&ff);
										}
										else if (strcmp(strHead, "Bezier") == 0)
										{
											RDchar(&key.BezierX_A[0]);
											RDchar(&key.BezierX_A[1]);
											RDchar(&key.BezierX_B[0]);
											RDchar(&key.BezierX_B[1]);
											RDchar(&key.BezierY_A[0]);
											RDchar(&key.BezierY_A[1]);
											RDchar(&key.BezierY_B[0]);
											RDchar(&key.BezierY_B[1]);
											RDchar(&key.BezierZ_A[0]);
											RDchar(&key.BezierZ_A[1]);
											RDchar(&key.BezierZ_B[0]);
											RDchar(&key.BezierZ_B[1]);
										}
									}
								}

								// 曲線系の補間は処理が重いので、曲線になるような線形補間として焼き込む
								if (bakeCurve)
								{
									ZAnimeKey_Vector3::BakeCurve(aniSet->m_Scale, 0);
								}
							}
							// 座標キー
							else if (strcmp(strHead, "PosKey") == 0)
							{
								// キー数
								int keyNum;
								RDint(&keyNum);
								for (int h = 0; h < keyNum; h++)
								{
									// キー追加
									ZAnimeKey_Vector3 key;
									while (1)
									{
										RHead();
										if (strcmp(strHead, "End") == 0)
										{
											aniSet->m_Pos.push_back(key);
											break;
										}
										else if (strcmp(strHead, "Time") == 0)
										{
											int n;
											RDint(&n);
											key.Time = (float)n;
										}
										else if (strcmp(strHead, "Pos") == 0)
										{
											RD(&key.Value, 4 * 3);
										}
										else if (strcmp(strHead, "CalcType") == 0)
										{
											RDchar(&key.CalcType);
										}
										else if (strcmp(strHead, "PowVal") == 0)
										{	// ※未使用
											float ff;
											RDfloat(&ff);
										}
										else if (strcmp(strHead, "SplineValBefore") == 0)
										{	// ※未使用
											float ff;
											RDfloat(&ff);
										}
										else if (strcmp(strHead, "SplineValAfter") == 0)
										{	// ※未使用
											float ff;
											RDfloat(&ff);
										}
										else if (strcmp(strHead, "Bezier") == 0)
										{
											RDchar(&key.BezierX_A[0]);
											RDchar(&key.BezierX_A[1]);
											RDchar(&key.BezierX_B[0]);
											RDchar(&key.BezierX_B[1]);
											RDchar(&key.BezierY_A[0]);
											RDchar(&key.BezierY_A[1]);
											RDchar(&key.BezierY_B[0]);
											RDchar(&key.BezierY_B[1]);
											RDchar(&key.BezierZ_A[0]);
											RDchar(&key.BezierZ_A[1]);
											RDchar(&key.BezierZ_B[0]);
											RDchar(&key.BezierZ_B[1]);
										}

									}
								}

								// 曲線系の補間は処理が重いので、曲線になるような線形補間として焼き込む
								if (bakeCurve)
								{
									ZAnimeKey_Vector3::BakeCurve(aniSet->m_Pos, 0);
								}
							}
						}
					}
				}
#pragma endregion
			}
		}

		// 
		fclose(fp);

		return true;
	}

	bool EzLib::ZAnimationSet::LoadVMD(const ZString& filename, ZBoneController& bc, const char* registerAnimeName, bool bakeCurve)
	{
		ZVMD vmd;
		if (ReadVMDFile(&vmd, filename.c_str()) == false)
			return false;

		ZAnimationSet* anime = this;
		if (registerAnimeName != nullptr)
			anime->m_AnimeName = registerAnimeName;

		ZString ext = ZPathUtil::GetExt(bc.GetGameModel()->GetFileName().c_str());

		// XED形式か
		bool isXEDModel = ext == "xed";

		anime->m_TicksPerSecond = 30;

		{
			uint32 endFrame = 0;
			for (auto& motion : vmd.Motions)
			{
				if (endFrame < motion.Frame)
					endFrame = motion.Frame;
			}

			anime->m_AnimeLen = endFrame;
		}

		// アニメーションで使われていないIKをOFFにするためにIKボーンの名前を保持
		ZAVector<ZString> ikBones;

		ZAUnorderedMap<ZString, size_t> boneIndexTbl;
		auto& boneTree = bc.GetBoneTree();
		for (size_t i = 0; i < boneTree.size(); i++)
		{
			ZString name = boneTree[i]->pSrcBoneNode->BoneName;
			if (boneTree[i]->pSrcBoneNode->BF_IsIK())
				ikBones.emplace_back(name);

			if (name.back() != '\0')
				name += '\0';

			boneIndexTbl.emplace(std::move(name), i);
		}

		// ボーン数分アニメーションデータ生成
		for (size_t i = 0; i < boneTree.size(); i++)
			m_FrameAnimeList.push_back(appnew(ZKeyframeAnime));

		ZAUnorderedMap<size_t, ZAVector<ZVMDMotion*>> frameMotions;
		// キーをボーンごとに振り分け
		for (auto& key : vmd.Motions)
		{
			ZString name = key.BoneName.toStr();
			if (name.back() != '\0')
				name += '\0';
			auto it = boneIndexTbl.find(name);
			if (it == boneIndexTbl.end())
				continue;
			size_t index = it->second;
			frameMotions[index].push_back(&key);
		}

		ZKeyframeAnime* aniSet;
		for (auto motions : frameMotions)
		{
			aniSet = m_FrameAnimeList[motions.first];

			for (auto motionKey : motions.second)
			{
				// 座標キー
				{
					ZAnimeKey_Vector3 key;
					key.CalcType = 2; // VMDの補完方法はベジェ曲線補完
					key.Time = (float)motionKey->Frame;
					key.Value = motionKey->Translate;

					// XED形式のモデルはPMX&PMD形式のモデルとモデルの向きが前後逆 &
					// モデルの大きさが1/10なのでモーションデータのx,z軸を反転 & 1/10に縮小加工する
					if (isXEDModel)
					{
						key.Value.x *= -0.1f;
						key.Value.y *= 0.1f;
						key.Value.z *= -0.1f;
					}

					key.Value += boneTree[motions.first]->TransMat.GetPos();

					key.BezierX_A[0] = motionKey->Interpolation[0];
					key.BezierY_A[0] = motionKey->Interpolation[1];
					key.BezierZ_A[0] = motionKey->Interpolation[2];
					key.BezierX_A[1] = motionKey->Interpolation[4];
					key.BezierY_A[1] = motionKey->Interpolation[5];
					key.BezierZ_A[1] = motionKey->Interpolation[6];
					key.BezierX_B[0] = motionKey->Interpolation[8];
					key.BezierY_B[0] = motionKey->Interpolation[9];
					key.BezierZ_B[0] = motionKey->Interpolation[10];
					key.BezierX_B[1] = motionKey->Interpolation[12];
					key.BezierY_B[1] = motionKey->Interpolation[13];
					key.BezierZ_B[1] = motionKey->Interpolation[14];
					aniSet->m_Pos.push_back(key);
				}

				// 回転キー
				{
					ZAnimeKey_Quaternion key;
					key.CalcType = 2; // VMDの補完方法はベジェ曲線補完
					key.Time = (float)motionKey->Frame;
					key.Value = motionKey->Quaternion;

					// XED形式のモデルはPMX&PMD形式のモデルとモデルの向きが前後逆なので
					// 回転方向もx,z軸が反対なため、反転させる
					if (isXEDModel)
					{
						// x,z軸反転
						key.Value.x *= -1;
						key.Value.z *= -1;
					}

					key.BezierA[0] = motionKey->Interpolation[3];
					key.BezierA[1] = motionKey->Interpolation[7];
					key.BezierB[0] = motionKey->Interpolation[11];
					key.BezierB[1] = motionKey->Interpolation[15];

					aniSet->m_Rotate.push_back(key);
				}

			}

			// ソート
			{
				std::sort(aniSet->m_Pos.begin(), aniSet->m_Pos.end(),
					[](const ZAnimeKey_Vector3& a, const ZAnimeKey_Vector3& b)
				{
					return a.Time < b.Time;
				});

				std::sort(aniSet->m_Rotate.begin(), aniSet->m_Rotate.end(),
					[](const ZAnimeKey_Quaternion& a, const ZAnimeKey_Quaternion& b)
				{
					return a.Time < b.Time;
				});
			}

			if (bakeCurve)
			{
				ZAnimeKey_Vector3::BakeCurve(aniSet->m_Pos, 0);
				ZAnimeKey_Quaternion::BakeCurve(aniSet->m_Rotate, 0);
			}

		}

		for (size_t i = 0; i < m_FrameAnimeList.size(); i++)
		{
			if (m_FrameAnimeList[i]->m_Pos.size() < 1 &&
				m_FrameAnimeList[i]->m_Rotate.size() < 1 &&
				m_FrameAnimeList[i]->m_Scale.size() < 1)
				continue;

			if (boneTree[i]->pSrcBoneNode->BF_IsIK() == false)
				continue;
			else
			{
				// アニメーションのあるIKはOFFにする対象から外す
				ZString ikBoneName = boneTree[i]->pSrcBoneNode->BoneName;
				auto& it = std::find_if(ikBones.begin(), ikBones.end(),
					[&ikBoneName](auto& boneName)
				{
					return boneName == ikBoneName;
				});

				if (it != ikBones.end())
					ikBones.erase(it);
			}

		}

		// アニメーションのないIKはOFF
		for (auto& disableIKName : ikBones)
			bc.EnableIKBone(disableIKName, false);

		return true;
	}

	//====================================================================================================
	//
	// ZAnimatorTrack
	//
	//====================================================================================================

	void ZAnimatorTrack::UpdateEvent(double val)
	{
		// イベント処理
		auto it = EventList.begin();
		while (it != EventList.end())
		{
			(*it)->fStartTime -= val;

			if ((*it)->fStartTime > 0)
			{
				++it;
				continue;
			}

			// Enable
			if ((*it)->Type == Event_Base::STE_ENABLE)
			{
				Event_Enable *p = static_cast<Event_Enable*>((*it).GetPtr());

				m_Enable = p->bl;

				// このイベントを消す
				it = EventList.erase(it);
				continue;
			}
			// DeleteTrack
			else if ((*it)->Type == Event_Base::STE_DELETETRACK)
			{
				Event_DeleteTrack *p = static_cast<Event_DeleteTrack*>((*it).GetPtr());

				m_DeleteTrack = true;

				// このイベントを消す
				it = EventList.erase(it);
				continue;
			}
			// Position
			else if ((*it)->Type == Event_Base::STE_POSITION)
			{
				Event_Position *p = static_cast<Event_Position*>((*it).GetPtr());

				m_AnimePos = p->Pos;

				// このイベントを消す
				it = EventList.erase(it);
				continue;
			}
			// Speed
			else if ((*it)->Type == Event_Base::STE_SPEED)
			{
				Event_Speed *p = static_cast<Event_Speed*>((*it).GetPtr());

				if (p->startflag == 0)
				{
					p->OldSpeed = m_Speed;// 現在の速度を記憶
					p->startflag = 1;
				}

				if (p->fDuration == 0)
				{	// 即座に変更

					m_Speed = p->NewSpeed;

					// このイベントを消す
					it = EventList.erase(it);
					continue;
				}
				else
				{					// 中間補間
					double f;
					f = (-p->fStartTime) / p->fDuration;
					if (f >= 1.0f)
					{
						// 最後
						m_Speed = p->NewSpeed;

						// このイベントを消す
						it = EventList.erase(it);
						continue;
					}
					else
					{
						// 補間
						m_Speed = (float)(p->NewSpeed*(f)+p->OldSpeed*(1 - f));
					}
				}
			}
			//Weight
			else if ((*it)->Type == Event_Base::STE_WEIGHT)
			{
				Event_Weight* p = static_cast<Event_Weight*>((*it).GetPtr());

				if (p->startflag == 0)
				{
					p->OldWeight = m_Weight;// 現在の重みを記憶
					p->startflag = 1;
				}

				if (p->fDuration == 0)
				{	// 即座に変更

					m_Weight = p->NewWeight;

					// このイベントを消す
					it = EventList.erase(it);
					continue;
				}
				else
				{					// 中間補間
					double f;
					f = (-p->fStartTime) / p->fDuration;
					if (f >= 1.0f)
					{
						// 最後
						m_Weight = p->NewWeight;

						// このイベントを消す
						it = EventList.erase(it);
						continue;
					}
					else
					{
						// 補間
						m_Weight = (float)(p->NewWeight*(f)+p->OldWeight*(1 - f));
					}
				}
			}

			++it;
		}

	}

	//====================================================================================================
	//
	// ZAnimator
	//
	//====================================================================================================

	void ZAnimator::Init()
	{
		m_BaseWeight = 1;
		m_BaseWeightAnime = 1;
		m_BaseWeightAnimeSpeed = 0;
		UnkeyAllTrackEvents();

		m_AnimeList.clear();
		m_EnableRootMotion = true;

		m_Track.clear();
		m_Track.push_back(Make_Shared(ZAnimatorTrack, appnew));
		m_Track[0]->Init();

		m_RefMatrixTbl.clear();

	}

	void ZAnimator::Init(ZAnimator& srcAnimator)
	{
		Init();

		// アニメリストコピー
		m_AnimeList = srcAnimator.m_AnimeList;

		// 参照行列コピー
		m_RefMatrixTbl.clear();
		m_RefMatrixTbl.resize(srcAnimator.m_RefMatrixTbl.size());
		for (UINT i = 0; i < m_RefMatrixTbl.size(); i++)
		{
			m_RefMatrixTbl[i].refTransMat = srcAnimator.m_RefMatrixTbl[i].refTransMat;
		}
	}


	void ZAnimator::Init(ZMatrix* refMat)
	{
		Init();

		ClearRefMatrix();
		AddRefMatrix(refMat);
	}

	void ZAnimator::AddAnimation(ZSP<ZAnimationSet> anime, bool allowSameName)
	{
		// 同名のアニメがあるなら入れ替え
		if (allowSameName == false)
		{
			int idx = SearchAnimation(anime->m_AnimeName);
			if (idx != -1)
			{
				// あり　差し替え
				m_AnimeList[idx] = anime;
				return;
			}
		}

		// 新規追加
		m_AnimeList.push_back(anime);
	}

	ZSP<ZAnimationSet> ZAnimator::AddAnimation(const ZString& XmdFileName, const char* registerAnimeName, bool allowSameName)
	{
		ZSP<ZAnimationSet> ani = Make_Shared(ZAnimationSet, appnew);
		ani->LoadXMD(XmdFileName, registerAnimeName);

		AddAnimation(ani, allowSameName);

		return ani;
	}


	ZAnimator::ZAnimator()
	{
	}

	int ZAnimator::GetMaxAnimeNum()
	{
		return (int)m_AnimeList.size();
	}

	void ZAnimator::Animation(double Val, ZMatrix* inoutRootMotionMatrix)
	{
		ZAnimatorTrack* track;

		//====================================================
		// 重みアニメ処理
		//====================================================
		if (Val > 0)
		{
			if (m_BaseWeight != m_BaseWeightAnime)
			{
				if (m_BaseWeight > m_BaseWeightAnime)
				{
					m_BaseWeight -= (float)(m_BaseWeightAnimeSpeed* Val);
					if (m_BaseWeight < m_BaseWeightAnime)
					{
						m_BaseWeight = m_BaseWeightAnime;
					}
				}
				else if (m_BaseWeight < m_BaseWeightAnime)
				{
					m_BaseWeight += (float)(m_BaseWeightAnimeSpeed* Val);
					if (m_BaseWeight > m_BaseWeightAnime)
					{
						m_BaseWeight = m_BaseWeightAnime;
					}
				}
			}

			//====================================================
			// 各トラックの処理
			//====================================================
			for (UINT i = 0; i < m_Track.size(); i++)
			{
				track = m_Track[i].GetPtr();

				//========================
				// アニメ進める
				//========================
				if (track->m_Enable)
				{
					ZAnimationSet* lpSA = track->m_pSkinAnime.GetPtr();

					// 進める
					track->m_PrevAnimePos = track->m_AnimePos;
					track->m_AnimePos += Val * track->m_Speed* (lpSA->m_TicksPerSecond / 60.0f);

					// 最後判定を先にする
					if (lpSA->m_AnimeLen <= 0)
					{	
						// アニメ長が0
						track->m_AnimePos = 0;
					}
					else if (track->m_Loop)
					{
						// ループ
						if (track->m_AnimePos >= lpSA->m_AnimeLen)
						{
							track->m_AnimePos = 0;
							track->m_PrevAnimePos = 0;
						}
					}
					else
					{				// ループなし
						if (track->m_AnimePos > lpSA->m_AnimeLen)
						{
							track->m_AnimePos = lpSA->m_AnimeLen;
						}
					}
				}

				//========================
				// イベント処理
				//========================
				track->UpdateEvent(Val);
				if (track->m_DeleteTrack)
				{
					// 最後の1個は消さない
					if (m_Track.size() >= 2)
					{
						m_Track.erase(m_Track.begin() + i);
						i--;
					}
				}
			}

		}

		//====================================================
		// アニメーションデータを使い、変換行列更新
		//====================================================
		CalcAnimation(inoutRootMotionMatrix);

	}

	void ZAnimator::AnimationAndScript(double Val, std::function<void(ZAnimeKey_Script*)> onScriptExecProc, ZMatrix* inoutRootMotionMatrix)
	{
		if (m_Track.size() == 0)return;

		if (onScriptExecProc == nullptr)
		{
			Animation(Val, inoutRootMotionMatrix);
			return;
		}

		ZAnimatorTrack* track;
		//===========================================
		// スクリプトにより、細かく進行させる
		//===========================================
		if (m_Track[0]->m_Enable)
		{
			track = m_Track[0].GetPtr();
			ZAnimationSet* lpSA = track->m_pSkinAnime.GetPtr();

			if (lpSA->m_Script.size() > 0)
			{
				ZAVector<double> valTbl;
				ZAVector<ZAnimeKey_Script*> scrTbl;

				double nowTime = track->m_AnimePos;
				double margin = 0;
				if (nowTime == 0)margin = -0.0000001;
				double nextTime = (track->m_AnimePos + (Val* track->m_Speed*(lpSA->m_TicksPerSecond / 60)));
				double prevPos = 0;
				// nowTime - nextTime間にスクリプトがあるか？
				for (auto& sc : lpSA->m_Script)
				{
					// 発見！
					if (sc.Time > nowTime + margin && sc.Time <= nextTime)
					{
						// 位置算出
						double p = (sc.Time - track->m_AnimePos);
						// 前のスクリプトからの相対位置を登録
						valTbl.push_back(p - prevPos);
						scrTbl.push_back(&sc);	// スクリプトも登録

						prevPos = p;
					}
				}

				// 発見したスクリプトの位置を元に全身のポーズを決定し、スクリプトも実行する
				if (valTbl.size() > 0)
				{
					for (UINT i = 0; i < valTbl.size(); i++)
					{

						double v = valTbl[i];
						Val -= v * track->m_Speed;
						double buSpeed = track->m_Speed;
						track->m_Speed = 1.0;
						Animation(v, inoutRootMotionMatrix);
						track->m_Speed = buSpeed;

						// スクリプト実行
						ZAnimeKey_Script* sc = scrTbl[i];
						onScriptExecProc(sc);
					}
					// 残りのぶんを進行
					if (Val > 0)
					{
						Animation(Val, inoutRootMotionMatrix);
					}

					return;
				}
			}

			// 実行するスクリプトがない場合は、通常アニメ

		}


		// 通常アニメ
		Animation(Val, inoutRootMotionMatrix);

	}

	void ZAnimator::CalcAnimation(ZMatrix* inoutRootMotionMatrix)
	{
		UINT i;

		ZAnimationSet* anime;
		ZMatrix m;
		ZQuat qTotal;
		ZVec3 vTotal;

		// フレーム数
		UINT FrameSize = m_RefMatrixTbl.size();

		ZAnimatorTrack* track;

		// 全フレーム処理
		double fAnimeTime;
		ZMatrix mOldTransMat;
		ZQuat qRota;
		ZVec3 vScale;
		ZVec3 sTotal;
		ZVec3 vPos;
		ZQuat qTmp;
		ZVec3 vTmp;

		for (i = 0; i < FrameSize; i++)
		{

			ZMatrix* refMat = m_RefMatrixTbl[i].refTransMat;
			if (refMat == nullptr)continue;

			int RotaCnt = 0;
			int PosCnt = 0;
			int ScaleCnt = 0;

			//-------------------------------------------------------
			//
			// Root Motion有効時
			//
			//-------------------------------------------------------
			// 0番目のボーン(Rootボーン)のみ特殊計算
			if (m_EnableRootMotion && i == 0 && inoutRootMotionMatrix)
			{
				// 全トラック
				for (UINT tr = 0; tr < m_Track.size(); tr++)
				{
					// トラック情報
					track = m_Track[tr].GetPtr();
					if (!track->m_Enable)continue;

					// アニメ情報
					anime = track->m_pSkinAnime.GetPtr();
					if (i >= anime->m_FrameAnimeList.size())continue;

					// アニメ位置
					fAnimeTime = track->m_AnimePos;
					if (fAnimeTime < 0)fAnimeTime = 0;

					qRota.CreateIdentity();
					vPos.Set(0, 0, 0);
					ZQuat qPrevRota;
					ZVec3 vPrevPos;
					// 回転
					if (ZAnimeKey_Quaternion::InterpolationFromTime(anime->m_FrameAnimeList[i]->m_Rotate, fAnimeTime, qRota, &m_RefMatrixTbl[i].nowKeyIndex_Rota))
					{
						// 前の状態
						ZAnimeKey_Quaternion::InterpolationFromTime(anime->m_FrameAnimeList[i]->m_Rotate, track->m_PrevAnimePos, qPrevRota, &m_RefMatrixTbl[i].nowKeyIndex_Rota);

						RotaCnt++;
					}

					// 座標
					if (ZAnimeKey_Vector3::InterpolationFromTime(anime->m_FrameAnimeList[i]->m_Pos, fAnimeTime, vPos, &m_RefMatrixTbl[i].nowKeyIndex_Pos))
					{
						// 前の状態
						ZAnimeKey_Vector3::InterpolationFromTime(anime->m_FrameAnimeList[i]->m_Pos, track->m_PrevAnimePos, vPrevPos, &m_RefMatrixTbl[i].nowKeyIndex_Pos);

						PosCnt++;
					}

					// 変化量算出
					ZMatrix changeAmountMat;
					ZMatrix prevMat;
					qRota.ToMatrix(changeAmountMat);
					changeAmountMat.SetPos(vPos);

					qPrevRota.ToMatrix(prevMat);
					prevMat.SetPos(vPrevPos);
					prevMat.Inverse();

					changeAmountMat *= prevMat;


					// 1個目の回転データは、そのまま使用
					if (RotaCnt == 1)
					{
						changeAmountMat.ToQuaternion(qTotal);
					}
					// 2個目以降は、補間
					else
					{
						ZQuat q;
						changeAmountMat.ToQuaternion(q);
						ZQuat::Slerp(qTotal, qTotal, q, track->m_Weight);
					}

					// 1個目の座標データは、そのまま使用
					if (PosCnt == 1)
					{
						vTotal = changeAmountMat.GetPos();
					}
					// 2個目以降は、補間
					else
					{
						ZVec3::Lerp(vTotal, vTotal, changeAmountMat.GetPos(), track->m_Weight);
					}

				}

				// キーがない場合は、何もしない
				if (RotaCnt == 0 && ScaleCnt == 0 && PosCnt == 0) continue;

				// 引数で持ってきた行列と、アニメータの重みで補間する
				ZQuat qBaseMotionMat;
				inoutRootMotionMatrix->ToQuaternion(qBaseMotionMat);
				ZQuat::Slerp(qTotal, qBaseMotionMat, qTotal, m_BaseWeight);
				ZVec3::Lerp(vTotal, inoutRootMotionMatrix->GetPos(), vTotal, m_BaseWeight);

				// 結果
				qTotal.ToMatrix(*inoutRootMotionMatrix);
				inoutRootMotionMatrix->SetPos(vTotal);

				// Root Motion時は、これ以降の通常処理をしない。
				continue;
			}

			//-------------------------------------------------------
			//
			// 通常補間処理
			//
			//-------------------------------------------------------

			// 全トラック
			for (UINT tr = 0; tr < m_Track.size(); tr++)
			{
				track = m_Track[tr].GetPtr();
				if (!track->m_Enable)continue;

				anime = track->m_pSkinAnime.GetPtr();
				if (i >= anime->m_FrameAnimeList.size())continue;

				// アニメ位置
				fAnimeTime = track->m_AnimePos;
				if (fAnimeTime < 0)fAnimeTime = 0;

				// 回転
				if (ZAnimeKey_Quaternion::InterpolationFromTime(anime->m_FrameAnimeList[i]->m_Rotate, fAnimeTime, qRota, &m_RefMatrixTbl[i].nowKeyIndex_Rota))
				{
					// 初回 または 重みが1
					if (RotaCnt == 0 || track->m_Weight >= 1.0f)
					{
						qTotal = qRota;
					}
					// ブレンド
					else
					{
						ZQuat::Slerp(qTotal,
							qTotal,
							qRota,
							track->m_Weight);		// 補間(0.0〜1.0)
					}
					RotaCnt++;
				}
				// 拡大
				if (ZAnimeKey_Vector3::InterpolationFromTime(anime->m_FrameAnimeList[i]->m_Scale, fAnimeTime, vScale, &m_RefMatrixTbl[i].nowKeyIndex_Scale))
				{
					// 初回 または 重みが1
					if (ScaleCnt == 0 || track->m_Weight >= 1.0f)
					{
						sTotal = vScale;
					}
					// ブレンド
					else
					{
						ZVec3::Lerp(sTotal,
							sTotal,
							vScale,
							track->m_Weight);
					}
					ScaleCnt++;
				}
				// 座標
				if (ZAnimeKey_Vector3::InterpolationFromTime(anime->m_FrameAnimeList[i]->m_Pos, fAnimeTime, vPos, &m_RefMatrixTbl[i].nowKeyIndex_Pos))
				{
					// 初回 または 重みが1
					if (PosCnt == 0 || track->m_Weight >= 1.0f)
					{
						vTotal = vPos;
					}
					// ブレンド
					else
					{
						ZVec3::Lerp(vTotal,
							vTotal,
							vPos,
							track->m_Weight);
					}
					PosCnt++;
				}
			}

			// キーがない場合は、何もしない
			if (RotaCnt == 0 && ScaleCnt == 0 && PosCnt == 0) continue;

			m.CreateIdentity();

			mOldTransMat = *refMat;	// 計算前のローカル行列を記憶

			// ベースウェイトでのブレンド
			// 回転
			if (RotaCnt > 0)
			{
				// ベースウェイトが1未満なら、計算前の行列とブレンド(1以上だと上書きになるので、計算しなくてよい)
				if (m_BaseWeight < 1)
				{
					ZMatrix::ToQuaternion(qTmp, mOldTransMat);
					if (m_BaseWeight > 0)
					{
						ZQuat::Slerp(qTotal, qTmp, qTotal, m_BaseWeight);
					}
					else
					{
						qTotal = qTmp;
					}
				}

				qTotal.ToMatrix(m);
				refMat->SetRotation(m);
			}
			// 拡大
			if (ScaleCnt > 0)
			{
				// ベースウェイトが1未満なら、計算前の行列とブレンド(1以上だと上書きになるので、計算しなくてよい)
				if (m_BaseWeight < 1)
				{
					vTmp.Set(mOldTransMat.GetXScale(), mOldTransMat.GetYScale(), mOldTransMat.GetZScale());

					if (m_BaseWeight > 0)
					{
						ZVec3::Lerp(sTotal, vTmp, sTotal, m_BaseWeight);
					}
					else
					{
						sTotal = vTmp;
					}
				}

				refMat->NormalizeScale();
				m.CreateScale(sTotal);
				*refMat = m * (*refMat);
			}
			// 座標
			if (PosCnt > 0)
			{
				// ベースウェイトが1未満なら、計算前の行列とブレンド(1以上だと上書きになるので、計算しなくてよい)
				if (m_BaseWeight < 1)
				{
					vTmp.Set(mOldTransMat.GetPos());

					if (m_BaseWeight > 0)
					{
						ZVec3::Lerp(vTotal, vTmp, vTotal, m_BaseWeight);
					}
					else
					{
						vTotal = vTmp;
					}
				}
				refMat->SetPos(vTotal);
			}
		}
	}

	bool ZAnimator::ChangeAnime(UINT AnimeNo, bool loop, UINT SetTrackNo, bool bEnableTrack, double Speed, float Weight, double AnimePos)
	{
		if (m_AnimeList.size() <= AnimeNo)return false;

		if (SetTrackNo >= m_Track.size())return false;

		m_Track[SetTrackNo]->m_pSkinAnime = m_AnimeList[AnimeNo];
		m_Track[SetTrackNo]->m_AnimeNo = AnimeNo;
		m_Track[SetTrackNo]->m_AnimePos = AnimePos;
		m_Track[SetTrackNo]->m_PrevAnimePos = AnimePos;
		m_Track[SetTrackNo]->m_Enable = bEnableTrack;
		m_Track[SetTrackNo]->m_Speed = Speed;
		m_Track[SetTrackNo]->m_Weight = Weight;
		m_Track[SetTrackNo]->m_Loop = loop;


		return true;
	}

	bool ZAnimator::AddAnime(UINT AnimeNo, bool loop, bool bEnableTrack, double Speed, float Weight, double AnimePos)
	{
		// 新しいトラック作成
		ZSP<ZAnimatorTrack> newTrack = Make_Shared(ZAnimatorTrack, appnew);
		m_Track.push_back(newTrack);

		return ChangeAnime(AnimeNo, loop, m_Track.size() - 1, bEnableTrack, Speed, Weight, AnimePos);
	}

	bool ZAnimator::ChangeAnime(const ZString& AnimeName, bool loop, UINT SetTrackNo, bool bEnableTrack, double Speed, float Weight, double AnimePos)
	{
		// アニメ検索
		int animeNo = SearchAnimation(AnimeName);
		if (animeNo == -1)return false;

		return ChangeAnime(animeNo, loop, SetTrackNo, bEnableTrack, Speed, Weight, AnimePos);
	}


	// ブレンドで滑らかアニメ変更
	bool ZAnimator::ChangeAnimeSmooth(UINT AnimeNo, float StartTime, float Duration, bool loop, double Speed, double AnimePos)
	{
		// 新しいトラック作成
		ZSP<ZAnimatorTrack> newTrack = Make_Shared(ZAnimatorTrack, appnew);
		m_Track.push_front(newTrack);

		// 即座に変更
		if (Duration == 0)
		{
			// 新しいアニメを設定
			if (ChangeAnime(AnimeNo, loop, 0, true, Speed, 1, AnimePos) == false)return false;

			// 前のアニメは無効にする。
			if (m_Track.size() >= 2)
			{
				// １つ前のトラック
				ZAnimatorTrack* prevTrack = m_Track[1].GetPtr();
				prevTrack->m_Enable = false;
				prevTrack->m_DeleteTrack = true;
				prevTrack->m_Weight = 0;
			}
		}
		// 滑らかに変更
		else
		{
			// 新しいアニメをセット
			if (ChangeAnime(AnimeNo, loop, 0, true, Speed, 0, AnimePos) == false)return false;
			// 新しいアニメの重みを上げていく設定
			newTrack->EventTrackWeight(1, StartTime, Duration);

			// 前のアニメの重みを減らし、無効にしていく。
			if (m_Track.size() >= 2)
			{
				// １つ前のトラック
				ZAnimatorTrack* prevTrack = m_Track[1].GetPtr();

				prevTrack->EventTrackEnable(false, StartTime + Duration);	// 無効イベント
				prevTrack->EventTrackDelete(StartTime + Duration);			// 削除イベント

				prevTrack->EventTrackSpeed(0, StartTime, Duration);			// 速度イベント
				prevTrack->EventTrackWeight(0.0f, StartTime, Duration);		// 重みイベント
			}

		}
		return true;
	}

	bool ZAnimator::ChangeAnimeSmooth(const ZString& AnimeName, float StartTime, float Duration, bool loop, double Speed, double AnimePos)
	{
		// アニメ検索
		int animeNo = SearchAnimation(AnimeName);
		if (animeNo == -1)return false;

		return ChangeAnimeSmooth(animeNo, StartTime, Duration, loop, Speed, AnimePos);
	}

	bool ZAnimator::AddAnimeSmooth(UINT AnimeNo, float StartTime, float Duration, bool loop, double Speed, double AnimePos, float startWeight, float endWeight)
	{
		// 新しいトラック作成
		ZSP<ZAnimatorTrack> newTrack = Make_Shared(ZAnimatorTrack, appnew);
		m_Track.push_back(newTrack);	// 後ろに追加

		// 新しいアニメをセット
		if (ChangeAnime(AnimeNo, loop, (int)m_Track.size() - 1, true, Speed, startWeight, AnimePos) == false)return false;

		// 新しいアニメの重みを変化させていく設定
		newTrack->EventTrackWeight(endWeight, StartTime, Duration);

		// endWeightが0なら、0になったらトラックから消す
		if (endWeight == 0)
		{
			newTrack->EventTrackEnable(false, StartTime + Duration);	// 無効イベント
			newTrack->EventTrackDelete(StartTime + Duration);			// 削除イベント
		}

		return true;
	}

	bool ZAnimator::AddAnimeSmooth(const ZString& AnimeName, float StartTime, float Duration, bool loop, double Speed, double AnimePos, float startWeight, float endWeight)
	{
		// アニメ検索
		int animeNo = SearchAnimation(AnimeName);
		if (animeNo == -1)return false;

		return AddAnimeSmooth(animeNo, StartTime, Duration, loop, Speed, AnimePos, startWeight, endWeight);
	}

	//====================================================================================================
	//
	// ZSimpleAnimator
	//
	//====================================================================================================
	void ZSimpleAnimator::Animation(double speed)
	{
		// 停止中
		if (m_Play == false)return;
		// アニメが無い
		if (m_Anime == nullptr || m_Anime->m_FrameAnimeList.size() == 0)return;

		// アニメ進行
		m_NowAnimePos += speed * (m_Anime->m_TicksPerSecond / 60.0f);
		if (m_Loop)
		{	// ループ
			if (m_NowAnimePos >= m_Anime->m_AnimeLen)
			{
				m_NowAnimePos = 0;
			}
		}
		else
		{				// ループなし
			if (m_NowAnimePos > m_Anime->m_AnimeLen)
			{
				m_NowAnimePos = m_Anime->m_AnimeLen;
			}
		}

		// 回転
		if (ZAnimeKey_Quaternion::InterpolationFromTime(m_Anime->m_FrameAnimeList[0]->m_Rotate, m_NowAnimePos, m_Rotate, nullptr))
		{

		}

		// 拡大
		if (ZAnimeKey_Vector3::InterpolationFromTime(m_Anime->m_FrameAnimeList[0]->m_Scale, m_NowAnimePos, m_Scale, nullptr))
		{

		}

		// 座標
		if (ZAnimeKey_Vector3::InterpolationFromTime(m_Anime->m_FrameAnimeList[0]->m_Pos, m_NowAnimePos, m_Pos, nullptr))
		{

		}

	}

}