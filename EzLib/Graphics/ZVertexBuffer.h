#ifndef __ZVERTEX_BUFFER_H__
#define __ZVERTEX_BUFFER_H__

namespace EzLib
{
	enum class ZBufferType
	{
		Static,Dynamic
	};
	
	class ZVertexBuffer
	{
	public:
		ZVertexBuffer(ZBufferType type);
		~ZVertexBuffer();
	
		void Release();

		void Resize(uint size);
		void SetLayout(const ZBufferLayout& layout);
		void SetData(uint size, const void* data);
	
		void ReleasePointer();
	
		void Bind();
	
		template<typename T>
		T* GetPointer()
		{
			return (T*)GetPointer();
		}
	
		static ZVertexBuffer* Create(ZBufferType type = ZBufferType::Static);
	
	private:
		void* GetPointer();
	
	private:
		D3D11_BUFFER_DESC m_BufferDesc;
		D3D11_MAPPED_SUBRESOURCE m_MappedSubresource;
		ID3D11Buffer* m_pBuffer;
		ID3D11InputLayout* m_InputLayout;
	
		ZBufferType m_BufferType;
		uint m_BufferSize;
		ZBufferLayout m_BufferLayout;
		
	};

}
#endif