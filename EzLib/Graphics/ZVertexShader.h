//===============================================================
//  @file ZVertexShader.h
//   頂点シェーダクラス
// 
//  @author 鎌田
//===============================================================
#ifndef ZVertexShader_h
#define ZVertexShader_h

namespace EzLib
{

	//=======================================================================
	//   頂点シェーダクラス
	// 
	//  [主な機能]								\n
	// 	・HLSLファイルから頂点シェーダ作成		\n
	// 	・頂点レイアウト作成					\n
	// 	・それらをデバイスにセットする機能		\n
	// 
	//  @ingroup Graphics_Shader
	//=======================================================================
	class ZVertexShader : public ZIResource
	{
	public:
		struct ForceUseInputLayoutParam
 		{
			UINT SlotIndex;
			D3D11_INPUT_CLASSIFICATION InputClassification;
			UINT DataStepRate;
		};

	public:
		ZVertexShader();
		~ZVertexShader()
		{
			Release();
		}

		// コピー禁止用
		ZVertexShader(const ZVertexShader& src) = delete;
		void operator=(const ZVertexShader& src) = delete;
		void operator=(ZVertexShader&& src) = delete;

		//--------------------------------------------------------------------------
		//   コンパイル済みシェーダファイル(.cso)を読み込み、シェーダオブジェクトを作成する
		//   csoFileName		… CSOファイルパス
		//   vertexTypeData	… 頂点レイアウト用のデータ　頂点シェーダの入力にあった形式を渡すこと
		//  @return 成功:true
		//--------------------------------------------------------------------------
		bool LoadShader(const ZString& csoFileName);

		//--------------------------------------------------------------------------
		//   HLSLソースファイル(.hlsl)を読み込み、頂点シェーダ・頂点レイアウトを作成する。
		//   hlslFileName		… HLSLファイルパス
		//   VsFuncName		… HLSL内の頂点シェーダ関数名
		//   shaderModel		… 使用するシェーダモデルを指定 例)"vs_5_0"
		//   shaderFlag		… 追加のシェーダコンパイルフラグ
		//   vertexTypeData	… 頂点レイアウト用のデータ　頂点シェーダの入力にあった形式を渡すこと
		//  @return 成功:true
		//--------------------------------------------------------------------------
		bool LoadShaderFromFile(const ZString& hlslFileName, const ZString& VsFuncName, const ZString& shaderModel, UINT shaderFlag);

		//--------------------------------------------------------------------------
		//   コンパイル済みシェーダをファイルに保存する
		//  Create()時に指定した、[hlslFileName]_[VsFuncName].cso のファイル名で保存される
		//   filename		… 保存するファイルパス
		//  @return 成功:true
		//--------------------------------------------------------------------------
		bool SaveToFile();

		//--------------------------------------------------------------------------
		//   解放
		//--------------------------------------------------------------------------
		void Release();

		//--------------------------------------------------------------------------
		//   シェーダをデバイスにセット(頂点レイアウトもセット)
		//   vertexLayout		… 頂点レイアウト　NULLだとInit()時に指定したものが使用される
		//--------------------------------------------------------------------------
		void SetShader(ID3D11InputLayout* vertexLayout = nullptr);

		//--------------------------------------------------------------------------
		//   頂点シェーダを無効にする
		//  SetShader()で最後設定すると有効になる
		//--------------------------------------------------------------------------
		static void DisableShader();

		ZString GetTypeName() const override
		{
			ZString str = "VertexShader : ";
			str += m_csoFileName;
			return str;
		}

		static void AddForceUseFormat(const std::string& semanticsName,DXGI_FORMAT format)
		{
			// すでに同一のセマンティクスに対してフォーマットが指定されてるか確認
			{
				auto it = ForceUseFormatMap.find(semanticsName);
				if (it != ForceUseFormatMap.end())
					return;
			}
			
			ForceUseFormatMap[semanticsName] = format;
		}

		static void AddForceUseInputLayoutParam(const std::string& semanticsName, const ForceUseInputLayoutParam& param)
		{
			// すでに同一のセマンティクスに対してフォーマットが指定されてるか確認
			{
				auto it = ForceUseInputLayoutParamMap.find(semanticsName);
				if (it != ForceUseInputLayoutParamMap.end())
					return;
			}

			ForceUseInputLayoutParamMap[semanticsName] = param;
		}

	private:
		void RefrectInputLayout();


	private:
		ID3D11VertexShader* m_VS;				// 頂点シェーダー
		ID3D11InputLayout*	m_VLayout;			// 頂点インプットレイアウト

		ID3DBlob*			m_pCompiledShader;	// コンパイル済みシェーダデータ

		ZString			m_hlslFileName;		// HLSLファイル名
		ZString			m_csoFileName;		// コンパイル済みシェーダ(CSO)時のファイル名


		// RefrectInputLayout()内で任意のセマンティクス値のフォーマットを強制的に指定する用
		static std::unordered_map<std::string, DXGI_FORMAT> ForceUseFormatMap;
		// RefrectInputLayout()内で任意のセマンティクス値の入力レイアウトパラメータを強制的に指定する用
		static std::unordered_map<std::string, ForceUseInputLayoutParam> ForceUseInputLayoutParamMap;

	};



}

#endif
