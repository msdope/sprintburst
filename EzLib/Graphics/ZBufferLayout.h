#ifndef __ZBUFFER_LAYOUT_H__
#define __ZBUFFER_LAYOUT_H__

namespace EzLib
{
	enum class ZInputClassification : uint
	{
		PER_VERTEX = 0,
		PER_INSTANCE = 1
	};

	struct ZBufferElement
	{
	public:
		ZBufferElement(ZString name, DXGI_FORMAT type, uint size, uint count, uint offset);
		ZBufferElement(ZString name, DXGI_FORMAT type, uint size, uint count, uint offset,
			ZInputClassification classification,uint instanceDataStepRate = 1);
	public:
		ZString Name;
		DXGI_FORMAT Type;
		uint Size;
		uint Count;
		uint Offset;
		ZInputClassification InputClassification; // PerVertex or PerInstance
		uint InstanceDataStepRate;
	};

	class ZBufferLayout
	{
	public:
		ZBufferLayout();

		template<typename T>
		void Push(const ZString& name, uint count = 1)
		{
			assert(false); // UnknownType
		}
		
		template<typename T>
		void Push(const ZString& name, uint count = 1,
			ZInputClassification classification = ZInputClassification::PER_VERTEX,uint insatanceDataStepRate = 1)
		{
			assert(false); // UnknownType
		}

		template<>
		void Push<float>(const ZString& name, uint count)
		{
			Push(name, DXGI_FORMAT_R32_FLOAT, sizeof(float), count);
		}

		template<>
		void Push<float>(const ZString& name, uint count, ZInputClassification classification, uint insatanceDataStepRate)
		{
			Push(name, DXGI_FORMAT_R32_FLOAT, sizeof(float), count,classification,insatanceDataStepRate);
		}

		template<>
		void Push<uint>(const ZString& name, uint count)
		{
			Push(name, DXGI_FORMAT_R32_UINT, sizeof(uint), count);
		}

		template<>
		void Push<uint>(const ZString& name, uint count, ZInputClassification classification, uint insatanceDataStepRate)
		{
			Push(name, DXGI_FORMAT_R32_UINT, sizeof(uint), count, classification, insatanceDataStepRate);
		}

		template<>
		void Push<uint8>(const ZString& name, uint count)
		{
			Push(name, DXGI_FORMAT_R32_UINT, sizeof(uint8), count);
		}

		template<>
		void Push<uint8>(const ZString& name, uint count, ZInputClassification classification, uint insatanceDataStepRate)
		{
			Push(name, DXGI_FORMAT_R8G8B8A8_UNORM, sizeof(uint8), count, classification, insatanceDataStepRate);
		}

		template<>
		void Push<ZVec2>(const ZString& name, uint count)
		{
			Push(name, DXGI_FORMAT_R32G32_FLOAT, sizeof(ZVec2), count);
		}

		template<>
		void Push<ZVec2>(const ZString& name, uint count, ZInputClassification classification, uint insatanceDataStepRate)
		{
			Push(name, DXGI_FORMAT_R32G32_FLOAT, sizeof(ZVec2), count, classification, insatanceDataStepRate);
		}

		template<>
		void Push<ZVec3>(const ZString& name, uint count)
		{
			Push(name, DXGI_FORMAT_R32G32B32_FLOAT, sizeof(ZVec3), count);
		}

		template<>
		void Push<ZVec3>(const ZString& name, uint count, ZInputClassification classification, uint insatanceDataStepRate)
		{
			Push(name, DXGI_FORMAT_R32G32B32_FLOAT, sizeof(ZVec3), count, classification, insatanceDataStepRate);
		}

		template<>
		void Push<ZVec4>(const ZString& name, uint count)
		{
			Push(name, DXGI_FORMAT_R32G32B32A32_FLOAT, sizeof(ZVec4), count);
		}

		template<>
		void Push<ZVec4>(const ZString& name, uint count, ZInputClassification classification, uint insatanceDataStepRate)
		{
			Push(name, DXGI_FORMAT_R32G32B32A32_FLOAT, sizeof(ZVec4), count, classification, insatanceDataStepRate);
		}

		template<>
		void Push<ZMatrix>(const ZString& name, uint count, ZInputClassification classification, uint insatanceDataStepRate)
		{
			Push(name, DXGI_FORMAT_R32G32B32A32_FLOAT, sizeof(ZMatrix), count, classification, insatanceDataStepRate);
		}

		const ZVector<ZBufferElement> GetLayout()const;
		uint GetStride()const;
	private:
		void Push(const ZString& name, DXGI_FORMAT type, uint size, uint count);
		void Push(const ZString& name, DXGI_FORMAT type, uint size, uint count, ZInputClassification classification, uint instanceDataStepRate);

	private:
		uint m_Size_PerVertex;
		uint m_Size_PerInstance;
		ZVector<ZBufferElement> m_Layout_PerVertex;
		ZVector<ZBufferElement> m_Layout_PerInstance;
	};

#include "ZBufferLayout.inl"

}

#endif