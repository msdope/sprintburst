#include "PCH/pch.h"
#include "MMD/ZPMX2ZGameModel.h"

using namespace EzLib;

//==========================================================================================
//
//
// ゲーム用多機能モデルクラス
//
//
//==========================================================================================

ZGameModel::~ZGameModel()
{
	Release();
}

void ZGameModel::CreateRefMeshTbl()
{
	m_RefModelTbl_SkinOnly.clear();
	m_RefModelTbl_SkinOnly.shrink_to_fit();
	m_RefModelTbl_StaticOnly.clear();
	m_RefModelTbl_StaticOnly.shrink_to_fit();
	for (UINT i = 0; i < m_ModelTbl.size(); i++)
	{
		if (m_ModelTbl[i]->GetMesh()->IsSkinMesh())
		{
			m_RefModelTbl_SkinOnly.push_back(m_ModelTbl[i]);
		}
		else
		{
			m_RefModelTbl_StaticOnly.push_back(m_ModelTbl[i]);
		}
	}
}

void ZGameModel::CalcBoundingData()
{
	// AABB算出
	ZVec3 vMin, vMax;
	for (UINT i = 0; i < m_ModelTbl.size(); i++)
	{
		if (i == 0)
		{
			vMin = m_ModelTbl[i]->GetMesh()->GetAABB_Center() - m_ModelTbl[i]->GetMesh()->GetAABB_HalfSize();
			vMax = m_ModelTbl[i]->GetMesh()->GetAABB_Center() + m_ModelTbl[i]->GetMesh()->GetAABB_HalfSize();
		}
		else
		{
			ZVec3::Min(vMin, vMin, m_ModelTbl[i]->GetMesh()->GetAABB_Center() - m_ModelTbl[i]->GetMesh()->GetAABB_HalfSize());
			ZVec3::Max(vMax, vMax, m_ModelTbl[i]->GetMesh()->GetAABB_Center() + m_ModelTbl[i]->GetMesh()->GetAABB_HalfSize());
		}
	}

	m_AABB_HalfSize = (vMax - vMin)* 0.5f;
	m_AABB_Center = vMin + m_AABB_HalfSize;

	// ついてに境界球も適当に算出
	{
		ZVec3 v = vMax - vMin;
		m_vBSCenter = vMin + v* 0.5f;
		m_fBSRadius = v.Length() / 2;
	}
}

bool ZGameModel::LoadMesh(const ZString& fileName, bool bakeCurve)
{
	// ヘッダチェックチェック
	char head[4];
	FILE* fp;
	fopen_s(&fp,fileName.c_str(), "r");
	if (fp == nullptr)return false;

	fread(head, 3, 1, fp);
	head[3] = '\0';
	fclose(fp);

	// XEDファイル
	if (strcmp(head, "XED") == 0)
		return LoadXEDFile(fileName, bakeCurve);
	// Xファイル
//	else if(strcmp(head, "xof") == 0) {
//		return LoadXFile(fileName);
//	}
	if(strcmp(head,"PMX") == 0)
		return LoadPMXFile(fileName);
	else
		return false;
}

//==========================================================================================
// スキンメッシュを読み込む
//==========================================================================================
bool ZGameModel::LoadXEDFile(const ZString& fileName, bool bakeCurve)
{
	// 解放
	Release();

	// パス、ファイル名抽出
	char szDir[_MAX_PATH];
	char szFname[_MAX_FNAME];
	char szExt[_MAX_EXT];
	_splitpath_s(fileName.c_str(), nullptr, 0, szDir, _MAX_PATH, szFname, _MAX_FNAME, szExt, _MAX_EXT);
	m_FileName = szFname;
	m_FileName += szExt;
	m_FilePath = szDir;

	// XED読み込み
	if (ZLoadXEDFile(fileName, m_ModelTbl, m_BoneTree, m_AnimeList, m_PhysicsDataSetList, bakeCurve))
	{

		// スキンメッシュとスタティックメッシュの参照を分けたテーブル作成
		CreateRefMeshTbl();

		// 
		CalcBoundingData();

		//------------------------------------------------------------------------------------------
		// KAMADA SkinMesh Editorでの、マテリアルのテキスト部分を解析　※必要ないなら削除してよい
		// 全メッシュ
		for (auto& model : m_ModelTbl)
		{
			// 全マテリアル
			for (auto& mate : model->GetMaterials())
			{
				// とりあえずCSVとして解析する
				ZCsv csv;
				csv.LoadFromText(mate.ExtScript);
				// 縦ループ
				for (UINT ci = 0; ci < csv.m_Tbl.size(); ci++)
				{
					const ZString strType = csv.m_Tbl[ci][0];
					// 
					if (strType == "なにか〜")
					{

					}
				}
			}
		}
		//------------------------------------------------------------------------------------------

		return true;
	}
	return false;

}

bool EzLib::ZGameModel::LoadPMXFile(const ZString& fileName)
{
	auto pmx = ZPMX();
	ReadPMXFile(&pmx, fileName.c_str());

	m_FileName = ZPathUtil::GetFileName(fileName);
	m_FilePath = ZPathUtil::GetDirName(fileName);

	m_ModelTbl.resize(1);
	m_ModelTbl[0] = Make_Shared(ZSingleModel,appnew);
	
	if (ZPMX2ZGameModel(pmx, *this, fileName) == false)
		return false;

	// スキンメッシュとスタティックメッシュの参照を分けたテーブル作成
	CreateRefMeshTbl();
	
	// 
	CalcBoundingData();

	return true;
}

//==========================================================================================
// スキンメッシュを解放
//==========================================================================================
void ZGameModel::Release()
{
	// ボーン解放
	m_BoneTree.clear();
	m_BoneTree.shrink_to_fit();

	// アニメーションデータ解放
	DeleteAnimation();

	// メッシュ解放
	m_ModelTbl.clear();
	m_ModelTbl.shrink_to_fit();
	m_RefModelTbl_SkinOnly.clear();
	m_RefModelTbl_SkinOnly.shrink_to_fit();
	m_RefModelTbl_StaticOnly.clear();
	m_RefModelTbl_StaticOnly.shrink_to_fit();

	m_FilePath.clear();
	m_FileName.clear();
}
