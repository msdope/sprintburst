inline const ZVector<ZBufferElement> ZBufferLayout::GetLayout()const
{
	ZVector<ZBufferElement> layout;
	for (auto elementPerVertex : m_Layout_PerVertex)
		layout.push_back(elementPerVertex);

	for (auto elementPerInstance : m_Layout_PerInstance)
		layout.push_back(elementPerInstance);

	return layout;
}

inline uint ZBufferLayout::GetStride()const
{
	return m_Size_PerVertex;
}