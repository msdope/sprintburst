//===============================================================
//  @file ZTexture.h
//   テクスチャクラス
// 
//  @author 鎌田
//===============================================================
#ifndef ZTexture_h
#define ZTexture_h

namespace EzLib
{

	//   テクスチャ読み込み情報
	struct ZIMAGE_LOAD_INFO
	{
		bool ShaderResource = true;		// シェーダリソース(読み込み用)として使用可能にする
		bool RenderTarget = false;		// レンダーターゲット(書き込み用)として使用可能にする
		bool DepthStencil = false;		// Zバッファとして使用可能にする
		UINT MipLevels = 0;				// ミップマップレベルの最大数 0で全てのミップマップチェーンを作成する

		bool Dynamic = false;			// 動的テクスチャ

		// その他、必要なら拡張してください

	};


	//===========================================================================
	// 
	//   テクスチャリソースクラス
	// 
	//   ※以下のものが取り扱える
	//    - シェーダリソース(通常のテクスチャ)
	//    - レンダーターゲット(描画先として設定可能なRenderTargetテクスチャ)
	//    - Zバッファ(Zバッファとステンシルバッファ)
	//    - アンオーダードアクセステクスチャ
	//    .
	//   ※その他
	//    - MSAA(マルチサンプル アンチエイリアシング)の設定は、バックバッファの設定を使用します
	// 
	//   ※Direct3D11などでは、テクスチャ配列というものが使用できる
	//     一言でいうと、１つのリソースデータの中に複数の画像が入る感じ(Direct3D9でいうキューブマップのようなもの)
	// 
	//  @ingroup Graphics_Important
	//===========================================================================
	class ZTexture : public ZIResource
	{
	public:
		//===================================================================
		// 読み込み・作成
		//===================================================================

		//--------------------------------------------------------------------------
		//   画像ファイル読み込み
		//  基本的な設定で読み込む、ShaderResourceViewのみを作成します
		//   filename		… 読み込む画像ファイル名
		//  @return  true:成功
		//--------------------------------------------------------------------------
		bool LoadTexture(const ZString& filename);

		//--------------------------------------------------------------------------
		//   画像ファイル読み込み
		//  詳細設定で読み込む、ShaderResourceViewのみを作成します
		//   filename		… 読み込む画像ファイル名
		//   loadInfo		… 読み込み詳細設定
		//  @return  true:成功
		//--------------------------------------------------------------------------
		bool LoadTextureEx(const ZString& filename, ZIMAGE_LOAD_INFO* loadInfo);

		//--------------------------------------------------------------------------
		//   通常テクスチャ作成(ShaderResourceViewのみの作成)
		//  テクスチャリソースを作成し、ShaderResourceViewのみを作成します
		//   w			… 画像の幅(ピクセル)
		//   h			… 画像の幅(ピクセル)
		//   format		… 画像の形式
		//   fillData		… バッファに書き込むデータ。w*h*formatのサイズで持っていくこと。nullptrだと書き込みなし。
		//  @return true:成功
		//--------------------------------------------------------------------------
		bool Create(int w, int h, DXGI_FORMAT format, const D3D11_SUBRESOURCE_DATA* fillData = nullptr, bool isDynamic = false);

		//--------------------------------------------------------------------------
		//   レンダーターゲットテクスチャとして作成(RenderTargetViewとShaderResourceViewを作成する)
		//  テクスチャリソースを作成し、RenderTargetViewとShaderResourceViewの2種類を作成します
		//   w			… 画像の幅(ピクセル)
		//   h			… 画像の幅(ピクセル)
		//   format		… 画像の形式
		//   arrayCnt		… テクスチャ配列を使用する場合、その数。1で通常の1枚テクスチャ
		//   fillData		… バッファに書き込むデータ。w*h*formatのサイズで持っていくこと。nullptrだと書き込みなし。
		//  @return true:成功
		//--------------------------------------------------------------------------
		bool CreateRT(int w, int h, DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM, UINT arrayCnt = 1, const D3D11_SUBRESOURCE_DATA* fillData = nullptr);

		//--------------------------------------------------------------------------
		//   レンダーターゲットテクスチャとして作成(キューブマップ用)(RenderTargetViewとShaderResourceViewを作成する)
		// 
		//  キューブマップは、テクスチャ配列6枚と同じ。ShaderResourceViewにD3D11_SRV_DIMENSION_TEXTURECUBEのフラグが付く
		// 
		//   w			… 画像の幅(ピクセル)
		//   h			… 画像の幅(ピクセル)
		//   format		… 画像の形式
		//  @return true:成功
		//--------------------------------------------------------------------------
		bool CreateCubeRT(int w, int h, DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM);

		//===================================================================
		//   アンオーダードアクセステクスチャとして作成(UnorderedAccessViewとShaderResourceViewを作成する)
		//   w			… 画像の幅(ピクセル)
		//   h			… 画像の幅(ピクセル)
		//   format		… 画像の形式
		//   arrayCnt		… テクスチャ配列を使用する場合、その数。1で通常の1枚テクスチャ
		//   fillData		… バッファに書き込むデータ。w*h*formatのサイズで持っていくこと。nullptrだと書き込みなし。
		//  return true:成功
		//--------------------------------------------------------------------------
		bool CreateUAV(int w, int h, DXGI_FORMAT format = DXGI_FORMAT_R8G8B8A8_UNORM, UINT arrayCnt = 1, const D3D11_SUBRESOURCE_DATA* fillData = nullptr);

		//===================================================================

		//--------------------------------------------------------------------------
		//   Zバッファテクスチャとして作成(DepthStencilViewとShaderResourceViewを作成する)
		//  テクスチャリソースを作成し、DepthStencilViewとShaderResourceViewの2種類を作成します
		//   w			… 画像の幅(ピクセル)
		//   h			… 画像の幅(ピクセル)
		//   format		… 画像の形式
		//   arrayCnt	… テクスチャ配列を使用する場合、その数。1で通常の1枚テクスチャ
		//   fillData	… バッファに書き込むデータ。w*h*formatのサイズで持っていくこと。nullptrだと書き込みなし。
		//  @return true:成功
		//--------------------------------------------------------------------------
		bool CreateDepth(int w, int h, DXGI_FORMAT format = DXGI_FORMAT_R24G8_TYPELESS, UINT arrayCnt = 1, const D3D11_SUBRESOURCE_DATA* fillData = nullptr);

		//--------------------------------------------------------------------------
		//   直接Texture2Dリソースをセット
		//  引数で渡したテクスチャリソースに最適なビューを作成する
		//    pTexture2D	… セットしたいテクスチャ
		//  @return true:成功
		//--------------------------------------------------------------------------
		bool CreateFromTexture2D(ID3D11Texture2D* pTexture2D);

	private:
		//   m_pTexture2Dの設定を元に、各ビューを作成する
		bool CreateViewsFromTexture2D();

	public:
		// 
		ZTexture();
		virtual ~ZTexture();


		//--------------------------------------------------------------------------
		//   解放
		//--------------------------------------------------------------------------
		void Release();

		//===================================================================
		// クリア関係
		//===================================================================

		//--------------------------------------------------------------------------
		//   RenderTargetをクリア
		//  ※RenderTargetViewを作成する必要があります
		//  ※テクスチャ配列の場合は、全部のテクスチャがクリアされる
		//  	ColorRGBA	… クリアする色
		//--------------------------------------------------------------------------
		void ClearRT(const ZVec4& ColorRGBA);

		//--------------------------------------------------------------------------
		//   テクスチャ配列の複数RenderTargetのうち、１つだけをクリア
		//  テクスチャ配列で作成したRenderTargetの、指定番目のものだけをクリアできる。
		//  テクスチャ配列として作成していなければこの関数は意味ないです。
		//  	idx			… テクスチャ配列の番号
		//  	ColorRGBA	… クリアする色
		//--------------------------------------------------------------------------
		void ClearRTArray(int idx, const ZVec4& ColorRGBA);

		//--------------------------------------------------------------------------
		//   DepthStencilをクリア
		//  ※DepthStencilViewを作成する必要があります
		//  ※テクスチャ配列の場合は、全部のテクスチャがクリアされる
		//   bDepth		… Zバッファクリアする？
		//   bStencil		… ステンシルバッファクリアする？
		//   depth		… クリアするZバッファの値
		//   stencil		… クリアするステンシルバッファの値
		//--------------------------------------------------------------------------
		void ClearDepth(bool bDepth = true, bool bStencil = true, FLOAT depth = 1.0f, UINT8 stencil = 0);

		//--------------------------------------------------------------------------
		//   テクスチャ配列の複数DepthStencilのうち、１つだけをクリア
		//  テクスチャ配列で作成したDepthStencilの、指定番目のものだけをクリアできる。
		//  テクスチャ配列として作成していなければこの関数は意味ないです。
		//  ※DepthStencilViewを作成する必要があります
		//   bDepth		… Zバッファクリアする？
		//   bStencil		… ステンシルバッファクリアする？
		//   depth		… クリアするZバッファの値
		//   stencil		… クリアするステンシルバッファの値
		//--------------------------------------------------------------------------
		void ClearDepthArray(int idx, bool bDepth = true, bool bStencil = true, FLOAT depth = 1.0f, UINT8 stencil = 0);

		//===================================================================
		// 取得・設定
		//===================================================================

		//   現在読み込んでいるファイル名取得
		const ZString&			GetFileName() { return m_strFileName; }

		//   テクスチャリソース取得(※戻り値のビューの参照カウンタはAddRefしません)
		ID3D11Texture2D*			GetTex2D() { return m_pTexture2D; }

		//   ShaderResourceビュー取得(※戻り値のビューの参照カウンタはAddRefしません)
		ID3D11ShaderResourceView*	GetTex() { return m_pTexView; }

		//   テクスチャ配列用 ShaderResourceビュー取得(※戻り値のビューの参照カウンタはAddRefしません)
		//  ※テクスチャ配列として作成しない場合のみ使用可能
		ID3D11ShaderResourceView*	GetTexArray(int no) { return m_pTexViewArray[no]; }



		//   RenderTargetビュー取得(※戻り値のビューの参照カウンタはAddRefしていません)
		ID3D11RenderTargetView*		GetRTTex() { return m_pTexRTView; }

		//   テクスチャ配列用 RenderTargetビュー取得(※戻り値のビューの参照カウンタはAddRefしません)
		//  ※テクスチャ配列として作成しない場合のみ使用可能
		ID3D11RenderTargetView*		GetRTTexArray(int no) { return m_pTexRTViewArray[no]; }



		//   UnorderedAccessView取得(※戻り値のビューの参照カウンタはAddRefしません)
		ID3D11UnorderedAccessView*	GetUATex() { return m_pTexUAView; }



		//   DepthStencilビュー取得(※戻り値のビューの参照カウンタはAddRefしません)
		ID3D11DepthStencilView*		GetDepthTex() { return m_pTexDepthView; }

		//   テクスチャ配列用 DepthStencilビュー取得(※戻り値のビューの参照カウンタはAddRefしません)
		//  ※テクスチャ配列として作成しない場合のみ使用可能
		ID3D11DepthStencilView*		GetDepthTexArray(int no) { return m_pTexDSViewArray[no]; }

		//   テクスチャ配列の場合、その配列数を取得
		int							GetArraySize() { return (int)m_pTexViewArray.size(); }


		//   画像情報取得
		const D3D11_TEXTURE2D_DESC&	GetInfo() const { return m_Desc; }		// 情報

		//   画像の幅・高のビューポートに変更する
		void SetViewport() const;

		//===================================================================
		// 各シェーダーステージにテクスチャをセット
		//  SlotNo	… HLSL側のregister(t0)の番号にあたる　範囲は0〜127
		//===================================================================

		//   ピクセルシェーダステージにテクスチャをセットする
		void SetTexturePS(UINT SlotNo) const;
		
		//   頂点シェーダステージにテクスチャをセットする
		void SetTextureVS(UINT SlotNo) const;

		//   ジオメトリシェーダステージにテクスチャをセットする
		void SetTextureGS(UINT SlotNo) const;
		
		//   コンピュートシェーダステージにテクスチャをセットする
		void SetTextureCS(UINT SlotNo) const;
		
		//   UAVをセットする
		void SetUavCS(UINT SlotNo) const;
		
		//===================================================================
		// その他
		//===================================================================

		//   現在の画像のミップマップチェーンを作成する
		//  ※描画系になるので、別の描画スレッドで並列実行しないこと
		bool GenerateMips(int mipLevels = 0);

		//   リソース情報
		virtual ZString GetTypeName() const override
		{
			ZString str = "ZTexture : ";
			str += m_strFileName;
			return str;
		}

	private:
		ZString					m_strFileName;		// ファイル名(LoadTexture()した場合のみ)

		ID3D11Texture2D*			m_pTexture2D;		// テクスチャリソース(画像データ本体)

		ID3D11ShaderResourceView*	m_pTexView;			// シェーダリソースビュー(m_pTexture2Dから色を読み込むためのもの)
		ID3D11RenderTargetView*		m_pTexRTView;		// レンダーターゲットビュー(m_pTexture2Dへ書き込みを行うためのもの)
		ID3D11UnorderedAccessView*	m_pTexUAView;		// アンオーダードアクセスビュー
		ID3D11DepthStencilView*		m_pTexDepthView;	// 深度ステンシルビュー(m_pTexture2DをZバッファとして書き込むためのもの)

		//  SRVテクスチャ配列用
		ZAVector<ID3D11ShaderResourceView*>	m_pTexViewArray;
		//  SRVテクスチャ配列解放
		void ClearSRVArray()
		{
			for (auto& val : m_pTexViewArray)
			{
				Safe_Release(val);
			}
			m_pTexViewArray.clear();
			m_pTexViewArray.shrink_to_fit();
		}

		//  RTVテクスチャ配列用
		ZAVector<ID3D11RenderTargetView*>	m_pTexRTViewArray;
		//  RTVテクスチャ配列解放
		void ClearRTVArray()
		{
			for (auto& val : m_pTexRTViewArray)
			{
				Safe_Release(val);
			}
			m_pTexRTViewArray.clear();
			m_pTexRTViewArray.shrink_to_fit();
		}

		//  DSVテクスチャ配列用
		ZAVector<ID3D11DepthStencilView*>	m_pTexDSViewArray;
		//  DSVテクスチャ配列解放
		void ClearDSVArray()
		{
			for (auto& val : m_pTexDSViewArray)
			{
				Safe_Release(val);
			}
			m_pTexDSViewArray.clear();
			m_pTexDSViewArray.shrink_to_fit();
		}

		D3D11_TEXTURE2D_DESC		m_Desc;				// 情報

	private:
		// コピー禁止用
		ZTexture(const ZTexture& src) {}
		void operator=(const ZTexture& src) {}
	};

	// formatメモ
	// _FLOAT … 浮動小数点型。floatみたいな。
	// _SINT  … 符号付き整数型。intみたいな。
	// _UINT  … 符号なし整数型。unsigned intみたいな。
	// _SNORM … 符号付き正規化整数型。つまり-1.0〜1.0で扱われる。
	// _UNORM … 符号なし正規化整数型。つまり0.0〜1.0で扱われる。
	// _TYPELESS … 型なしデータ型。RTとかには使えないっぽい。Depthはこれじゃないといけないぽい。



	//================================================================================================
	//   テクスチャセットクラス
	//  
	//  単純に複数のテクスチャ(ZTexture)を１つの構造体にまとめたもの \n
	//  LoadTexture関数により、ファイル名をキーに一気に読み込まれる \n
	//  シェーダで高品質な表現を行うのに便利						\n
	// 																\n
	//  例)texSet.LoadTextureSet("aaa.png");		\n
	// 			↓
	// 　　・TexList[0]   … aaa.pngが読み込まれる\n
	// 　　・TexList[1]   … aaa.ext.pngが読み込まれる\n
	// 　　・TexList[2]   … aaa.ext2.pngが読み込まれる\n
	// 　　・TexList[3]   … aaa.ext3.pngが読み込まれる\n
	// 　　・TexList[4]   … aaa.ext4.pngが読み込まれる\n
	// 　　・TexList[5]   … aaa.ext5.pngが読み込まれる\n
	// 　　・TexList[6]   … aaa.ext6.pngが読み込まれる\n
	// 　　・TexList[7]   … aaa.ext7.pngが読み込まれる\n
	// 　　・TexList[8]   … aaa.ext8.pngが読み込まれる\n
	// 　　・TexList[9]   … aaa.normal.pngが読み込まれる\n
	// 　※ファイル名と拡張子の間に、上記のような名前を挿入したファイルを読み込むようになる\n
	// 
	//  @ingroup Graphics_Important
	//================================================================================================
	struct ZPMXMaterial;
	class ZTextureSet : public ZIResource
	{
	public:
		//---------------------------------------------------------------------------
		//   テクスチャセット読み込み(法線マップや拡張テクスチャなども読み込む)
		//  テクスチャの読み込みには、ZDirectXGraphicsに登録されているResourceStorageが使われます
		//   filename		… ファイル名
		//   forceLoad	… 既に読み込み済みの画像でも強制読み込み
		//---------------------------------------------------------------------------
		void LoadTextureSet(const ZString& filename, bool forceLoad = false);

		void LoadTextureSetFromPMXMaterial(const ZAVector<ZString>& texturePaths,const ZPMXMaterial& mate, bool forceLoad = false);

		//   解放
		void Release()
		{
			TexList.clear();
		}

		// 
		ZTextureSet()
		{
			TexList.resize(10);
		}

		//
		ZTextureSet(int size)
		{
			TexList.resize(size);
		}

		// 
		ZTextureSet(const ZSP<ZTexture>& src)
		{
			*this = src;
		}

		virtual ~ZTextureSet()
		{
			Release();
		}

		//   ZTextureのshared_ptrからコピーしてくる。srcはTexList[0]に格納される。
		//  MeshTex以外のメンバは、空のZTextureになる
		void operator=(const ZSP<ZTexture>& src)
		{
			Release();
			TexList.resize(1);
			TexList[0] = src;
		}

		//   idx番目のテクスチャを取得(速度重視の生ポインタを取得版)
		//  idxにテクスチャが存在しない場合はnullptrが返る
		ZTexture* GetTex(UINT idx) const
		{
			if (idx >= TexList.size())return nullptr;
			return TexList[idx].GetPtr();
		}

		//   idx番目のテクスチャを取得
		//  idxにテクスチャが存在しない場合はnullptrが返る
		ZSP<ZTexture> GetTexZSP(UINT idx) const
		{
			if (idx >= TexList.size())return nullptr;
			return TexList[idx];
		}

		//   テクスチャをセット
		//  	idx		… セットするインデックス番号
		//  	tex		… セットするテクスチャ
		//  @return セットする前のテクスチャ
		ZSP<ZTexture> SetTex(UINT idx, const ZSP<ZTexture>& tex)
		{
			// サイズが足りない時は、拡張する
			if (idx >= TexList.size())
			{
				TexList.resize(idx + 1);
			}
			auto oldTex = TexList[idx];
			// セット
			TexList[idx] = tex;

			return oldTex;
		}






		//	テクスチャのクリア
		void AllClearRT(ZVec4& col = ZVec4(0, 0, 0, 0))
		{
			for (auto& rt : TexList) {
				rt->ClearRT(col);
			}
		}

		//	リストのサイズを返す(デフォルトコンストラクタを走らせると正しい結果が得られないので注意)
		int GetListSize()
		{
			return TexList.size();
		}

		//	まとめてレンダーターゲットを作成
		void CreateRTSet(int w, int h, int cnt, DXGI_FORMAT* fmt=nullptr)
		{
			TexList.resize(cnt);
			for (int i = 0; i < cnt; i++)
			{
				TexList[i]= Make_Shared(ZTexture,appnew);
				if (!fmt)
				{
					TexList[i]->CreateRT(w, h);
				}
				else
				{
					TexList[i]->CreateRT(w, h, fmt[i]);
				}
			}
		}


		//
		virtual ZString GetTypeName() const override
		{
			return "ZTextureSet";
		}

	private:
		// テクスチャ配列
		ZAVector<ZSP<ZTexture>>	TexList;

	};

}

#endif
