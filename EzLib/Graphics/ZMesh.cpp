#include "PCH/pch.h"
#include "ZMesh.h"

using namespace EzLib;

//=======================================================================
//
// ZMesh
//
//=======================================================================

const ZMeshSubset* ZMesh::GetSubset(int subsetNo) const
{
	if (subsetNo >= (int)m_Subset.size())return nullptr;

	return &m_Subset[subsetNo];
}

void ZMesh::SetDrawData()
{
	// 頂点バッファーをセット
	UINT stride = m_VertexTypeData.ByteSize;
	UINT offset = 0;
	ZDx.GetDevContext()->IASetVertexBuffers(0, 1, &m_VB, &stride, &offset);

	// インデックスバッファセット
	ZDx.GetDevContext()->IASetIndexBuffer(m_IB, DXGI_FORMAT_R32_UINT, 0);

	//プリミティブ・トポロジーをセット
	ZDx.GetDevContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

}

void ZMesh::SetPrimitiveTopology()
{
	//プリミティブ・トポロジーをセット
	ZDx.GetDevContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void ZMesh::DrawSubset(int subsetNo)
{
	if (subsetNo >= (int)m_Subset.size())return;
	if (m_Subset[subsetNo].FaceCount == 0)return;

	// メッシュ描画
	ZDx.GetDevContext()->DrawIndexed(m_Subset[subsetNo].FaceCount* 3, m_Subset[subsetNo].FaceStart* 3, 0);
}

void ZMesh::DrawSubsetInstance(int subsetNo, int instanceNum)
{
	if (subsetNo >= (int)m_Subset.size())return;
	if (m_Subset[subsetNo].FaceCount == 0)return;

	// メッシュ描画
	ZDx.GetDevContext()->DrawIndexedInstanced(m_Subset[subsetNo].FaceCount * 3, instanceNum, m_Subset[subsetNo].FaceStart * 3, 0,0);
}

void ZMesh::Release()
{
	m_VertexBuf.clear();
	m_VertexBuf.shrink_to_fit();

	m_Faces.clear();
	m_Faces.shrink_to_fit();

	m_ExtFaces.clear();
	m_ExtFaces.shrink_to_fit();

	Safe_Release(m_VB);
	Safe_Release(m_IB);
	m_Subset.clear();

	ZeroMemory(&m_VertexTypeData, sizeof(m_VertexTypeData));
}

bool ZMesh::CreateVertexBuffer()
{
	if (m_VertexBuf.size() == 0)return false;

	Safe_Release(m_VB);

	// 頂点バッファ作成
	D3D11_BUFFER_DESC bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = m_VertexBuf.size();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = D3D11_RESOURCE_MISC_DRAWINDIRECT_ARGS;
	bd.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = &m_VertexBuf[0];
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	if (FAILED(ZDx.GetDev()->CreateBuffer(&bd, &InitData, &m_VB)))
	{
		return false;
	}

	return true;
}

bool ZMesh::SetFaceArray(const Face* faceArray, unsigned int numFace, bool bCreateIndexBuffer)
{
	m_Faces.resize(numFace);
	m_Faces.shrink_to_fit();

	// 面情報コピー
	memcpy(&m_Faces[0], faceArray, numFace* sizeof(Face));

	// 
	m_ExtFaces.clear();

	// インデックスバッファ作成
	if (bCreateIndexBuffer)
	{
		return CreateIndexBuffer();
	}
	return true;
}


bool ZMesh::CreateIndexBuffer()
{
	if (m_Faces.size() == 0)return false;

	Safe_Release(m_IB);

	D3D11_BUFFER_DESC bd;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = m_Faces.size()* sizeof(Face);
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = &m_Faces[0];
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;
	if (FAILED(ZDx.GetDev()->CreateBuffer(&bd, &InitData, &m_IB)))
	{
		return false;
	}

	return true;
}

bool ZMesh::CreateExtData()
{
	if (m_Faces.size() == 0 || m_VertexBuf.size() == 0)
	{
		m_AABB_Center.Set(0, 0, 0);
		m_AABB_HalfSize.Set(0, 0, 0);
		m_vBSCenter.Set(0, 0, 0);
		m_fBSRadius = 0;
		m_ExtFaces.clear();
		return false;
	}

	// 面拡張データ作成
	m_ExtFaces.resize(m_Faces.size());
	m_ExtFaces.shrink_to_fit();
	for (UINT i = 0; i < m_ExtFaces.size(); i++)
	{
		// 頂点座標
		const ZVec3& vp0 = GetVertex_Pos(i, 0);
		const ZVec3& vp1 = GetVertex_Pos(i, 1);
		const ZVec3& vp2 = GetVertex_Pos(i, 2);

		// 面の方向
		ZVec3 v1 = vp1 - vp0;
		ZVec3 v2 = vp2 - vp0;
		ZVec3::Cross(m_ExtFaces[i].vN, v1, v2);	// 外積
		m_ExtFaces[i].vN.Normalize();

		// 面のAABB
		ZVec3::CreateAABB(vp0, vp1, vp2, v1, v2);
		m_ExtFaces[i].vAAABB_HalfSize = (v2 - v1)*0.5f;
		m_ExtFaces[i].vAAABB_Center = v1 + m_ExtFaces[i].vAAABB_HalfSize;

	}

	// メッシュ自体のAABB
	ZVec3 vMin = GetVertex_Pos(0);
	ZVec3 vMax = vMin;
	for (UINT i = 1; i < GetNumVerts(); i++)
	{
		vMin.x = min(vMin.x, GetVertex_Pos(i).x);
		vMin.y = min(vMin.y, GetVertex_Pos(i).y);
		vMin.z = min(vMin.z, GetVertex_Pos(i).z);

		vMax.x = max(vMax.x, GetVertex_Pos(i).x);
		vMax.y = max(vMax.y, GetVertex_Pos(i).y);
		vMax.z = max(vMax.z, GetVertex_Pos(i).z);
	}
	m_AABB_HalfSize = (vMax - vMin)* 0.5f;
	m_AABB_Center = vMin + m_AABB_HalfSize;

	// バウンディングスフィア(現在簡易的なものです)
	ZVec3 v = vMax - vMin;
	m_vBSCenter = vMin + v* 0.5f;
	m_fBSRadius = v.Length() / 2;

	// 8分木はそこそこメモリを使うので衝突判定に使わないメッシュは生成しないようにした方がいいかも
	// 8分木初期化
	{
		const int splitLevel = 5; // 分割レベル これ以上だと当たり判定が何故か不安定になる
		ZVec3 bias{ 1.f }; // バイアスを少しかけてメッシュより少し大きいAABBにする
		m_FaceOctree.Init(splitLevel, ZAABB(vMin-bias, vMax+bias));
	}

	// 面をすべて8分木に登録
	{
		auto numFace = m_Faces.size();
		for (int i = 0; i < numFace; i++)
		{
			// 面の3頂点からAABBを算出し8分木に登録
			ZOperationalVec3 pos[3];
			GetFaceVertices_Pos(i, pos);
			ZAABB faceAABB;
			ZVec3::CreateAABB({ pos[0] }, { pos[1] }, { pos[2] }, faceAABB.Min, faceAABB.Max);
			m_FaceOctree.RegisterObject(faceAABB, &m_Faces[i]);
		}
	}

	return true;
}

ZMesh::ZMesh()
{
}


