#include "PCH/pch.h"
#include "ZTexture.h"
#include <iomanip>

using namespace Microsoft::WRL;

using namespace EzLib;

bool ZTexture::LoadTexture(const ZString& filename)
{
	Release();
	if (filename.empty())return false;

	ZIMAGE_LOAD_INFO info;
	info.ShaderResource = true;
	info.RenderTarget = false;
	info.DepthStencil = false;
	info.MipLevels = 0;

	return LoadTextureEx(filename, &info);
}

bool ZTexture::LoadTextureEx(const ZString& filename, ZIMAGE_LOAD_INFO* loadInfo)
{
	Release();
	if (filename.empty())return false;

	// wchar変換
	int len = strlen(filename.c_str());


	ZWString wStr = ConvertStringToWString(filename);

	// Bind Flags
	UINT bindFlags = 0;
	if (loadInfo->ShaderResource)bindFlags |= D3D11_BIND_SHADER_RESOURCE;
	if (loadInfo->RenderTarget)bindFlags |= D3D11_BIND_RENDER_TARGET;
	if (loadInfo->DepthStencil)bindFlags |= D3D11_BIND_DEPTH_STENCIL;

	DirectX::TexMetadata meta;
	DirectX::ScratchImage image;

	bool bLoaded = false;
	// WIC読み込み
	//  WIC_FLAGS_ALL_FRAMES … gifアニメなどの複数フレームを読み込んでくれる
	if (SUCCEEDED(DirectX::LoadFromWICFile(wStr.c_str(), DirectX::WIC_FLAGS_ALL_FRAMES, &meta, image)))
	{
		bLoaded = true;
	}

	// DDS読み込み
	if (bLoaded == false)
	{
		if (SUCCEEDED(DirectX::LoadFromDDSFile(wStr.c_str(), DirectX::DDS_FLAGS_NONE, &meta, image)))
		{
			ZVec4* vv = (ZVec4*)image.GetImage(0, 0, 0)->pixels;
			bLoaded = true;
		}
	}

	// TGA読み込み
	if (bLoaded == false)
	{
		if (SUCCEEDED(DirectX::LoadFromTGAFile(wStr.c_str(), &meta, image)))
		{
			bLoaded = true;
		}
	}

	// HDR読み込み
	if (bLoaded == false)
	{
		if (SUCCEEDED(DirectX::LoadFromHDRFile(wStr.c_str(), &meta, image)))
		{
			bLoaded = true;
		}
	}

	// 失敗
	if (bLoaded == false)
	{
		Release();
		return false;
	}

	// ミップマップ生成
	if (meta.mipLevels == 1 && loadInfo->MipLevels != 1)
	{
		DirectX::ScratchImage mipChain;
		if (FAILED(DirectX::GenerateMipMaps(image.GetImages(), image.GetImageCount(), image.GetMetadata(), DirectX::TEX_FILTER_DEFAULT, loadInfo->MipLevels, mipChain)))
		{
			return false;
		}
		image.Release();

		image = std::move(mipChain);
	}

	// ファイル名記憶
	m_strFileName = filename;

	// テクスチャリソース作成

	D3D11_USAGE usage = D3D11_USAGE_DEFAULT;
	UINT cpuAccessFlags = 0;
	if (loadInfo && loadInfo->Dynamic)
	{	// 動的テクスチャ
		usage = D3D11_USAGE_DYNAMIC;
		cpuAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}

	if (FAILED(DirectX::CreateTextureEx(ZDx.GetDev(),
		image.GetImages(),
		image.GetImageCount(),
		image.GetMetadata(),
		usage,						// Usage
		bindFlags,					// Bind Flags
		cpuAccessFlags,				// CPU Access Flags
		0,							// MiscFlags
		false,						// ForceSRGB
		(ID3D11Resource**)&m_pTexture2D)
		))
	{
		Release();
		return false;
	}


	// m_pTexture2Dから、各ビューを作成する
	if (CreateViewsFromTexture2D() == false)
	{
		Release();
		return false;
	}


	return true;
}


bool ZTexture::Create(int w, int h, DXGI_FORMAT format, const D3D11_SUBRESOURCE_DATA* fillData, bool isDynamic)
{
	Release();

	// 2Dテクスチャの設定
	memset(&m_Desc, 0, sizeof(m_Desc));
	m_Desc.Format = format;
	m_Desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	m_Desc.Width = (UINT)w;
	m_Desc.Height = (UINT)h;
	m_Desc.MipLevels = 1;
	m_Desc.ArraySize = 1;
	m_Desc.MiscFlags = 0;
	m_Desc.SampleDesc.Count = 1;
	m_Desc.SampleDesc.Quality = 0;

	if (isDynamic)
	{
		m_Desc.Usage = D3D11_USAGE_DYNAMIC;
		m_Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		m_Desc.Usage = D3D11_USAGE_DEFAULT;
		m_Desc.CPUAccessFlags = 0;
	}


	// 2Dテクスチャの生成
	HRESULT hr = ZDx.GetDev()->CreateTexture2D(&m_Desc, fillData, &m_pTexture2D);
	if (FAILED(hr))
	{
		Release();
		return false;
	}

	// 
	m_strFileName = "";

	// m_pTexture2Dから、各ビューを作成する
	if (CreateViewsFromTexture2D() == false)
	{
		Release();
		return false;
	}

	return true;

}

bool ZTexture::CreateRT(int w, int h, DXGI_FORMAT format, UINT arrayCnt, const D3D11_SUBRESOURCE_DATA* fillData)
{
	Release();

	// 2Dテクスチャの設定
	memset(&m_Desc, 0, sizeof(m_Desc));
	m_Desc.Usage = D3D11_USAGE_DEFAULT;
	m_Desc.Format = format;
	m_Desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	m_Desc.Width = (UINT)w;
	m_Desc.Height = (UINT)h;
	m_Desc.CPUAccessFlags = 0;
	m_Desc.MipLevels = 1;
	m_Desc.ArraySize = arrayCnt;
	//	m_Desc.SampleDesc.Count = 1;
	//	m_Desc.SampleDesc.Quality = 0;
		// MSAA(マルチサンプル アンチエイリアシング)は、バックバッファの設定と合わせる
	m_Desc.SampleDesc = ZDx.GetBackBuffer()->GetInfo().SampleDesc;


	// 2Dテクスチャの生成
	HRESULT hr = ZDx.GetDev()->CreateTexture2D(&m_Desc, fillData, &m_pTexture2D);
	if (FAILED(hr))
	{
		Release();
		return false;
	}
	
	// 
	m_strFileName = "";

	// m_pTexture2Dから、各ビューを作成する
	if (CreateViewsFromTexture2D() == false)
	{
		Release();
		return false;
	}

	return true;

}

bool ZTexture::CreateCubeRT(int w, int h, DXGI_FORMAT format)
{
	Release();

	// 2Dテクスチャの設定
	memset(&m_Desc, 0, sizeof(m_Desc));
	m_Desc.Usage = D3D11_USAGE_DEFAULT;
	m_Desc.Format = format;
	m_Desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	m_Desc.Width = (UINT)w;
	m_Desc.Height = (UINT)h;
	m_Desc.CPUAccessFlags = 0;
	m_Desc.MipLevels = 0;
	m_Desc.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;	// キューブマップ
	m_Desc.ArraySize = 6;		// 6面必要なので、テクスチャ配列6枚分
	//	m_Desc.SampleDesc.Count = 1;
	//	m_Desc.SampleDesc.Quality = 0;
	// MSAA(マルチサンプル アンチエイリアシング)は、バックバッファの設定と合わせる
	m_Desc.SampleDesc = ZDx.GetBackBuffer()->GetInfo().SampleDesc;

	// 2Dテクスチャの生成
	HRESULT hr = ZDx.GetDev()->CreateTexture2D(&m_Desc, nullptr, &m_pTexture2D);
	if (FAILED(hr))
	{
		Release();
		return false;
	}

	// 
	m_strFileName = "";

	// m_pTexture2Dから、各ビューを作成する
	if (CreateViewsFromTexture2D() == false)
	{
		Release();
		return false;
	}

	return true;
}

bool ZTexture::CreateUAV(int w, int h, DXGI_FORMAT format, UINT arrayCnt, const D3D11_SUBRESOURCE_DATA* fillData)
{
	Release();

	// 2Dテクスチャの設定
	memset(&m_Desc, 0, sizeof(m_Desc));
	m_Desc.Usage = D3D11_USAGE_DEFAULT;
	m_Desc.Format = format;
	m_Desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	m_Desc.Width = (UINT)w;
	m_Desc.Height = (UINT)h;
	m_Desc.CPUAccessFlags = 0;
	m_Desc.MipLevels = 1;
	m_Desc.ArraySize = arrayCnt;
	//m_Desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS;	// D3D11_RESOURCE_MISC_BUFFER_STRUCTURED
	// m_Desc.StructureByteStride = sizeof(a); // RWStructuredBufferなら構造体のサイズを指定する
	// MSAA(マルチサンプル アンチエイリアシング)は、バックバッファの設定と合わせる
	m_Desc.SampleDesc = ZDx.GetBackBuffer()->GetInfo().SampleDesc;

	// 2Dテクスチャの生成
	HRESULT hr = ZDx.GetDev()->CreateTexture2D(&m_Desc, fillData, &m_pTexture2D);
	if (FAILED(hr))
	{
		Release();
		return false;
	}

	// 
	m_strFileName = "";

	// m_pTexture2Dから、各ビューを作成する
	if (CreateViewsFromTexture2D() == false)
	{
		Release();
		return false;
	}


	return true;

}

bool ZTexture::CreateDepth(int w, int h, DXGI_FORMAT format, UINT arrayCnt, const D3D11_SUBRESOURCE_DATA* fillData)
{
	Release();

	// 2Dテクスチャの設定
	memset(&m_Desc, 0, sizeof(m_Desc));
	m_Desc.Usage = D3D11_USAGE_DEFAULT;
	m_Desc.Format = format;
	m_Desc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	m_Desc.Width = (UINT)w;
	m_Desc.Height = (UINT)h;
	m_Desc.CPUAccessFlags = 0;
	m_Desc.MipLevels = 1;
	m_Desc.ArraySize = arrayCnt;
	//	m_Desc.SampleDesc.Count = 1;
	//	m_Desc.SampleDesc.Quality = 0;
		// MSAA(マルチサンプル アンチエイリアシング)は、バックバッファの設定と合わせる
	m_Desc.SampleDesc = ZDx.GetDepthStencil()->GetInfo().SampleDesc;

	// 2Dテクスチャの生成
	HRESULT hr = ZDx.GetDev()->CreateTexture2D(&m_Desc, fillData, &m_pTexture2D);
	if (FAILED(hr))
	{
		Release();
		return false;
	}

	// 
	m_strFileName = "";

	// m_pTexture2Dから、各ビューを作成する
	if (CreateViewsFromTexture2D() == false)
	{
		Release();
		return false;
	}


	return true;

}

bool ZTexture::CreateFromTexture2D(ID3D11Texture2D* pTexture2D)
{
	Release();
	if (pTexture2D == nullptr)return false;

	// 
	m_strFileName = "";

	// 参照カウンタ増加
	pTexture2D->AddRef();
	// アドレスを記憶
	m_pTexture2D = pTexture2D;
	// テクスチャの情報も取得
//	m_pTexture2D->GetDesc(&m_Desc);

	// m_pTexture2Dから、各ビューを作成する
	if (CreateViewsFromTexture2D() == false)
	{
		Release();
		return false;
	}


	return true;
}

bool ZTexture::CreateViewsFromTexture2D()
{
	if (m_pTexture2D == nullptr)return false;

	// テクスチャ本体の情報取得
	m_pTexture2D->GetDesc(&m_Desc);

	//===========================================================
	// RenderTargetViewを作成する
	//===========================================================
	Safe_Release(m_pTexRTView);	// 既にあるなら解放
	ClearRTVArray();
	// レンダーターゲットフラグがついてる時
	if (m_Desc.BindFlags& D3D11_BIND_RENDER_TARGET)
	{
		// 作成するビューの設定
		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		ZeroMemory(&rtvDesc, sizeof(rtvDesc));
		rtvDesc.Format = m_Desc.Format;
		// 単品のテクスチャ(通常テクスチャ)の場合
		if (m_Desc.ArraySize == 1)
		{
			if (m_Desc.SampleDesc.Count == 1)
			{
				rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
			}
			else
			{
				rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DMS;
			}
		}
		// テクスチャ配列の場合
		else if (m_Desc.ArraySize > 1)
		{
			if (m_Desc.SampleDesc.Count == 1)
			{
				rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
				rtvDesc.Texture2DArray.ArraySize = m_Desc.ArraySize;
				rtvDesc.Texture2DArray.FirstArraySlice = 0;
				rtvDesc.Texture2DArray.MipSlice = 0;
			}
			else
			{
				rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DMSARRAY;
				rtvDesc.Texture2DMSArray.ArraySize = m_Desc.ArraySize;
				rtvDesc.Texture2DMSArray.FirstArraySlice = 0;
			}
		}
		// レンダーターゲットビュー作成
		HRESULT hr = ZDx.GetDev()->CreateRenderTargetView(m_pTexture2D, &rtvDesc, &m_pTexRTView);
		if (FAILED(hr))
		{
			Release();
			return false;
		}

		// テクスチャ配列なら単品アクセス用にビューも複数つくっとこ(配列の要素１つ１つのビュー　アクセスしやすくするため)
		if (m_Desc.ArraySize > 1)
		{
			m_pTexRTViewArray.resize(m_Desc.ArraySize);
			for (UINT i = 0; i < m_Desc.ArraySize; i++)
			{

				if (m_Desc.SampleDesc.Count == 1)
				{
					rtvDesc.Texture2DArray.ArraySize = 1;			// 単品用なので1
					rtvDesc.Texture2DArray.FirstArraySlice = i;		// ここが配列のインデックスになる
				}
				else
				{
					rtvDesc.Texture2DMSArray.ArraySize = 1;			// 単品用なので1
					rtvDesc.Texture2DMSArray.FirstArraySlice = i;		// ここが配列のインデックスになる
				}

				HRESULT hr = ZDx.GetDev()->CreateRenderTargetView(m_pTexture2D, &rtvDesc, &m_pTexRTViewArray[i]);
				if (FAILED(hr))
				{
					Release();
					return false;
				}
			}
		}
	}

	//===========================================================
	// ShaderResourceViewの情報を作成する
	//===========================================================
	Safe_Release(m_pTexView);	// 既にあるなら解放
	ClearSRVArray();
	// シェーダリソースビューフラグがついてる時
	if (m_Desc.BindFlags& D3D11_BIND_SHADER_RESOURCE)
	{
		// 作成するビューの設定
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		ZeroMemory(&srvDesc, sizeof(srvDesc));

		// テクスチャがZバッファの場合は、最適なフォーマットにする
		if (m_Desc.BindFlags& D3D11_BIND_DEPTH_STENCIL)
		{
			switch (m_Desc.Format)
			{
				// 8ビットフォーマットは使用できない？
				case DXGI_FORMAT_R8_TYPELESS:
					srvDesc.Format = DXGI_FORMAT_R8_UNORM;
					break;
					// 16ビット
				case DXGI_FORMAT_R16_TYPELESS:
					srvDesc.Format = DXGI_FORMAT_R16_UNORM;
					break;
					// 32ビット
				case DXGI_FORMAT_R32_TYPELESS:
					srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
					break;
					// 24ビット(Zバッファ) + 8ビット(ステンシルバッファ) 
				case DXGI_FORMAT_R24G8_TYPELESS:
					srvDesc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
					break;
				default:
					srvDesc.Format = m_Desc.Format;
					break;
			}
		}
		// Zバッファでない場合は、そのまま同じフォーマットを使用
		else
		{
			srvDesc.Format = m_Desc.Format;
		}

		// 単品のテクスチャ(通常テクスチャ)の場合
		if (m_Desc.ArraySize == 1)
		{
			if (m_Desc.SampleDesc.Count == 1)
			{
				srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				srvDesc.Texture2D.MostDetailedMip = 0;
				srvDesc.Texture2D.MipLevels = m_Desc.MipLevels;
				if (srvDesc.Texture2D.MipLevels <= 0)srvDesc.Texture2D.MipLevels = -1;
			}
			else
			{
				srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
				srvDesc.Texture2DMS.UnusedField_NothingToDefine = 0;
			}
		}
		// テクスチャ配列の場合
		else
		{
			if (m_Desc.SampleDesc.Count == 1)
			{
				if (m_Desc.MiscFlags& D3D11_RESOURCE_MISC_TEXTURECUBE)
				{
					srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
				}
				else
				{
					srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
				}
				srvDesc.Texture2DArray.MostDetailedMip = 0;
				srvDesc.Texture2DArray.MipLevels = m_Desc.MipLevels;
				srvDesc.Texture2DArray.ArraySize = m_Desc.ArraySize;
				srvDesc.Texture2DArray.FirstArraySlice = 0;
			}
			else
			{
				srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMSARRAY;
				srvDesc.Texture2DMSArray.ArraySize = m_Desc.ArraySize;
				srvDesc.Texture2DMSArray.FirstArraySlice = 0;
			}
		}

		// シェーダリソースビュー作成
		HRESULT hr = ZDx.GetDev()->CreateShaderResourceView(m_pTexture2D, &srvDesc, &m_pTexView);
		if (FAILED(hr))
		{
			Release();
			return false;
		}

		// テクスチャ配列なら単品アクセス用にビューも複数つくっとこ(配列の要素１つ１つのビュー　アクセスしやすくするため)
		if (m_Desc.ArraySize > 1)
		{
			m_pTexViewArray.resize(m_Desc.ArraySize);
			for (UINT i = 0; i < m_Desc.ArraySize; i++)
			{

				if (m_Desc.SampleDesc.Count == 1)
				{
					srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;	// CubeMap時でも単品はこれで。D3D11_SRV_DIMENSION_TEXTURECUBEだとうまくいかない
					srvDesc.Texture2DArray.ArraySize = 1;			// 単品用なので1
					srvDesc.Texture2DArray.FirstArraySlice = i;		// ここが配列のインデックスになる
				}
				else
				{
					srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMSARRAY;
					srvDesc.Texture2DMSArray.ArraySize = 1;			// 単品用なので1
					srvDesc.Texture2DMSArray.FirstArraySlice = i;		// ここが配列のインデックスになる
				}

				HRESULT hr = ZDx.GetDev()->CreateShaderResourceView(m_pTexture2D, &srvDesc, &m_pTexViewArray[i]);
				if (FAILED(hr))
				{
					Release();
					return false;
				}
			}
		}

	}

	//===========================================================
	// Unordered Access Viewを作成する
	//===========================================================
	Safe_Release(m_pTexUAView);
	if (m_Desc.BindFlags& D3D11_BIND_UNORDERED_ACCESS)
	{
		/*
		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
		ZeroMemory(&uavDesc, sizeof(uavDesc));
		uavDesc.Format = DXGI_FORMAT_R32_TYPELESS;
		uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;// D3D11_UAV_DIMENSION_BUFFER;
		uavDesc.Buffer.FirstElement = 0;
		uavDesc.Buffer.NumElements = 0;
		uavDesc.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_RAW;
		*/

		HRESULT hr = ZDx.GetDev()->CreateUnorderedAccessView(m_pTexture2D, nullptr, &m_pTexUAView);
		if (FAILED(hr))
		{
			Release();
			return false;
		}
	}

	//===========================================================
	// DepthStencilViewを作成する
	//===========================================================
	Safe_Release(m_pTexDepthView);	// 既にあるなら解放
	ClearDSVArray();
	// Zバッファフラグがついてるとき
	if (m_Desc.BindFlags& D3D11_BIND_DEPTH_STENCIL)
	{
		// 作成するビューの設定
		CD3D11_DEPTH_STENCIL_VIEW_DESC dpDesc;
		ZeroMemory(&dpDesc, sizeof(dpDesc));

		// テクスチャー作成時に指定したフォーマットと互換性があり、深度ステンシルビューとして指定できるフォーマットを指定する
		switch (m_Desc.Format)
		{
			// 8ビットフォーマットは使用できない？
			case DXGI_FORMAT_R8_TYPELESS:
				dpDesc.Format = DXGI_FORMAT_R8_UNORM;
				break;
				// 16ビット
			case DXGI_FORMAT_R16_TYPELESS:
				dpDesc.Format = DXGI_FORMAT_D16_UNORM;
				break;
				// 32ビット
			case DXGI_FORMAT_R32_TYPELESS:
				dpDesc.Format = DXGI_FORMAT_D32_FLOAT;
				break;
				// 24ビット(Zバッファ) + 8ビット(ステンシルバッファ) 
			case DXGI_FORMAT_R24G8_TYPELESS:
				dpDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
				break;
			default:
				dpDesc.Format = m_Desc.Format;
				break;
		}

		// 単品のテクスチャ(通常テクスチャ)の場合
		if (m_Desc.ArraySize == 1)
		{
			if (m_Desc.SampleDesc.Count == 1)
			{
				dpDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
				dpDesc.Texture2D.MipSlice = 0;
			}
			else
			{
				dpDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
				dpDesc.Texture2DMS.UnusedField_NothingToDefine = 0;
			}
			//			dpDesc.ViewDimension = (m_Desc.SampleDesc.Count == 1 ? D3D11_DSV_DIMENSION_TEXTURE2D : D3D11_DSV_DIMENSION_TEXTURE2DMS);
		}
		// テクスチャ配列の場合
		else
		{
			if (m_Desc.SampleDesc.Count == 1)
			{
				dpDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
				dpDesc.Texture2DArray.ArraySize = m_Desc.ArraySize;
				dpDesc.Texture2DArray.MipSlice = 0;
				dpDesc.Texture2DArray.FirstArraySlice = 0;
			}
			else
			{
				dpDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMSARRAY;
				dpDesc.Texture2DMSArray.ArraySize = m_Desc.ArraySize;
				dpDesc.Texture2DMSArray.FirstArraySlice = 0;
			}
			//			dpDesc.ViewDimension = (m_Desc.SampleDesc.Count == 1 ? D3D11_DSV_DIMENSION_TEXTURE2DARRAY : D3D11_DSV_DIMENSION_TEXTURE2DMSARRAY);
		}

		// デプスステンシルビュー作成
		HRESULT hr = ZDx.GetDev()->CreateDepthStencilView(m_pTexture2D, &dpDesc, &m_pTexDepthView);
		if (FAILED(hr))
		{
			Release();
			return false;
		}

		// テクスチャ配列なら単品アクセス用にビューも複数つくっとこ(配列の要素１つ１つのビュー　アクセスしやすくするため)
		if (m_Desc.ArraySize > 1)
		{
			m_pTexDSViewArray.resize(m_Desc.ArraySize);
			for (UINT i = 0; i < m_Desc.ArraySize; i++)
			{

				if (m_Desc.SampleDesc.Count == 1)
				{
					dpDesc.Texture2DArray.ArraySize = 1;			// 単品用なので1
					dpDesc.Texture2DArray.FirstArraySlice = i;		// ここが配列のインデックスになる
				}
				else
				{
					dpDesc.Texture2DMSArray.ArraySize = 1;			// 単品用なので1
					dpDesc.Texture2DMSArray.FirstArraySlice = i;		// ここが配列のインデックスになる
				}

				HRESULT hr = ZDx.GetDev()->CreateDepthStencilView(m_pTexture2D, &dpDesc, &m_pTexDSViewArray[i]);
				if (FAILED(hr))
				{
					Release();
					return false;
				}
			}
		}
	}

	return true;
}

void ZTexture::SetViewport() const
{

	D3D11_VIEWPORT vp;
	vp.Width = 1.0f* m_Desc.Width;
	vp.Height = 1.0f* m_Desc.Height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	ZDx.GetDevContext()->RSSetViewports(1, &vp);
}

void ZTexture::SetTexturePS(UINT SlotNo) const
{
	ZDx.GetDevContext()->PSSetShaderResources(SlotNo, 1, &m_pTexView);
}

void ZTexture::SetTextureVS(UINT SlotNo) const
{
	ZDx.GetDevContext()->VSSetShaderResources(SlotNo, 1, &m_pTexView);
}

void ZTexture::SetTextureGS(UINT SlotNo) const
{
	ZDx.GetDevContext()->GSSetShaderResources(SlotNo, 1, &m_pTexView);
}

void ZTexture::SetTextureCS(UINT SlotNo) const
{
	ZDx.GetDevContext()->CSSetShaderResources(SlotNo, 1, &m_pTexView);
}

void ZTexture::SetUavCS(UINT SlotNo) const
{
	ZDx.GetDevContext()->CSSetUnorderedAccessViews(SlotNo, 1, &m_pTexUAView, nullptr);
}

bool ZTexture::GenerateMips(int mipLevels)
{
	// CaptureTextureはDeviceContextを使用するため、マルチスレッドで動かすと落ちるので注意
	DirectX::ScratchImage image;
	if (FAILED(DirectX::CaptureTexture(ZDx.GetDev(), ZDx.GetDevContext(), m_pTexture2D, image)))
	{
		return false;
	}

	// ミップマップ生成
	DirectX::ScratchImage mipChain;
	if (FAILED(DirectX::GenerateMipMaps(image.GetImages(), image.GetImageCount(), image.GetMetadata(), DirectX::TEX_FILTER_DEFAULT, mipLevels, mipChain)))
	{
		return false;
	}

	D3D11_TEXTURE2D_DESC saveDesc = m_Desc;

	//
	Release();

	// テクスチャ作成
	if (FAILED(DirectX::CreateTexture(ZDx.GetDev(), mipChain.GetImages(), mipChain.GetImageCount(), mipChain.GetMetadata(), (ID3D11Resource**)&m_pTexture2D))
		)
	{
		Release();
		return false;
	}

	// 
	m_strFileName = "";

	// m_pTexture2Dから、各ビューを作成する
	if (CreateViewsFromTexture2D() == false)
	{
		Release();
		return false;
	}

	return true;
}

EzLib::ZTexture::ZTexture() : m_pTexture2D(0), m_pTexView(0), m_pTexRTView(0), m_pTexUAView(0), m_pTexDepthView(0)
{
	ZeroMemory(&m_Desc, sizeof(m_Desc));
}

ZTexture::~ZTexture()
{
	Release();
}

void ZTexture::Release()
{
	m_strFileName = "";
	Safe_Release(m_pTexture2D);
	Safe_Release(m_pTexView);
	Safe_Release(m_pTexRTView);
	Safe_Release(m_pTexUAView);
	Safe_Release(m_pTexDepthView);
	ClearSRVArray();
	ClearRTVArray();
	ClearDSVArray();
	memset(&m_Desc, 0, sizeof(m_Desc));
}

void ZTexture::ClearRT(const ZVec4& ColorRGBA)
{
	if (m_pTexRTView)
	{
		ZDx.GetDevContext()->ClearRenderTargetView(m_pTexRTView, &ColorRGBA.x);
	}
}

void ZTexture::ClearRTArray(int idx, const ZVec4& ColorRGBA)
{
	if (idx < (int)m_pTexRTViewArray.size())
	{
		ZDx.GetDevContext()->ClearRenderTargetView(m_pTexRTViewArray[idx], &ColorRGBA.x);
	}
}

void ZTexture::ClearDepth(bool bDepth, bool bStencil, FLOAT depth, UINT8 stencil)
{
	if (m_pTexDepthView)
	{
		UINT flags = 0;
		if (bDepth)flags |= D3D11_CLEAR_DEPTH;
		if (bStencil)flags |= D3D11_CLEAR_STENCIL;

		ZDx.GetDevContext()->ClearDepthStencilView(m_pTexDepthView, flags, depth, stencil);
	}
}

void ZTexture::ClearDepthArray(int idx, bool bDepth, bool bStencil, FLOAT depth, UINT8 stencil)
{
	if (m_pTexDepthView)
	{
		UINT flags = 0;
		if (bDepth)flags |= D3D11_CLEAR_DEPTH;
		if (bStencil)flags |= D3D11_CLEAR_STENCIL;

		ZDx.GetDevContext()->ClearDepthStencilView(m_pTexDSViewArray[idx], flags, depth, stencil);
	}
}


//=======================================================================
// ZTextureSet
//=======================================================================
void ZTextureSet::LoadTextureSet(const ZString& filename, bool forceLoad)
{
	Release();

	TexList.resize(10);

	ZResourceStorage* pResStg = ZDx.GetResStg();

	// 通常テクスチャ
	TexList[0] = pResStg->LoadTexture(filename, forceLoad);

	// 法線マップ
	ZString strFile = ConvertExtFileName(filename, "normal");
	TexList[9] = pResStg->LoadTexture(strFile, forceLoad);

	// その他テクスチャ
	strFile = ConvertExtFileName(filename, "ext");
	TexList[1] = pResStg->LoadTexture(strFile, forceLoad);

	strFile = ConvertExtFileName(filename, "ext2");
	TexList[2] = pResStg->LoadTexture(strFile, forceLoad);

	strFile = ConvertExtFileName(filename, "ext3");
	TexList[3] = pResStg->LoadTexture(strFile, forceLoad);

	strFile = ConvertExtFileName(filename, "ext4");
	TexList[4] = pResStg->LoadTexture(strFile, forceLoad);

	strFile = ConvertExtFileName(filename, "ext5");
	TexList[5] = pResStg->LoadTexture(strFile, forceLoad);

	strFile = ConvertExtFileName(filename, "ext6");
	TexList[6] = pResStg->LoadTexture(strFile, forceLoad);

	strFile = ConvertExtFileName(filename, "ext7");
	TexList[7] = pResStg->LoadTexture(strFile, forceLoad);

	strFile = ConvertExtFileName(filename, "ext8");
	TexList[8] = pResStg->LoadTexture(strFile, forceLoad);

}

void EzLib::ZTextureSet::LoadTextureSetFromPMXMaterial(const ZAVector<ZString>& texturePaths,const ZPMXMaterial& mate, bool forceLoad)
{
	Release();
	TexList.resize(10);

	ZResourceStorage* pResStg = ZDx.GetResStg();

	ZString texPath;
	if (mate.TextureIndex != -1)
	{
		texPath = ZPathUtil::Normalize(texturePaths[mate.TextureIndex]);
		if(texPath != "")
			TexList[0] = pResStg->LoadTexture(texPath, forceLoad);
	}

	texPath = "";
	if (mate.ToonTextureIndex != -1)
	{
		if (mate.ToonMode == ZPMXToonMode::Common)
		{
				ZStringStream ss;
				ss << "toon" << std::setfill('0') << std::setw(2) << (mate.ToonTextureIndex + 1) << ".bmp";
				texPath = ZPathUtil::Combine("data/Texture/MMDToon", ss.str());
		}
		else if(mate.ToonMode == ZPMXToonMode::Separate)
				texPath = ZPathUtil::Normalize(texturePaths[mate.ToonTextureIndex]);
	}
	
	if(texPath != "")
		TexList[2] = pResStg->LoadTexture(texPath, forceLoad);

	texPath = "";
	if (mate.SphereTextureIndex != -1 && mate.SphereMode != ZPMXSphereMode::None)
		texPath = ZPathUtil::Normalize(texturePaths[mate.SphereTextureIndex]);
	
	if (texPath != "")
	{
		if (mate.SphereMode == ZPMXSphereMode::Add)
			TexList[3] = pResStg->LoadTexture(texPath, forceLoad);
		else if(mate.SphereMode == ZPMXSphereMode::Mul)
			TexList[6] = pResStg->LoadTexture(texPath, forceLoad);
		else if (mate.SphereMode == ZPMXSphereMode::SubTexture)
		{
			// SphereTextureがSubTextureなら~~
		}
	}

}

