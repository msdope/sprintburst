#ifndef __ZPOLYGON_H__
#define __ZPOLYGON_H__

namespace EzLib
{

	//=============================================================
	//   単純ポリゴンメッシュクラス
	// 
	//  頂点バッファのみでインデックスバッファは使用していません。
	// 
	//  @ingroup Graphics_Model
	//=============================================================
	class ZPolygon
	{
	public:

		//   バッファの種類
		enum class BufType
		{
			None,			// なし
			Default,		// GPUからWrite○ Read○　CPUからWrite×Read×(ただしUpdateSubresource()で更新は可能)　高速
			Dynamic,		// GPUからWrite× Read○　CPUからWrite○Read×　頻繁に更新されるようなバッファはこっちの方が効率いいが、Defaultに比べたら少し速度は落ちる
		};

		//   頂点数取得
		int							GetVertexNum() { return m_NumVertex; }

		//   頂点形式情報取得
		const ZVertexTypeData&		GetVertexType() { return m_VTD; }

		//--------------------------------------------------------------------------
		//   頂点バッファ作成＆頂点データ書き込み
		//   vertices		… 頂点データバッファの先頭アドレス
		//   vertexNum	… 頂点数
		//   vtd			… 頂点レイアウトデータ
		//   isDynamic	… 動的バッファとして作成する(頻繁に更新する場合はこれがいい Map/Unmap可能)
		//--------------------------------------------------------------------------
		void Create(const void* vertices, int vertexNum, const ZVertexTypeData& vtd, bool isDynamic = true);

		//--------------------------------------------------------------------------
		//   解放
		//--------------------------------------------------------------------------
		void Release();

		//=========================================
		// データ書き込み系
		//=========================================

		//--------------------------------------------------------------------------
		//   頂点データ書き込み
		//  	vertices	… 頂点データバッファの先頭アドレス
		//  	vertexNum	… 頂点数
		//--------------------------------------------------------------------------
		void WriteVertexData(const void* vertices, int vertexNum);

		//--------------------------------------------------------------------------
		//   頂点バッファのアドレスを取得
		//   ※Create()で作成した時のみ
		//  @param[out]		pOutMappedResource	… リソースをマップした情報が返る
		//--------------------------------------------------------------------------
		bool Map(D3D11_MAPPED_SUBRESOURCE* pOutMappedResource);

		//--------------------------------------------------------------------------
		//   Map()したバッファをUnmap()する
		//--------------------------------------------------------------------------
		void Unmap();

		//=========================================
		// 描画系
		//=========================================

		//--------------------------------------------------------------------------
		//   描画に必要なデータをデバイスにセット
		//  	primitiveTopologyを省略 … Triangle Stripとしてセットする
		//--------------------------------------------------------------------------
		void SetDrawData(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

		//--------------------------------------------------------------------------
		//   描画
		//   vertexCount … 使用頂点数 0xFFFFFFFF(-1)でCreateしたときのサイズを使用
		//--------------------------------------------------------------------------
		void Draw(UINT vertexCount = 0xFFFFFFFF);

		//--------------------------------------------------------------------------
		//   頂点を書き込み、プリミティブトポロジーをセットして、描画する。
		//--------------------------------------------------------------------------
		void WriteAndDraw(const void* vertices, int vertexNum, D3D11_PRIMITIVE_TOPOLOGY primitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

		//   頂点バッファ取得
		ID3D11Buffer*		GetVB() { return m_VB; }
		
		// 
		ZPolygon();
		~ZPolygon()
		{
			Release();
		}

	private:
		ID3D11Buffer* m_VB;			// 頂点バッファ
		ZVertexTypeData	m_VTD;			// 頂点レイアウトデータ
		int					m_NumVertex;	// 頂点数

		BufType				m_TypeFlag;		// BufType
	};

	//=============================================================
	//   リング動的バッファクラス
	// 
	//  GPUのバッファをリングバッファっぽく書き込むためのクラス
	// 
	//  @ingroup Graphics_Model
	//=============================================================
	class ZRingDynamicBuffer
	{
	public:

		// ※参照カウンタは加算していないので注意
		ID3D11Buffer* GetBuffer() { return m_pBuffer; }

		//   バッファの情報を取得
		const D3D11_BUFFER_DESC& GetDesc() { return m_Desc; }

		//   現在のオフセット(Byte)を取得
		int GetOffset() { return m_nNextOffset; }

		void ResetOffset();

		int GetMaxBufferSize() { return m_MaxBufferSize; }

		//   指定サイズのデータが書き込める？
		bool IsWrite(int size);

		//--------------------------------------------------------------------------
		//   動的バッファ作成
		//   bindFlag			… 頂点バッファ、インデックスバッファなどを指定
		//   maxBufferSize	… 作成するバッファのバイト数
		//--------------------------------------------------------------------------
		bool Create(D3D11_BIND_FLAG bindFlag, int maxBufferSize);

		//--------------------------------------------------------------------------
		//   解放
		//--------------------------------------------------------------------------
		void Release();

		//--------------------------------------------------------------------------
		//   データをバッファに書き込む
		//  	data		… 書き込むデータの先頭アドレス
		//  	size		… 書き込むデータのバイト数
		//  @return バッファに書き込んだ先頭のoffset(Byte)が返る。
		//--------------------------------------------------------------------------
		int WriteData(const void* data, int size);

		ZRingDynamicBuffer()
		{
		}

		//
		~ZRingDynamicBuffer()
		{
			Release();
		}


	private:
		ID3D11Buffer* m_pBuffer = nullptr;	// バッファ

		int					m_MaxBufferSize = 0;	// バッファの最大サイズ
		int					m_nNextOffset = 0;		// 現在の書き込み位置(Byte)

		D3D11_BUFFER_DESC	m_Desc;					// 作成したバッファの情報
	
	};

	//=============================================================
	//   リング動的頂点バッファクラス
	// 
	//  頂点バッファをZRingDynamicBufferでリングバッファ化したもの
	//  メモリを多く取るが、ZPolygonより効率がよくなる。
	// 
	//  @ingroup Graphics_Model
	//=============================================================
	class ZRingDynamicVB
	{
	public:
		~ZRingDynamicVB()
		{
			Release();
		}

		//   現在のオフセット(Byte)を取得
		int GetOffset() { return m_RingBuf.GetOffset(); }

		//   指定数の頂点データが書き込める？
		bool IsWriteVertices(int vertexNum)
		{
			return m_RingBuf.IsWrite(m_VTD.ByteSize* vertexNum);
		}

		//--------------------------------------------------------------------------
		//   頂点バッファ作成＆頂点データ書き込み
		//   vtd			… 頂点レイアウトデータ
		//   maxVertexNum	… バッファの最大頂点数
		//--------------------------------------------------------------------------
		void Create(const ZVertexTypeData& vtd, UINT maxVertexNum)
		{
			m_VTD = vtd;

			m_RingBuf.Create(D3D11_BIND_VERTEX_BUFFER, m_VTD.ByteSize* maxVertexNum);
		}

		//--------------------------------------------------------------------------
		//   解放
		//--------------------------------------------------------------------------
		void Release()
		{
			m_RingBuf.Release();
		}

		//--------------------------------------------------------------------------
		//   頂点バッファのセット、プリミティブトポロジーをセット。
		//--------------------------------------------------------------------------
		void SetDrawData(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

		//--------------------------------------------------------------------------
		//   登録している頂点形式の頂点データを書き込み
		//   vertices		… 書き込む頂点の先頭アドレス
		//   vertexNum	… 書き込む頂点数
		//--------------------------------------------------------------------------
		void WriteVertexData(const void* vertices, int vertexNum);

		//--------------------------------------------------------------------------
		//   最後に書き込まれた頂点バッファのオフセットから描画
		//   vertices			… 描画する頂点の先頭アドレス
		//   startByteOffset	… startByteOffsetバイトの位置から頂点を使用する。-1だと最後に書き込まれた１つ前から。
		//--------------------------------------------------------------------------
		void Draw(int vertexNum, int startByteOffset = -1);

		//--------------------------------------------------------------------------
		//   頂点データ書き込み& 描画
		//   vertices		… 描画する頂点の先頭アドレス
		//   vertexNum	… 描画する頂点数
		//--------------------------------------------------------------------------
		void WriteAndDraw(const void* vertices, int vertexNum);

		ZRingDynamicBuffer	m_RingBuf;			// 動的リングバッファ

		ZVertexTypeData	m_VTD;				// 頂点レイアウトデータ

		UINT				m_LastOffset = 0;
		
	};

	
	// インスタンシング用バッファ
	class ZInstancingBuffer
	{
	public:
		ZInstancingBuffer() : m_pBuffer(nullptr)
		{
		}

		//
		~ZInstancingBuffer()
		{
			Release();
		}

		// ※参照カウンタは加算していないので注意
		ID3D11Buffer* GetBuffer() { return m_pBuffer; }

		//   バッファの情報を取得
		const D3D11_BUFFER_DESC& GetDesc() { return m_Desc; }

		//   現在のオフセット(Byte)を取得
		uint GetSlotNumber() { return m_Slot; }

		//   指定サイズのデータが書き込める？
		bool IsWrite(uint size);

		//--------------------------------------------------------------------------
		//   バッファ作成
		//   maxBufferSize	… 作成するバッファのバイト数
		//   bufferSlot		… 何番スロットにセットするか
		//--------------------------------------------------------------------------
		bool Create(uint maxBufferSize,uint bufferSlot);

		//--------------------------------------------------------------------------
		//   解放
		//--------------------------------------------------------------------------
		void Release();

		// スロットにバッファをセット
		void SetToSlot(uint stride,uint offset);

		//--------------------------------------------------------------------------
		//   データをバッファに書き込む
		//  	data		… 書き込むデータの先頭アドレス
		//  	size		… 書き込むデータのバイト数
		//--------------------------------------------------------------------------
		void WriteData(const void* data, int size);

	private:
		ID3D11Buffer* m_pBuffer;	// バッファ

		uint m_MaxBufferSize;

		uint m_Slot;	// 何番スロットにセットするか

		D3D11_BUFFER_DESC	m_Desc;					// 作成したバッファの情報

	};


}
#endif
