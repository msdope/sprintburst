#include "PCH/pch.h"

namespace EzLib
{
	ZShaderResourceDeclaration::ZShaderResourceDeclaration(Type type, const ZString & name, uint count)
		: m_Type(type), m_Name(name), m_Count(count)
	{
	}

	ZShaderResourceDeclaration::Type ZShaderResourceDeclaration::StringToType(const ZString& typeName)
	{
		if (typeName == "Texture2D")		return Type::Texture2D;
		if (typeName == "Texture2DArray")	return Type::Texture2DArray;
		if (typeName == "TextureCube")		return Type::TextureCube;
		if (typeName == "SamplerState")		return Type::SamplerState;
		return Type::None;
	}

	ZString ZShaderResourceDeclaration::TypeToString(ZShaderResourceDeclaration::Type type)
	{
		switch (type)
		{
			case Type::Texture2D:		return "Texture2D";
			case Type::Texture2DArray:	return "Texture2DArray";
			case Type::TextureCube:		return "TextureCube";
			case Type::SamplerState:	return "SamplerState";
		}
		return "";
	}

}
