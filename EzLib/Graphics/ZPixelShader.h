//===============================================================
//  @file ZPixelShader.h
//   ピクセルシェーダクラス
// 
//  @author 鎌田
//===============================================================
#ifndef ZPixelShader_h
#define ZPixelShader_h

namespace EzLib
{

	//=======================================================================
	//   ピクセルシェーダクラス
	// 
	//  [主な機能]									\n
	// 	・HLSLファイルからピクセルシェーダ作成		\n
	// 	・それをデバイスにセットする機能			\n
	// 
	//  @ingroup Graphics_Shader
	//=======================================================================
	class ZPixelShader:public ZIResource
	{
	public:
		// 
		ZPixelShader();
		~ZPixelShader()
		{
			Release();
		}

		// コピー禁止用
		ZPixelShader(const ZPixelShader& src) = delete;
		void operator=(const ZPixelShader& src) = delete;
		void operator=(ZPixelShader&& src) = delete;

		//--------------------------------------------------------------------------
		//   コンパイル済みシェーダファイル(.cso)を読み込み、シェーダオブジェクトを作成する
		//   csoFileName		… CSOファイルパス
		//  @return 成功:true
		//--------------------------------------------------------------------------
		bool LoadShader(const ZString& csoFileName);

		//--------------------------------------------------------------------------
		//   HLSLソースファイル(.hlsl)を読み込み、ピクセルシェーダを作成する。
		//   hlslFileName		… HLSLファイルパス
		//   VsFuncName		… HLSL内のピクセルシェーダ関数名
		//   shaderModel		… 使用するシェーダモデルを指定 例)"ps_5_0"
		//   shaderFlag		… 追加のシェーダコンパイルフラグ
		//  @return 成功:true
		//--------------------------------------------------------------------------
		bool LoadShaderFromFile(const ZString& hlslFileName, const ZString& PsFuncName, const ZString& shaderModel, UINT shaderFlag);

		//--------------------------------------------------------------------------
		//   コンパイル済みシェーダをファイルに保存する
		//  Create()時に指定した、[hlslFileName]_[VsFuncName].cso のファイル名で保存される
		//   filename		… 保存するファイルパス
		//  @return 成功:true
		//--------------------------------------------------------------------------
		bool SaveToFile();

		//--------------------------------------------------------------------------
		//   解放
		//--------------------------------------------------------------------------
		void Release();

		//--------------------------------------------------------------------------
		//   シェーダをデバイスにセット
		//--------------------------------------------------------------------------
		void SetShader();

		//--------------------------------------------------------------------------
		//   ピクセルシェーダを無効にする
		//  SetShader()で最後設定すると有効になる
		//--------------------------------------------------------------------------
		static void DisableShader();

		virtual ZString GetTypeName() const override 
		{
			ZString str = "PixelShader : ";
			str += m_csoFileName;
			return str;
		}

	private:
		ID3D11PixelShader* m_PS;				// ピクセルシェーダー

		ID3DBlob*			m_pCompiledShader;	// コンパイル済みシェーダデータ

		ZString			m_hlslFileName;		// HLSLファイル名
		ZString			m_csoFileName;		// コンパイル済みシェーダ(CSO)時のファイル名

	};

}
#endif
