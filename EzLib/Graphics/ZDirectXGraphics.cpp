#include "PCH/pch.h"

namespace EzLib
{
	ZDirectXGraphics::ZDirectXGraphics()
		: m_pDevice(NULL),
		m_pDeviceContext(NULL),
		m_pGIAdapter(NULL),
		m_pGIFactory(NULL),
		m_pGISwapChain(NULL),
		m_pResStg(NULL),
		m_UseVSync(false)
	{
		ZeroMemory(m_srvReset, sizeof(m_srvReset));
	}
	
	ZDirectXGraphics::~ZDirectXGraphics()
	{
		Release();
	}

	bool ZDirectXGraphics::Init(HWND hWnd, int w, int h, ZResourceStorage* resStg, MSAA msaa, ZString* errMsg)
	{
		m_pResStg = resStg;

		HRESULT hr;

		//=====================================================
		// ファクトリー作成(ビデオ グラフィックの設定の列挙や指定に使用されるオブジェクト)
		//=====================================================
		hr = CreateDXGIFactory1(__uuidof(IDXGIFactory1), reinterpret_cast<void**>(&m_pGIFactory));
		if (FAILED(hr))
		{
			if (errMsg)*errMsg = "ファクトリー作成失敗";
			goto InitError;
		}
		// ALT+Enterでフルスクリーンを許可する
		if (FAILED(m_pGIFactory->MakeWindowAssociation(hWnd, 0)))
		{
			if (errMsg)*errMsg = "MakeWindowAssociation失敗";
			goto InitError;
		}

		//=====================================================
		// アダプター作成(アダプター (ビデオ カード) を列挙)
		//=====================================================
		hr = m_pGIFactory->EnumAdapters(0, &m_pGIAdapter);
		if (FAILED(hr))
		{
			if (errMsg)*errMsg = "アダプター取得失敗";
			goto InitError;
		}

		//=====================================================
		//デバイスの生成
		//=====================================================
		UINT creationFlags = 0;

#ifdef DEBUG_DIRECT3D11
		// Direct3Dのデバッグを有効にする(重いが細かいエラーがわかる)
		creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

		D3D_FEATURE_LEVEL featureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_1,	// Direct3D 11.1  ShaderModel 5
			D3D_FEATURE_LEVEL_11_0,	// Direct3D 11    ShaderModel 5
			D3D_FEATURE_LEVEL_10_1,	// Direct3D 10.1  ShaderModel 4
			D3D_FEATURE_LEVEL_10_0,	// Direct3D 10.0  ShaderModel 4
			D3D_FEATURE_LEVEL_9_3,	// Direct3D 9.3   ShaderModel 3
			D3D_FEATURE_LEVEL_9_2,	// Direct3D 9.2   ShaderModel 3
			D3D_FEATURE_LEVEL_9_1,	// Direct3D 9.1   ShaderModel 3
		};
		const int featureNum = 7;

		// デバイスとでデバイスコンテキストを作成
		hr = D3D11CreateDevice
		(
			nullptr,
			D3D_DRIVER_TYPE_HARDWARE,
			nullptr,
			creationFlags,
			featureLevels,
			featureNum,
			D3D11_SDK_VERSION,
			&m_pDevice,
			nullptr,
			&m_pDeviceContext
		);

		if(FAILED(hr))
		{
			if (errMsg)*errMsg = "DirectX11デバイス作成失敗";
			goto InitError;
		}

		// 使用可能なMSAAを取得
		UINT maxMsaa = 1;
		for (UINT i = 0; i <= D3D11_MAX_MULTISAMPLE_SAMPLE_COUNT; i++)
		{
			UINT Quality;
			if (SUCCEEDED(m_pDevice->CheckMultisampleQualityLevels(DXGI_FORMAT_D24_UNORM_S8_UINT, i, &Quality)))
			{
				if (0 < Quality) maxMsaa = i;
			}
		}
		// MSAA用データ
		DXGI_SAMPLE_DESC sampleDesc;
		sampleDesc.Count = msaa;
		sampleDesc.Quality = 0;
		if (sampleDesc.Count > maxMsaa)
			sampleDesc.Count = maxMsaa;

		//=====================================================
		// スワップチェイン作成
		//=====================================================
		m_DXGISwapChainDesc.BufferDesc.Width = w;
		m_DXGISwapChainDesc.BufferDesc.Height = h;
		m_DXGISwapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
		m_DXGISwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
		m_DXGISwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		m_DXGISwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		m_DXGISwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
		m_DXGISwapChainDesc.SampleDesc = sampleDesc;
		m_DXGISwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_SHADER_INPUT;
		m_DXGISwapChainDesc.BufferCount = 2;
		m_DXGISwapChainDesc.OutputWindow = hWnd;
		m_DXGISwapChainDesc.Windowed = TRUE;
		m_DXGISwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		m_DXGISwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
		if (FAILED(m_pGIFactory->CreateSwapChain(m_pDevice, &m_DXGISwapChainDesc, &m_pGISwapChain)))
		{
			if (errMsg)*errMsg = "スワップチェイン作成失敗";
			goto InitError;
		}

		// スワップチェインからバックバッファ取得
		ID3D11Texture2D* pBackBuffer;
		if (FAILED(m_pGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&pBackBuffer)))
		{
			if (errMsg)*errMsg = "バックバッファ取得失敗";
			goto InitError;
		}
		// バックバッファ用のレンダーターゲットビュー作成
		m_texBackBuffer = Make_Shared(ZTexture, appnew);
		if (m_texBackBuffer->CreateFromTexture2D(pBackBuffer) == false)
		{
			if (errMsg)*errMsg = "バックバッファ用View作成失敗";
			goto InitError;
		}
		pBackBuffer->Release();

		//=========================================================
		// Zバッファ・ステンシルバッファ作成
		//=========================================================
		D3D11_TEXTURE2D_DESC descDS;
		ID3D11Texture2D* pDepthBuffer = nullptr;	// Zバッファ

		// スワップチェーンの設定を取得する
		DXGI_SWAP_CHAIN_DESC chainDesc;
		hr = m_pGISwapChain->GetDesc(&chainDesc);
		if (FAILED(hr))
		{
			if (errMsg)*errMsg = "スワップチェーンの情報取得失敗";
			goto InitError;
		}
		// パラメータ設定
		ZeroMemory(&descDS, sizeof(D3D11_TEXTURE2D_DESC));
		descDS.Width = chainDesc.BufferDesc.Width;					// バックバッファと同じにする。
		descDS.Height = chainDesc.BufferDesc.Height;				// バックバッファと同じにする。
		descDS.MipLevels = 1;										// ミップマップを作成しない
		descDS.ArraySize = 1;										// テクスチャーの配列数
		descDS.Format = DXGI_FORMAT_R24G8_TYPELESS;					// フォーマット
		descDS.SampleDesc.Count = chainDesc.SampleDesc.Count;		// バックバッファと同じにする。
		descDS.SampleDesc.Quality = chainDesc.SampleDesc.Quality;	// バックバッファと同じにする。
		descDS.Usage = D3D11_USAGE_DEFAULT;							// GPU による読み取りおよび書き込みアクセスを必要とするリソース。
		descDS.BindFlags = D3D11_BIND_DEPTH_STENCIL |				// 深度ステンシルバッファとして作成する
						   D3D11_BIND_SHADER_RESOURCE;				// シェーダーリソースビューとして作成する
		descDS.CPUAccessFlags = 0;									// CPU アクセスが不要。
		descDS.MiscFlags = 0;										// その他のフラグも設定しない。
		// 深度バッファ用のテクスチャー作成
		hr = m_pDevice->CreateTexture2D(&descDS, nullptr, &pDepthBuffer);
		if (FAILED(hr))
		{
			if (errMsg)*errMsg = "Zバッファ生成失敗";
			goto InitError;
		}

		// そのテクスチャ(Zバッファ)用の深度ステンシルビュー作成
		m_texDepthStencil = Make_Shared(ZTexture, appnew);
		if (m_texDepthStencil->CreateFromTexture2D(pDepthBuffer) == false)
		{
			if (errMsg)*errMsg = "深度ステンシル用View作成失敗";
			goto InitError;
		}
		pDepthBuffer->Release();


		//=========================================================
		// バックバッファ、深度バッファをRT、Depthとしてデバイスコンテキストへセットする
		//=========================================================
		{
			ID3D11RenderTargetView* rtv = m_texBackBuffer->GetRTTex();
			m_pDeviceContext->OMSetRenderTargets(1, &rtv, m_texDepthStencil->GetDepthTex());
		}

		//=========================================================
		// ビューポートの設定
		//=========================================================
		SetViewport((float)w, (float)h);

		//=========================================================
		// 標準的なステートの設定
		//=========================================================
		SetDefaultStates();

		//=========================================================
		// 1x1 白テクスチャ作成
		//=========================================================
		{
			// 0xAABBGGRR
			DWORD col = 0xFFFFFFFF;
			D3D11_SUBRESOURCE_DATA srd;
			srd.pSysMem = &col;
			srd.SysMemPitch = 4;
			srd.SysMemSlicePitch = 0;
			m_texWhite = Make_Shared(ZTexture, appnew);
			m_texWhite->Create(1, 1, DXGI_FORMAT_R8G8B8A8_UNORM, &srd);
		}

		//=========================================================
		// 1x1 デフォルト用法線マップ作成
		//=========================================================
		{
			DWORD col = RGBA(0.5f, 0.5f, 1.0f, 0);//0x00FF8080;
			D3D11_SUBRESOURCE_DATA srd;
			srd.pSysMem = &col;
			srd.SysMemPitch = 4;
			srd.SysMemSlicePitch = 0;
			m_texNormal = Make_Shared(ZTexture, appnew);
			m_texNormal->Create(1, 1, DXGI_FORMAT_R8G8B8A8_UNORM, &srd);
		}

		//=========================================================
		// デフォルト用 トゥーンテクスチャ読み込み
		//=========================================================
		m_texToon = Make_Shared(ZTexture, appnew);
		m_texToon->LoadTexture("data/Texture/toon.png");

		// 
		ZeroMemory(m_srvReset, D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT);

		// DrawQuad用ポリゴン作成
		m_polyQuad.Create(nullptr, 4, ZVertex_Pos_UV::GetVertexTypeData(), true);

		return true;

		// エラー時
	InitError:
		Release();
		return false;
	}


	void ZDirectXGraphics::Release()
	{
		// 解放
		m_polyQuad.Release();
		m_pResStg = nullptr;
		m_texWhite = nullptr;
		m_texNormal = nullptr;
		m_texToon = nullptr;
		m_texBackBuffer = nullptr;
		m_texDepthStencil = nullptr;
		Safe_Release(m_pGISwapChain);
		Safe_Release(m_pDeviceContext);
		Safe_Release(m_pDevice);
		Safe_Release(m_pGIAdapter);
		Safe_Release(m_pGIFactory);
	}

	void ZDirectXGraphics::SetDefaultStates()
	{
		// ラスタライザステート設定
		ZRasterizeState rs;
		rs.SetAll_Standard();
		rs.SetState();

		// デプスステンシルステート設定
		ZDepthStencilState dss;
		dss.SetAll_Standard();
		dss.SetState();

		// ブレンドステート設定
		ZBlendState bs;
		bs.Set_Alpha(-1);
		bs.SetState();

		// サンプラステート設定
		ZSamplerState ss;
		ss.SetAll_Standard();
		ss.SetStatePS();
	}

	bool ZDirectXGraphics::ResizeResolution(size_t w, size_t h)
	{
		//===================================================
		// バッファ解放
		//===================================================
		m_texBackBuffer = nullptr;
		m_texDepthStencil = nullptr;

		//===================================================
		// リサイズ
		//===================================================
		m_pGISwapChain->ResizeBuffers(m_DXGISwapChainDesc.BufferCount, w, h, DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH);
		m_pGISwapChain->GetDesc(&m_DXGISwapChainDesc);

		//===================================================
		// バッファ作成
		//===================================================
		// スワップチェインからバックバッファ取得
		ID3D11Texture2D* pBackBuffer;
		if (FAILED(m_pGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&pBackBuffer)))
			goto FailedResize;

		// バックバッファ用のレンダーターゲットビュー作成
		m_texBackBuffer = Make_Shared(ZTexture, appnew);
		if (m_texBackBuffer->CreateFromTexture2D(pBackBuffer) == false)
			goto FailedResize;

		pBackBuffer->Release();


		//=========================================================
		// Zバッファ・ステンシルバッファ作成
		//=========================================================
		D3D11_TEXTURE2D_DESC descDS;
		ID3D11Texture2D* pDepthBuffer = nullptr;	// Zバッファ

		// スワップチェーンの設定を取得する
		DXGI_SWAP_CHAIN_DESC chainDesc;
		HRESULT hr = m_pGISwapChain->GetDesc(&chainDesc);
		if (FAILED(hr)) goto FailedResize;

		// パラメータ設定
		ZeroMemory(&descDS, sizeof(D3D11_TEXTURE2D_DESC));
		descDS.Width = chainDesc.BufferDesc.Width;					// バックバッファと同じにする。
		descDS.Height = chainDesc.BufferDesc.Height;				// バックバッファと同じにする。
		descDS.MipLevels = 1;										// ミップマップを作成しない
		descDS.ArraySize = 1;										// テクスチャーの配列数
		descDS.Format = DXGI_FORMAT_R24G8_TYPELESS;					// フォーマット
		descDS.SampleDesc.Count = chainDesc.SampleDesc.Count;		// バックバッファと同じにする。
		descDS.SampleDesc.Quality = chainDesc.SampleDesc.Quality;	// バックバッファと同じにする。
		descDS.Usage = D3D11_USAGE_DEFAULT;							// GPU による読み取りおよび書き込みアクセスを必要とするリソース。
		descDS.BindFlags = D3D11_BIND_DEPTH_STENCIL |				// 深度ステンシルバッファとして作成する
						   D3D11_BIND_SHADER_RESOURCE;				// シェーダーリソースビューとして作成する
		descDS.CPUAccessFlags = 0;									// CPU アクセスが不要。
		descDS.MiscFlags = 0;										// その他のフラグも設定しない

		// 深度バッファ用のテクスチャー作成
		hr = m_pDevice->CreateTexture2D(&descDS, nullptr, &pDepthBuffer);
		if (FAILED(hr)) goto FailedResize;

		// そのテクスチャ(Zバッファ)用の深度ステンシルビュー作成
		m_texDepthStencil = Make_Shared(ZTexture, appnew);
		if (m_texDepthStencil->CreateFromTexture2D(pDepthBuffer) == false)
			goto FailedResize;

		pDepthBuffer->Release();

		//=========================================================
		// バックバッファ、深度バッファをRT、Depthとしてデバイスコンテキストへセットする
		//=========================================================
		{
			ID3D11RenderTargetView* rtv = m_texBackBuffer->GetRTTex();
			m_pDeviceContext->OMSetRenderTargets(1, &rtv, m_texDepthStencil->GetDepthTex());
		}

		//=========================================================
		// ビューポートの設定
		//=========================================================
		SetViewport((float)w, (float)h);

		return true;

		// リサイズ失敗時
	FailedResize:
		Release();
		return false;
	}

	void ZDirectXGraphics::SetViewport(float w, float h)
	{
		// ビューポートの設定
		D3D11_VIEWPORT vp;
		vp.Width = w <= 0 ? w : m_texBackBuffer->GetInfo().Width;
		vp.Height = h <= 0 ? h : m_texBackBuffer->GetInfo().Height;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		m_pDeviceContext->RSSetViewports(1, &vp);
	}

	void ZDirectXGraphics::SetVSync(bool flg)
	{
		m_UseVSync = flg;
	}

	void ZDirectXGraphics::RS_CullMode(D3D11_CULL_MODE mode)
	{
		ZRasterizeState rs;
		rs.SetAll_Standard();
		rs.Set_CullMode(mode);
		rs.SetState();
	}

	void ZDirectXGraphics::DS_ZSetting(bool ZEnable, bool ZWriteEnable)
	{
		ZDepthStencilState ds;
		ds.SetAll_GetState();
		ds.Set_ZEnable(ZEnable);
		ds.Set_ZWriteEnable(ZWriteEnable);
		ds.SetState();
	}

	void ZDirectXGraphics::DrawQuad(int x, int y, int w, int h)
	{
		UINT pNumVierports = 1;
		D3D11_VIEWPORT vp;
		m_pDeviceContext->RSGetViewports(&pNumVierports, &vp);

		//==================================================
		// ピクセル座標系 → 射影座標系へ変換
		//==================================================
		float fConvX = (vp.Width * 0.5f);
		float fConvY = (vp.Height * 0.5f);

		float tx = x / fConvX - 1.0f;
		float ty = y / fConvY - 1.0f;

		float tx2 = (x + w) / fConvX - 1.0f;
		float ty2 = (y + h) / fConvY - 1.0f;

		//==================================================
		//四角形データ書き込み
		//==================================================
		// 座標は射影座標系
		ZVertex_Pos_UV vertices[] =
		{
			{ ZVec3( tx, -ty2, 0), ZVec2(0, 1) },
			{ ZVec3( tx, -ty , 0), ZVec2(0, 0) },
			{ ZVec3(tx2, -ty2, 0), ZVec2(1, 1) },
			{ ZVec3(tx2, -ty , 0), ZVec2(1, 0) }
		};

		//==================================================
		// 頂点書き込み
		//==================================================
		m_polyQuad.WriteVertexData(vertices, 4);

		//==================================================
		// 描画
		//==================================================
		// 描画用データをセット
		m_polyQuad.SetDrawData();
		// 描画
		m_polyQuad.Draw();
	}

	void ZDirectXGraphics::Begin(ZVec4& clearColor, bool isDepth, bool isStencl, float depth, UINT stencil)
	{
		ID3D11RenderTargetView* rtv = m_texBackBuffer->GetRTTex();
		m_pDeviceContext->OMSetRenderTargets(1, &rtv, m_texDepthStencil->GetDepthTex());
		SetViewport();
		m_texBackBuffer->ClearRT(clearColor);
		m_texDepthStencil->ClearDepth(isDepth, isStencl, depth, stencil);
	}

	void ZDirectXGraphics::End()
	{
		if (m_UseVSync == false)
			m_pGISwapChain->Present(0, 0);
		else
			m_pGISwapChain->Present(1, 0);
	}

	void ZDirectXGraphics::RemoveTextureVS(int slotNo)
	{
		if (slotNo >= 0)
		{
			ID3D11ShaderResourceView* srv = nullptr;
			m_pDeviceContext->VSSetShaderResources(slotNo, 1, &srv);
			return;
		}
		
		m_pDeviceContext->VSSetShaderResources(0, D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, m_srvReset);
	}

	void ZDirectXGraphics::RemoveTexturePS(int slotNo)
	{
		if (slotNo >= 0)
		{
			ID3D11ShaderResourceView* srv = nullptr;
			m_pDeviceContext->PSSetShaderResources(slotNo, 1, &srv);
		}
		else
			m_pDeviceContext->PSSetShaderResources(0, D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, m_srvReset);
	}

	void ZDirectXGraphics::RemoveTextureGS(int slotNo)
	{
		if (slotNo >= 0)
		{
			ID3D11ShaderResourceView* srv = nullptr;
			m_pDeviceContext->GSSetShaderResources(slotNo, 1, &srv);
		}
		else
			m_pDeviceContext->GSSetShaderResources(0, D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, m_srvReset);
	}

	void ZDirectXGraphics::RemoveTextureCS(int slotNo)
	{
		if (slotNo >= 0)
		{
			ID3D11ShaderResourceView* srv = nullptr;
			m_pDeviceContext->CSSetShaderResources(slotNo, 1, &srv);
		}
		else
			m_pDeviceContext->CSSetShaderResources(0, D3D11_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, m_srvReset);
	}

	void ZDirectXGraphics::RemoveUavCS(int slotNo)
	{
		ID3D11UnorderedAccessView* uav = nullptr;
		m_pDeviceContext->CSSetUnorderedAccessViews(slotNo, 1, &uav, nullptr);
	}

	HRESULT ZDirectXGraphics::Present(UINT SyncInterval, UINT Flags)
	{
		// Present
		return m_pGISwapChain->Present(SyncInterval, Flags);
	}
}