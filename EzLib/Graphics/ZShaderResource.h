#ifndef __ZSHADER_RESOURCE_H__
#define __ZSHADER_RESOURCE_H__

namespace EzLib
{
	class ZShaderResourceDeclaration
	{
	private:
		friend class ZIShader;

	public:
		enum class Type
		{
			None,Texture2D, Texture2DArray, TextureCube,SamplerState
		};
	
	public:
		ZShaderResourceDeclaration(Type type, const ZString& name, uint count = 1);

		const ZString& GetName()const;
		uint GetRegister()const;
		Type GetType()const;
		uint GetCount()const;

		static Type StringToType(const ZString& typeName);
		static ZString TypeToString(Type type);

	private:
		Type m_Type;
		ZString m_Name;
		uint m_Count;
		uint m_Register;

	};

	using ShaderResourceList = ZVector<ZShaderResourceDeclaration*>;

#include "ZShaderResource.inl"

}

#endif