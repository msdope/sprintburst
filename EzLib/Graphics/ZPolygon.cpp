#include "PCH/pch.h"
#include "ZPolygon.h"

namespace EzLib
{

	//=============================================================
	//
	// ZPolygon
	//
	//=============================================================

	void ZPolygon::Create(const void* vertices, int vertexNum, const ZVertexTypeData& vtd, bool isDynamic)
	{
		Release();

		m_VTD = vtd;
		m_NumVertex = vertexNum;

		D3D11_BUFFER_DESC bd;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.ByteWidth = m_VTD.ByteSize* vertexNum;
		if (isDynamic)
		{
			bd.Usage = D3D11_USAGE_DYNAMIC;
			bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		}
		else
		{
			bd.Usage = D3D11_USAGE_DEFAULT;
			bd.CPUAccessFlags = 0;
		}
		bd.MiscFlags = 0;
		bd.StructureByteStride = 0;
		D3D11_SUBRESOURCE_DATA InitData;
		InitData.pSysMem = vertices;
		if (FAILED(ZDx.GetDev()->CreateBuffer(&bd, (vertices ? &InitData : nullptr), &m_VB)))
		{
			return;
		}

		if (isDynamic)
		{
			m_TypeFlag = BufType::Dynamic;
		}
		else
		{
			m_TypeFlag = BufType::Default;
		}
	}

	void ZPolygon::Release()
	{
		Safe_Release(m_VB);
		m_TypeFlag = BufType::None;
		m_NumVertex = 0;
	}

	void ZPolygon::WriteVertexData(const void* vertices, int vertexNum)
	{
		if (m_TypeFlag == BufType::Dynamic)
		{
			D3D11_MAPPED_SUBRESOURCE pData;
			if (SUCCEEDED(ZDx.GetDevContext()->Map(m_VB, 0, D3D11_MAP_WRITE_DISCARD, 0, &pData)))
			{

				memcpy_s(pData.pData, pData.RowPitch, vertices, m_VTD.ByteSize* vertexNum);

				ZDx.GetDevContext()->Unmap(m_VB, 0);
			}
		}
		else if (m_TypeFlag == BufType::Default)
		{
			ZDx.GetDevContext()->UpdateSubresource(m_VB, 0, 0, vertices, 0, 0);
		}
	}

	bool ZPolygon::Map(D3D11_MAPPED_SUBRESOURCE* pOutMappedResource)
	{
		if (m_TypeFlag == BufType::Dynamic)
		{
			if (SUCCEEDED(ZDx.GetDevContext()->Map(m_VB, 0, D3D11_MAP_WRITE_DISCARD, 0, pOutMappedResource)))
			{
				return true;
			}
		}
		return false;
	}
	void ZPolygon::Unmap()
	{
		ZDx.GetDevContext()->Unmap(m_VB, 0);
	}

	void ZPolygon::SetDrawData(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology)
	{
		// 頂点バッファーをセット
		UINT stride = m_VTD.ByteSize;
		UINT offset = 0;
		ZDx.GetDevContext()->IASetVertexBuffers(0, 1, &m_VB, &stride, &offset);

		// プリミティブ・トポロジーをセット
		ZDx.GetDevContext()->IASetPrimitiveTopology(primitiveTopology);
	}

	void ZPolygon::Draw(UINT vertexCount)
	{
		if (vertexCount == 0xFFFFFFFF)vertexCount = m_NumVertex;

		ZDx.GetDevContext()->Draw(vertexCount, 0);
	}

	void ZPolygon::WriteAndDraw(const void *vertices, int vertexNum, D3D11_PRIMITIVE_TOPOLOGY primitiveTopology)
	{
		WriteVertexData(vertices, vertexNum);

		SetDrawData(primitiveTopology);

		ZDx.GetDevContext()->Draw(vertexNum, 0);
	}

	ZPolygon::ZPolygon() : m_VB(0), m_TypeFlag(BufType::None)
	{
	}


	//=============================================================
	//
	// ZRingDynamicBuffer
	//
	//=============================================================
	bool ZRingDynamicBuffer::Create(D3D11_BIND_FLAG bindFlag, int maxBufferSize)
	{
		Release();

		// バッファの最大サイズ
		m_MaxBufferSize = maxBufferSize;

		m_nNextOffset = 0;

		ZeroMemory(&m_Desc, sizeof(m_Desc));
		m_Desc.BindFlags = bindFlag;			// D3D11_BIND_VERTEX_BUFFER;
		m_Desc.ByteWidth = maxBufferSize;		// データの総数分作成する
		// 動的バッファ
		m_Desc.Usage = D3D11_USAGE_DYNAMIC;
		m_Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		m_Desc.MiscFlags = 0;
		m_Desc.StructureByteStride = 0;

		if (FAILED(ZDx.GetDev()->CreateBuffer(&m_Desc, nullptr, &m_pBuffer)))
			return false;

		return true;
	}

	void ZRingDynamicBuffer::Release()
	{
		Safe_Release(m_pBuffer);

		m_nNextOffset = 0;
	}

	void ZRingDynamicBuffer::ResetOffset()
	{
		m_nNextOffset = 0;
	}

	bool ZRingDynamicBuffer::IsWrite(int size)
	{
		// バッファにスペースが無い時
		//  現在の位置 > 頂点バッファのサイズサイズ - 1データのサイズ
		if (m_nNextOffset > m_MaxBufferSize - size)
		{
			return false;
		}

		// スペースはある
		return true;
	}


	int ZRingDynamicBuffer::WriteData(const void *data, int size)
	{
		D3D11_MAP dwLockFlags = D3D11_MAP_WRITE_NO_OVERWRITE;

		// バッファにスペースが無い時
		//  現在の位置 > 頂点バッファのサイズサイズ - 1データのサイズ
		if (m_nNextOffset > m_MaxBufferSize - size)
		{
			// スペースがないので、最初からやり直すためDISCARD
			dwLockFlags = D3D11_MAP_WRITE_DISCARD;
			m_nNextOffset = 0;
		}

		// 書き込み
		D3D11_MAPPED_SUBRESOURCE pData;
		if (SUCCEEDED(ZDx.GetDevContext()->Map(m_pBuffer, 0, dwLockFlags, 0, &pData)))
		{

			// verticesから1データのサイズぶんコピーする
			memcpy_s((char*)pData.pData + m_nNextOffset, m_MaxBufferSize, data, size);

			ZDx.GetDevContext()->Unmap(m_pBuffer, 0);
		}

		// 書き込んだ先頭のオフセット値を返す
		int retOffset = m_nNextOffset;

		// オフセットを進める
		m_nNextOffset += size;

		return retOffset;
	}


	//=============================================================
	//
	// ZRingDynamicVB
	//
	//=============================================================


	void ZRingDynamicVB::SetDrawData(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology)
	{
		// プリミティブ・トポロジーをセット
		ZDx.GetDevContext()->IASetPrimitiveTopology(primitiveTopology);

		// 頂点バッファーをセット
		UINT stride = m_VTD.ByteSize;
		UINT offset = 0;
		auto buf = m_RingBuf.GetBuffer();
		ZDx.GetDevContext()->IASetVertexBuffers(0, 1, &buf, &stride, &offset);
	}

	void EzLib::ZRingDynamicVB::WriteVertexData(const void* vertices, int vertexNum)
	{
		// 頂点データをリングバッファに書き込み
		m_LastOffset = m_RingBuf.WriteData(vertices, vertexNum* m_VTD.ByteSize);
	}

	void ZRingDynamicVB::WriteAndDraw(const void *vertices, int vertexNum)
	{

		// 頂点データをリングバッファに書き込み
		m_LastOffset = m_RingBuf.WriteData(vertices, vertexNum* m_VTD.ByteSize);

		// 描画
		ZDx.GetDevContext()->Draw(vertexNum, m_LastOffset / m_VTD.ByteSize);

	}

	void ZRingDynamicVB::Draw(int vertexNum, int startByteOffset)
	{
		if (startByteOffset < 0)startByteOffset = m_LastOffset;

		// 描画
		ZDx.GetDevContext()->Draw(vertexNum, startByteOffset / m_VTD.ByteSize);
	}


	//=============================================================
	//
	// ZInstancingBuffer
	//
	//=============================================================

	bool ZInstancingBuffer::IsWrite(uint size)
	{
		if (m_MaxBufferSize < size)
			return false;

		return true;
	}

	bool ZInstancingBuffer::Create(uint maxBufferSize, uint bufferSlot)
	{
		Release();

		// バッファの最大サイズ
		m_MaxBufferSize = maxBufferSize;
		m_Slot = bufferSlot;

		ZeroMemory(&m_Desc, sizeof(m_Desc));
		m_Desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		m_Desc.ByteWidth = maxBufferSize;		// データの総数分作成する
		// 動的バッファ
		m_Desc.Usage = D3D11_USAGE_DYNAMIC;
		m_Desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		m_Desc.MiscFlags = 0;
		m_Desc.StructureByteStride = 0;

		if (FAILED(ZDx.GetDev()->CreateBuffer(&m_Desc, nullptr, &m_pBuffer)))
			return false;

		return true;
	}

	void ZInstancingBuffer::Release()
	{
		Safe_Release(m_pBuffer);
		m_Slot = 0;
	}

	void ZInstancingBuffer::SetToSlot(uint stride, uint offset)
	{
		ZDx.GetDevContext()->IASetVertexBuffers(m_Slot, 1, &m_pBuffer, &stride, &offset);
	}

	void ZInstancingBuffer::WriteData(const void * data, int size)
	{
		D3D11_MAP dwLockFlags = D3D11_MAP_WRITE_DISCARD;

		// 書き込み
		D3D11_MAPPED_SUBRESOURCE pData;
		if (SUCCEEDED(ZDx.GetDevContext()->Map(m_pBuffer, 0, dwLockFlags, 0, &pData)))
		{
			// verticesから1データのサイズぶんコピーする
			memcpy_s((char*)pData.pData, m_MaxBufferSize, data, size);

			ZDx.GetDevContext()->Unmap(m_pBuffer, 0);
		}

	}



}