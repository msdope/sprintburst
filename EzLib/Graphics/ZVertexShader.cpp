﻿#include "PCH/pch.h"

namespace EzLib
{
	std::unordered_map<std::string, DXGI_FORMAT> ZVertexShader::ForceUseFormatMap;
	std::unordered_map<std::string, ZVertexShader::ForceUseInputLayoutParam> ZVertexShader::ForceUseInputLayoutParamMap;

	ZVertexShader::ZVertexShader()
		:m_VS(0), m_VLayout(0), m_pCompiledShader(0)
	{
	}
	
	bool ZVertexShader::LoadShader(const ZString& csoFileName)
	{
		Release();
	
		m_csoFileName = csoFileName;

		ZFile file;
		if (file.Open(csoFileName, ZFile::OPEN_MODE::READ))
		{
			ZVector<uint8> binaryBuffer; // コンパイル済みシェーダ読み込み用
			// 読み込み
			file.ReadAll(binaryBuffer);
	
			// ファイルサイズ
			uint len = file.GetSize();
			
			// Blob(バッファ)作成
			D3DCreateBlob(len, &m_pCompiledShader);

			// コピー
			memcpy(m_pCompiledShader->GetBufferPointer(), binaryBuffer.data(), len);
		}
		else
		{
			MessageBox(0, "指定したcsoファイルが存在しません", csoFileName.c_str(), MB_OK);
			Release();
			return false;
		}
	
		//==========================================================================
		// ブロブから頂点シェーダー作成
		//==========================================================================
		if (FAILED(ZDx.GetDev()->CreateVertexShader(m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize(), nullptr, &m_VS)))
		{
			MessageBox(0, "頂点シェーダー作成失敗", csoFileName.c_str(), MB_OK);
			Release();
			return false;
		}
		
		//==========================================================================
		// シェーダのバイナリデータから頂点インプットレイアウトを作成
		//==========================================================================
		RefrectInputLayout();
	
		return true;
	}
	
	bool ZVertexShader::LoadShaderFromFile(const ZString& hlslFileName, const ZString& VsFuncName, const ZString& shaderModel, UINT shaderFlag)
	{
		Release();
	
		// 最適化
		#ifdef OPTIMIZESHADER
		shaderFlag |= D3DCOMPILE_OPTIMIZATION_LEVEL3;	// 最大の最適化 リリース用
		#else
		shaderFlag |= D3DCOMPILE_OPTIMIZATION_LEVEL0 | D3DCOMPILE_DEBUG;	// 最低の最適化 開発用
		#endif
	
		ID3DBlob* pErrors = nullptr;
	
		m_hlslFileName = hlslFileName;
	
		// csoファイル名生成( [hlslFileName]_[VsFuncName].cso )
		// 例) aaa.hlslを指定した場合で頂点シェーダ関数名が"VS"の時、aaa_VS.cso
		char szDir[_MAX_PATH];
		char szFname[_MAX_FNAME];
		char szExt[_MAX_EXT];
		_splitpath_s(hlslFileName.c_str(), nullptr, 0, szDir, _MAX_PATH, szFname, _MAX_FNAME, szExt, _MAX_EXT);
	
		m_csoFileName = szDir;
		m_csoFileName += szFname;
		m_csoFileName += "_";
		m_csoFileName += VsFuncName;
		m_csoFileName += ".cso";
	
	
		//==========================================================================
		// HLSLから頂点シェーダをコンパイルし読み込む
		//==========================================================================
		if (m_pCompiledShader == nullptr)
		{
			if (FAILED(D3DCompileFromFile(ConvertStringToWString(hlslFileName).c_str(), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, VsFuncName.c_str(), shaderModel.c_str(), shaderFlag, 0, &m_pCompiledShader, &pErrors)))
			{
				if (pErrors)
				{
					MessageBox(0, (char*)pErrors->GetBufferPointer(), "HLSLコンパイル失敗", MB_OK);
				}
				else
				{
					MessageBox(0, "HLSLファイルが存在しない", hlslFileName.c_str(), MB_OK);
				}
				Release();
				return false;
			}
			Safe_Release(pErrors);
		}
	
		//==========================================================================
		// ブロブから頂点シェーダー作成
		//==========================================================================
		if (FAILED(ZDx.GetDev()->CreateVertexShader(m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize(), nullptr, &m_VS)))
		{
			MessageBox(0, "頂点シェーダー作成失敗", hlslFileName.c_str(), MB_OK);
			Release();
			return false;
		}
	
		//==========================================================================
		// シェーダのバイナリデータから頂点インプットレイアウトを作成
		//==========================================================================
		{
			uint size = m_pCompiledShader->GetBufferSize();
			ZVector<uint8> buffer(size);
			memcpy(&buffer[0], m_pCompiledShader->GetBufferPointer(), size);
			RefrectInputLayout();
		}
	
		return true;
	}
	
	bool ZVertexShader::SaveToFile()
	{
		if (m_pCompiledShader == nullptr)return false;
	
		//==========================================================================
		// コンパイル済みのシェーダを保存する
		//==========================================================================
		std::ofstream ofs(m_csoFileName.c_str(), std::ios::binary);
		ofs.write((char*)m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize());
	
		return true;
	}
	
	
	void ZVertexShader::Release()
	{
		Safe_Release(m_pCompiledShader);
		Safe_Release(m_VS);
		Safe_Release(m_VLayout);
	
		m_hlslFileName = "";
		m_csoFileName = "";
	}
	
	void ZVertexShader::SetShader(ID3D11InputLayout* vertexLayout)
	{
		if (vertexLayout == nullptr)vertexLayout = m_VLayout;

		// 頂点入力レイアウト設定
		ZDx.GetDevContext()->IASetInputLayout(vertexLayout);
		// 頂点シェーダ設定
		ZDx.GetDevContext()->VSSetShader(m_VS, nullptr, 0);
	}
	
	void ZVertexShader::DisableShader()
	{
		// 頂点シェーダ解除
		ZDx.GetDevContext()->VSSetShader(nullptr, 0, 0);
	}

	void ZVertexShader::RefrectInputLayout()
	{
		ComPtr<ID3D11ShaderReflection> refrection;
		auto hr = D3DReflect(m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize(), IID_PPV_ARGS(&refrection));
		if (FAILED(hr)) assert(false);
		D3D11_SHADER_DESC shaderDesc{};
		hr = refrection->GetDesc(&shaderDesc);
		if (FAILED(hr)) assert(false);
		uint numInputParam = shaderDesc.InputParameters;
		ZVector<D3D11_SIGNATURE_PARAMETER_DESC> signatureParamDescs(numInputParam);
		
		ZVector<DXGI_FORMAT> format(numInputParam);
		ZVector<D3D11_INPUT_ELEMENT_DESC> elemDesc(numInputParam);

		// フォーマット抽出
		for (uint i = 0; i < shaderDesc.InputParameters; i++)
		{
			
			hr = refrection->GetInputParameterDesc(i, &signatureParamDescs[i]);
			
			// フォーマット強制指定
			{
				auto it = ForceUseFormatMap.find(signatureParamDescs[i].SemanticName);
				if (it != ForceUseFormatMap.end())
				{
					format[i] = it->second;
					continue;
				}
			}

			uint mask = signatureParamDescs[i].Mask;
			D3D_REGISTER_COMPONENT_TYPE type = signatureParamDescs[i].ComponentType;

			// フォーマット判別
			switch (type)
			{
				case D3D10_REGISTER_COMPONENT_UINT32:
				{
					switch (mask)
					{
						case '\x0f':
						format[i] = DXGI_FORMAT_R32G32B32A32_UINT;
						break;

						case '\x07':
						format[i] = DXGI_FORMAT_R32G32B32_UINT;
						break;

						case '\x03':
						format[i] = DXGI_FORMAT_R32G32_UINT;
						break;

						case '\x01':
						format[i] = DXGI_FORMAT_R32_UINT;
						break;

						default:
						format[i] = DXGI_FORMAT_UNKNOWN;
						break;
					}
				}
				break;

				case D3D10_REGISTER_COMPONENT_SINT32:
				{
					switch (mask)
					{
						case '\x0f':
						format[i] = DXGI_FORMAT_R32G32B32A32_SINT;
						break;

						case '\x07':
						format[i] = DXGI_FORMAT_R32G32B32_SINT;
						break;

						case '\x03':
						format[i] = DXGI_FORMAT_R32G32_SINT;
						break;

						case '\x01':
						format[i] = DXGI_FORMAT_R32_SINT;
						break;

						default:
						format[i] = DXGI_FORMAT_UNKNOWN;
						break;
					}
				}
				break;


				case D3D10_REGISTER_COMPONENT_FLOAT32:
				{
					switch (mask)
					{
						case '\x0f':
						format[i] = DXGI_FORMAT_R32G32B32A32_FLOAT;
						break;

						case '\x07':
						format[i] = DXGI_FORMAT_R32G32B32_FLOAT;
						break;

						case '\x03':
						format[i] = DXGI_FORMAT_R32G32_FLOAT;
						break;

						case '\x01':
						format[i] = DXGI_FORMAT_R32_FLOAT;
						break;

						default:
						format[i] = DXGI_FORMAT_UNKNOWN;
						break;
					}
				}
				break;

				default:
				format[i] = DXGI_FORMAT_UNKNOWN;
				break;
			} // switch (type)

		} // for (uint i = 0; i < shaderDesc.InputParameters;i++)

		// INPUT_ELEMENT_DESC構築
		uint offset = 0;
		for (int i = 0; i < numInputParam; i++)
		{
			UINT slotIndex = 0;
			D3D11_INPUT_CLASSIFICATION inputClassification = D3D11_INPUT_PER_VERTEX_DATA;
			UINT dataStepRate = 0;

			// パラメータ強制指定
			{
				auto it = ForceUseInputLayoutParamMap.find(signatureParamDescs[i].SemanticName);
				if (it != ForceUseInputLayoutParamMap.end())
				{
					slotIndex = it->second.SlotIndex;
					inputClassification = it->second.InputClassification;
					dataStepRate = it->second.DataStepRate;
				}
			}

			//入力スロットが次と違うならオフセットをリセット
			if (i > 0 && slotIndex != elemDesc[i - 1].InputSlot)
				offset = 0;

			elemDesc[i] = D3D11_INPUT_ELEMENT_DESC
			{
				signatureParamDescs[i].SemanticName,
				signatureParamDescs[i].SemanticIndex,
				format[i],
				slotIndex,
				offset,
				inputClassification,
				dataStepRate
			};

			switch (format[i])
			{
				case DXGI_FORMAT_R32G32B32A32_UINT:
				case DXGI_FORMAT_R32G32B32A32_SINT:
				case DXGI_FORMAT_R32G32B32A32_FLOAT:
				{
					offset += sizeof(float) * 4;
				}
				break;

				case DXGI_FORMAT_R32G32B32_UINT:
				case DXGI_FORMAT_R32G32B32_SINT:
				case DXGI_FORMAT_R32G32B32_FLOAT:
				offset += sizeof(float) * 3;
				break;

				case DXGI_FORMAT_R32G32_UINT:
				case DXGI_FORMAT_R32G32_SINT:
				case DXGI_FORMAT_R32G32_FLOAT:
				offset += sizeof(float) * 2;
				break;

				case DXGI_FORMAT_R32_UINT:
				case DXGI_FORMAT_R32_SINT:
				case DXGI_FORMAT_R32_FLOAT:
				offset += sizeof(float);
				break;

				case DXGI_FORMAT_R8G8B8A8_UNORM:
				offset += 4;
				break;

				case DXGI_FORMAT_R16G16B16A16_UINT:
				offset += 8;
				break;
			}
		}
		ID3D11InputLayout* layout = nullptr;
		//入力レイアウトを作成
		hr = ZDx.GetDev()->CreateInputLayout(elemDesc.data(), elemDesc.size(), m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize(), &m_VLayout);
		if (FAILED(hr)) assert(false);
	
	}
}

