// 定数バッファ作成
template<typename T>
bool ZConstantBuffer<T>::Create(UINT StartSlot, bool isDynamic)
{
	return CreateBySize(StartSlot, sizeof(T), isDynamic);
}

template<typename T>
bool ZConstantBuffer<T>::CreateBySize(UINT StartSlot, int byteSize, bool isDynamic)
{
	Release();

	m_BufferSize = byteSize;

	// サイズチェック(定数バッファは16バイトアライメント(16の倍数)にする必要がある)
	if (byteSize % 16 != 0)
	{
		MessageBox(nullptr, "コンスタントバッファのサイズは16の倍数バイトでなければならない！", "ZConstantBuffer", MB_OK);
		return false;
	}

	D3D11_BUFFER_DESC cb;
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cb.ByteWidth = byteSize;	// D3D11_BIND_CONSTANT_BUFFERの場合は、float4の倍数(16バイト)が必須
	if (isDynamic)
	{
		cb.Usage = D3D11_USAGE_DYNAMIC;
		cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		cb.Usage = D3D11_USAGE_DEFAULT;
		cb.CPUAccessFlags = 0;
	}
	cb.StructureByteStride = 0;
	cb.MiscFlags = 0;
	if (FAILED(ZDx.GetDev()->CreateBuffer(&cb, nullptr, &m_pBuffer)))
	{
		return false;
	}

	m_StartSlot = StartSlot;
	if (isDynamic)
	{
		m_TypeFlag = BufType::Dynamic;
	}
	else
	{
		m_TypeFlag = BufType::Default;
	}

	return true;
}

// 解放
template<typename T>
void ZConstantBuffer<T>::Release()
{
	Safe_Release(m_pBuffer);
	m_TypeFlag = BufType::None;
	m_StartSlot = 0;
	m_BufferSize = 0;
}

template<typename T>
void ZConstantBuffer<T>::WriteData(int writeBytes)
{
	if (writeBytes == -1)
	{
		writeBytes = m_BufferSize;// sizeof(T);
	}

	if (m_TypeFlag == BufType::Dynamic)
	{
		// Discard指定なので、全領域更新となります。
		// コピーだけがwriteBytesのぶんだけになり、コピーしていない領域は未定義になりますので注意。
		D3D11_MAPPED_SUBRESOURCE pData;
		if (SUCCEEDED(ZDx.GetDevContext()->Map(m_pBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &pData)))
		{
			memcpy_s(pData.pData, pData.RowPitch, &m_Data, writeBytes);
			ZDx.GetDevContext()->Unmap(m_pBuffer, 0);
		}
	}
	else if (m_TypeFlag == BufType::Default)
	{
		ZDx.GetDevContext()->UpdateSubresource(m_pBuffer, 0, nullptr, &m_Data, 0, 0);
	}
}
