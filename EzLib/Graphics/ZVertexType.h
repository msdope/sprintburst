//===============================================================
//  @file ZVerteType.h
//   頂点形式データ集
// 
//  @author 鎌田
//===============================================================
#ifndef ZVertexType_h
#define ZVertexType_h

namespace EzLib
{
	//===============================================
	//   頂点レイアウト用データ
	//  １つの頂点データの詳細を入れる
	//===============================================
	struct ZVertexTypeData
	{
		int								ByteSize;		// １頂点のバイト数
		bool							IsSkinMeshVertexType;
	};

	//===============================================
	// 頂点データ
	//===============================================

	//   頂点データ(座標のみ)
	//  これが基本の頂点データクラスになる
	struct ZVertex_Pos
	{
		ZVec3 Pos;	// 位置

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			// データ
			static const ZVertexTypeData vtd = {sizeof(ZVertex_Pos),false};
			return vtd;
		}

	};


	//   頂点データ(座標,色)
	struct ZVertex_Pos_Color : public ZVertex_Pos
	{
		ZVec4 Color;	// 色

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			static const ZVertexTypeData vtd = {sizeof(ZVertex_Pos_Color),false};
			return vtd;
		}

		ZVertex_Pos_Color() = default;
		ZVertex_Pos_Color(const ZVec3& pos, const ZVec4& color)
		{
			Pos = pos;
			Color = color;
		}

	};


	//   頂点データ(座標,UV)
	struct ZVertex_Pos_UV : public ZVertex_Pos
	{
		ZVec2 UV;		// UV

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			// データ
			static const ZVertexTypeData vtd = {sizeof(ZVertex_Pos_UV),false};
			return vtd;
		}

		ZVertex_Pos_UV() = default;

		ZVertex_Pos_UV(const ZVec3& pos, const ZVec2& uv)
		{
			Pos = pos;
			UV = uv;
		}
	};

	//   頂点データ(座標,UV,Color)
	struct ZVertex_Pos_UV_Color : public ZVertex_Pos
	{
		ZVec2 UV;		// UV
		ZVec4 Color;	// 色

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			// データ
			static const ZVertexTypeData vtd = {sizeof(ZVertex_Pos_UV_Color),false };
			return vtd;
		}

		ZVertex_Pos_UV_Color() = default;

		ZVertex_Pos_UV_Color(const ZVec3& pos, const ZVec2& uv, const ZVec4& color)
		{
			Pos = pos;
			UV = uv;
			Color = color;
		}
	};


	//   頂点データ(座標,UV,法線)
	struct ZVertex_Pos_UV_Normal : public ZVertex_Pos
	{
		ZVec2 UV;			// UV
		ZVec3 Normal;		// 法線

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			// データ
			static const ZVertexTypeData vtd = {sizeof(ZVertex_Pos_UV_Normal),false };
			return vtd;
		}

		ZVertex_Pos_UV_Normal() = default;

		ZVertex_Pos_UV_Normal(const ZVec3& pos, const ZVec2& uv, const ZVec3& normal)
		{
			Pos = pos;
			UV = uv;
			Normal = normal;
		}
	};


	//   頂点データ(座標,UV,法線マップ用法線)
	//  ZGameModelクラスで読み込んだモデル(Static mesh)は、この形式です
	struct ZVertex_Pos_UV_TBN : public ZVertex_Pos
	{
		ZVec2 UV;			// UV
		ZVec3 Tangent;		// 接線
		ZVec3 Binormal;	// 従法線
		ZVec3 Normal;		// 法線

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			// データ
			static const ZVertexTypeData vtd = {sizeof(ZVertex_Pos_UV_TBN),false };
			return vtd;
		}

	};

	//   頂点データ(座標,UV,法線マップ用法線,頂点ブレンド情報)
	//  ZGameModelクラスで読み込んだモデル(Skin mesh)は、この形式です
	struct ZVertex_Pos_UV_TBN_Skin : public ZVertex_Pos_UV_TBN
	{
		uint8				BlendWeight[4];		// ブレンドの重み
		unsigned short		BlendIndices[4];	// ボーン番号

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			// データ
			static const ZVertexTypeData vtd = { sizeof(ZVertex_Pos_UV_TBN_Skin),true };
			return vtd;
		}

	};

	struct ZVertex_Pos_UV_Skin : public ZVertex_Pos_UV
	{
		uint8_t				BlendWeight[4];		// ブレンドの重み
		unsigned short		BlendIndices[4];	// ボーン番号

		//   頂点データ取得
		//  @return 頂点レイアウトデータ
		static ZVertexTypeData GetVertexTypeData()
		{
			// データ
			static const ZVertexTypeData vtd = { sizeof(ZVertex_Pos_UV_Skin) ,true};
			return vtd;
		}

	};

}
#endif
