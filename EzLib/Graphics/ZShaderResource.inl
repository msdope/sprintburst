inline const ZString& ZShaderResourceDeclaration::GetName()const
{
	return m_Name;
}

inline uint ZShaderResourceDeclaration::GetRegister()const
{
	return m_Register;
}

inline ZShaderResourceDeclaration::Type ZShaderResourceDeclaration::GetType()const
{
	return m_Type;
}

inline uint ZShaderResourceDeclaration::GetCount()const
{
	return m_Count;
}