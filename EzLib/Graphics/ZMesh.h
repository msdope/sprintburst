//===============================================================
//  @file ZMesh.h
//   メッシュクラス
// 
//  @author 鎌田
//===============================================================
#ifndef ZMesh_h
#define ZMesh_h

namespace EzLib
{

	//==========================================================
	//   サブセットデータ
	// 
	//  AttribId番目のマテリアルは、FaceStart番目の面からFaceCount枚ぶん	\n
	//   描画すれば良い、という情報
	// 
	//  @ingroup Graphics_Model
	//==========================================================
	struct ZMeshSubset
	{
		DWORD AttribId;			// マテリアルNo

		DWORD FaceStart;		// 面Index　このマテリアルで使用されている最初の面のIndex
		DWORD FaceCount;		// 面数　FaceStartから、何枚の面が使用されているかの
	};


	//=============================================================================
	//   メッシュ
	// 
	// 	頂点・面情報・属性テーブルなど、純粋なメッシュ情報のみ(マテリアルは無し)					\n
	// 	m_Verticesやm_Facesやm_ExtFacesに全ての頂点や面情報が入ってるので、当たり判定などに使用可能
	// 
	//  ・Create関数でメッシュを作成
	//  ・描画直前でSetDrawData関数でデータをセットし、DrawSubset関数で描画
	//  ・
	// 
	//  @ingroup Graphics_Model
	//=============================================================================
	class ZMesh
	{
	public:
		struct Face;
		struct ExtFace;

		//================================================================
		// データ取得系
		//================================================================

		//   頂点数取得
		unsigned int						GetNumVerts() const { return m_VertexBuf.size() / m_VertexTypeData.ByteSize; }

		//   面数取得
		unsigned int						GetNumFaces() const { return m_Faces.size(); }

		//   頂点形式情報取得
		const ZVertexTypeData&				GetVertexType() const { return m_VertexTypeData; }

		//   サブセット情報取得
		const ZAVector<ZMeshSubset>&	GetSubset() const { return m_Subset; }

		//   サブセット情報取得　指定したmateNoと一致するもののみ返す
		//   subsetNo … サブセット番号
		//  @return 見つかったマテリアル番号のサブセット　ない場合はnullptrが返る
		const ZMeshSubset*					GetSubset(int subsetNo) const;

		//   このモデルはスキンメッシュかどうか
		bool								IsSkinMesh() { return m_bSkinMesh; }

		//======================================================
		// 境界データ取得系
		//======================================================

		//   メッシュのAABBの中心座標を取得
		const ZVec3&	GetAABB_Center() const { return m_AABB_Center; }

		//   メッシュのAABBのハーフサイズを取得
		const ZVec3&	GetAABB_HalfSize() const { return m_AABB_HalfSize; }

		//   メッシュの境界球のセンター座標(ローカル)を取得
		const ZVec3&	GetBSCenter() const { return m_vBSCenter; }

		//   メッシュの境界球の半径を取得
		float			GetBSRadius() const { return m_fBSRadius; }

		//======================================================
		// 頂点や面のデータ取得
		//======================================================

		//   指定番号の面の、指定番号の頂点座標を取得
		//   faceIdx		… 面番号
		//   vIdx			… その面の頂点番号(0〜2) ※頂点バッファのIndexではありません。この面内の頂点番号です。
		//  @return 頂点座標
		const ZVec3&	GetVertex_Pos(UINT faceIdx, int vIdx) const
		{
			return ((ZVertex_Pos&)m_VertexBuf[m_Faces[faceIdx].idx[vIdx]* m_VertexTypeData.ByteSize]).Pos;
		}

		//   指定番号の面の、3頂点の座標を取得
		//   faceIdx		… 面番号
		//  @param[out] outPos		… 取得した頂点を格納する座標配列 ※[3]以上必要
		void GetFaceVertices_Pos(UINT faceIdx, ZOperationalVec3 *outPos) const
		{
			auto& face = m_Faces[faceIdx];
			outPos[0] = ((ZVertex_Pos&)m_VertexBuf[face.idx[0]* m_VertexTypeData.ByteSize]).Pos;
			outPos[1] = ((ZVertex_Pos&)m_VertexBuf[face.idx[1]* m_VertexTypeData.ByteSize]).Pos;
			outPos[2] = ((ZVertex_Pos&)m_VertexBuf[face.idx[2]* m_VertexTypeData.ByteSize]).Pos;
		}

		//   指定番号の頂点データ座標
		//   verIndex		… 頂点番号(0 〜 頂点数-1)
		//  @return 頂点座標
		inline const ZVec3& GetVertex_Pos(int verIndex)const
		{
			return ((ZVertex_Pos&)m_VertexBuf[verIndex* m_VertexTypeData.ByteSize]).Pos;
		}

		//   面配列を取得
		const ZAVector<Face>&			GetFace() const { return m_Faces; }

		//   面拡張データ配列を取得
		const ZAVector<ExtFace>&			GetExtFace() const { return m_ExtFaces; }

		const ZOctree<Face>&				GetFaceOctree()const { return m_FaceOctree; }

		//================================================================
		// その他、取得系
		//================================================================

		//   頂点バッファ取得(参照カウンタは加算されません)
		ID3D11Buffer*						GetVB() { return m_VB; }

		//   インデックスバッファ取得(参照カウンタは加算されません)
		ID3D11Buffer*						GetIB() { return m_IB; }


		//================================================================
		//
		// 描画関係
		//
		//================================================================

		//------------------------------------------------------
		//   描画時に必要なデータをデバイスへセット
		//  以下の物をデバイスへセットする	\n
		//  ・頂点バッファ					\n
		//  ・インデックスバッファ			\n
		//  ・プリミティブ・トポロジー
		//------------------------------------------------------
		void SetDrawData();

		void SetPrimitiveTopology();

		//------------------------------------------------------
		//   指定サブセットを描画
		//  	subsetNo	… 描画するサブセット番号(マテリアル番号)
		//------------------------------------------------------
		void DrawSubset(int subsetNo);

		//------------------------------------------------------
		//   指定サブセットを描画(インスタンシング描画)
		//  	subsetNo	… 描画するサブセット番号(マテリアル番号)
		//  	instanceNum	… 描画するモデル数
		//------------------------------------------------------
		void DrawSubsetInstance(int subsetNo, int instanceNum);

		//======================================================
		//
		// メッシュ作成・解放
		//
		//======================================================

		//------------------------------------------------------
		//   解放
		//------------------------------------------------------
		void Release();

		//------------------------------------------------------
		//   メッシュを作成する
		//   verArray				… コピー元の頂点配列の先頭アドレス
		//   numVertex			… コピー元の頂点配列の数
		//   faceArray			… コピー元の面配列の先頭アドレス
		//   numFace				… コピー元の面配列の数
		//   subsetTbl			… コピー元サブセット配列
		//------------------------------------------------------
		template<typename VType>
		bool Create(const VType* verArray, unsigned int numVertex, const Face* faceArray, unsigned int numFace, const ZAVector<ZMeshSubset>& subsetTbl);

		//------------------------------------------------------
		//   頂点配列を登録する
		//  	verArray			… コピー元の頂点配列の先頭アドレス
		//  	numVertex			… コピー元の頂点配列の数
		//  	bCreateVertexBuffer	… Direct3D11の頂点バッファを作成するか？
		//  @return 成功:true
		//------------------------------------------------------
		template<typename VType>
		bool SetVertexArray(const VType* verArray, unsigned int numVertex, bool bCreateVertexBuffer);

		//------------------------------------------------------
		//   登録した頂点配列から、Direct3D11の頂点バッファを作成する
		//------------------------------------------------------
		bool CreateVertexBuffer();

		//------------------------------------------------------
		//   面配列を登録する
		//   faceArray			… コピー元の面配列の先頭アドレス
		//   numFace				… コピー元の面配列の数
		//   bCreateIndexBuffer	… Direct3D11のインデックスバッファを作成するか？
		//  @return 成功:true
		//------------------------------------------------------
		bool SetFaceArray(const Face* faceArray, unsigned int numFace, bool bCreateIndexBuffer);

		//------------------------------------------------------
		//   登録した面配列から、Direct3D11のインデックスバッファを作成する
		//------------------------------------------------------
		bool CreateIndexBuffer();

		//------------------------------------------------------
		//   サブセットリスト登録
		//   subsetTbl	… コピー元サブセット配列
		//------------------------------------------------------
		void SetSubset(const ZAVector<ZMeshSubset>& subsetTbl)
		{
			m_Subset = subsetTbl;
		}


		//------------------------------------------------------
		//   あたり判定などに便利な、追加情報を作成する
		//------------------------------------------------------
		bool CreateExtData();


		//======================================================



		//   面データ構造体
		struct Face
		{
			UINT idx[3];	// 頂点１〜３へのインデックス
		};

		//   面拡張データ構造体
		struct ExtFace
		{
			ZVec3	vN;				// 面の方向

			ZVec3	vAAABB_Center;		// 面のAABB中心座標
			ZVec3	vAAABB_HalfSize;	// 面のAABBハーフサイズ
		};

		//
		ZMesh();
		//
		~ZMesh()
		{
			Release();
		}

	private:
		// Direct3D11用バッファ
		ID3D11Buffer*				m_VB = nullptr;		// 頂点バッファ
		ID3D11Buffer*				m_IB = nullptr;		// インデックスバッファ

		ZAVector<ZMeshSubset>	m_Subset;			// サブセット情報配列　通常マテリアルと同じ数になる

		ZOctree<Face>			m_FaceOctree;		// 当たり判定用8分木

		// 頂点データ
		ZVertexTypeData			m_VertexTypeData;	// 頂点レイアウトデータ
		ZAVector<char>			m_VertexBuf;		// 頂点配列(そのまま頂点バッファとして作成可能な形)

		// 面データ
		ZAVector<Face>			m_Faces;			// 面配列(そのままインデックスバッファとして作成可能な形)

		ZAVector<ExtFace>		m_ExtFaces;			// 面拡張データ配列(上記の面配列では足りないデータを補足したもの。特にあたり判定の高速化などに使用)

		bool						m_bSkinMesh = false;	// スキンメッシュか？

		// 境界データ
		ZVec3						m_AABB_Center;		// このメッシュのAABBのローカル中心座標
		ZVec3						m_AABB_HalfSize;	// このメッシュのAABBのハーフサイズ
		ZVec3						m_vBSCenter;		// このメッシュの境界球ローカル座標
		float						m_fBSRadius = 0;	// このメッシュの境界球半径

	private:
		// コピー禁止用
		ZMesh(const ZMesh& src) {}
		void operator=(const ZMesh& src) {}

	};

	// 作成
	template<typename VType>
	bool ZMesh::Create(const VType* verArray, unsigned int numVertex, const Face* faceArray, unsigned int numFace, const ZAVector<ZMeshSubset>& subsetTbl)
	{
		// 頂点配列作成
		if (SetVertexArray(verArray, numVertex, true) == false)
		{
			Release();
			return false;
		}
		// 面配列作成
		if (SetFaceArray(faceArray, numFace, true) == false)
		{
			Release();
			return false;
		}

		// Subset設定
		SetSubset(subsetTbl);

		// 拡張データ作成
		CreateExtData();

		return true;
	}

	// 頂点配列を登録する
	template<typename VType>
	bool ZMesh::SetVertexArray(const VType* verArray, unsigned int numVertex, bool bCreateVertexBuffer)
	{
		uint32_t totalSize = sizeof(VType) * numVertex;
		m_VertexBuf.resize(totalSize);
		m_VertexBuf.shrink_to_fit();

		// 頂点配列コピー
		memcpy(&m_VertexBuf[0], verArray, totalSize);

		// 頂点タイプ
		m_VertexTypeData = VType::GetVertexTypeData();

		// 頂点タイプがにスキンメッシュ用の物か
		m_bSkinMesh = m_VertexTypeData.IsSkinMeshVertexType;
		
		// 頂点バッファ作成
		if (bCreateVertexBuffer)
		{
			return CreateVertexBuffer();
		}
		return true;
	}


}

#endif
