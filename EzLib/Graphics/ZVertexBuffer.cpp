#include "PCH/pch.h"

namespace EzLib
{
	D3D11_USAGE ZBufferTypeToD3D11USAGE(ZBufferType type)
	{
		switch (type)
		{
			case ZBufferType::Static: return D3D11_USAGE_DEFAULT;
			case ZBufferType::Dynamic:return D3D11_USAGE_DYNAMIC;
		}

		return D3D11_USAGE_DEFAULT;
	}

	ZVertexBuffer::ZVertexBuffer(ZBufferType type)
		: m_BufferType(type),m_BufferSize(0)
	{
		ZeroMemory(&m_BufferDesc, sizeof(D3D11_BUFFER_DESC));
		m_BufferDesc.Usage = ZBufferTypeToD3D11USAGE(type);
		m_BufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		if (type == ZBufferType::Dynamic)
			m_BufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		else
			m_BufferDesc.CPUAccessFlags = 0;
	}

	ZVertexBuffer::~ZVertexBuffer()
	{
		Release();
	}

	void ZVertexBuffer::Release()
	{
		m_BufferSize = 0;
		Safe_Release(m_pBuffer);
	}

	void ZVertexBuffer::Resize(uint size)
	{
		m_BufferSize = size;
		m_BufferDesc.ByteWidth = size;
		HRESULT hr = ZDx.GetDev()->CreateBuffer(&m_BufferDesc, NULL, &m_pBuffer);
		assert(!FAILED(hr));
	}

	void ZVertexBuffer::SetLayout(const ZBufferLayout& bufferLayout)
	{
		m_BufferLayout = bufferLayout;
		const ZVector<ZBufferElement> layout = std::move(bufferLayout.GetLayout());
		size_t elemCnt = 0;
		for (auto& elem : layout)
		{
			if (elem.Size <= sizeof(ZVec4))
				elemCnt++;
			else
				elemCnt += elem.Size / sizeof(ZVec4);
		}

		D3D11_INPUT_ELEMENT_DESC* desc = sysnewArray(D3D11_INPUT_ELEMENT_DESC, elemCnt);
		for (size_t i = 0; i < elemCnt;)
		{
			const auto& elem = layout[i];
			uint slotIndex = 0;
			if (elem.InputClassification == ZInputClassification::PER_INSTANCE)
				slotIndex = 1;

			if(elem.Size <= sizeof(ZVec4))
			{
				desc[i] =
				{
					elem.Name.c_str(),
					0,
					elem.Type,
					slotIndex,
					elem.Offset,
					(D3D11_INPUT_CLASSIFICATION)elem.InputClassification,
					elem.InstanceDataStepRate
				};
				i++;
				continue;
			}

			for(size_t j = 0;j < elem.Size / sizeof(ZVec4);j++,i++)
			{
				desc[i] =
				{
					elem.Name.c_str(),
					j,
					elem.Type,
					slotIndex,
					elem.Offset + sizeof(ZVec4) * j,
					(D3D11_INPUT_CLASSIFICATION)elem.InputClassification,
					elem.InstanceDataStepRate
				};
			}

		} // for (size_t i = 0; i < elemCnt;)

		// shader get
		// create input layout

		delptr(desc);
	}

	void ZVertexBuffer::SetData(uint size, const void* data)
	{
		if (m_BufferSize < size)
			Resize(size);
		
		GetPointer();
		memcpy(m_MappedSubresource.pData, data, size);
		ReleasePointer();
	}

	void* ZVertexBuffer::GetPointer()
	{
		HRESULT hr = ZDx.GetDevContext()->Map(m_pBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &m_MappedSubresource);
		assert(!FAILED(hr));
		return m_MappedSubresource.pData;
	}

	void ZVertexBuffer::ReleasePointer()
	{
		ZDx.GetDevContext()->Unmap(m_pBuffer, NULL);
	}

	void ZVertexBuffer::Bind()
	{
		uint offset = 0;
		uint stride = m_BufferLayout.GetStride();
		ZDx.GetDevContext()->IASetInputLayout(m_InputLayout);
		ZDx.GetDevContext()->IASetVertexBuffers(0, 1, &m_pBuffer, &stride, &offset);
	}

}
