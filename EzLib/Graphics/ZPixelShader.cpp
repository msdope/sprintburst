#include "PCH/pch.h"

namespace EzLib
{
	ZPixelShader::ZPixelShader()
	{
	}
	
	bool ZPixelShader::LoadShader(const ZString& csoFileName)
	{
		Release();
	
		m_csoFileName = csoFileName;
	
		ZFile file;
		if (file.Open(csoFileName, ZFile::OPEN_MODE::READ))
		{
			// ファイルサイズ
			int len = file.GetSize();
			
			// 読み込み
			ZVector<uint8> buffer;
			file.ReadAll(buffer);

			// Blob(バッファ)作成
			D3DCreateBlob(len, &m_pCompiledShader);	
			
			memcpy(m_pCompiledShader->GetBufferPointer(), buffer.data(), len);
		}
		else
		{
			MessageBox(0, "指定したcsoファイルが存在しません", csoFileName.c_str(), MB_OK);
			Release();
			return false;
		}
	
		//==========================================================================
		// ブロブからピクセルシェーダー作成
		//==========================================================================
		if (FAILED(ZDx.GetDev()->CreatePixelShader(m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize(), nullptr, &m_PS)))
		{
			MessageBox(0, "ピクセルシェーダー作成失敗", csoFileName.c_str(), MB_OK);
			Release();
			return false;
		}
	
		return true;
	}
	
	bool ZPixelShader::LoadShaderFromFile(const ZString& hlslFileName, const ZString& PsFuncName, const ZString& shaderModel, UINT shaderFlag)
	{
		Release();
	
		// 最適化
		#ifdef OPTIMIZESHADER
		shaderFlag |= D3DCOMPILE_OPTIMIZATION_LEVEL3;	// 最大の最適化 リリース用
		#else
		shaderFlag |= D3DCOMPILE_OPTIMIZATION_LEVEL0 | D3DCOMPILE_DEBUG;	// 最低の最適化 開発用
		#endif
	
		ID3DBlob *pErrors = nullptr;
	
		m_hlslFileName = hlslFileName;
	
		// csoファイル名生成( [hlslFileName]_[VsFuncName].cso )
		char szDir[_MAX_PATH];
		char szFname[_MAX_FNAME];
		char szExt[_MAX_EXT];
		_splitpath_s(m_hlslFileName.c_str(), nullptr, 0, szDir, _MAX_PATH, szFname, _MAX_FNAME, szExt, _MAX_EXT);
	
		m_csoFileName = szDir;
		m_csoFileName += szFname;
		m_csoFileName += "_";
		m_csoFileName += PsFuncName;
		m_csoFileName += ".cso";
	
		//==========================================================================
		// HLSLからピクセルシェーダをコンパイル
		//==========================================================================
		if (m_pCompiledShader == nullptr)
		{
			if (FAILED(D3DCompileFromFile(ConvertStringToWString(hlslFileName).c_str(), nullptr, D3D_COMPILE_STANDARD_FILE_INCLUDE, PsFuncName.c_str(), shaderModel.c_str(), shaderFlag, 0, &m_pCompiledShader, &pErrors)))
			{
				if (pErrors)
				{
					MessageBox(0, (char*)pErrors->GetBufferPointer(), "HLSLコンパイル失敗", MB_OK);
				}
				else
				{
					MessageBox(0, "HLSLファイルが存在しない", hlslFileName.c_str(), MB_OK);
				}
				return false;
			}
			Safe_Release(pErrors);
		}
	
		//==========================================================================
		// ブロブからピクセルシェーダー作成
		//==========================================================================
		if (FAILED(ZDx.GetDev()->CreatePixelShader(m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize(), nullptr, &m_PS)))
		{
			MessageBox(0, "ピクセルシェーダー作成失敗", hlslFileName.c_str(), MB_OK);
			Release();
			return false;
		}
	
		return true;
	}
	
	bool ZPixelShader::SaveToFile()
	{
		if (m_pCompiledShader == nullptr)return false;
	
		//==========================================================================
		// コンパイル済みのシェーダを保存する
		//==========================================================================
		std::ofstream ofs(m_csoFileName.c_str(), std::ios::binary);
		ofs.write((char*)m_pCompiledShader->GetBufferPointer(), m_pCompiledShader->GetBufferSize());
	
		return true;
	}
	
	void ZPixelShader::Release()
	{
		Safe_Release(m_pCompiledShader);
		Safe_Release(m_PS);
	}
	
	void ZPixelShader::SetShader()
	{
		// ピクセルシェーダ設定
		ZDx.GetDevContext()->PSSetShader(m_PS, 0, 0);
	}
	
	void ZPixelShader::DisableShader()
	{
		// ピクセルシェーダ解除
		ZDx.GetDevContext()->PSSetShader(nullptr, 0, 0);
	}
}