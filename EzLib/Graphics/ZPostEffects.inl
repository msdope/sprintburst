inline bool ZPostEffectPass::IsEnable()const
{
	return m_IsEnable;
}

inline void ZPostEffectPass::SetPostEffcts(ZPostEffects* pPostEffects)
{
	m_pPostEffects = pPostEffects;
}

inline void ZPostEffectPass::SetEnable(bool flg)
{
	m_IsEnable = flg;
}

inline void ZPostEffects::AddEffectPass(const ZString& passName, ZPostEffectPass* pass)
{
	{
		auto it = m_PassMap.find(passName);
		// すでに同名で設定済みなら
		if (it != m_PassMap.end())
		{
			// リストから排除
			// あとから登録したものを優先
			RemovePassForPassList(it->second);
			SAFE_DELPTR(it->second);
		}
	}

	m_PassMap[passName] = pass;
	m_Passes.push_back(pass);
	// EffectPassに自身のポインタをもたせる
	pass->SetPostEffcts(this);
	
	// 設定データがあるなら設定読み込み処理
	if (m_SettingJsonObject.is_null() == false)
		pass->LoadSettingFromJson(m_SettingJsonObject);
}

inline ZPostEffectPass* ZPostEffects::GetEffectPass(const ZString& passName)
{
	try
	{
		auto pass = m_PassMap.at(passName);
		return pass;
	}
	catch(std::out_of_range&)
	{
		return nullptr;
	}
}

inline void ZPostEffects::RemoveEffectPass(const ZString& passName)
{
	try
	{
		auto& it = m_PassMap.at(passName);
		RemovePassForPassList(it);
		SAFE_DELPTR(it);
		m_PassMap.erase(passName);
	}
	catch (std::out_of_range&)
	{
		// 存在しない
		return;
	}
}

inline void ZPostEffects::SetTextureResource(const ZString& name, ZSP<ZTexture>& texture)
{
	m_TextureResources[name] = texture;
}

inline ZSP<ZTexture> ZPostEffects::GetSourceTexture(const ZString& name)
{
	try
	{
		auto tex = m_TextureResources.at(name);
		return tex;
	}
	catch (std::out_of_range&)
	{
		return ZSP<ZTexture>();
	}
}

inline void ZPostEffects::RemoveAllTextureResources()
{
	m_TextureResources.clear();
}

inline ZSP<ZTexture> ZPostEffects::GetCompletedTexture()
{
	return m_TexCompleted;
}
