#include "PCH/pch.h"

namespace EzLib
{

	ZBufferElement::ZBufferElement(ZString name, DXGI_FORMAT type, uint size, uint count, uint offset)
		: Name(name), Type(type), Size(size), Count(count), Offset(offset),
		InputClassification(ZInputClassification::PER_VERTEX), InstanceDataStepRate(0)
	{
	}

	ZBufferElement::ZBufferElement(ZString name, DXGI_FORMAT type, uint size, uint count, uint offset,
		ZInputClassification classification, uint instanceDataStepRate)
		: Name(name), Type(type), Size(size), Count(count), Offset(offset),
		InputClassification(classification), InstanceDataStepRate(instanceDataStepRate)
	{
	}

	ZBufferLayout::ZBufferLayout()
		: m_Size_PerVertex(0),m_Size_PerInstance(0)
	{
	}

	void ZBufferLayout::Push(const ZString & name, DXGI_FORMAT type, uint size, uint count)
	{
		m_Layout_PerVertex.push_back(ZBufferElement(name, type, size, count, m_Size_PerVertex));
		m_Size_PerVertex += size * count;
	}

	void ZBufferLayout::Push(const ZString & name, DXGI_FORMAT type, uint size, uint count,
		ZInputClassification classification, uint instanceDataStepRate)
	{
		m_Layout_PerInstance.push_back(ZBufferElement(name, type, size, count, m_Size_PerInstance, classification, instanceDataStepRate));
		m_Size_PerInstance += size * count;
	}



}
