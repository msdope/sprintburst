#include "PCH/pch.h"

using namespace EzLib;

// ファイル名と拡張子の間に、指定文字列を挿入した文字列を生成
ZString EzLib::ConvertExtFileName(const ZString& FileName, const ZString& ext)
{
	ZString extName = FileName;
	int pos = extName.find_last_of(".");
	if (pos == -1)return "";
	ZString tmp = ".";
	tmp += ext;
	extName.insert(pos, tmp);
	return extName;
}

//=====================================================================
// 法線マップ用のTangent,Binormalを算出する。
//=====================================================================
void EzLib::CalcTangentAndBinormal(
	ZVec3* p0, ZVec2* uv0,
	ZVec3* p1, ZVec2* uv1,
	ZVec3* p2, ZVec2* uv2,
	const ZVec3* vN, ZVec3* outTangent, ZVec3* outBinormal
)
{
	// 5次元→3次元頂点に
	ZVec3 CP0[3] = {
		ZVec3(p0->x, uv0->x, uv0->y),
		ZVec3(p0->y, uv0->x, uv0->y),
		ZVec3(p0->z, uv0->x, uv0->y),
	};
	ZVec3 CP1[3] = {
		ZVec3(p1->x, uv1->x, uv1->y),
		ZVec3(p1->y, uv1->x, uv1->y),
		ZVec3(p1->z, uv1->x, uv1->y),
	};
	ZVec3 CP2[3] = {
		ZVec3(p2->x, uv2->x, uv2->y),
		ZVec3(p2->y, uv2->x, uv2->y),
		ZVec3(p2->z, uv2->x, uv2->y),
	};

	// 平面パラメータからUV軸座標算出
	float U[3], V[3];
	for (int i = 0; i < 3; ++i)
	{
		ZVec3 V1 = CP1[i] - CP0[i];
		ZVec3 V2 = CP2[i] - CP1[i];
		ZVec3 ABC;
		ZVec3::Cross(ABC, V1, V2);

		if (ABC.x == 0.0f)
		{
			// ポリゴンかUV上のポリゴンが縮退してます
			//			_ASSERT(0);
			// とりあえずvNを元に適当な物を用意
			ZVec3::Cross(*outTangent, ZVec3(0, 1, 0), *vN);
			outTangent->Normalize();
			ZVec3::Cross(*outBinormal, *vN, *outTangent);
			outBinormal->Normalize();
			return;
		}
		U[i] = -ABC.y / ABC.x;
		V[i] = -ABC.z / ABC.x;
	}

	memcpy(outTangent, U, sizeof(float)* 3);
	memcpy(outBinormal, V, sizeof(float)* 3);

	// 正規化
	outTangent->Normalize();
	outBinormal->Normalize();
}

ZWString EzLib::ConvertStringToWString(const ZString& str)
{
	size_t len = str.length();
	ZWString wStr;
	wStr.resize(len);
	size_t wLen;
	mbstowcs_s(&wLen, &wStr[0], len + 1, str.c_str(), _TRUNCATE);
	return wStr;
}

ZString EzLib::ConvertWStringToString(const ZWString& wstr)
{
	size_t wLen = wstr.length();
	ZString str;
	str.resize(wLen * MB_CUR_MAX + 1);
	size_t len;
	wcstombs_s(&len, &str[0], wLen * MB_CUR_MAX + 1, wstr.c_str(), wLen * MB_CUR_MAX + 1);
	
	return str;
}

ZString EzLib::Utf8ToMulti(const ZString& str)
{
	auto const uni_size = MultiByteToWideChar(CP_UTF8, 0, str.c_str(),str.size()+1, nullptr, 0);
	ZVector<wchar_t>bufUni(uni_size);
	// utf8 -> unicord
	MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.size() + 1, bufUni.data(), uni_size);

	int sjis_size = WideCharToMultiByte(CP_THREAD_ACP, 0, bufUni.data(), -1, nullptr, 0, nullptr, nullptr);

	ZVector<char> sjisBuf(sjis_size);
	WideCharToMultiByte(CP_THREAD_ACP, 0, bufUni.data(), uni_size + 1, sjisBuf.data(), sjis_size, nullptr, nullptr);
	
	return ZString(sjisBuf.begin(), sjisBuf.end());
}

ZString EzLib::Utf16ToUtf8(const ZU16String & str)
{
	std::wstring_convert<std::codecvt_utf8_utf16<uint16_t>, uint16_t,ZSTLAllocator<uint16_t>,ZSTLAllocator<char>> converter;
	ZString dest = converter.to_bytes(reinterpret_cast<const uint16_t*>(str.c_str()));
	return dest;
}

ZString EzLib::Utf16ToMulti(const ZU16String& str)
{
	const int len = ::WideCharToMultiByte(CP_ACP, 0, (wchar_t*)str.c_str(), -1, nullptr, 0, nullptr, nullptr);
	ZString re(len * 2, '\0');
	WideCharToMultiByte(CP_ACP, 0, (wchar_t*)str.c_str(), -1, &re[0], len, nullptr, nullptr);
	const size_t real_len = std::strlen(re.c_str());
	re.resize(real_len);
	re.shrink_to_fit();
	return re;
}

