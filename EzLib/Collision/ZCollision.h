//===============================================================
//  @file ZCollision.h
//   当たり判定クラス
// 
//  @author 鎌田
//===============================================================
#ifndef ZCollision_h
#define ZCollision_h

namespace EzLib
{
	class ZMesh;

	// AABB
	struct ZAABB
	{
	public:
		ZAABB()
			: Min(0), Max(0)
		{
		}

		ZAABB(const ZVec3& _min, const ZVec3& _max)
			: Min(_min), Max(_max)
		{
		}

		void Transform(const ZMatrix& mat);

	public:
		ZVec3 Min;
		ZVec3 Max;
	};

	struct ZRay
	{
	public:
		ZRay() : Start(0),End(0),Dir(0)
		{
		}

		ZRay(const ZVec3& start, const ZVec3& end)
			: Start(start),End(end),Dir((end - start).Normalized())
		{
		}

	public:
		ZVec3 Start;
		ZVec3 End;
		ZVec3 Dir;
	};

	//================================================================
	//   当たり判定クラス
	// 
	//  関数はすべてstatic関数です
	// 
	//  @ingroup Collision
	//================================================================
	class ZCollision
	{
	public:
		//=======================================================================
		//   点と立方体(回転なし)
		//   vMin		… BOX1の左上の座標
		//   vMax		… BOX1の右上の座標
		//   pnt		… 点の座標
		//  @return trueだとヒットした
		//=======================================================================
		static bool PointToBox(const ZVec3& vMin, const ZVec3& vMax, const ZVec3& pnt)
		{
			if ((pnt.x < vMin.x) ||
				(pnt.y < vMin.y) ||
				(pnt.z < vMin.z) ||
				(pnt.x > vMax.x) ||
				(pnt.y > vMax.y) ||
				(pnt.z > vMax.z))
				return false;

			return true;
		}

		//=======================================================================
		//   2D四角形の同士判定(AABB)
		//   min1		… BOX1の左上の座標
		//   max1		… BOX1の右上の座標
		//   min2		… BOX2の左上の座標
		//   max2		… BOX2の右上の座標
		//  @return trueだとヒットした
		//=======================================================================
		static bool BoxToBox2D(const ZVec2 &min1, const ZVec2 &max1, const ZVec2 &min2, const ZVec2 &max2)
		{
			if ((max1.x < min2.x) ||
				(min1.x > max2.x) ||
				(max1.y < min2.y) ||
				(min1.y > max2.y))
				return false;

			return true;
		}
		//=======================================================================
		//   3D四角形の同士判定(AABB)
		//   min1		… BOX1の最少座標
		//   max1		… BOX1の最大座標
		//   min2		… BOX2の最少座標
		//   max2		… BOX2の最大座標
		//  @return trueだとヒットした
		//=======================================================================
		static bool BoxToBox3D_MinMax(const ZVec3 &min1, const ZVec3 &max1, const ZVec3 &min2, const ZVec3 &max2)
		{
			if ((max1.x < min2.x) ||
				(min1.x > max2.x) ||
				(max1.y < min2.y) ||
				(min1.y > max2.y) ||
				(max1.z < min2.z) ||
				(min1.z > max2.z))
				return false;

			return true;
		}

		//=======================================================================
		//   3D四角形の同士判定(AABB)
		//   center1		… BOX1の中心座標
		//   halfSize1	… BOX1の半分のサイズ
		//   center2		… BOX2の中心座標
		//   halfSize2	… BOX2の半分のサイズ標
		//  @return trueだとヒットした
		//=======================================================================
		static bool BoxToBox3D(const ZVec3 &center1, const ZVec3 &halfSize1, const ZVec3 &center2, const ZVec3 &halfSize2)
		{
			if ((ABS(center1.x - center2.x) > (halfSize1.x + halfSize2.x)) ||
				(ABS(center1.y - center2.y) > (halfSize1.y + halfSize2.y)) ||
				(ABS(center1.z - center2.z) > (halfSize1.z + halfSize2.z)))
				return false;
			return true;
		}

		//=======================================================================
		//   回転ありの3D四角形の同士判定(OBB)
		//   localMin1		… BOX1の最少座標
		//   localMax1		… BOX1の最大座標
		//   m1				… BOX1の行列
		//   localMin2		… BOX2の最少座標
		//   localMax2		… BOX2の最大座標
		//   m2				… BOX2の行列
		//  @return trueだとヒットした
		//=======================================================================
		static bool BoxToBox3D_OBB(const ZVec3 &localMin1, const ZVec3 &localMax1, const ZMatrix &m1,
			const ZVec3 &localMin2, const ZVec3 &localMax2, const ZMatrix &m2)
		{
			ZQuat q;
			ZVec3 scale;
			ZVec3 vExtents;
			ZVec3 vCenter;

			scale = m1.GetScale();
			vExtents = (localMax1 - localMin1) / 2;
			vCenter = localMin1 + vExtents;
			vCenter.Transform(m1);
			vExtents *= scale;

			m1.ToQuaternion(q, true);
			DirectX::BoundingOrientedBox box1(vCenter, vExtents, q);

			scale = m2.GetScale();
			vExtents = (localMax2 - localMin2) / 2;
			vCenter = localMin2 + vExtents;
			vCenter.Transform(m2);
			vExtents *= scale;

			m2.ToQuaternion(q, true);
			DirectX::BoundingOrientedBox box2(vCenter, vExtents, q);

			return box1.Intersects(box2);
		}

		//=======================================================================
		//   回転ありの箱 vs 球
		//=======================================================================
		static bool Box3DToSphere_OBB(const ZVec3 &localMin1, const ZVec3 &localMax1, const ZMatrix &m1,
			const ZVec3& vQpos, float r);


		//=======================================================================
		//   球 vs 球
		//  	vQpos1	… 球１の座標
		//  	r1		… 球１の半径
		//  	vQpos2	… 球２の座標
		//  	r2		… 球２の半径
		//  @param[out] outMove	… 球２から球１へのベクトルが入る
		//  @return めり込んでいる距離。つまり、＋値だとＨＩＴ、−値だと当たっていない。
		//=======================================================================
		static float SphereToSphere(const ZVec3& vQpos1, float r1, const ZVec3& vQpos2, float r2, ZVec3& outMove)
		{
			outMove = vQpos1 - vQpos2;
			float len = ZVec3::Length(outMove);

			return r1 + r2 - len;
		}
		//=======================================================================
		//   球 vs 球
		//   vQpos1	… 球１の座標
		//   r1		… 球１の半径
		//   vQpos2	… 球２の座標
		//   r2		… 球２の半径
		//  @return trueだとヒットした
		//=======================================================================
		static bool SphereToSphere(const ZVec3& vQpos1, float r1, const ZVec3& vQpos2, float r2)
		{
			ZVec3 vOut = vQpos1 - vQpos2;
			float lenPow2 = ZVec3::Dot(vOut, vOut);

			return lenPow2 < (r1 + r2);
		}

		//=======================================================================
		//   球 vs レイ
		//  	vQpos		… 球の座標
		//  	Qr			… 球の半径
		//  	rayPos		… レイの座用
		//  	rayDir		… レイの方向(正規化しておくこと)
		//  @param[out]	dist		… レイがヒットした距離が入る
		//  @param[out] outPush		… 球を押し戻したい場合に使用できるベクトルが入る
		//  @return trueだとヒット
		//=======================================================================
		static bool SphereToRay(const ZVec3& vQpos, float Qr, const ZVec3& rayPos, const ZVec3& rayDir, float* dist, ZVec3* outPush);

		//=======================================================================
		//   点 vs 三角形 の最近接点を求める
		//  [書籍]ゲームプログラミングのためのリアルタイム衝突判定のClosestPtPointTriangle()を参照
		//  	p			… 点の座標
		//  	a			… 三角形の座標１
		//  	b			… 三角形の座標２
		//  	c			… 三角形の座標３
		//  @param[out]	outPt		… 三角形上の最近接座標が返る
		//=======================================================================
		static void PointToTriangle(const ZOperationalVec3& p, const ZOperationalVec3& a, const ZOperationalVec3& b, const ZOperationalVec3& c, ZOperationalVec3& outPt);

		//=======================================================================
		//   球 vs 三角形
		//  [書籍]ゲームプログラミングのためのリアルタイム衝突判定のTestSphereTriangle()を参照
		//  	QPos		… 球の座標
		//  	Qr			… 球の半径
		//  	a			… 三角形の座標１
		//  	b			… 三角形の座標２
		//  	c			… 三角形の座標３
		//  @param[out]	outPt		… 三角形上の最近接座標が返る
		//=======================================================================
		static bool SphereToTriangle(const ZOperationalVec3& QPos, float Qr, const ZOperationalVec3& a, const ZOperationalVec3& b, const ZOperationalVec3& c, ZOperationalVec3& outNearPt);

		//=======================================================================
		//   レイ vs 三角形
		//=======================================================================
		static bool RayToTriangle(const ZVec3& p1, const ZVec3& p2, const ZVec3& p3, const ZVec3& rayPos, const ZVec3& rayDir, float& dist);

		//=======================================================================
		//   線分 vs 線分 の最近接距離
		//  @param[out]	outS		… 線分p1〜q1を0〜1としたときの位置が返る
		//  @param[out]	outT		… 線分p2〜q2を0〜1としたときの位置が返る
		//  @param[out]	outC1		… 線分p1〜q1上の、最近接距離の座標
		//  @param[out]	outC2		… 線分p2〜q2上の、最近接距離の座標
		//  @return 最近接距離の２乗
		//=======================================================================
		static float LineToLine(const ZOperationalVec3& p1, const ZOperationalVec3& q1, const ZOperationalVec3& p2, const ZOperationalVec3& q2, float& outS, float outT, ZOperationalVec3& outC1, ZOperationalVec3& outC2);

		//=======================================================================
		//   メッシュとレイとの判定(レイの指定は方向ではなく座標１と座標２での指定なので注意)
		// 
		//  高速化のためmeshDataの内部にあるAABBデータを使って簡易判定を行い、明らかにHITしないものは除外する			\n
		//  それによりレイの方向を指定ではなく、レイの開始座標〜終了座標(長さのあるレイ)を指定する形になっています。	\n
		//  対象メッシュの変換行列も指定できるので、動いている物体とも判定可能。
		// 
		//  	mesh			… 判定するメッシュ
		//  	lpMat			… メッシュの変換行列　nullptrだと指定なし
		//  	rayPosStart		… レイの開始座標
		//  	rayPosEnd		… レイの終了座標
		//  @param[out]	outDist			… レイがヒットした距離が入る
		//  @param[out]	outHitFaceIndex	… レイがヒットした面の番号が入る
		//  @return trueだとヒット
		//=======================================================================
		static bool RayToMesh(const ZMesh& mesh, const ZMatrix* lpMat, const ZVec3& rayPosStart, const ZVec3& rayPosEnd, float* outDist, UINT* outHitFaceIndex);



		//=======================================================================
		//   AABBとレイとの判定(レイの指定は方向ではなく座標１と座標２での指定なので注意)
		// 
		//  高速化のためmeshDataの内部にあるAABBデータを使って簡易判定を行い、明らかにHITしないものは除外する			\n
		//  それによりレイの方向を指定ではなく、レイの開始座標〜終了座標(長さのあるレイ)を指定する形になっています。	\n
		//  対象メッシュの変換行列も指定できるので、動いている物体とも判定可能。
		// 
		//  	aabb			… 判定するAABB
		//  	lpMat			… メッシュの変換行列　nullptrだと指定なし
		//  	ray				… レイ
		//  @param[out]	outDist			… レイがヒットした距離が入る
		//  @param[out]	outHitPos		… レイがヒットした座標が入る
		//=======================================================================
		static bool RayToBox(const ZRay& ray, const ZMatrix* lpMat, const ZAABB& aabb, float* outDist,ZVec3* outHitPos);


		// 
		struct MTS_HitData
		{
			int		faceNo;		// 面番号
			ZVec3	vNearPos;	// 衝突した面の最近接距離

			MTS_HitData(int no, const ZVec3& nearPos) : faceNo(no), vNearPos(nearPos) {}
			MTS_HitData(int no, const ZOperationalVec3& nearPos) : faceNo(no), vNearPos(nearPos) {}
		};

		//=======================================================================
		//   球とメッシュ(ZMeshの中にある頂点や面情報を使用)
		//  	meshData	… 判定するメッシュ
		//  	lpMat		… 変換行列 nullptrだと指定なし
		//  	lpQPos		… 球の位置
		//  	Qr			… 球の半径
		//  @param[out] outMove		… めり込んだ分のベクトルが返る(面→球の方向)
		//   startFaceNo	… 判定する面の開始番号　0で最初から
		//   faceNum		… 判定する面の数　-1で全ての面数
		//  @param[out] outHitDataTbl … ヒットした座標が返る
		//  @return trueだとヒット
		//=======================================================================
		static bool MeshToSphere(const ZMesh& mesh, const ZMatrix* lpMat, const ZVec3& vQPos, float Qr, ZVec3* outMove, int startFaceNo = 0, int faceNum = -1, ZVector<MTS_HitData>* outHitDataTbl = nullptr);

		//=======================================================================
		//   球とメッシュ(ZMeshの中にある頂点や面情報を使用 指定マテリアルの面のみ)
		//  meshDataの指定したマテリアルNoの面とだけ判定
		//  	mateNo	… 判定したいマテリアル番号
		//=======================================================================
		static bool MeshToSphere(const ZMesh& mesh, int mateNo, const ZMatrix* lpMat, const ZVec3& vQPos, float Qr, ZVec3* outMove);

		// その他
		static constexpr float ABS(float a)
		{
			//uint32_t n = ((*(uint32_t*)&a)& 0x7FFFFFFF);
			//return *(float*)&n;
			return a < 0 ? -a : a;
		}
	};

}

#endif