#include "PCH/pch.h"

using namespace EzLib;

static inline float Clamp(float n, float min, float max)
{
	if (n < min)
		return min;
	if (n > max)
		return max;
	return n;
}

// 回転ありの箱 vs 球
bool ZCollision::Box3DToSphere_OBB(const ZVec3& localMin1, const ZVec3& localMax1, const ZMatrix& m1, const ZVec3& vQpos, float r)
{
	ZQuat q;
	ZVec3 scale;
	ZVec3 vExtents;
	ZVec3 vCenter;

	scale = m1.GetScale();
	vExtents = (localMax1 - localMin1) / 2;
	vCenter = localMin1 + vExtents;
	vCenter.Transform(m1);
	vExtents *= scale;

	m1.ToQuaternion(q, true);
	DirectX::BoundingOrientedBox box1(vCenter, vExtents, q);

	DirectX::BoundingSphere sph(vQpos, r);
	return box1.Intersects(sph);
}

// 球 vs レイ
bool ZCollision::SphereToRay(const ZVec3& vQpos, float Qr, const ZVec3& rayPos, const ZVec3& rayDir, float* dist, ZVec3* outPush)
{
	bool bHit = false;	// HITしたかどうかを入れる

	const ZVec3 v = vQpos - rayPos;
	float l = ZVec3::Dot(v, rayDir);

	if (l < 0)
	{
		// 内積がマイナスだと、触れていない可能性が高いので別判定
		// 球とレイ座標(点)との判定
		l = v.Length();
		if (l <= Qr)
		{	// HIT確定
			bHit = true;
			if (dist)
				*dist = 0;

			// 点の場合は、点と球を結ぶ方向に押し戻すためのベクトルを返す
			if (outPush)
			{
				*outPush = v;
				((ZVec3*)outPush)->Normalize();
				*outPush *= Qr - l;
			}
		}
	}
	// 内積がプラスの時は、レイの線上にいる
	else
	{
		const ZVec3 pos = rayPos + rayDir * l;
		const ZVec3 vQ = vQpos - pos;
		const float L = vQ.Length();
		if (L < Qr)
		{
			bHit = true;
			if (dist)
				*dist = l - sqrtf(Qr * Qr - L * L);

			// 線上の場合は、垂直の方向に押し戻すためのベクトルを返す
			if (outPush)
			{
				*outPush = vQ;
				((ZVec3*)outPush)->Normalize();
				*outPush *= Qr - L;
			}
		}
	}

	return bHit;
}

bool ZCollision::RayToMesh(const ZMesh& mesh, const ZMatrix* lpMat, const ZVec3& rayPosStart, const ZVec3& rayPosEnd, float* outDist, UINT* outHitFaceIndex)
{
	if (mesh.GetExtFace().size() == 0)
		return false;

	// レイ情報を対象の座標系へ変換
	ZOperationalVec3 vRayPos1;
	ZOperationalVec3 vRayPos2;
	ZOperationalVec3 vDir;
	float rayLen = 0;
	if (lpMat)
	{
		ZOperationalMatrix mat = *lpMat;
		ZOperationalMatrix invMat;
		mat.Inverse(invMat);
		// レイ情報を指定した行列の逆行列で変換し、ローカル座標系へ変換する。
		ZOperationalVec3::Transform(vRayPos1.v, rayPosStart, invMat);
		ZOperationalVec3::Transform(vRayPos2.v, rayPosEnd, invMat);

		vDir = rayPosEnd - rayPosStart;
		rayLen = vDir.Length();
		vDir /= rayLen;
		ZOperationalVec3::TransformNormal(vDir.v, vDir, invMat);
	}
	else
	{
		vRayPos1 = rayPosStart;
		vRayPos2 = rayPosEnd;

		vDir = vRayPos2 - vRayPos1;
		rayLen = vDir.Length();
		//		vDir /= rayLen;
		vDir.Normalize();
	}

	//---------------------------------------------
	// 高速化の為、レイ vs AABBで簡易判定を行う
	//---------------------------------------------
	DirectX::BoundingBox bbox(mesh.GetAABB_Center(), mesh.GetAABB_HalfSize());

	{
		float dist;
		if (bbox.Intersects(vRayPos1, vDir, dist) == false)
			return false;
	}


	//---------------------------------------------
	// 判定
	//---------------------------------------------

	// レイが衝突するメッシュの空間のリンクリストの先頭オブジェクトを取得
	ZRay ray({ vRayPos1 }, { vRayPos2 });
	ZVector<ZSP<ZObjectForTree<ZMesh::Face>>> firstTreeObjects;
	mesh.GetFaceOctree().GetCollisionList(ray, firstTreeObjects);

	// 返信用
	float fDist = FLT_MAX;
	UINT hitFaceIdx;

	// 作業用
	float d = 0;
	//	bool bHit;
	bool ret = false;

	ZAABB rayAABB;
	ZVec3::CreateAABBFromLineSegment({ vRayPos1 }, { vRayPos2 }, rayAABB.Min, rayAABB.Max);
	struct FacePositions
	{
		// 面の各頂点へのポインタ
		const ZVec3* Pos[3];
	};

	ZVector<FacePositions> facePositions; // ↓でレイと判定する面の頂点座標
	{
		FacePositions facePos; // 作業用
		for(auto& firstTreeObj : firstTreeObjects)
		{
			auto pOFT = firstTreeObj;
			while (pOFT != nullptr)
			{
				auto face = pOFT->m_pObject;
				for (int i = 0; i < 3; i++)
					facePos.Pos[i] = &mesh.GetVertex_Pos(face->idx[i]);
				// レイ対面のAABBで仮判定して余計なレイ対ポリゴンの判定を避ける
				ZAABB faceAABB;
				ZVec3::CreateAABB(*facePos.Pos[0], *facePos.Pos[1], *facePos.Pos[2], faceAABB.Min, faceAABB.Max);
				if(RayToBox(ray,nullptr,faceAABB,nullptr,nullptr)) // 当たっているなら
					facePositions.push_back(facePos);
				pOFT = pOFT->m_pNext;
			}
		}
	}

	const size_t faceTblNum = facePositions.size();
	for (size_t i = 0; i < faceTblNum; i++)
	{
		//---------------------------------------------
		// 0,1,2の面とレイ判定
		//---------------------------------------------
		auto& facePos = facePositions[i];
		if (DirectX::TriangleTests::Intersects(vRayPos1, vDir, *facePos.Pos[0], *facePos.Pos[1], *facePos.Pos[2], d))
		{
			if (d < fDist)
			{
				fDist = d;
				hitFaceIdx = i;
			}
			ret = true;
		}

	}

	if (ret)
	{
		// レイの範囲外なら当たってない
		if (fDist > rayLen)
			return false;

		// 当たってるときは、距離と面番号を入れる
		if (outDist)*outDist = fDist;
		if (outHitFaceIndex)*outHitFaceIndex = hitFaceIdx;

		return ret;
	}

	return false;
}

bool ZCollision::RayToBox(const ZRay& ray, const ZMatrix* lpMat, const ZAABB& aabb, float* outDist, ZVec3* outHitPos)
{
	ZVec3 vMin = aabb.Min;
	ZVec3 vMax = aabb.Max;

	ZRay tmpRay = ray;

	if(lpMat != nullptr)
	{
		// レイをAABBのローカル空間へ
		auto invMat = lpMat->Inversed();
		tmpRay.Start.Transform(invMat);
		tmpRay.End.Transform(invMat);
		tmpRay.Dir.TransformNormal(invMat);
	}
	float rayDist = (tmpRay.End - tmpRay.Start).Length();

	float p[3]{ tmpRay.Start.x, tmpRay.Start.y, tmpRay.Start.z };
	float d[3]{ tmpRay.Dir.x, tmpRay.Dir.y, tmpRay.Dir.z };
	float min[3]{ vMin.x, vMin.y, vMin.z };
	float max[3]{ vMax.x, vMax.y, vMax.z };

	float t = -FLT_MAX;
	float t_max = FLT_MAX;
	for (int i = 0; i < 3; i++)
	{
		if (abs(d[i]) < FLT_EPSILON)
		{
			if (p[i] < min[i] || p[i] > max[i])
				return false; //交差していない

			float odd = 1.0f / d[i];
			float t1 = (min[i] - p[i]) * odd;
			float t2 = (max[i] - p[i]) * odd;
			if (t1 > t2) std::swap(t1, t2);
			if (t1 > t) t = t1;
			if (t2 < t_max) t_max = t2;

			// スクラブ交差チェック
			if (t >= t_max)
				return false;
		}
	}

	// レイの範囲外
	if (t >= rayDist) return false;

	if (outDist != nullptr) *outDist = t;
	if (*outHitPos != nullptr)
		*outHitPos = ray.Start + (t * ray.Dir);

	return true;
}

// [書籍]ゲームプログラミングのためのリアルタイム衝突判定のClosestPtPointTriangle()を参照
void ZCollision::PointToTriangle(const ZOperationalVec3& p, const ZOperationalVec3& a, const ZOperationalVec3& b, const ZOperationalVec3& c, ZOperationalVec3& outPt)
{
	// pがaの外側の頂点領域の中にあるかどうかチェック
	const ZOperationalVec3 ab = b - a;
	const ZOperationalVec3 ac = c - a;
	const ZOperationalVec3 ap = p - a;

	const float d1 = ZOperationalVec3::Dot(ab, ap);
	const float d2 = ZOperationalVec3::Dot(ac, ap);
	if (d1 <= 0.0f && d2 <= 0.0f)
	{
		outPt = a;	// 重心座標(1,0,0)
		return;
	}

	// pがbの外側の頂点領域の中にあるかどうかチェック
	ZOperationalVec3 bp = p - b;
	const float d3 = ZOperationalVec3::Dot(ab, bp);
	const float d4 = ZOperationalVec3::Dot(ac, bp);
	if (d3 >= 0.0f && d4 <= d3)
	{
		outPt = b;	// 重心座標(0,1,0)
		return;
	}

	// pがabの辺領域の中にあるかどうかチェックし、あればpのab上に対する射影を返す
	const float vc = d1 * d4 - d3 * d2;
	if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f)
	{
		float v = d1 / (d1 - d3);
		outPt = a + ab * v;	// 重心座標(1-v,v,0)
		return;
	}

	// pがcの外側の頂点領域の中にあるかどうかチェック
	ZOperationalVec3 cp = p - c;
	const float d5 = ZOperationalVec3::Dot(ab, cp);
	const float d6 = ZOperationalVec3::Dot(ac, cp);
	if (d6 >= 0.0f && d5 <= d6)
	{
		outPt = c;	// 重心座標(0,0,1)
		return;
	}

	// pがacの辺領域の中にあるかどうかチェックし、あればpのac上に対する射影を返す
	const float vb = d5 * d2 - d1 * d6;
	if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f)
	{
		const float w = d2 / (d2 - d6);
		outPt = a + ac * w;	// 重心座標(1-w,0,w)
		return;
	}

	// pがbcの辺領域の中にあるかどうかチェックし、あればpのbc上に対する射影を返す
	const float va = d3 * d6 - d5 * d4;
	if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f)
	{
		const float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
		outPt = b + (c - b) * w;	// 重心座標(0,1-w,w)
		return;
	}

	// pは面領域の中にある。Qをその重心座標(u,v,w)を用いて計算
	const float denom = 1.0f / (va + vb + vc);
	const float v = vb * denom;
	const float w = vc * denom;
	outPt = a + ab * v + ac * w;
}

bool ZCollision::SphereToTriangle(const ZOperationalVec3& QPos, float Qr, const ZOperationalVec3& a, const ZOperationalVec3& b, const ZOperationalVec3& c, ZOperationalVec3& outNearPt)
{
	PointToTriangle(QPos, a, b, c, outNearPt);

	const ZOperationalVec3 v = outNearPt - QPos;

	//	return ZOperationalVec3::Dot(v, v) <= Qr*Qr;
	return v.LengthSq() <= Qr * Qr;
}

bool ZCollision::RayToTriangle(const ZVec3& p1, const ZVec3& p2, const ZVec3& p3, const ZVec3& rayPos, const ZVec3& rayDir, float& dist)
{
	return DirectX::TriangleTests::Intersects(rayPos, rayDir, p1, p2, p3, dist);
}

float ZCollision::LineToLine(const ZOperationalVec3& p1, const ZOperationalVec3& q1, const ZOperationalVec3& p2, const ZOperationalVec3& q2, float& outS, float outT, ZOperationalVec3& outC1, ZOperationalVec3& outC2)
{
	static constexpr float EPSILON = 1.0e-5f;	// 誤差吸収用の微小な値

	const ZOperationalVec3 d1 = q1 - p1;
	const ZOperationalVec3 d2 = q2 - p2;
	const ZOperationalVec3 r = p1 - p2;
	const float a = d1.LengthSq();
	const float e = d2.LengthSq();
	const float f = d2.Dot(r);

	// 片方あるいは両方の線分が点に縮退しているかどうかチェック
	if (a <= EPSILON && e <= EPSILON)
	{
		// 両方の線分が点に縮退
		outS = outT = 0.0f;
		outC1 = p1;
		outC2 = p2;
		return (outC1 - outC2).LengthSq();
	}
	if (a <= EPSILON)
	{
		// 最初の線分が点に縮退
		outS = 0.0f;
		outT = f / e;
		outT = Clamp(outT, 0.0f, 1.0f);
	}
	else
	{
		const float c = d1.Dot(r);
		if (e <= EPSILON)
		{
			// 2番目の線分が点に縮退
			outT = 0.0f;
			outS = Clamp(-c / a, 0.0f, 1.0f);
		}
		else
		{
			// ここから一般的な縮退の場合を開始
			const float b = d1.Dot(d2);

			{
				const float denom = a * e - b * b;
				if (denom != 0.0f)
					outS = Clamp((b *f - c * e) / denom, 0.0f, 1.0f);
				else
					outS = 0.0f;
			}

			outT = (b * outS + f) / e;

			if (outT < 0.0f)
			{
				outT = 0.0f;
				outS = Clamp(-c / a, 0.0f, 1.0f);
			}
			else if (outT > 1.0f)
			{
				outT = 1.0f;
				outS = Clamp((b - c) / a, 0.0f, 1.0f);
			}
		}
	}

	outC1 = p1 + d1 * outS;
	outC2 = p2 + d2 * outT;

	return (outC1 - outC2).LengthSq();
}

bool ZCollision::MeshToSphere(const ZMesh& mesh, const ZMatrix* lpMat, const ZVec3& vQPos, float Qr, ZVec3* outMove, int startFaceNo, int faceNum, ZVector<MTS_HitData>* outHitDataTbl)
{
	// 面がない場合は、なにもしない
	if (mesh.GetExtFace().size() == 0)
		return false;

	// 行列指定がある場合は、球の座標をローカル座標系へ変換
	ZOperationalVec3 QPos;
	ZOperationalMatrix tranMat;
	ZMatrix invTranMat;
	if (lpMat)
	{
		tranMat = *lpMat;
		lpMat->Inverse(invTranMat);
	}

	// とりあえず一番大きな軸の、このオブジェクト座標系のAABBを作る
	ZVec3 vScale = invTranMat.GetScale();
	float maxScale = vScale.x;
	if (maxScale < vScale.y)
		maxScale = vScale.y;
	if (maxScale < vScale.z)
		maxScale = vScale.z;

	ZVec3 vP;
	vQPos.Transform(vP, invTranMat);
	ZVec3 vQ_Min(vP.x - Qr * maxScale, vP.y - Qr * maxScale, vP.z - Qr * maxScale);
	ZVec3 vQ_Max(vP.x + Qr * maxScale, vP.y + Qr * maxScale, vP.z + Qr * maxScale);

	struct AABB
	{
		ZVec3 center;
		ZVec3 r;
	};

	AABB aabbQ;
	aabbQ.r = (vQ_Max - vQ_Min) * 0.5f;
	aabbQ.center = vQ_Min + aabbQ.r;

	//---------------------------------------------
	// 高速化の為、AABBで簡易判定を行う
	//---------------------------------------------
	// メッシュと球のAABB判定
	if (BoxToBox3D_MinMax(vQ_Min, vQ_Max, mesh.GetAABB_Center() - mesh.GetAABB_HalfSize(), mesh.GetAABB_Center() + mesh.GetAABB_HalfSize()) == false)
		return false;

	//---------------------------------------------
	// ここから詳細判定
	//---------------------------------------------
	// 球の座標
	QPos = vQPos;

	// 戻り値用
	bool bRet = false;

	// 出力ベクトル
	ZOperationalVec3 vOut;

	// 頂点変換用データ
	ZOperationalVec3 vertexPos[3];

	// 作業用変数
	int iFace;
	ZOperationalVec3 ret;
	ZOperationalVec3 vTmp, v1, vPos;

	ZOperationalVec3 vLine[3];	// 三角形の辺のベクトル３つ
	ZOperationalVec3 vVtoQ[3];	// 各頂点から球へのベクトル３つ

	ZOperationalVec3 nearPt;

	const ZAVector<ZMesh::ExtFace>& extFaces = mesh.GetExtFace();

	if (faceNum < 0)
		faceNum = mesh.GetFace().size();

	for (iFace = startFaceNo; iFace < faceNum; iFace++)
	{
		// 面情報
		auto& extFc = extFaces[iFace];

		// AABB判定
		if ((ABS(extFc.vAAABB_Center.x - aabbQ.center.x) > (extFc.vAAABB_HalfSize.x + aabbQ.r.x)) ||
			(ABS(extFc.vAAABB_Center.y - aabbQ.center.y) > (extFc.vAAABB_HalfSize.y + aabbQ.r.y)) ||
			(ABS(extFc.vAAABB_Center.z - aabbQ.center.z) > (extFc.vAAABB_HalfSize.z + aabbQ.r.z)))
			continue;

		// 面の3頂点の座標を取得
		mesh.GetFaceVertices_Pos(iFace, vertexPos);

		// 頂点情報を行列で変換
		vertexPos[0].Transform(tranMat);
		vertexPos[1].Transform(tranMat);
		vertexPos[2].Transform(tranMat);
		//		vVtoQ[0] = QPos - vertexPos[0];

		// 面の法線
//		vertexN = extFc.vN;
//		vertexN.TransformNormal(tranMat);
//		vertexN.Normalize();

		// 球 vs 三角形
		if (SphereToTriangle(QPos, Qr, vertexPos[0], vertexPos[1], vertexPos[2], nearPt))
		{
			// 最近接距離から球への方向
			vTmp = QPos - nearPt;
			// 球への距離
			const float L = vTmp.Length();
			vTmp.Normalize();
			// めり込んでいる分のベクトル
			vTmp *= Qr - L;

			vOut += vTmp;
			QPos += vTmp;
			bRet = true;

			if (outHitDataTbl)
				(*outHitDataTbl).emplace_back(iFace, nearPt);
		}

	}

	if (outMove)
		*outMove = vOut.v;

	return bRet;
}

bool ZCollision::MeshToSphere(const ZMesh& mesh, int mateNo, const ZMatrix* lpMat, const ZVec3& vQPos, float Qr, ZVec3* outMove)
{
	if (mesh.GetExtFace().size() == 0)
		return false;

	const ZMeshSubset* subset = mesh.GetSubset(mateNo);
	if (subset->FaceCount == 0)
		return false;

	return MeshToSphere(mesh, lpMat, vQPos, Qr, outMove, subset->FaceStart, subset->FaceCount);
}

void ZAABB::Transform(const ZMatrix & mat)
{
	Min.Transform(mat);
	Max.Transform(mat);

	// OBBの各頂点
	ZVec3 sampPos[8];
	sampPos[0] = ZVec3(Min.x, Max.y, Min.z);	// 左上手前
	sampPos[1] = ZVec3(Max.x, Max.y, Min.z);	// 右上手前
	sampPos[2] = ZVec3(Min.x, Max.y, Max.z);	// 左上奥
	sampPos[3] = ZVec3(Max.x, Max.y, Max.z);	// 右上奥 
	sampPos[4] = ZVec3(Min.x, Min.y, Min.z);	// 左下手前 
	sampPos[5] = ZVec3(Max.x, Min.y, Min.z);	// 右下手前
	sampPos[6] = ZVec3(Min.x, Min.y, Max.z);	// 左下奥
	sampPos[7] = ZVec3(Max.x, Min.y, Max.z);	// 右下奥 

	Max = FLT_MIN;
	Min = FLT_MAX;

	// AABB算出
	for (auto& pos : sampPos)
	{
		for (uint i = 0; i < 3; i++)
		{
			if (Min[i] > pos[i]) Min[i] = pos[i];
			if (Max[i] < pos[i]) Max[i] = pos[i];
		}
	}

}
