#include "PCH/pch.h"

namespace EzLib
{
namespace Memory
{
	size_t ZAllocator::MemorySizeList[MEMORY_AREA_TYPE::NUM_TYPE] = {};
	std::string ZAllocator::MemoryAreaNemes[MEMORY_AREA_TYPE::NUM_TYPE] = {{"SYS_MEMORY"},{"APP_MEMORY"},{"STL_MEMORY"}};
	ZMemoryArea ZAllocator::MemoryArea[MEMORY_AREA_TYPE::NUM_TYPE] = {};
	bool ZAllocator::IsInitialized = false;

	ZMemoryArea::ZMemoryArea()
	{
	}

	ZMemoryArea::~ZMemoryArea()
	{
	}

	bool ZMemoryArea::CreateMemorySpace(const char* name, size_t allocSize, size_t partitionBits)
	{
		if (allocSize <= 0)
			return false;
	
		m_AllocSize = allocSize;
		strcpy(m_Name, name);
		return m_MemoryAllocator.Init(allocSize, partitionBits);
	}

	void ZMemoryArea::DeleteMemorySpace()
	{
		DebugLog_Line("--------------- %s ---------------", m_Name);
		DebugLog_Line("ActiveMemorySize : %d", m_MemoryAllocator.GetAllocationInfo().ActiveMemory);
		DebugLog_Line("----------------------------------");

		m_MemoryAllocator.Release();
	}

	const ZMemoryAllocator::AllocationInfo& ZMemoryArea::GetAllocationInfo()const
	{
		return m_MemoryAllocator.GetAllocationInfo();
	}

	const size_t ZMemoryArea::GetMemorySize() const
	{
		return m_MemoryAllocator.GetMemorySize();
	}
	
	const std::string& ZAllocator::GetMemoryAreaName(MEMORY_AREA_TYPE area)
	{
		return MemoryAreaNemes[area];
	}

	const size_t ZAllocator::GetMemorySize(MEMORY_AREA_TYPE area)
	{
		return MemoryArea[area].GetMemorySize();
	}

	void ZAllocator::Init()
	{
		if (IsInitialized)
			return;

		for (size_t i = 0;i<MEMORY_AREA_TYPE::NUM_TYPE;i++)
			MemoryArea[i].CreateMemorySpace(MemoryAreaNemes[i].c_str(), MemorySizeList[i]);

		IsInitialized = true;
	}

	void ZAllocator::Release()
	{
		if (IsInitialized == false)
			return;

		for (auto& memoryArea : MemoryArea)
			memoryArea.DeleteMemorySpace();
		IsInitialized = false;
	}

	void ZAllocator::DisplayMemoryInfo(uint scrollIndex, MEMORY_AREA_TYPE area)
	{
		const std::string& memoryName = MemoryAreaNemes[area];
		const ZMemoryAllocator::AllocationInfo& memoryInfo = ZAllocator::GetAllocationInfo(area);
		std::string unit[4] = { "Byte" };
		size_t infoValues[4] =
		{
			ZAllocator::GetMemorySize(area),
			memoryInfo.ActiveMemory,
			memoryInfo.AllAllocSize,
			memoryInfo.AllFreeSize
		};

		for (size_t i = 0; i < 4; i++)
		{
			auto& value = infoValues[i];
			if (value > 1_KB)
			{
				if (value > 1_MB)
				{
					value = Byte2MB(value);
					unit[i] = "MB";
				}
				else
				{
					value = Byte2KB(value);
					unit[i] = "KB";
				}
			}
		}

		DW_STATIC(scrollIndex + 0, "----------------- %s -----------------", memoryName.c_str());
		DW_STATIC(scrollIndex + 1, "MemorySize   : %d %s", infoValues[0], unit[0].c_str());
		DW_STATIC(scrollIndex + 2, "ActiveMemory : %d %s", infoValues[1], unit[1].c_str());
		DW_STATIC(scrollIndex + 3, "AllAllocSize : %d %s", infoValues[2], unit[2].c_str());
		DW_STATIC(scrollIndex + 4, "AllFreeSize  : %d %s", infoValues[3], unit[3].c_str());
	}

	void ZAllocator::DisplayAllMemoryInfo(uint beginScrollIndex)
	{
		for (size_t i = 0; i < MEMORY_AREA_TYPE::NUM_TYPE; i++)
			DisplayMemoryInfo(beginScrollIndex + i * 6, (MEMORY_AREA_TYPE)i);
	}

	ZAllocatorInitializer::ZAllocatorInitializer()
	{
		static std::unordered_map<std::string, MEMORY_AREA_TYPE> AreaTypeMap =
		{
			{"SYS",MEMORY_AREA_TYPE::SYSTEM},
			{"APP",MEMORY_AREA_TYPE::APPLICATION},
			{"STL",MEMORY_AREA_TYPE::STL}
		};

		std::unordered_map<std::string, IniFileParam> paramMap;
		{
			std::ifstream ifs(ALLOCATOR_INI_FILE);
			if (ifs)
			{
				while (!ifs.eof())
				{
					std::string paramName;
					ifs >> paramName;
					auto& param = paramMap[paramName];
					ifs >> param.separater >> param.param >> param.unit;

				}
			}
		}

		for (auto param : paramMap)
		{
			std::string paramName = param.first;
			IniFileParam iniParam = param.second;
			if (iniParam.unit.empty())
				continue;

			switch (iniParam.unit[0])
			{
				case 'K':
				case 'k':
					ZAllocator::MemorySizeList[AreaTypeMap[paramName]] = KB2Byte(iniParam.param);
				break;

				case 'M':
				case 'm':
					ZAllocator::MemorySizeList[AreaTypeMap[paramName]] = MB2Byte(iniParam.param);
				break;

				case 'G':
				case 'g':
					ZAllocator::MemorySizeList[AreaTypeMap[paramName]] = GB2Byte(iniParam.param);
				break;
			}
			
		} //for (auto param : paramMap)

		ZAllocator::Init();
	}


	ZAllocatorInitializer::~ZAllocatorInitializer()
	{
		ZAllocator::Release();
	}


}
}