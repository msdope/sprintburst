#include "ZMemoryAllocator.h"

size_t KB2Byte(size_t kb)
{
	return kb << 10;
}

size_t MB2Byte(size_t mb)
{
	return mb << 20;
}

size_t GB2Byte(size_t gb)
{
	return gb << 30;
}

size_t Byte2KB(size_t byte)
{
	return byte >> 10;
}

size_t Byte2MB(size_t byte)
{
	return byte >> 20;
}

size_t Byte2GB(size_t byte)
{
	return byte >> 30;
}

namespace EzLib
{
namespace Memory
{
namespace BoundaryTag
{

	BoundaryEndTag::BoundaryEndTag(size_t size)
	{
		m_MemorySize = size;
	}

	BoundaryEndTag::~BoundaryEndTag()
	{
	}

	BoundaryBeginTag::BoundaryBeginTag()
		: m_Next(nullptr),
		  m_Prev(nullptr),
		  m_Header()
	{
		SetSize(0);
	}

	BoundaryBeginTag::BoundaryBeginTag(size_t size)
		: m_Next(nullptr),
		  m_Prev(nullptr),
		  m_Header()
	{
		SetSize(size);
	}

	BoundaryBeginTag::~BoundaryBeginTag()
	{
	}

	BoundaryBeginTag* NewTag(size_t size, uint8* p)
	{
		BoundaryBeginTag* beginTag = new(p)BoundaryBeginTag(size);
		p += BoundaryBeginTag::HeaderOverhead + beginTag->GetSize();
		BoundaryEndTag* endTag = new(p)BoundaryEndTag(size);
		
		return beginTag;
	}

	void DeleteTag(BoundaryBeginTag* tag)
	{
		BoundaryEndTag* endTag = tag->GetEndTag();

		tag->~BoundaryBeginTag();
		endTag->~BoundaryEndTag();
	}
	
}

	const size_t ZMemoryAllocator::Aligment = 4;

	ZMemoryAllocator::ZMemoryAllocator()
		: m_MinAllocSize(0),
		  m_Base(nullptr),
		  m_pFreeBlocks(nullptr),
		  m_pFreeListBitSLI(nullptr),
		  m_IsInitialized(false)
	{
	}
	ZMemoryAllocator::ZMemoryAllocator(size_t memorySize, size_t partitionBits)
		: m_MinAllocSize(0),
		  m_Base(nullptr),
		  m_pFreeBlocks(nullptr),
		  m_pFreeListBitSLI(nullptr),
		  m_IsInitialized(false)
	{
		Init(memorySize, partitionBits);
	}

	
	ZMemoryAllocator::~ZMemoryAllocator()
	{
		// 初期化されていなければメモリは確保されていない
		if (m_IsInitialized == false)
			return;

		Release();
	}

	bool ZMemoryAllocator::ReAlloc()
	{
		// 初期化されていなければ再確保するメモリサイズは不明
		if (m_IsInitialized == false)
			return false;

		Release();

		return Init(m_MemorySize,m_SLIPartitionBits);
	}

	bool ZMemoryAllocator::Init(size_t memorySize, size_t partitionBits)
	{
		if (m_IsInitialized == false)
			m_IsInitialized = true;
		else
			return false; // 初期化済み

		bool isError;

		m_SLIPartitionBits = partitionBits; // 分割数 2 ^ n
		m_SLIPartition = (1 << m_SLIPartitionBits);

		// 割り当てる最小メモリブロックサイズを求める
		uint32 linkPtrOverwrap = (BoundaryTag::BBTAG_SIZE - BBeginTag::HeaderOverhead);
		uint32 minUserMemory = m_SLIPartition; // 16Byte
		uint32 minAllocSize = max(minUserMemory, linkPtrOverwrap); // 前後リンクポインタサイズ or ユーザメモリのどちらか
		m_MinAllocSize = GetAlignmentedSize(minAllocSize);
		isError = !(memorySize > m_MinAllocSize);
		if (isError)
		{
			assert(0);
			return false;
		}

		// バッファサイズを超えるアラインされた領域サイズに設定
		size_t bufAllocSize = GetAlignmentedSize(memorySize);
		isError = !(bufAllocSize > m_MinAllocSize);
		if (isError)
		{
			assert(0);
			return false;
		}

		// 管理領域タグ
		m_MemorySize = bufAllocSize + BBeginTag::BlockOverhead + BoundaryTag::BBTAG_SIZE;
		m_Base = new uint8[m_MemorySize]{0};

		{
			auto msb = GetMSB(m_MemorySize)+1;
			const size_t numFreeBlocks = (GetMSB(m_MemorySize) + 1) * m_SLIPartition;
			m_pFreeBlocks = new BBeginTag*[numFreeBlocks] {0}; //std::vector<BBeginTag*>((GetMSB(m_MemorySize) + 1) * m_SLIPartition, nullptr);
			//memset(m_pFreeBlocks, 0, sizeof(BBeginTag*) * numFreeBlocks);

			const size_t numFreeListBitSLIs = GetMSB(m_MemorySize) + 1;
			m_pFreeListBitSLI = new uint32[numFreeListBitSLIs]{0}; //std::vector<uint32>(GetMSB(m_MemorySize) + 1, 0);
			//memset(m_pFreeListBitSLI, 0, sizeof(uint32) * numFreeListBitSLIs);
		}
		m_FreeListBitFLI = 0;

		// ダミータグ作成
		uint8* p = m_Base;
		p += m_MemorySize - BoundaryTag::BBTAG_SIZE;
		BBeginTag* dummyEnd = new(p)BBeginTag(0);
		dummyEnd->SetFlag(true);

		// 本物の未使用領域ヘッダ
		p = m_Base;
		BBeginTag* begin = BoundaryTag::NewTag(bufAllocSize, p);
		RegisterFreeBlock(begin);

		isError = !((ulong)(begin + BoundaryTag::BBTAG_SIZE) % Aligment == 0);
		if (isError)
		{
			assert(0);
			return false;
		}
		
		m_AllocationInfo.FreeMemory = bufAllocSize;
		m_AllocationInfo.ActiveMemory = 0;
		m_AllocationInfo.AllBoundaryTagSize = m_MemorySize - bufAllocSize;
		m_AllocationInfo.AllFreeSize = 0;
		m_AllocationInfo.AllAllocSize = 0;

		return true;
	}

	void ZMemoryAllocator::Release()
	{
		if (m_IsInitialized == false)
			return;
		uint8* p = m_Base;
		p += m_MemorySize - BoundaryTag::BBTAG_SIZE;
		BBeginTag* dummyEnd = (BBeginTag*)p;
		dummyEnd->~BoundaryBeginTag();

		delete[] m_Base;
		delete[] m_pFreeBlocks;
		delete[] m_pFreeListBitSLI;

		m_AllocationInfo = {0};

		m_IsInitialized = false;
	}

	ZMemoryAllocator::BBeginTag* ZMemoryAllocator::SerchFreeBlock(size_t size)
	{
		unsigned fli, sli;
		GetIndex(size, &fli, &sli);
		unsigned index = CalcFreeBlockIndex(fli, sli);

		BBeginTag* block = m_pFreeBlocks[index];

		if (block == nullptr)
		{
			auto freeListBitSLI = m_pFreeListBitSLI[fli];
			if (GetMinFreeListBit(sli + 1, m_pFreeListBitSLI[fli], &sli) == false)
			{
				if (GetMinFreeListBit(fli + 1, m_FreeListBitFLI, &fli) == false)
					return nullptr; // メモリ確保不可

				if (GetMinFreeListBit(0, m_pFreeListBitSLI[fli], &sli) == false)
					assert(0);
			}

			index = CalcFreeBlockIndex(fli, sli);
			block = m_pFreeBlocks[index];
		}

		assert(block);
		if (block == nullptr)
			return nullptr;
	
		m_pFreeBlocks[index] = block->GetNext();
		block->Detach();

		// FreeListBit更新
		ClearBit(index, fli, sli);

		return block;
	}

	void ZMemoryAllocator::UnRegisterFreeBlock(BBeginTag * beginTag)
	{
		assert(beginTag);

		size_t fli;
		size_t sli;
		size_t index;

		GetIndex(beginTag->GetSize(), &fli, &sli);
		// GetBlockIndex -1 -> 確実に入る大きさのFreeBlock
		index = CalcFreeBlockIndex(fli, sli) - 1;
		assert(m_pFreeBlocks[index]);
		BBeginTag* topBlock = m_pFreeBlocks[index];
		if (topBlock->GetNext())
		{
			if (beginTag == topBlock)
				m_pFreeBlocks[index] = topBlock->GetNext();
		}
		else
		{
			m_pFreeBlocks[index] = nullptr;
			fli = index >> m_SLIPartitionBits;
			sli = index & (m_SLIPartition - 1);

			ClearBit(index, fli, sli);
		}

		beginTag->Detach();
	}

	void ZMemoryAllocator::RegisterFreeBlock(BBeginTag * beginTag)
	{
		BBeginTag* freeBlock = beginTag;
		size_t memorySize = freeBlock->GetSize();
		// メモリブロックが小さすぎ
		assert(memorySize >= m_MinAllocSize);

		size_t fli;
		size_t sli;
		size_t index;
		GetIndex(memorySize, &fli, &sli);

		index = CalcFreeBlockIndex(fli, sli) - 1;

		// フリーリストに追加
		if (m_pFreeBlocks[index])
		{
			freeBlock->SetNext(m_pFreeBlocks[index]);
			m_pFreeBlocks[index]->SetPrev(freeBlock);
			m_pFreeBlocks[index] = freeBlock;

			freeBlock->SetPrev(nullptr);
		}
		else
		{
			fli = index >> m_SLIPartitionBits;
			sli = index & (m_SLIPartition - 1);

			// フリーリストビットを立てる
			m_pFreeListBitSLI[fli] |= (1 << sli);
			m_FreeListBitFLI |= (1 << fli);
			m_pFreeBlocks[index] = freeBlock;

			freeBlock->SetNext(nullptr);
			freeBlock->SetPrev(nullptr);
		}
	}

	ZMemoryAllocator::BBeginTag* ZMemoryAllocator::MergeFreeBlock(BBeginTag * block)
	{
		BBeginTag* freeBlock = block;

		BBeginTag* next = freeBlock->NextTag();
		BEndTag* prevEnd = (BEndTag*)((uint8*)block - BoundaryTag::BETAG_SIZE);

		if (next->IsUsed() == false)
		{
			size_t memSize = next->GetSize();
			UnRegisterFreeBlock(next);
			freeBlock->Merge(next);
		}
		
		// 一番先頭タグの場合、前のタグはメモリ範囲外
		if ((uint8*)prevEnd > m_Base)
		{
			BBeginTag* prev = freeBlock->PrevTag();
			assert(m_Base <= (uint8*)prev);

			if (prev->IsUsed() == false)
			{
				size_t memSize = next->GetSize();
				UnRegisterFreeBlock(prev);
				prev->Merge(freeBlock);
				freeBlock = prev;
			}
		}

		return freeBlock;
	}

	void ZMemoryAllocator::DeAllocateBlock(BBeginTag* block)
	{
		if (!block)
			return;

		// メモリの正当性確認
		assert(block->IsUsed());
		assert(m_Base <= (uint8*)block);
		assert((m_Base + m_MemorySize - BoundaryTag::BBTAG_SIZE) > (uint8*)block);

		std::lock_guard<std::mutex> lg(m_Mtx);
		// 解放情報記録
		{
			size_t memorySize = block->GetSize();
			m_AllocationInfo.FreeMemory += memorySize;
			m_AllocationInfo.ActiveMemory -= memorySize;
			m_AllocationInfo.AllFreeSize += memorySize;
			m_AllocationInfo.AllBoundaryTagSize -= BBeginTag::BlockOverhead;
		}

		BBeginTag* tmpBlock = block;

		tmpBlock->SetFlag(false);
		tmpBlock = MergeFreeBlock(tmpBlock);
		RegisterFreeBlock(tmpBlock);
	}

}
}