#ifndef __MEMORY_H__
#define __MEMORY_H__

#define ALLOCATOR_INI_FILE "MemoryAllocator.ini"

namespace EzLib
{
namespace Memory
{
	enum MEMORY_AREA_TYPE : size_t
	{
		SYSTEM = 0,
		APPLICATION = 1,
		STL = 2,
		NUM_TYPE = 3
	};

	class ZMemoryArea
	{
	public:
		ZMemoryArea();
		~ZMemoryArea();

		bool CreateMemorySpace(const char* name, size_t allocSize, size_t partitionBits = 4);
		void DeleteMemorySpace();

		template<typename T>
		void* Malloc(size_t size,MEMORY_AREA_TYPE area);

		template<typename T>
		void Free(T* p);
		
		const ZMemoryAllocator::AllocationInfo& GetAllocationInfo()const;
		
		const size_t GetMemorySize()const;

	private:
		size_t m_AllocSize;
		char m_Name[32];
		ZMemoryAllocator m_MemoryAllocator;
	};

	class ZAllocator
	{
	public:
		static void Init();

		static void Release();

		template<typename T>
		static void* Malloc(size_t size, MEMORY_AREA_TYPE area);

		template<typename T>
		static void Free(T* p, MEMORY_AREA_TYPE area);

		static void DisplayMemoryInfo(uint scrollIndex,MEMORY_AREA_TYPE area);

		static void DisplayAllMemoryInfo(uint beginScrollIndex);

		static const ZMemoryAllocator::AllocationInfo& GetAllocationInfo(MEMORY_AREA_TYPE area);
		static const std::string& GetMemoryAreaName(MEMORY_AREA_TYPE area);
		static const size_t GetMemorySize(MEMORY_AREA_TYPE area);

	public:
		static size_t MemorySizeList[MEMORY_AREA_TYPE::NUM_TYPE];

	private:
		static std::string MemoryAreaNemes[MEMORY_AREA_TYPE::NUM_TYPE];
		static ZMemoryArea MemoryArea[MEMORY_AREA_TYPE::NUM_TYPE];
		static bool IsInitialized;
	};
	

	class ZAllocatorInitializer
	{
	private:
		struct IniFileParam
		{
			char separater;
			size_t param;
			std::string unit;
		};

	public:
		ZAllocatorInitializer();
		~ZAllocatorInitializer();

	};
}
}

// アロケータ初期化用クラスインスタンス
static EzLib::Memory::ZAllocatorInitializer g_AllocatorInitializer;

#pragma warning(disable:4291)

// new
#define _ALLOCATE(T,area,...)																\
		T* ALLOCED_MEMORY = (T*)EzLib::Memory::ZAllocator::Malloc<T>(sizeof(T),area);		\
		if(ALLOCED_MEMORY == nullptr)														\
			MessageBox(NULL, "Missing Alloc!!", "Memory Alloc Error", MB_OK);				\
		new(ALLOCED_MEMORY) T(__VA_ARGS__);													\
		return ALLOCED_MEMORY

#define _ALLOCATE_ARRAY(T,area,numInstance)															\
		T* ALLOCED_MEMORY = (T*)EzLib::Memory::ZAllocator::Malloc<T>(sizeof(T) * numInstance,area);	\
		if(ALLOCED_MEMORY == nullptr)																\
			MessageBox(NULL, "Missing Alloc!!", "Memory Alloc Error", MB_OK);						\
		new(ALLOCED_MEMORY) T[numInstance];															\
		return ALLOCED_MEMORY




#define sysnew(T,...)																				\
		[&](){_ALLOCATE(T,EzLib::Memory::MEMORY_AREA_TYPE::SYSTEM,__VA_ARGS__);}()

#define sysnewArray(T,numInstance)																	\
		[&](){_ALLOCATE_ARRAY(T,EzLib::Memory::MEMORY_AREA_TYPE::SYSTEM,numInstance);}()

#define appnew(T,...)																				\
		[&](){_ALLOCATE(T,EzLib::Memory::MEMORY_AREA_TYPE::APPLICATION,__VA_ARGS__);}()

#define appnewArray(T,numInstance)																	\
		[&](){_ALLOCATE_ARRAY(T,EzLib::Memory::MEMORY_AREA_TYPE::APPLICATION,numInstance);}()

// delete
template<typename T>
void delptr(T* p);

// delete & p -> nullptr
template<typename T>
void SAFE_DELPTR(T*& p);

#include "Memory.inl"


using namespace EzLib::Memory;

#endif // MEMORY_H