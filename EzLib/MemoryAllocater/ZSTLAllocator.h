#ifndef _ZSTL_ALLOCATOR_H__
#define _ZSTL_ALLOCATOR_H__

namespace EzLib
{
namespace Memory
{


	template<typename T,MEMORY_AREA_TYPE area = MEMORY_AREA_TYPE::STL>
	class ZSTLAllocator
	{
	public:
		typedef size_t		size_type;
		typedef ptrdiff_t	difference_type;
		typedef T*			pointer;
		typedef const T*	const_pointer;
		typedef T&			reference;
		typedef const T&	const_reference;
		typedef T			value_type;

		template<typename U>
		struct rebind
		{
			typedef ZSTLAllocator<U,area> other;
		};

		const size_t typeSize = sizeof(T);

	public:
		ZSTLAllocator()
		{
		}
		ZSTLAllocator(const ZSTLAllocator&)
		{
		}
		ZSTLAllocator(ZSTLAllocator&&)
		{
		}
		template<typename U>
		ZSTLAllocator(const ZSTLAllocator<U,area>&)
		{
		}
		
		~ZSTLAllocator()
		{
		}

		ZSTLAllocator<T>& operator=(const ZSTLAllocator&)
		{
			return *this;
		}

		template<typename U>
		ZSTLAllocator& operator=(const ZSTLAllocator<U,area>&)
		{
			return *this;
		}

		pointer allocate(size_type num)
		{
			pointer p = (pointer)ZAllocator::Malloc<T>(typeSize * num, area);
			if (p == nullptr)
				throw std::bad_alloc();
			return p;
		}


		void deallocate(pointer p, size_type num)
		{
			ZAllocator::Free((void*)p, area);
		}

		pointer address(reference x)const
		{
			return &x;
		}

		const_pointer address(const_reference x)const
		{
			return &x;
		}

		size_type max_size()const
		{
			return (size_t)-1;
		}

		template<typename ...Args>
		void construct(pointer p, Args && ...args)
		{
			new((T*)p) T(std::forward<Args>(args)...);
		}

		void destroy(pointer p)
		{
			p->~T();
		}
	};
	
	template<typename T1,typename T2, MEMORY_AREA_TYPE area>
	bool operator==(const ZSTLAllocator<T1,area>&, const ZSTLAllocator<T2,area>&)noexcept
	{
		return true;
	}

	template<typename T1, typename T2, MEMORY_AREA_TYPE area>
	bool operator!=(const ZSTLAllocator<T1,area>&, const ZSTLAllocator<T2,area>&)noexcept
	{
		return false;
	}


#pragma region STLAllocator使用

	template<typename T>
	using ZVector = std::vector<T, ZSTLAllocator<T>>;
	template<typename T>
	using ZList = std::list<T, ZSTLAllocator<T>>;
	template<typename T>
	using ZDeque = std::deque<T, ZSTLAllocator<T>>;
	template<typename T>
	using ZQueue = std::queue<T, ZDeque<T>>;
	template<typename T>
	using ZSet = std::set<T, std::less<T>, ZSTLAllocator<T>>;
	template<typename Key,typename T>
	using ZMap = std::map<Key, T, std::less<Key>, ZSTLAllocator<std::pair<Key, T>>>;
	template<typename Key,typename T>
	using ZUnorderedMap = std::unordered_map<Key, T, std::hash<Key>, std::equal_to<Key>, ZSTLAllocator<std::pair<Key, T>>>;
	template<typename T>
	using ZStack = std::stack<T,ZDeque<T>>;
	using ZString = std::basic_string<char, std::char_traits<char>, ZSTLAllocator<char>>;
	using ZWString = std::basic_string<wchar_t, std::char_traits<wchar_t>, ZSTLAllocator<wchar_t>>;
	using ZU16String = std::basic_string<char16_t, std::char_traits<char16_t>, ZSTLAllocator<char16_t>>;
	
	using ZISStream = std::basic_istringstream<char, std::char_traits<char>, ZSTLAllocator<char>>;
	using ZOSStream = std::basic_ostringstream<char, std::char_traits<char>, ZSTLAllocator<char>>;
	using ZStringStream = std::basic_stringstream<char, std::char_traits<char>, ZSTLAllocator<char>>;
	
#pragma endregion

#pragma region ApplicationAllocator使用

	template<typename T>
	using ZAVector = std::vector<T, ZSTLAllocator<T,MEMORY_AREA_TYPE::APPLICATION>>;
	template<typename T>
	using ZAList = std::list<T, ZSTLAllocator<T, MEMORY_AREA_TYPE::APPLICATION>>;
	template<typename T>
	using ZADeque = std::deque<T, ZSTLAllocator<T, MEMORY_AREA_TYPE::APPLICATION>>;
	template<typename T>
	using ZAQueue = std::queue<T, ZADeque<T>>;
	template<typename T>
	using ZASet = std::set<T, std::less<T>, ZSTLAllocator<T,MEMORY_AREA_TYPE::APPLICATION>>;
	template<typename Key, typename T>
	using ZAMap = std::map<Key, T, std::less<Key>, ZSTLAllocator<std::pair<Key, T>, MEMORY_AREA_TYPE::APPLICATION>>;
	template<typename Key, typename T>
	using ZAUnorderedMap = std::unordered_map<Key, T, std::hash<Key>, std::equal_to<Key>, ZSTLAllocator<std::pair<Key, T>, MEMORY_AREA_TYPE::APPLICATION>>;
	template<typename T>
	using ZAStack = std::stack<T, ZADeque<T>>;
	using ZAString = std::basic_string<char, std::char_traits<char>, ZSTLAllocator<char, MEMORY_AREA_TYPE::APPLICATION>>;
	using ZAWString = std::basic_string<wchar_t, std::char_traits<wchar_t>, ZSTLAllocator<wchar_t, MEMORY_AREA_TYPE::APPLICATION>>;
	using ZAISStream = std::basic_istringstream<char, std::char_traits<char>, ZSTLAllocator<char, MEMORY_AREA_TYPE::APPLICATION>>;
	using ZAOSStream = std::basic_ostringstream<char, std::char_traits<char>, ZSTLAllocator<char, MEMORY_AREA_TYPE::APPLICATION>>;
	using ZAStringStream = std::basic_stringstream<char, std::char_traits<char>, ZSTLAllocator<char, MEMORY_AREA_TYPE::APPLICATION>>;

#pragma endregion

#pragma region SystemAllocator使用
	
	template<typename T>
	using ZSVector = std::vector<T, ZSTLAllocator<T, MEMORY_AREA_TYPE::SYSTEM>>;
	template<typename T>
	using ZSList = std::list<T, ZSTLAllocator<T, MEMORY_AREA_TYPE::SYSTEM>>;
	template<typename T>
	using ZSDeque = std::deque<T, ZSTLAllocator<T, MEMORY_AREA_TYPE::SYSTEM>>;
	template<typename T>
	using ZSQueue = std::queue<T, ZSDeque<T>>;
	template<typename T>
	using ZSSet = std::set<T, std::less<T>, ZSTLAllocator<T, MEMORY_AREA_TYPE::SYSTEM>>;
	template<typename Key, typename T>
	using ZSMap = std::map<Key, T, std::less<Key>, ZSTLAllocator<std::pair<Key, T>, MEMORY_AREA_TYPE::SYSTEM>>;
	template<typename Key, typename T>
	using ZSUnorderedMap = std::unordered_map<Key, T, std::hash<Key>, std::equal_to<Key>, ZSTLAllocator<std::pair<Key, T>, MEMORY_AREA_TYPE::SYSTEM>>;
	template<typename T>
	using ZSStack = std::stack<T, ZSDeque<T>>;
	using ZSString = std::basic_string<char, std::char_traits<char>, ZSTLAllocator<char, MEMORY_AREA_TYPE::SYSTEM>>;
	using ZSWString = std::basic_string<wchar_t, std::char_traits<wchar_t>, ZSTLAllocator<wchar_t, MEMORY_AREA_TYPE::SYSTEM>>;
	using ZSISStream = std::basic_istringstream<char, std::char_traits<char>, ZSTLAllocator<char, MEMORY_AREA_TYPE::SYSTEM>>;
	using ZSOSStream = std::basic_ostringstream<char, std::char_traits<char>, ZSTLAllocator<char, MEMORY_AREA_TYPE::SYSTEM>>;
	using ZSStrengStream = std::basic_stringstream<char, std::char_traits<char>, ZSTLAllocator<char, MEMORY_AREA_TYPE::SYSTEM>>;

#pragma endregion


}
}


#endif