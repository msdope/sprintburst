namespace BoundaryTag
{
	inline size_t BoundaryEndTag::GetSize()const
	{
		return m_MemorySize;
	}
	
	inline void BoundaryEndTag::SetSize(size_t size)
	{
		m_MemorySize = size;
	}

	inline void BoundaryBeginTag::SetFlag(bool isUsed)
	{
		m_Header.IsUsed = isUsed ? 1 : 0;
	}

	inline void BoundaryBeginTag::SetSize(size_t size)
	{
		m_Header.Size = size;
	}

	inline void BoundaryBeginTag::SetGeneralData(size_t data)
	{
		m_Header.GeneralData = (short)data;
	}

	inline void BoundaryBeginTag::SetNumInstance(size_t num)
	{
		m_Header.NumInstance = num;
	}

	inline bool BoundaryBeginTag::Merge(BoundaryBeginTag* next)
	{
		if (IsUsed() || next->IsUsed())
			return false;

		BoundaryEndTag* end = GetEndTag();
		BoundaryEndTag* newEnd = next->GetEndTag();
		size_t newBufSize = m_Header.Size + BoundaryBeginTag::BlockOverhead + next->GetSize();
		SetSize(newBufSize);
		newEnd->SetSize(newBufSize);

		end->~BoundaryEndTag();
		next->~BoundaryBeginTag();
		
		return true;
	}

	inline BoundaryBeginTag* BoundaryBeginTag::Split(size_t size)
	{
		assert(m_Header.Size > size);

		size_t leftSize = size;
		size_t rightSize = m_Header.Size - size;

		if (rightSize <= BoundaryTag::BBTAG_SIZE + BoundaryTag::BETAG_SIZE)
			return nullptr;

		rightSize -= BoundaryBeginTag::HeaderOverhead + BoundaryTag::BETAG_SIZE;
		SetSize(leftSize);

		uint8* p = (uint8*)this;
		p += BoundaryBeginTag::HeaderOverhead + m_Header.Size;
		BoundaryEndTag* leftEnd = new(p)BoundaryEndTag(leftSize);
		
		p += BoundaryTag::BETAG_SIZE;
		BoundaryBeginTag* rightBegin = new(p)BoundaryBeginTag(rightSize);

		p += BoundaryBeginTag::HeaderOverhead + rightBegin->GetSize();
		BoundaryEndTag* rightEnd = new(p)BoundaryEndTag(rightSize);
		
		return rightBegin;
	}

	inline BoundaryBeginTag* BoundaryBeginTag::PrevTag()
	{
		uint8* p = (uint8*)this;
		BoundaryEndTag* prebEnd = (BoundaryEndTag*)(p - BoundaryTag::BETAG_SIZE);
		p -= HeaderOverhead + prebEnd->GetSize() + BoundaryTag::BETAG_SIZE;
		return (BoundaryBeginTag*)p;
	}

	inline BoundaryBeginTag* BoundaryBeginTag::NextTag()
	{
		uint8* p = (uint8*)this;
		p += HeaderOverhead + m_Header.Size + BoundaryTag::BETAG_SIZE;
		return (BoundaryBeginTag*)p;
	}

	inline BoundaryEndTag* BoundaryBeginTag::GetEndTag()
	{
		uint8* p = (uint8*)this;
		p += BoundaryBeginTag::HeaderOverhead + m_Header.Size;

		return (BoundaryEndTag*)p;
	}

	inline void BoundaryBeginTag::Detach()
	{
		if (m_Prev)
			m_Prev->SetNext(m_Next);
		if (m_Next)
			m_Next->SetPrev(m_Prev);

		m_Prev = nullptr;
		m_Next = nullptr;
	}

	inline void BoundaryBeginTag::SetNext(BoundaryBeginTag * p)
	{
		m_Next = p;
	}

	inline void BoundaryBeginTag::SetPrev(BoundaryBeginTag * p)
	{
		m_Prev = p;
	}

	inline BoundaryBeginTag* BoundaryBeginTag::GetNext()
	{
		return m_Next;
	}

	inline BoundaryBeginTag* BoundaryBeginTag::GetPrev()
	{
		return m_Prev;
	}

	inline bool BoundaryBeginTag::IsUsed() const
	{
		return m_Header.IsUsed;
	}

	inline size_t BoundaryBeginTag::GetSize() const
	{
		return m_Header.Size;
	}

	inline int BoundaryBeginTag::GetGeneralData()const
	{
		return m_Header.GeneralData;
	}

	inline size_t BoundaryBeginTag::GetNumInstance() const
	{
		return m_Header.NumInstance;
	}

}

template<typename T>
inline void* ZMemoryAllocator::Malloc(size_t size)
{
	if (m_IsInitialized == false)
	{
		assert(false);
		return nullptr;
	}

	return (void*)_Malloc(size,size/sizeof(T));
}

template<typename T>
inline void* ZMemoryAllocator::Malloc(size_t size,int generalData)
{
	if (m_IsInitialized == false)
	{
		assert(false);
		return nullptr;
	}

	return (void*)_Malloc(size, size / sizeof(T),generalData);
}

template<class T>
inline void ZMemoryAllocator::Free(T* p)
{
	if (m_IsInitialized == false)
	{
		assert(false);
		return;
	}
	BBeginTag* block = (BBeginTag*)((uint8*)p - BBeginTag::HeaderOverhead);
	
	size_t numObj = block->GetNumInstance();
	for(size_t i = 0;i < numObj;i++)
		(p + i)->~T();

	ZeroMemory((void*)p, block->GetSize());
	DeAllocateBlock(block);
}

template<>
inline void ZMemoryAllocator::Free<void>(void* p)
{
	if (m_IsInitialized == false)
	{
		assert(false);
		return;

	}
	BBeginTag* block = (BBeginTag*)((uint8*)p - BBeginTag::HeaderOverhead);
	ZeroMemory(p, block->GetSize());
	DeAllocateBlock(block);
}

inline bool ZMemoryAllocator::IsActivePtr(void* p)
{
	if(p == nullptr)
		return false;

	BBeginTag* block = (BBeginTag*)((uint8*)p - BBeginTag::HeaderOverhead);
	return block->IsUsed();
}

inline size_t ZMemoryAllocator::GetPtrMemSize(void * p)
{
	if (p == nullptr)
		return 0;

	BBeginTag* block = (BBeginTag*)((uint8*)p - BBeginTag::HeaderOverhead);
	
	return block->GetSize();
}

inline int ZMemoryAllocator::GetGeneralData(void* p)
{
	if (p == nullptr)
		return -1;

	BBeginTag* block = (BBeginTag*)((uint8*)p - BBeginTag::HeaderOverhead);

	return block->GetGeneralData();
}

inline const ZMemoryAllocator::AllocationInfo& ZMemoryAllocator::GetAllocationInfo()const
{
	return m_AllocationInfo;
}

inline const size_t EzLib::Memory::ZMemoryAllocator::GetMemorySize() const
{
	return m_MemorySize;
}

inline ZMemoryAllocator::BBeginTag* ZMemoryAllocator::GetFreeBlock(size_t size)
{
	assert(size < 1_GB);

	// 確保するメモリ領域をアラインメントの倍数以上へ
	size_t allocSize = GetAlignmentedSize(size);

	// 空き領域確保
	BBeginTag* freeBlock = SerchFreeBlock(allocSize);
	if (freeBlock == nullptr)
		return nullptr;	// メモリ確保失敗
	assert(freeBlock->GetSize() >= allocSize);

	freeBlock->SetFlag(true);

	// FreeBlockの要求メモリサイズより大きい場合分割
	if (freeBlock->GetSize() >= allocSize + m_MinAllocSize + BBeginTag::BlockOverhead)
	{
		BBeginTag* remainder = freeBlock->Split(allocSize);
		assert(remainder);
		RegisterFreeBlock(remainder);
	}

	// 確保情報記録
	{
		size_t memSize = freeBlock->GetSize();
		m_AllocationInfo.ActiveMemory += memSize;
		m_AllocationInfo.FreeMemory -= memSize;
		m_AllocationInfo.AllBoundaryTagSize += BBeginTag::BlockOverhead;
		m_AllocationInfo.AllAllocSize += memSize;
	}

	return freeBlock;
}

inline uint8* ZMemoryAllocator::_Malloc(size_t size, size_t numInstance)
{
	// 空き領域確保
	BBeginTag* freeBlock = GetFreeBlock(size);
	if (freeBlock == nullptr)
	{
		assert(false);
		return nullptr;	// メモリ確保失敗
	}

	freeBlock->SetNumInstance(numInstance);
	uint8* p = (uint8*)freeBlock;
	assert((ulong)(p + BBeginTag::HeaderOverhead) % Aligment == 0);

	return p + BBeginTag::HeaderOverhead;
}

inline uint8* ZMemoryAllocator::_Malloc(size_t size, size_t numInstance,int generalData)
{
	std::lock_guard<std::mutex> lg(m_Mtx);
	// 空き領域確保
	BBeginTag* freeBlock = GetFreeBlock(size);
	if (freeBlock == nullptr)
		return nullptr;	// メモリ確保失敗
	freeBlock->SetGeneralData(generalData);

	freeBlock->SetNumInstance(numInstance);
	uint8* p = (uint8*)freeBlock;
	assert((ulong)(p + BBeginTag::HeaderOverhead) % Aligment == 0);

	return p + BBeginTag::HeaderOverhead;
}

inline size_t ZMemoryAllocator::GetMSB(size_t size) const
{
	ulong index = 0;
	_BitScanReverse(&index, size);
	return (size_t)index;
}

inline size_t ZMemoryAllocator::GetLSB(size_t size)const
{
	ulong index = 0;
	_BitScanForward(&index, size);
	return (size_t)index;
}

inline size_t ZMemoryAllocator::CalcFreeBlockIndex(size_t fli, size_t sli)const
{
	return (fli << m_SLIPartitionBits) + sli;
}

inline size_t ZMemoryAllocator::GetSLI(size_t size, size_t msb)const
{
	size_t rs = msb - m_SLIPartitionBits;

	return (size >> rs) & (m_SLIPartition - 1);
}

inline bool ZMemoryAllocator::GetMinFreeListBit(size_t bit, size_t freeListBit, size_t * result)const
{
	const unsigned mask = 0xFFFFFFFF << bit;
	unsigned enableListBit = freeListBit & mask;
	if (enableListBit == 0)
		return false;

	*result = GetLSB(enableListBit);
	return true;
}

inline size_t ZMemoryAllocator::GetAlignmentedSize(size_t size) const
{
	if (size < m_MinAllocSize)
		return m_MinAllocSize;

	size = (size + (Aligment - 1)) & ~(Aligment - 1);
	assert((size % Aligment) == 0);

	return size;
}

inline void ZMemoryAllocator::GetIndex(unsigned size, unsigned* fli, unsigned* sli)const
{
	*fli = GetMSB(size);
	*sli = GetSLI(size, *fli);
}

inline void ZMemoryAllocator::ClearBit(unsigned index, unsigned fli, unsigned sli)
{
	assert(index == CalcFreeBlockIndex(fli, sli));

	if (m_pFreeBlocks[index])
		return;
	
	m_pFreeListBitSLI[fli] &= ~(1 << sli);
	
	if (m_pFreeListBitSLI[fli])
		return;

	m_FreeListBitFLI &= ~(1 << fli);
}