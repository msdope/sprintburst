#ifndef __ZSMART_POINTER_H__
#define __ZSMART_POINTER_H__

// ZMemoryAllocator用スマートポインター
// スレッドセーフじゃないのでマルチスレッド化の際は注意

namespace EzLib
{
namespace Memory
{

	template<typename T>
	class ZUP;

	template<typename T>
	class ZSP;
	
	template<typename T>
	class ZWP;

	class ZRefCountObjectBase
	{
	protected:
		ZRefCountObjectBase(): m_SPRefCnt(1), m_WPRefCnt(1)
		{
		}

	public:
		virtual ~ZRefCountObjectBase()
		{
		}

		void AddSPRef()
		{
			++m_SPRefCnt;
		}

		bool TryAddSPRef()
		{
			if (m_SPRefCnt == 0)
				return false;
			++m_SPRefCnt;
			return true;
		}

		void AddWPRef()
		{
			++m_WPRefCnt;
		}

		void SubSPRef()
		{
			if (--m_SPRefCnt != 0)
				return;
			
			DeleteObject();
			SubWPRef();
		}

		void SubWPRef()
		{
			if (--m_WPRefCnt != 0)
				return;
			DeleteThis();
		}

		size_t GetSPRefCnt()
		{
			return m_SPRefCnt;
		}

	private:
		virtual void DeleteObject() = 0;
		virtual void DeleteThis() = 0;

	private:
		std::atomic<size_t> m_SPRefCnt;
		std::atomic<size_t> m_WPRefCnt;

	};

	template<typename T>
	class ZRefCountObject : public ZRefCountObjectBase
	{
	public:
		explicit ZRefCountObject(T* pObj) :ZRefCountObjectBase(), m_pObj(pObj)
		{

		}

	private:
		virtual void DeleteObject()override
		{
			delptr(m_pObj);
		}

		virtual void DeleteThis()override
		{
			delptr(this);
		}


	private:
		T* m_pObj;
	};

	template<class T, class = void>
	struct IsEnableShared : std::false_type {};

	template<typename T>
	struct IsEnableShared<T, std::void_t<typename T::ESFT_Type>>
		: std::is_convertible<std::remove_cv_t<T>*, typename T::ESFT_Type*>::type {};

	template<typename T, typename U>
	void _EnableSharedFromThis(const ZSP<T>& sp, U* p, std::false_type);

	template<typename T, typename U>
	void _EnableSharedFromThis(const ZSP<T>& sp, U* p, std::true_type);

	template<typename T, typename U>
	void EnableSharedFromThis(const ZSP<T>& sp, U* p);

	// Unique Pointer
	template<typename T>
	class ZUP
	{
	public:
		using elemType = T;
		
		ZUP();
		ZUP(T* p);
		ZUP(ZUP<T>&& p);
		template<typename U>
		ZUP(ZUP<U>&& p);
		~ZUP();

		T& operator*()const;
		T* operator->()const;
		
		ZUP<T>& operator=(ZUP<T>&& p);
		ZUP<T>& operator=(T* p);
		ZUP<T>& operator=(nullptr_t nullval);
		
		template<typename U>
		ZUP<T>& operator=(U* p);
		template<typename U>
		ZUP<T>& operator=(ZUP<U>&& p);


		bool operator==(T* val)const;

		bool operator==(nullptr_t nullval)const;

		bool operator!=(T* val)const;
		
		bool operator!=(nullptr_t nullval)const;

		T& operator[](size_t index)const;

		operator bool()const;

		bool operator!()const;

		T* GetPtr()const;
		
		void Release();

		template<typename U>
		bool DownCast(ZUP<U>& p);

		bool IsActive()const;
		
		void Swap(ZUP<T>& up);
	private:
		template<typename U>
		friend class ZUP;

		T* m_Ptr;

	};

	// Unique Pointer生成マクロ
#define Make_Unique(T,newFunc,...)		\
		ZUP<T>(newFunc(T,__VA_ARGS__))

	// Shared Pointer
	template<typename T>
	class ZSP
	{
	public:
		using elemType = T;
		
		constexpr ZSP()
		{
		}
		
		constexpr ZSP(nullptr_t)
		{
		}

		template<typename U>
		ZSP(U* p);

		template<typename U>
		ZSP(const ZSP<U>& sp, elemType* p);

		// コピーコンストラクタ
		ZSP(const ZSP<T>& p);
		
		// コンストラクタ(暗黙的アップキャスト)
		template<typename U>
		ZSP(const ZSP<U>& sp);
		template<typename U>
		ZSP(const ZWP<U>& p);

		// ムーブコンストラクタ
		ZSP(ZSP<T>&& sp);
		template<typename U>
		ZSP(ZSP<U>&& sp);

		~ZSP();

		ZSP<T>& operator=(const ZSP<T>& sp);
		template<typename U>
		ZSP<T>& operator=(const ZSP<U>& sp);
		ZSP<T>& operator=(ZSP<T>&& sp);
		template<typename U>
		ZSP<T>& operator=(ZSP<U>&& sp);

		template<class U = T,
			std::enable_if_t<!std::disjunction_v<std::is_array<U>, std::is_void<U>>, int> = 0>
		U& operator*()const;

		T* operator->()const;

		bool operator==(T* val)const;

		bool operator==(nullptr_t nullval)const;

		bool operator!=(T* val)const;

		bool operator!=(nullptr_t nullval)const;

		operator bool()const;

		bool operator!()const;

		template<class U = T,class Elem = elemType,
			std::enable_if_t<std::is_array_v<U>, int> = 0>
		Elem& operator[](size_t index)const;

		// リセット
		void Reset();
		void Reset(T* p); // ポインタセット

		// ポインタ取得
		T* GetPtr()const;

		// 参照カウント取得
		size_t GetRefCnt()const;

		// ダウンキャスト(ダイナミックキャスト)
		template<typename U>
		bool DownCast(ZSP<U>& p);
		template<typename U>
		ZSP<U> DownCast();
		
		template<typename U>
		ZSP<U> Cast();

		// ポインタが解放されていないか
		bool IsActive()const;

		// Swap
		void SwapPtr(ZSP<T>& sp);

	private:
		template<typename U>
		friend class ZSP;

		template<typename U>
		friend class ZWP;

	private:
		ZRefCountObjectBase* m_pRefCountObj{nullptr};	// 参照カウンタオブジェクト
		T* m_Ptr{nullptr};							// T型オブジェクトのポインタ
	};

	// Shared Pointer生成マクロ
#define Make_Shared(T,newFunc,...)		\
		ZSP<T>(newFunc(T,__VA_ARGS__))

	template<typename T>
	class ZWP
	{
	public:
		using elemType = T;
		constexpr ZWP()
		{
		}
		
		// コピーコンストラクタ
		ZWP(const ZWP<T>& p);

		// コンストラクタ(暗黙的アップキャスト)
		template<typename U>
		ZWP(const ZSP<U>& p);
		template<typename U>
		ZWP(const ZWP<U>& p);

		// ムーブコンストラクタ
		ZWP(ZWP<T>&& p);
		template<typename U>
		ZWP(ZWP<U>&& p);

		~ZWP();

		ZWP<T>& operator=(const ZWP<T>& p);
		template<typename U>
		ZWP<T>& operator=(const ZWP<U>& p);
		ZWP<T>& operator=(ZWP<T>&& p);
		template<typename U>
		ZWP<T>& operator=(ZWP<U>&& p);

		template<typename U>
		ZWP<T>& operator=(const ZSP<U>& p);

		bool operator==(T* val);

		bool operator==(nullptr_t nullval);

		bool operator!=(T* val);

		bool operator!=(nullptr_t nullval);

		operator bool()const;

		bool operator!()const;

		// ポインタ取得
		T* GetPtr()const;

		ZSP<T> Lock()const;

		// ポインタが解放されていないか
		bool IsActive()const;

		// Swap
		void SwapPtr(ZWP<T>& sp);

		void Reset();

	private:
		template<typename U>
		friend class ZWP;
		template<typename U>
		friend class ZSP;

	private:
		ZRefCountObjectBase* m_pRefCountObj{ nullptr };	// 参照カウンタオブジェクト
		T* m_Ptr{ nullptr };							// T型オブジェクトのポインタ
	};

	template<typename T>
	class ZEnable_Shared_From_This
	{
	public:
		using ESFT_Type = ZEnable_Shared_From_This;

	public:
		ZSP<T> SharedFromThis();
		ZSP<const T> SharedFromThis()const;
		ZWP<T> WeakFromThis();
		ZWP<const T> WeakFromThis()const;

	protected:
		constexpr ZEnable_Shared_From_This() : m_WP()
		{
		}
		ZEnable_Shared_From_This(const ZEnable_Shared_From_This&);
		virtual ~ZEnable_Shared_From_This() = default;

		ZEnable_Shared_From_This& operator=(const ZEnable_Shared_From_This&);


	private:
		template<typename U, typename V>
		friend void _EnableSharedFromThis(const ZSP<U>& sp, V* p, std::true_type);

		ZWP<T> m_WP;

	};


}
}

namespace EzLib
{
namespace Memory
{
#include "ZSmartPointer.inl"
}
}

// std::hash 特殊化(これを定義しないとmao系のクラスのキー値に使えない)
template<typename T>
struct std::hash<EzLib::Memory::ZSP<T>>
{
	size_t operator()(const ZSP<T>& obj)const
	{
		return (size_t)obj.GetPtr();
	}
};

template<typename T>
struct std::hash<EzLib::Memory::ZUP<T>>
{
	size_t operator()(const ZUP<T>& obj)const
	{
		return (size_t)obj.GetPtr();
	}
};

template<typename T>
struct std::hash<EzLib::Memory::ZWP<T>>
{
	size_t operator()(const ZWP<T>& obj)const
	{
		return (size_t)obj.GetPtr();
	}
};

#endif