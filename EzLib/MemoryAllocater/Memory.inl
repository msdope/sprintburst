template<typename T>
inline void delptr(T* p)
{
	if (p == nullptr)
		return;

	// system,application,stlのどのアロケータからメモリを取得したかの情報取得
	int area = EzLib::Memory::ZMemoryAllocator::GetGeneralData((void*)p);
	assert(area > -1); // -1 = アロケータを使ってメモリを確保していない

	// 解放
	EzLib::Memory::ZAllocator::Free(p, (EzLib::Memory::MEMORY_AREA_TYPE)area);
}

template<typename T>
inline void SAFE_DELPTR(T*& p)
{
	delptr(p);
	p = nullptr;
}

namespace EzLib
{
namespace Memory
{
	template<typename T>
	inline void ZMemoryArea::Free(T* p)
	{
		m_MemoryAllocator.Free<T>(p);
	}

	template<typename T>
	inline void* ZMemoryArea::Malloc(size_t size,MEMORY_AREA_TYPE area)
	{
		return m_MemoryAllocator.Malloc<T>(size,(int)area);
	}

	template<typename T>
	inline void* ZAllocator::Malloc(size_t size, MEMORY_AREA_TYPE area)
	{
		if (IsInitialized == false)
		{
			assert(false);
			return nullptr;
		}

		void* p = MemoryArea[area].Malloc<T>(size, area);
		assert(p);

		return p;
	}

	inline const ZMemoryAllocator::AllocationInfo& ZAllocator::GetAllocationInfo(MEMORY_AREA_TYPE area)
	{
		return MemoryArea[area].GetAllocationInfo();
	}

	template<typename T>
	inline void ZAllocator::Free(T* p, MEMORY_AREA_TYPE area)
	{
		if (IsInitialized == false)
			assert(false);
	
		MemoryArea[area].Free<T>(p);
	}

}
}