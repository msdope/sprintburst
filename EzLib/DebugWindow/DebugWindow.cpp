#include "Game/Application.h"

#ifdef SHOW_DEBUG_WINDOW

bool DebugWindow::Init(HWND hWnd, int scrollWindowNum, void(*commandProc)(ZString))
{
	// すでに作成済みなら
	if (m_Initialized)
		return false;

	m_CommandProc = commandProc;
	m_NumScrollWindow = scrollWindowNum;
	m_ScrollMsgBufferList.resize(m_NumScrollWindow);
	m_ImGuiFunctions.clear();
	// 1フレームで処理するメッセージ数を適当な数に設定
#ifdef _DEBUG
	NumUpdateMessagePerFrame = 2000 * (int)(m_NumScrollWindow * 0.5f);
#else
#ifdef NDEBUG
	NumUpdateMessagePerFrame = 10000 * (int)(m_NumScrollWindow * 0.5f);
#endif
#endif

	// スクロールウィンドウに表示されるメッセージ数は適当に
	MaxScrollWindowMessage = 150;

	// ImGui 初期化
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
	io.ConfigFlags |= ImGuiConfigFlags_DpiEnableScaleFonts;     // FIXME-DPI: THIS CURRENTLY DOESN'T WORK AS EXPECTED. DON'T USE IN USER APP!
	io.ConfigFlags |= ImGuiConfigFlags_DpiEnableScaleViewports; // FIXME-DPI
	io.ConfigResizeWindowsFromEdges = true;
	io.ConfigDockingWithShift = true;
	
	if (ImGui_ImplWin32_Init(hWnd) == false)
		return false;
	
	if (ImGui_ImplDX11_Init(ZDx.GetDev(), ZDx.GetDevContext()) == false)
		return false;

	// Setup style
	ImGui::StyleColorsDark();
	
	for (int i = 0; i<STATIC_MESSAGE_MAX; i++)
	{
		char s[20];
		sprintf(s, "%3d :", i + 1);
		m_LineIndexNumbers[i] = s;
		m_StaticMessages[i] = s;
	}

	m_Initialized = true;
	Open();
	DebugLog("<><><><> デバッグウィンドウ初期化終了 <><><><>\n");
	return true;
}

void DebugWindow::Release()
{
	if (m_Initialized == false)
		return;
	Close();
	m_Initialized = false;

	for (int i = 0; i < STATIC_MESSAGE_MAX; i++)
		m_StaticMessages[i].clear();
	for (UINT i = 0; i < m_ScrollMsgBufferList.size(); i++)
		m_ScrollMsgBufferList[i].clear();

	m_CommandProc = nullptr;

	m_TimeMap.clear();
	
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();
}

void DebugWindow::Update()
{
	if (m_Initialized == false)
		return;

	if (m_IsOpen)
	{
		m_MsgMutex.lock();
		
		if(m_MessageBuffer[DebugMessage::STATIC].size() > 0)
		{
			for(auto& staticMsg : m_MessageBuffer[DebugMessage::STATIC])
				m_StaticMessages[staticMsg.m_Index] = m_LineIndexNumbers[staticMsg.m_Index] + staticMsg.m_Message;
			m_MessageBuffer[DebugMessage::STATIC].clear();
		}
		
		if(m_MessageBuffer[DebugMessage::SCROLL].size() > 0)
		{
			// 各スクロールウィンドウごとにメッセージデータを振り分け
			ZVector<ZVector<DebugMessage>> scrollMsgBufferList(m_ScrollMsgBufferList.size());
			
			// メモリを適当な量事前確保
			for(size_t i = 0; i < scrollMsgBufferList.size();i++)
				scrollMsgBufferList[i].reserve(NumUpdateMessagePerFrame / scrollMsgBufferList.size());
			
			// 振り分け
			for (size_t i = 0; i < m_MessageBuffer[DebugMessage::SCROLL].size() && i< NumUpdateMessagePerFrame; i++)
			{
				auto& msg = m_MessageBuffer[DebugMessage::SCROLL][i];
				int index = msg.m_Index;
				scrollMsgBufferList[index].push_back(msg);
			}

			// スクロールメッセージ処理数制限用カウンター
			size_t scrollMsgCount;
			{
				// マルチスレッドでの処理数制限用カウンター
				std::atomic<size_t> atomicScrollMsgCount = 0;

				// 各スクロールウィンドウ用バッファにメッセージバッファの中身をコピー
				auto addToScrollMsgBufferThread = [this, &scrollMsgBufferList,&atomicScrollMsgCount](size_t index)
				{
					if (scrollMsgBufferList[index].size() <= 0)
						return;

					// 事前にメモリ確保
					m_ScrollMsgBufferList[index].reserve(scrollMsgBufferList[index].size());

					for(auto& msg : scrollMsgBufferList[index])
					{	
						size_t cnt = atomicScrollMsgCount.fetch_add(1);
						if (cnt >= NumUpdateMessagePerFrame)
							return;
						// スクロールウィンドウへのクリアメッセージが来ていれば
						if (msg.m_Flag == DebugMessage::CLEAR_SCROLL)
							m_ScrollMsgBufferList[index].clear();
						else
							m_ScrollMsgBufferList[index].push_back(msg.m_Message);
					}
				};

				// ↑の処理をマルチスレッドで全スクロールウィンドウ分を並行処理
				{
					ZVector<std::thread> threads;
					for(size_t i = 0;i < scrollMsgBufferList.size();i++)
						threads.push_back(std::thread(addToScrollMsgBufferThread,i));
					for (auto& th : threads)
						th.join();
				}

				// atomicのカウンターをそのまま使うと排他制御に処理が多少かかるので普通の変数に値コピー
				if (atomicScrollMsgCount > NumUpdateMessagePerFrame)
					scrollMsgCount = NumUpdateMessagePerFrame;
				else
					scrollMsgCount = atomicScrollMsgCount;
			}
			// vectorのerase関数が遅いのでerase関数は使わずに、
			// 処理したデータはいらないのでvectorの処理済みデータ数+1から最後までをvectorの先頭位置にコピーし、
			// (要は次のフレームで処理する分をvectorの先頭位置にスライドさせる)
			// vectorを元のvectorのサイズ-処理済みデータ数にリサイズする
			if(scrollMsgCount > 0)
			{
				if (scrollMsgCount >= m_MessageBuffer[DebugMessage::SCROLL].size())
					m_MessageBuffer[DebugMessage::SCROLL].clear();
				else
				{
					auto begin = m_MessageBuffer[DebugMessage::SCROLL].begin() + scrollMsgCount;
					auto end = m_MessageBuffer[DebugMessage::SCROLL].end();	
					std::copy(begin, end, m_MessageBuffer[DebugMessage::SCROLL].begin());

					auto size = m_MessageBuffer[DebugMessage::SCROLL].size();
					m_MessageBuffer[DebugMessage::SCROLL].resize(size - scrollMsgCount);
				}
			}
		}

		for(auto& scrollMsgBuffer : m_ScrollMsgBufferList)
		{
			// 300より多いならオーバーしている分古いものを削除
			if (scrollMsgBuffer.size() > MaxScrollWindowMessage)
			{
				// ↑のm_MessageBufferの処理済みデータの削除と同じ方法で削除
				size_t numOverData = scrollMsgBuffer.size() - MaxScrollWindowMessage;
				auto begin = scrollMsgBuffer.begin() + numOverData;
				auto end = scrollMsgBuffer.end();
				std::copy(begin, end, &scrollMsgBuffer[0]);
				
				scrollMsgBuffer.resize(MaxScrollWindowMessage);
			}
		}

		m_MsgMutex.unlock();
	}

	ZSQueue<ZString> tmpQueue = m_CommandQueue;
	m_CommandQueue = ZSQueue<ZString>();

	while (tmpQueue.empty() == false)
	{
		if (m_CommandProc)
			m_CommandProc(tmpQueue.front());
		tmpQueue.pop();
	}

}

void DebugWindow::Draw(ZSP<ZTexture> renderTarget)
{
	if (m_Initialized == false)
		return;

	GUIUpdate();

	ID3D11RenderTargetView* rtv = ZDx.GetBackBuffer()->GetRTTex();
	if (renderTarget != nullptr)
		rtv = renderTarget->GetRTTex();
	if (rtv == nullptr)
		return;

	ZDx.GetDevContext()->OMSetRenderTargets(1, &rtv, NULL);

	GUIDraw();
}

void DebugWindow::GUIUpdate()
{
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();

	if (m_IsOpen == false)
		return;

	ImGuiWindowFlags flag = 0;
	flag |= //ImGuiWindowFlags_NoResize |
			//ImGuiWindowFlags_NoCollapse |
			//ImGuiWindowFlags_NoMove |
			//ImGuiWindowFlags_NoBringToFrontOnFocus |
			//ImGuiWindowFlags_NoTitleBar |
			ImGuiWindowFlags_MenuBar;
	
	auto dockID = ImGui::GetID("Debug_Messages_Tab");
	if (ImGui::Begin("Debug_Messages", &m_IsOpen, flag) == false)
	{
		ImGui::End();
		return;
	}
	ImGui::DockSpace(dockID);
	ImGui::End();

	ImGui::SetNextWindowDockId(dockID, ImGuiCond_FirstUseEver);
	ImGui::Begin("Static");
	{
		for (int i = 0; i < 100; i++)
			ImGui::Text(m_StaticMessages[i].c_str());
	}
	ImGui::End();


	std::string dockName;
	for(int scrollWndIndex = 0; scrollWndIndex < m_NumScrollWindow; scrollWndIndex++)
	{
		dockName = "Scroll_" + std::to_string(scrollWndIndex);
		ImGui::SetNextWindowDockId(dockID, ImGuiCond_FirstUseEver);
		if (ImGui::Begin(dockName.c_str()))
		{
			for(auto& msg : m_ScrollMsgBufferList[scrollWndIndex])
				ImGui::Text(msg.c_str());
		}
		ImGui::End();
	}

	
	std::lock_guard<std::mutex> lg(m_ImGuiMutex);
	for (size_t i = 0; i < m_ImGuiFunctions.size(); i++)
		m_ImGuiFunctions[i]();
	
	m_ImGuiFunctions.clear();

}

void DebugWindow::GUIDraw()
{
	ImGui::Render();
	// GUI描画
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
	ImGuiIO& io = ImGui::GetIO();
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
	}
}

#endif