#pragma comment(lib,"d3dcompiler.lib")
#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"dxgi.lib")
#pragma comment(lib,"dxguid.lib")

#pragma comment(lib,"dsound.lib")

//=====================================================================
// DirectX Tool Kit ライブラリ
//=====================================================================
#ifdef _DEBUG
	// Visual Studio 2015
#if _MSC_VER == 1900
#pragma comment(lib,"DirectXTK/Lib/2015/Debug/DirectXTK.lib")
#pragma comment(lib,"DirectXTK/Lib/2015/Debug/DirectXTKAudioWin8.lib")
// Visual Studio 2017 Update5
#elif _MSC_VER >= 1912
#pragma comment(lib,"DirectXTK/Lib/2017/Debug/DirectXTK.lib")
#pragma comment(lib,"DirectXTK/Lib/2017/Debug/DirectXTKAudioWin8.lib")
#endif
#else
	// Visual Studio 2015
#if _MSC_VER == 1900
#pragma comment(lib,"DirectXTK/Lib/2015/Release/DirectXTK.lib")
#pragma comment(lib,"DirectXTK/Lib/2015/Release/DirectXTKAudioWin8.lib")
// Visual Studio 2017 Update5
#elif _MSC_VER >= 1912
#pragma comment(lib,"DirectXTK/Lib/2017/Release/DirectXTK.lib")
#pragma comment(lib,"DirectXTK/Lib/2017/Release/DirectXTKAudioWin8.lib")
#endif
#endif

//=====================================================================
// DirectXTex ライブラリ
//=====================================================================
#ifdef _DEBUG
	// Visual Studio 2015
#if _MSC_VER == 1900
#pragma comment(lib,"DirectXTex/Lib/2015/Debug/DirectXTex.lib")
// Visual Studio 2017 Update5
#elif _MSC_VER >= 1912
#pragma comment(lib,"DirectXTex/Lib/2017/Debug/DirectXTex.lib")
#endif
#else
	// Visual Studio 2015
#if _MSC_VER == 1900
#pragma comment(lib,"DirectXTex/Lib/2015/Release/DirectXTex.lib")
// Visual Studio 2017 Update5
#elif _MSC_VER >= 1912
#pragma comment(lib,"DirectXTex/Lib/2017/Release/DirectXTex.lib")
#endif
#endif

//=====================================================================
// Bullet Physics Engine ライブラリ
//=====================================================================
#ifdef _DEBUG
#pragma comment(lib,"BulletPhysics/lib/Debug/BulletCollision.lib")
#pragma comment(lib,"BulletPhysics/lib/Debug/BulletDynamics.lib")
#pragma comment(lib,"BulletPhysics/lib/Debug/LinearMath.lib")
#else
#pragma comment(lib,"BulletPhysics/lib/Release/BulletCollision.lib")
#pragma comment(lib,"BulletPhysics/lib/Release/BulletDynamics.lib")
#pragma comment(lib,"BulletPhysics/lib/Release/LinearMath.lib")
#endif

//=====================================================================
// Effekseer ライブラリ
//=====================================================================
#if _DEBUG
#pragma comment(lib,"Effekseer/lib/Debug/Effekseer.lib")
#pragma comment(lib,"Effekseer/lib/Debug/EffekseerRendererDX11.lib")
#pragma comment(lib,"Effekseer/lib/Debug/EffekseerSoundXAudio2.lib")
#else
#pragma comment(lib,"Effekseer/lib/Release/Effekseer.lib")
#pragma comment(lib,"Effekseer/lib/Release/EffekseerRendererDX11.lib")
#pragma comment(lib,"Effekseer/lib/Release/EffekseerSoundXAudio2.lib")
#endif


//=====================================================================
// json ライブラリ https://github.com/dropbox/json11
//=====================================================================
#include "json11.hpp"

//=====================================================================

//=====================================================================
// ImGui		https://github.com/ocornut/imgui
//   &
// ImGui_Dock	https://github.com/BentleyBlanks/imguiDock
//=====================================================================
#include "imgui.h"
#include "imgui_impl_win32.h"
#include "imgui_impl_dx11.h"
//=====================================================================

//=====================================================================
// Cereal
//=====================================================================
#include <cereal.hpp>
#include <archives\json.hpp>
#include <archives\binary.hpp>
#include <types\array.hpp>
#include <types\string.hpp>
#include <types\unordered_map.hpp>
#include <types\vector.hpp>
#include <types\list.hpp>

//=====================================================================

#pragma comment(lib,"winmm.lib")
#pragma comment(lib,"ws2_32.lib")
#pragma comment(lib,"imm32.lib")
#pragma comment(lib,"rpcrt4.lib")

#ifndef __EzLib_H__
#define __EzLib_H__

#include <winsock2.h>
#include <windows.h>
#include <time.h>
#include <stdio.h>
#include <process.h>
#include <mmsystem.h>
#include <math.h>
#include <commctrl.h>
#include <ctime>
#include <wrl.h>	// ComPtrなどで使用

// DirectX11
#include <d3dcommon.h>
#include <dxgi.h>
#include <d3d11.h>
#include <d3dcompiler.h>

// DirectX Tool Kit
#ifdef _DEBUG	// 一部の衝突判定関数などで、assetを出してほしくないとこでassetが出るので、NDEBUGを定義してコンパイル。
#define NDEBUG
#include <DirectXMath.h>
#include <SimpleMath.h>
#include <DirectXCollision.h>
#undef NDEBUG
#else
#include <DirectXMath.h>
#include <SimpleMath.h>
#include <DirectXCollision.h>
#endif

#include <DirectXPackedVector.h>
#include <WICTextureLoader.h>
#include <DDSTextureLoader.h>
#include <SpriteBatch.h>
#include <SpriteFont.h>
#include <Audio.h>

// DirectXTex
#include <DirectXTex.h>

// STL
#include <set>
#include <map>
#include <unordered_map>
#include <array>
#include <string>
#include <vector>
#include <stack>
#include <list>
#include <iterator>
#include <queue>
#include <deque>
#include <algorithm>
#include <memory>
#include <random>
#include <fstream>
#include <iostream>
#include <sstream>
#include <functional>
#include <thread>
#include <atomic>
#include <mutex>
#include <shlobj.h>
#include <new>
#include <future>
#include <locale>
#include <codecvt>
#include <chrono>
#include <condition_variable>
#include <cassert>
#include <type_traits>
#include <bitset>
#include <stdexcept>
#include <exception>

// Bullet
#include "btBulletDynamicsCommon.h"
#include "BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h"
#include "BulletCollision/Gimpact/btGImpactShape.h"
#include "BulletCollision/CollisionDispatch/btGhostObject.h"

// Effekseer
#include "Effekseer.h"
#include "EffekseerRendererDX11.h"
#include "EffekseerSoundXAudio2.h"


// doxygen用グループ設定
//  @defgroup Graphics_Important グラフィック関係[*主要機能*](基本)
//  @defgroup Graphics グラフィック関係(基本)
//  @defgroup Graphics_Model_Important グラフィック関係[*主要機能*](モデル)
//  @defgroup Graphics_Model グラフィック関係(モデル)
//  @defgroup Graphics_Shader グラフィック関係(シェーダ)
//  @defgroup Animation アニメーション関係
//  @defgroup Collision あたり判定関係
//  @defgroup Math 算術関係
//  @defgroup Physics 物理エンジン関係
//  @defgroup Sound サウンド関係
//  @defgroup Etc その他


//==================================================
//
// EzLib
//
//==================================================

//==================================================
//   SoundManagerのインスタンス取得 短縮記述版
//  @ingroup Sound
//==================================================
#define ZSndMgr ZSoundManager::GetInstance()

#include "SystemDefines.h"

#include "MemoryAllocater/ZMemoryAllocator.h"
#include "MemoryAllocater/Memory.h"
#include "MemoryAllocater/ZSTLAllocator.h"
#include "MemoryAllocater/ZSmartPointer.h"

#include "ZHelper.h"

#include "SubSystem/ZSubSystem.h"

#include "Tools/ZIResource.h"

#include "Job/ZJobException.h"
#include "Job/ZJobFunctionTypes.h"
#include "Job/ZJobStateBase.h"
#include "Job/ZTemplateJobStateBase.h"
#include "Job/ZJobState.h"
#include "Job/ZAbortableJobState.h"
#include "Job/ZJob.h"
#include "Job/ZJobGroup.h"
#include "Job/ZWorker.h"
#include "Job/ZJobManager.h"

#include "Utils/ZTimer.h"
#include "Utils/ZLocking.h"
#include "Utils/ZFpsControl.h"
#include "Utils/ZMTRand.h"
#include "Utils/ZThreadID.h"
#include "Utils/ZThreadPool.h"
#include "Tools/CSV/ZCsv.h"

#include "Math/ZMath.h"

#include "Camera/ZCamera.h"

#include "Animation/ZUVAnimation.h"
#include "Animation/ZKeyframeAnimation.h"

#include "Graphics/ZVertexType.h"

#include "Collision/ZCollision.h"
#include "Octree/ZOctree.h"

#include "Graphics/ZTexture.h"
#include "Graphics/ZStates.h"

#include "Graphics/ZShaderResource.h"
#include "Graphics/ZBufferLayout.h"
#include "Graphics/ZVertexBuffer.h"

#include "Graphics/ZConstantBuffer.h"
#include "Graphics/ZVertexShader.h"
#include "Graphics/ZPixelShader.h"
#include "Graphics/ZShaderSet.h"

#include "Graphics/ZPolygon.h"
#include "Graphics/ZBillBoard.h"
#include "Graphics/ZLaser.h"
#include "Graphics/ZMesh.h"
#include "Graphics/ZSingleModel.h"
#include "Graphics/ZGameModel.h"

#include "Graphics/ZPostEffects.h"

#include "Physics/ZBulletPhysicsEngine.h"

#include "Animation/ZBoneController.h"

#include "Font/ZFont.h"
#include "Graphics/ZDirectXGraphics.h"

#include "Sounds/ZSound.h"

#include "Tools/ZDataStorage.h"
#include "Tools/ZResourceStorage.h"

#include "Effect/ZEffectManager.h"

#include "Utils/ZFile.h"
#include "Utils/TextFileReader.h"
#include "Utils/ZPathUtil.h"
#include "Utils/DebugLog.h"

#include "Input/ZInput.h"

#include "MMD/ZMMD.h"
#include "./DebugWindow/DebugWindow.h"
#include "ZECS/ZECS.h"

#include "Scene/ZScene.h"


/* SubStystem */
#define ZDx GetSubSystem<ZDirectXGraphics>()
#define PHYSICS GetSubSystem<ZPhysicsWorld>()
#define EFFECT GetSubSystem<ZEffectManager>()


#endif
