#include "PCH/pch.h"

using namespace EzLib;

#define DXTRACE_ERR(str,hr)           (hr)

//==================================================================
// ZSound
//==================================================================
void ZSound::Play(bool loop)
{
	if (m_Inst == nullptr)return;

	// 設定
	m_Inst->Stop();
	m_Inst->SetVolume(1);

	// 再生
	m_Inst->Play(loop);

	// 再生リストへ追加
	ZSndMgr.AddPlayList(ToZSP(this));
}

// 3D Sound Ver
void ZSound::Play3D(const ZVec3& pos, bool loop)
{
	if (m_Inst == nullptr)return;

	// 設定
	m_Inst->Stop();
	m_Inst->SetVolume(1);

	// 再生
	m_Inst->Play(loop);

	// 座標設定
	DirectX::AudioEmitter emitter;
	emitter.SetPosition(pos);

	m_Inst->Apply3D(ZSndMgr.GetListener(), emitter);


	// 再生リストへ追加
	ZSndMgr.AddPlayList(SharedFromThis());
}

void ZSound::SetVolume(float vol)
{
	if (m_Inst == nullptr)return;

	m_Inst->SetVolume(vol);
}

void ZSound::SetPos(const ZVec3& pos)
{
	if (m_Inst == nullptr)return;

	// 座標設定
	DirectX::AudioEmitter emitter;
	emitter.SetPosition(pos);

	m_Inst->Apply3D(ZSndMgr.GetListener(), emitter);
}

bool ZSound::IsPlay()
{
	if (m_Inst == nullptr)return false;

	if (m_Inst->GetState() == DirectX::SoundState::PLAYING)return true;

	return false;
}

ZSound::~ZSound()
{
	Stop();
	m_Inst = nullptr;
	m_srcData = nullptr;
}

//==================================================================
// ZSoundData
//==================================================================
bool ZSoundData::LoadWaveFile(const ZString& fileName)
{
	if (!ZSndMgr.IsInit())return false;

	// ファイル名をwcharへ変換
	int len = (int)fileName.size();
	WCHAR* wStr = sysnewArray(WCHAR,len + 1);
	wStr[len] = 0;
	size_t wLen;
	mbstowcs_s(&wLen, wStr, len + 1, fileName.c_str(), _TRUNCATE);
	// 読み込み
	m_SoundEffect = Make_Unique(DirectX::SoundEffect,sysnew,ZSndMgr.GetAudioEngine().GetPtr(), wStr);

	SAFE_DELPTR(wStr);

	m_FileName = fileName;

	return true;
}

ZSP<ZSound> ZSoundData::CreateInstance(bool b3D)
{
	if (!ZSndMgr.IsInit())return nullptr;
	if (m_SoundEffect == nullptr)return nullptr;

	ZSP<ZSound> inst(sysnew(ZSound));

	// フラグ
	DirectX::SOUND_EFFECT_INSTANCE_FLAGS flags = DirectX::SoundEffectInstance_Default;
	if (b3D)
	{
		flags = flags | DirectX::SoundEffectInstance_Use3D | DirectX::SoundEffectInstance_ReverbUseFilters;
	}

	// インスタンス作成
	inst->m_Inst = m_SoundEffect->CreateInstance(flags);
	inst->m_b3D = b3D;
	inst->m_srcData = ToZSP(this);

	return inst;
}


//==================================================================
//
//
// SoundManager
//
//
//==================================================================
ZSoundManager::ZSoundManager()
{
	m_AudioEng = nullptr;
}

ZSoundManager::~ZSoundManager()
{
	Release();
}

bool ZSoundManager::Init()
{
	Release();

	DirectX::AUDIO_ENGINE_FLAGS eflags = DirectX::AudioEngine_ReverbUseFilters;//DirectX::AudioEngine_EnvironmentalReverb | DirectX::AudioEngine_ReverbUseFilters;
	#ifdef _DEBUG
	eflags = eflags | DirectX::AudioEngine_Debug;
	#endif

	m_AudioEng = Make_Unique(DirectX::AudioEngine,sysnew,eflags);
	m_AudioEng->SetReverb(DirectX::Reverb_ConcertHall);

	return true;
}


void ZSoundManager::Release()
{
	m_PlayList.clear();


	m_AudioEng = nullptr;

}

void ZSoundManager::StopAll()
{
	if (m_AudioEng == nullptr)return;

	// 再生リスト
	{
		auto it = m_PlayList.begin();
		while (it != m_PlayList.end())
		{
			(*it).second->Stop();
			++it;
		}
	}

}

void ZSoundManager::Update()
{
	if (m_AudioEng == nullptr)return;

	if (m_AudioEng->Update())
	{
		// No audio device is active
		if (m_AudioEng->IsCriticalError())
		{

		}
	}

	// 再生リスト処理
	{
		auto it = m_PlayList.begin();
		while (it != m_PlayList.end())
		{
			// 再生中でないときは、リストから削除(どこからも参照されていなければ解放される)
			if (!(*it).second->IsPlay())
			{
				it = m_PlayList.erase(it);
				continue;
			}

			++it;
		}
	}

}
