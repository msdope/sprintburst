// その他補助的なもの
#ifndef ZHelper_h
#define ZHelper_h

namespace EzLib
{
	class ZVec2;
	class ZVec3;
	class ZVec4;

	// 安全な解放系

	//   安全にReleaseするマクロ
	template<typename T>
	inline void Safe_Release(T*& p)
	{
		if (p)
		{
			p->Release();
			p = nullptr;
		}
	}

	//   newされた変数を安全に解放するマクロ
	template<typename T>
	inline void Safe_Delete(T*& p)
	{
		if (p)
		{
			delete p;
			p = nullptr;
		}
	}

	//   newされた配列を安全に解放するマクロ
	template<typename T>
	inline void Safe_Delete_Array(T*& p)
	{
		if (p)
		{
			delete[] p;
			p = nullptr;
		}
	}


	// 生ポインタからshared_ptrへ変換する
	// ※ただし、std::enable_shared_from_thisを継承したクラスに限る(それ以外のクラスはコンパイルエラーになる)
	// ※当然そのクラスは、make_shared関数やnewで作られたshared_ptrに限る(普通の変数の場合はダメ)
	// ※つまりこの生ポインタ(ptr)は、どこかでshared_ptrで保持されている必要がある
	// 詳しくはstd::enable_shared_from_thisを調べてください
	//  ptr	… 変換元となる生ポインタ
	//  戻り値 : 変換後のshared_ptr
	template<typename T>
	inline sptr<T> ToSptr(T* ptr)
	{
		return sptr_static_cast<T>(ptr->shared_from_this());
	}

	template<typename T>
	inline ZSP<T> ToZSP(T* ptr)
	{
		return ptr->SharedFromThis().Cast<T>();
	}

	inline UUID GetUUID()
	{
		UUID uuid;
		UuidCreate(&uuid);
		return uuid;
	}

	inline ZString&& UUIDToStr(UUID& uuid)
	{
		ZString s;
		char* strUuid = nullptr;
		UuidToStringA(&uuid, (RPC_CSTR*)&strUuid);
		s = strUuid;
		RpcStringFreeA((RPC_CSTR*)&strUuid);
		return std::move(s);
	}

	//=====================================================================
	//   ファイル名と拡張子の間に、指定文字列(ext.)を入れる
	//  	FileName	… 元となるファイル名
	//  	ext			… 挿入したい文字列
	//  @return extが挿入された文字列
	//=====================================================================
	ZString ConvertExtFileName(const ZString& FileName, const ZString& ext);


	//===================================================
	//   法線マップ用のTangent,Binormalを算出する
	//===================================================
	void CalcTangentAndBinormal
	(
		ZVec3* p0, ZVec2* uv0,
		ZVec3* p1, ZVec2* uv1,
		ZVec3* p2, ZVec2* uv2,
		const ZVec3* vN, ZVec3* outTangent, ZVec3* outBinormal
	);

	//===================================================
	//   string -> wstring変換
	//===================================================
	ZWString ConvertStringToWString(const ZString& str);

	//===================================================
	//   wstring -> string変換
	//===================================================
	ZString ConvertWStringToString(const ZWString& wstr);

	//===================================================
	//   UTF-8 -> Shift-JIS変換
	//===================================================
	ZString Utf8ToMulti(const ZString& str);

	//===================================================
	//   UTF-16 -> UTF-8変換
	//===================================================
	ZString Utf16ToUtf8(const ZU16String& str);
	//===================================================
	//   UTF-16 -> Shift-JIS変換
	//===================================================
	ZString Utf16ToMulti(const ZU16String& str);

	//===================================================
	//   sprintfのZStringバージョン
	//===================================================
	template<typename... Args>
	ZString strprintf(const ZString format, Args... args)
	{
		char tmp[1024];
		sprintf_s(tmp, sizeof(tmp), format.c_str(), args...);

		return tmp;
	}

	//===================================================
	//   指定ファイルを文字列として読み込む
	//===================================================
	inline std::string LoadStringFromFile(const std::string& filename)
	{
		std::ifstream ifs(filename);
		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());

		return std::move(str);
	}

	//===================================================
	//   JSONファイルを読み込む
	//   json11ライブラリを使用しています
	//===================================================
	inline json11::Json LoadJsonFromFile(const std::string& jsonFilename, std::string& errorMessage)
	{
		// jsonファイルを文字列として全読み込み
		auto strFile = LoadStringFromFile(jsonFilename);
		// json解析
		return json11::Json::parse(strFile, errorMessage);
	}

	template<typename T>
	static inline T ZLerp(T a, T b, float t)
	{
		return (T)(a + (b - a)* t);
	}




	//===================================================
	//	CelealでのVector出力
	//	Key名を指定した配列で出力するため
	//===================================================

	template<class T>
	void SaveVector4(T& ar, ZVec4& vec, const std::string& key) {
		std::vector<float> v;
		v.push_back(vec.x);
		v.push_back(vec.y);
		v.push_back(vec.z);
		v.push_back(vec.w);
		ar(cereal::make_nvp(key, v));
	}

	template<class T>
	void SaveVector3(T& ar, ZVec3& vec, const std::string& key) {
		std::vector<float> v;
		v.push_back(vec.x);
		v.push_back(vec.y);
		v.push_back(vec.z);
		ar(cereal::make_nvp(key, v));
	}

	template<class T>
	void SaveVector2(T& ar, ZVec2& vec, const std::string& key) {
		std::vector<float> v;
		v.push_back(vec.x);
		v.push_back(vec.y);
		ar(cereal::make_nvp(key, v));
	}

}

#include "StringHelper.h"

#endif