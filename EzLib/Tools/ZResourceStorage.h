//===============================================================
//  @file ZResourceStorage.h
//   リソース管理関係
// 
//  @author 鎌田
//===============================================================
#ifndef ZResourceStorage_h
#define ZResourceStorage_h

#include <map>

namespace EzLib
{

	class ZTexture;
	class ZTextureSet;
	class ZGameModel;
	class ZSoundData;
	class ZVertexShader;
	class ZPixelShader;

	//===================================================================================
	//   リソース倉庫
	// 
	// 	ZDataWeakStorage<ZIResource>を継承(FlyWeightパターン的な感じ)						\n
	// 		※これはZIResourceクラスを継承したクラスインスタンスを作成・保管できるクラス。	\n
	// 		　内部ではmapで登録しているので、重複読み込みが防げる。							\n
	// 		※戻り値のデータはsharedポインタとして返るので、sharedポインタの特性上、		\n
	// 		　解放処理は書かなくてよい。持ち主がいなくなった時点で自動で解放される。		\n
	// 
	//  @ingroup Etc
	//===================================================================================
	class ZResourceStorage : public ZDataWeakStorage<ZIResource, ZLock>
	{
	public:
		//====================================================================================================
		//   テスクチャを読み込みそれを返す。既に登録済みなら読み込みは行わず、読み込み済みのものを返す。
		//  詳細な読み込みの設定は、変数m_LoadTextureInfoを使用してるので、この変数を変更すると読み込みの細かな設定を変更できます
		//  	filename	… ファイル名
		//  	forceLoad	… trueだと既に読み込まれているいても無条件で再読み込みする
		//  @return テクスチャ
		//====================================================================================================
		ZSP<ZTexture> LoadTexture(const ZString& filename, bool forceLoad = false);
		ZIMAGE_LOAD_INFO	m_LoadTextureInfo;		// LoadTexture関数で画像読み込み時の詳細設定　読み込みの細かな設定はこいつを変更すればOK

		//====================================================================================================
		//   テスクチャを読み込みそれを返す。既に登録済みなら読み込みは行わず、読み込み済みのものを返す。
		//  ZtextureSetバージョン
		//====================================================================================================
		ZSP<ZTextureSet> LoadTextureSet(const ZString& filename, bool forceLoad = false)
		{
			auto ts = Make_Shared(ZTextureSet,appnew);
			ts->LoadTextureSet(filename, forceLoad);
			return ts;
		}

		//====================================================================================================
		//   メッシュを読み込みそれを返す。既に登録済みなら読み込みは行わず、それを返す。
		// 	XファイルとXEDファイルに対応(スキンメッシュ対応)
		//  	filename	… ファイル名
		//  	bakeCurve	… 曲線補間系のアニメキーは、全て線形補間として変換・追加する(XED用 処理が軽くなります)
		//  	forceLoad	… trueだと既に読み込まれているいても無条件で再読み込みする
		//  @return ZGameModelモデル
		//====================================================================================================
		ZSP<ZGameModel> LoadMesh(const ZString& filename, bool bakeCurve = true, bool forceLoad = false);

		//====================================================================================================
		//   音声データのを読み込みそれを返す。既に登録済みなら読み込みは行わず、それを返す。
		//  	filename	… ファイル名
		//  	forceLoad	… trueだと既に読み込まれているいても無条件で再読み込みする
		//  @return サウンドデータ
		//====================================================================================================
		ZSP<ZSoundData> LoadSound(const ZString& filename, bool forceLoad = false);


		ZSP<ZPixelShader> LoadPixelShader(const ZString& filename);

		ZSP<ZVertexShader> LoadVertexShader(const ZString& filename);


		//====================================================================================================
		// 全リソース名を取得
		//====================================================================================================
		void DEBUG_GetNowResources(ZVector<ZString>& out)
		{
			auto& dataMap = GetDataMap();
			auto it = dataMap.begin();
			while (it != dataMap.end())
			{
				if ((*it).second.IsActive())
					out.push_back((*it).second.Lock()->GetTypeName());

				++it;
			}
		}

		// 
		ZResourceStorage();
	};

}

#endif
