#ifndef __ZIRESORCE_H__
#define __ZIRESORCE_H__

namespace EzLib
{

	//===============================================
	//   リソース基底クラス
	// 
	//  リソース系のクラスの基本となるクラス
	// 
	//  @ingroup Etc
	//===============================================
	class ZIResource : public ZEnable_Shared_From_This<ZIResource>
	{
	public:
		virtual ~ZIResource()
		{
		}

		//   リソースタイプ名取得
		//  @return リソースの型名が返る
		virtual ZString GetTypeName() const = 0;
	};

}

#endif