#include "MainFrame/ZMainFrame.h"

using namespace EzLib;

ZCamera ZCamera::LastCam;			// 最後に使用されたカメラ情報のコピーが入る(ZCameraクラスにより操作)

ZCamera::ZCamera()
{
	// 射影行列
	mProj.CreatePerspectiveFovLH(	Angle,			// 視野角
									Aspect,			// 画面のアスペクト比
									Near,			// 最近接距離
									Far);			// 最遠方距離
	ZCamera::LastCam.mProj = mProj;

	mOrtho.CreateOrthoLH(1280, 720, 0.01f, 100.f);
	ZCamera::LastCam.mProj = mOrtho;
}

//===============================================================================================
// カメラ行列mCamから、ビュー行列mViewを作成
//===============================================================================================
void ZCamera::CameraToView()
{
	// カメラ行列の逆行列を作成
	mCam.Inverse(mView);

	ZCamera::LastCam.mCam	= mCam;
	ZCamera::LastCam.mView	= mView;
}

//===============================================================================================
// ビュー行列mViewから、カメラ行列mCamを作成
//===============================================================================================
void ZCamera::ViewToCamera()
{
	// ビュー行列の逆行列を作成
	mView.Inverse(mCam);

	ZCamera::LastCam.mCam = mCam;
	ZCamera::LastCam.mView = mView;
}

// 直接指定Ver
void ZCamera::SetView(const ZMatrix &lpmView)
{
	mView = lpmView;

	// 逆にカメラ行列を作成
	mView.Inverse(mCam);

	ZCamera::LastCam.mCam = mCam;
	ZCamera::LastCam.mView = mView;
}

//===============================================================================================
// 射影行列設定
//===============================================================================================
void ZCamera::SetPerspectiveFovLH(float ViewAngle, float Aspect, float zNear, float zFar)
{
	mProj.CreatePerspectiveFovLH(	ViewAngle,	// 視野角
									Aspect,		// 画面のアスペクト比
									zNear,		// 最近接距離
									zFar);		// 最遠方距離
	
	ZCamera::LastCam.mProj	= mProj;
	this->Near		= zNear;
	this->Far		= zFar;
	this->Aspect	= Aspect;
	this->Angle		= ViewAngle;
}

// 正射影
void ZCamera::SetOrthoLH(float w, float h, float zNear, float zFar)
{
	mProj.CreateOrthoLH(w, h, zNear, zFar);
	ZCamera::LastCam.mOrtho	= mProj;
	Near			= zNear;
	Far				= zFar;
	this->Aspect	= -1;
}

// 直接指定Ver
void ZCamera::SetProj(const ZMatrix& lpmProj)
{
	mProj = lpmProj;
	ZCamera::LastCam.mProj = mProj;
}

void ZCamera::UpdateFrustumPlanes()
{
	ZMatrix viewProj = mView * mProj;

	// left plane
	FrustumPlanes[0].x = viewProj._14 + viewProj._11;
	FrustumPlanes[0].y = viewProj._24 + viewProj._21;
	FrustumPlanes[0].z = viewProj._34 + viewProj._31;
	FrustumPlanes[0].w = viewProj._44 + viewProj._41;


	// right plane
	FrustumPlanes[1].x = viewProj._14 - viewProj._11;
	FrustumPlanes[1].y = viewProj._24 - viewProj._21;
	FrustumPlanes[1].z = viewProj._34 - viewProj._31;
	FrustumPlanes[1].w = viewProj._44 - viewProj._41;

	// top plane
	FrustumPlanes[2].x = viewProj._14 - viewProj._12;
	FrustumPlanes[2].y = viewProj._24 - viewProj._22;
	FrustumPlanes[2].z = viewProj._34 - viewProj._32;
	FrustumPlanes[2].w = viewProj._44 - viewProj._42;


	// bottom plane
	FrustumPlanes[3].x = viewProj._14 + viewProj._12;
	FrustumPlanes[3].y = viewProj._24 + viewProj._22;
	FrustumPlanes[3].z = viewProj._34 + viewProj._32;
	FrustumPlanes[3].w = viewProj._44 + viewProj._42;


	// near plane
	FrustumPlanes[4].x = viewProj._13;
	FrustumPlanes[4].y = viewProj._23;
	FrustumPlanes[4].z = viewProj._33;
	FrustumPlanes[4].w = viewProj._43;

	// far plane
	FrustumPlanes[5].x = viewProj._14 - viewProj._13;
	FrustumPlanes[5].y = viewProj._24 - viewProj._23;
	FrustumPlanes[5].z = viewProj._34 - viewProj._33;
	FrustumPlanes[5].w = viewProj._44 - viewProj._43;

	// normalize
	for (int i = 0; i < 6; i++) FrustumPlanes[i].Normalize();

}

void ZCamera::Convert3Dto2D(ZVec3& lpvOut, const ZVec3& lpPos)const
{
	UINT Num = 1;
	D3D11_VIEWPORT vp;
	ZDx.GetDevContext()->RSGetViewports(&Num, &vp);

	ZMatrix mW;
	mW.CreateMove(lpPos);
	ZMatrix m = mW* mView* mProj;

	float halfW = vp.Width* 0.5f;
	float halfH = vp.Height* 0.5f;

	lpvOut.x = (m._41 / m._44) * halfW + halfW;
	lpvOut.y = (m._42 / m._44) * -halfH + halfH;
	lpvOut.z = m._44;
}

void ZCamera::Convert2Dto3D(ZVec3& lpvOut, const ZVec3& lpvPos)const
{
	UINT Num = 1;
	D3D11_VIEWPORT vp;
	ZDx.GetDevContext()->RSGetViewports(&Num, &vp);

	ZMatrix mW;
	lpvOut = DirectX::XMVector3Unproject(lpvPos, vp.TopLeftX, vp.TopLeftY, vp.Width, vp.Height, vp.MinDepth, vp.MaxDepth, mProj, mView, mW);
}

ZAABB ZCamera::GetAABB()const
{
	float width = (float)APP.m_Window->GetWidth();
	float height = (float)APP.m_Window->GetHeight();
	
	ZVec3 FrustumPos[8] =
	{
		{0,0,0},{0,width,0},
		{height,0,0},{height,width,0},
		{0,0,1},{0,width,1},
		{height,0,1},{height,width,1}
	};

	ZAABB aabb(ZVec3(FLT_MAX), ZVec3(FLT_MIN));
	// スクリーン座用 -> 3D座標
	for (auto& pos : FrustumPos)
	{
		Convert2Dto3D(pos, pos);
		for (int i = 0; i < 3; i++)
		{
			if (aabb.Min[i] > pos[i])
				aabb.Min[i] = pos[i];
			if (aabb.Max[i] < pos[i])
				aabb.Max[i] = pos[i];
		}
	}

	return std::move(aabb);
}

bool ZCamera::IsInsideTheFrustum(const ZAABB & aabb)
{	
	// 視錐台に収まり切らないオブジェクトはどうするかは未定
	
	ZMatrix viewProj = mView * mProj;

	auto min = aabb.Min;
	auto max = aabb.Max;
	
	bool isOutside = false;
	for (int i = 0; i < 6; i++)
	{
		// 視錐台平面の法線
		ZVec3 planeNormal = ZVec3(FrustumPlanes[i].x, FrustumPlanes[i].y, FrustumPlanes[i].z);
		float planeConstant = FrustumPlanes[i].w;
		
		// 視錐台平面の向いている方向から最も離れたAABBの頂点取得
		ZVec3 axisVert;
		for(int j = 0;j<3;j++)
		{
			if (FrustumPlanes[i][j] < 0)
				axisVert[j] = min[j];
			else
				axisVert[j] = max[j];
		}

		// 算出した頂点と視錐台の法線との内積で面の内側か判定
		// 6つの面のうちいずれかの面の外側にあるなら視錐台内にはない
		if (ZVec3::Dot(planeNormal, axisVert) + planeConstant < 0.0f)
		{
			isOutside = true;
			break;
		}
	}

	// 念の為AABBの中心点が視錐台内にあるか確認
	// あるならAABBの各点が視錐台内に収まらなかったものとしてtrueを返す
	if (isOutside)
	{
		ZVec3 aabbCenter = aabb.Min + (aabb.Max - aabb.Min) * 0.5f;
		for (int i = 0; i < 6; i++)
		{
			// 視錐台平面の法線
			ZVec3 planeNormal = ZVec3(FrustumPlanes[i].x, FrustumPlanes[i].y, FrustumPlanes[i].z);
			float planeConstant = FrustumPlanes[i].w;

			// AABBの中心点と視錐台の法線との内積で面の内側か判定
			// 6つの面のうちいずれかの面の外側にあるなら視錐台内にはない
			if (ZVec3::Dot(planeNormal, aabbCenter) + planeConstant < 0.0f)
				return false;
		}
	}

	return true;
}
