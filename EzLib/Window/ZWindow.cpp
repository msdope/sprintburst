#include "PCH/pch.h"
#include "ZWindow.h"

namespace EzLib
{
	std::unordered_map<void*, ZWindow*> ZWindow::m_sHandles;

	ZWindow::ZWindow(const char* wndTitle, const ZWindowProperties& properties)
		: m_WndTitle(wndTitle),m_Properties(properties),m_Closed(false), m_Initialized(false), m_WndHandle(NULL)
	{
		Init();
	}
	
	ZWindow::~ZWindow()
	{
		m_sHandles[m_WndHandle] = nullptr;
		m_sHandles.erase(m_WndHandle);
		
		UnregisterClass(m_WndTitle.c_str(), m_hInst);
		m_hInst = NULL;
	}

	void ZWindow::Update()
	{
		MSG msg;
		while (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE) > 0)
		{
			if (msg.message == WM_QUIT)
			{
				m_Closed = true;
				return;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		INPUT.UpdateKey();
		INPUT.UpdateMouseData(m_WndHandle);
	}

	bool ZWindow::IsClosed() const
	{
		return m_Closed;
	}

	void ZWindow::SetTitle(const char * wndTitle)
	{
		if (wndTitle == nullptr)
			return;

		m_WndTitle = wndTitle;
		SetWindowText(m_WndHandle, m_WndTitle.c_str());
	}

	void ZWindow::SetClientSize(uint32 width, uint32 height)
	{
		m_Properties.Width = width;
		m_Properties.Height = height;

		RECT rcWnd, rcCli;

		GetWindowRect(m_WndHandle, &rcWnd); // ウィンドウのRECT取得
		GetClientRect(m_WndHandle, &rcCli); // クライアント領域のRECT取得

		// ウィンドウの余白を考えて、クライアントのサイズを指定サイズにする。
		MoveWindow(m_WndHandle,
				   rcWnd.left,	// X座標
				   rcWnd.top,	// Y座標
				   width + (rcWnd.right - rcWnd.left) - (rcCli.right - rcCli.left),
				   height + (rcWnd.bottom - rcWnd.top) - (rcCli.bottom - rcCli.top),
				   TRUE);
	}

	bool ZWindow::Init()
	{
		m_hInst = m_Properties.hInst;
		WNDCLASSEX wc;	//ウィンドウクラス用構造体
		//ウィンドウクラスの定義
		{
			wc.cbSize = sizeof(WNDCLASSEX);							// 構造体のサイズ
			wc.style = 0;											// スタイル
			wc.lpfnWndProc = m_Properties.WindowProc;				// ウインドウ関数
			wc.cbClsExtra = 0;										// エキストラクラス情報 
			wc.cbWndExtra = 0;										// エキストラウィンドウ情報
			wc.hInstance = m_hInst;									// インスタンスハンドル
			wc.hIcon = NULL;										// ラージアイコン
			wc.hIconSm = NULL;										// スモールアイコン 
			wc.hCursor = LoadCursor(NULL, IDC_ARROW);				// マウスカーソル
			wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);	// 背景色(黒)
			wc.lpszMenuName = NULL;									// メインメニュー名
			wc.lpszClassName = m_WndTitle.c_str();					// ウィンドウクラス名
		}

		//ウィンドウクラスの登録
		if (!RegisterClassEx(&wc))
		{
			MessageBox(nullptr, "Faild : RegisterClass", "Error", MB_OK);
			return false;
		}

		// ウィンドウ作成
		UINT WindowStyle = WS_OVERLAPPEDWINDOW - WS_MAXIMIZEBOX - WS_THICKFRAME;

		m_WndHandle = CreateWindow
		(
			m_WndTitle.c_str(),		// 登録されているクラス名
			m_WndTitle.c_str(),		// ウィンドウタイトル
			WindowStyle,			// ウィンドウスタイル
			0,						// ウィンドウの横方向の位置
			0,						// ウィンドウの縦方向の位置
			m_Properties.Width,		// ウィンドウの幅
			m_Properties.Height,	// ウィンドウの高さ
			NULL,					// 親ウィンドウまたはオーナーウィンドウのハンドル
			NULL,					// メニューハンドルまたは子ウィンドウID
			m_hInst,				// アプリケーションインスタンスのハンドル
			NULL					// ウィンドウ作成データ
		);

		RegisterWindowClass(m_WndHandle, this);

		// クライアントのサイズを設定(一応)
		SetClientSize(m_Properties.Width, m_Properties.Height);
		
		// ウィンドウ表示
		ShowWindow(m_WndHandle, SW_SHOW);

		m_Initialized = true;
		return true;
	}

	void ZWindow::RegisterWindowClass(void * handle, ZWindow * window)
	{
		m_sHandles[handle] = window;
	}

	ZWindow* ZWindow::GetWindowClass(void * handle)
	{
		if (handle == NULL)
			return nullptr;

		try
		{
			return m_sHandles.at(handle);
		}
		catch(std::out_of_range&)
		{
			return nullptr;
		}

		return nullptr;
	}

}
