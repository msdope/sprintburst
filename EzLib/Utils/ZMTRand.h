//===============================================================
//  @file ZMTRand.h
//   乱数クラス
// 
//  @author 鎌田
//===============================================================

#ifndef ZMTRand_h
#define ZMTRand_h

namespace EzLib
{

	//=================================================================
	// 
	//   メルセンヌ･ツイスター乱数クラス
	// 
	//  @ingroup Etc
	//=================================================================
	class ZRand
	{
	public:
		//   乱数シードを設定する(つまりシャッフル)
		//  	seed	… 乱数シード
		void InitSeed(ULONG seed)
		{
			m_Rnd.seed(seed);
		}

		//   整数の乱数取得
		UINT GetInt() { return m_Rnd(); }

		//   整数の乱数取得
		//  	num	… 0 〜 num-1 までの乱数になる
		int GetInt(int num)
		{
			return m_Rnd() % num;
		}

		//    st 〜 ed間の整数乱数を発生
		//  	st	… 最少値
		//  	ed	… 最大値
		int GetInt(int st, int ed)
		{
			if (ed < st)std::swap(ed, st);
			return (int)GetInt(ed - st + 1) + st;
		}

		//   st 〜 ed間の浮動小数点乱数を発生
		//  	st	… 最少値
		//  	ed	… 最大値
		float GetFloat(float st, float ed)
		{
			if (ed < st)std::swap(ed, st);
			return (GetInt() / 4294967295.0f)*(ed - st) + st;
		}

		float GetFloat()
		{
			return (GetInt() / 4294967295.0f);
		}

		//
		ZRand() {}
		ZRand(ULONG seed) : m_Rnd(seed)
		{
		}

	private:
		std::mt19937	m_Rnd;		// メルセンヌ・ツイスター乱数
	};

}

#endif
