#ifndef __DEBUG_LOG_H__
#define __DEBUG_LOG_H__

#include "PCH/pch.h"

namespace EzLib
{
	
	inline static void DebugLog(const char* fmt, ...)
	{
		va_list argList;

		va_start(argList, fmt);

		constexpr size_t MAX_CHARS = 1024;
		static char strBuf[MAX_CHARS];
		int charsWritten = vsnprintf(strBuf, MAX_CHARS, fmt, argList);

		OutputDebugString(strBuf);

		va_end(argList);
	}

	template <typename ... Args>
	inline static void DebugLog_Line(const char* fmt, Args const& ...args)
	{
		std::string s = fmt;
		if (*s.rbegin() != '\n')
			s += '\n';

		DebugLog(s.c_str(), args ...);
	}

}


#endif