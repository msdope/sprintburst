#ifndef __ZTIMER_H__
#define __ZTIMER_H__

namespace EzLib
{
	class ZTimer
	{
	public:
		ZTimer(std::chrono::duration<double>& dulationTime)
			: m_DulationTime(dulationTime), m_Stoped(true)
		{
		}

		inline void Start()
		{
			if (m_Stoped == false)
				return;

			m_DulationTime = std::chrono::microseconds(0);
			m_StartTime = std::chrono::high_resolution_clock::now();
			m_Stoped = false;
		}

		inline void Stop()
		{
			if (m_Stoped)
				return;
			m_EndTime = std::chrono::high_resolution_clock::now();
			m_DulationTime = m_EndTime - m_StartTime;
			m_Stoped = true;
		}

	private:
		std::chrono::time_point<std::chrono::high_resolution_clock> m_StartTime;
		std::chrono::time_point<std::chrono::high_resolution_clock> m_EndTime;
		std::chrono::duration<double>& m_DulationTime;
		bool m_Stoped;
	};

}

#endif