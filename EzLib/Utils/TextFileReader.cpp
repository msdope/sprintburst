#include "TextFileReader.h"

namespace EzLib
{
	TextFileReader::TextFileReader(const char* path)
	{
		Open(path);
	}

	TextFileReader::TextFileReader(const ZString& path)
	{
		Open(path);
	}

	bool TextFileReader::Open(const char* path)
	{
		return m_File.Open(path, ZFile::OPEN_MODE::READ);
	}

	bool TextFileReader::Open(const ZString& path)
	{
		return m_File.Open(path, ZFile::OPEN_MODE::READ);
	}

	void TextFileReader::Close()
	{
		m_File.Close();
	}

	bool TextFileReader::IsOpen()
	{
		return m_File.IsOpen();
	}

	ZString TextFileReader::ReadLine()
	{
		if (IsOpen() == false || IsEOF())
			return ZString();

		ZString line;
		auto it = std::back_inserter(line);
		int ch = fgetc(m_File.GetFilePtr());
		
		while(ch != EOF && ch != '\r' && ch != '\n')
		{
			line.push_back(ch);
			ch = fgetc(m_File.GetFilePtr());
		}

		if(ch != EOF)
		{
			if(ch == '\r')
			{
				ch = fgetc(m_File.GetFilePtr());
				if (ch != EOF && ch != '\n')
					ungetc(ch, m_File.GetFilePtr());
			}
			else
			{
				ch = fgetc(m_File.GetFilePtr());
				if (ch != EOF)
					ungetc(ch, m_File.GetFilePtr());
			}
		}

		return line;
	}

	void TextFileReader::ReadAllLine(ZVector<ZString>& lines)
	{
		lines.clear();
		if (IsOpen() == false || IsEOF())
			return;

		while (IsEOF() == false)
			lines.emplace_back(ReadLine());

	}

	ZString TextFileReader::ReadAll()
	{
		ZString s;

		if(m_File.IsOpen())
		{
			int ch = fgetc(m_File.GetFilePtr());

			while(ch != EOF)
			{
				s.push_back(ch);
				ch = fgetc(m_File.GetFilePtr());
			}
		}

		return s;
	}

	bool TextFileReader::IsEOF()
	{
		if(m_File.IsOpen() == false)
			return false;
		return m_File.IsEOF();
	}

}
