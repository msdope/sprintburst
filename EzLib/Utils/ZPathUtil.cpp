#include "PCH/pch.h"

namespace EzLib
{

namespace
{
	const char PathDelimiter = '\\';
	const char* PathDelimiters = "\\/";
}

	ZString ZPathUtil::GetCurrentDir()
	{
		ZString cDir;
		DWORD sz = GetCurrentDirectory(0, nullptr);
		cDir.resize(sz);
		GetCurrentDirectory(sz, &cDir[0]);
		return std::move(cDir);
	}

	ZString ZPathUtil::GetExecutablePath()
	{
		ZString modulePath;
		modulePath.resize(MAX_PATH);
		if (GetModuleFileName(NULL,&modulePath[0], (DWORD)modulePath.size()) == 0)
			return std::move(ZString());
	
		return std::move(modulePath);
	}

	ZString ZPathUtil::Combine(const std::vector<ZString>& parts)
	{
		ZString result;

		for(const auto& part : parts)
		{
			if(part.empty() == false)
			{
				auto pos = part.find_last_not_of(PathDelimiters);
				if (pos != ZString::npos)
				{
					if (result.empty() == false)
						result.append(&PathDelimiter, 1);
					result.append(part.c_str(), pos + 1);
				}
			}
		}

		return std::move(result);
	}

	ZString ZPathUtil::Combine(const ZString& a, const ZString& b)
	{
		return std::move(Combine({a,b}));
	}

	ZString ZPathUtil::GetDirName(const ZString& path)
	{
		auto pos = path.find_last_of(PathDelimiters);
		if(pos == ZString::npos)
			return std::move(ZString());

		return std::move(path.substr(0, pos + 1));
	}

	ZString ZPathUtil::GetFileName(const ZString& path)
	{
		auto pos = path.find_last_of(PathDelimiters);
		if (pos == ZString::npos)
			return std::move(path);

		return std::move(path.substr(pos + 1,path.size()-pos));
	}

	ZString ZPathUtil::GetFileName_NonExt(const ZString& path)
	{
		const ZString fname = GetFileName(path);
		auto pos = fname.find_last_of('.');
		if (pos == ZString::npos)
			return std::move(fname);

		return std::move(fname.substr(0,pos));
	}

	ZString ZPathUtil::GetExt(const ZString& path)
	{
		auto pos = path.find_last_of('.');
		if (pos == ZString::npos)
			return std::move(ZString());

		ZString ext = path.substr(pos + 1, path.size() - pos);
		for (auto& c : ext)
			c = (char)tolower(c);

		return std::move(ext);
	}

	ZString ZPathUtil::GetDelimiter()
	{
		return std::move(ZString("\\"));
	}

	ZString ZPathUtil::Normalize(const ZString& path)
	{
		ZString result = path;
		std::replace(result.begin(), result.end(), '/', '\\');

		return std::move(result);
	}


}
