#include "PCH/pch.h"

namespace EzLib
{
	ZThreadPool::ZThreadPool()
		: m_IsInitialized(false), m_IsTerminationRequested(false)
	{
	}

	ZThreadPool::ZThreadPool(size_t threadCount)
		: m_IsInitialized(false), m_IsTerminationRequested(false)
	{
		Init(threadCount);
	}

	ZThreadPool::~ZThreadPool()
	{
		Release();
	}

	void ZThreadPool::Init(size_t threadCount)
	{
		if (m_IsInitialized)
			Release();

		m_TotalTasks = 0;

		if (threadCount <= 0)
			threadCount = std::thread::hardware_concurrency() - 1;
		else if (threadCount > std::thread::hardware_concurrency()) // コア数よりスレッド数が多いとあまり良くない
			DebugLog_Line("thread has exceeded more than maximum core count");

		for (size_t i = 0; i < threadCount; i++)
			m_Threads.emplace_back(std::bind(&ZThreadPool::ThreadLoop,this));
		m_IsInitialized = true;
	}

	void ZThreadPool::Release()
	{
		if (m_IsInitialized == false || m_Threads.empty())
			return;

		{
			std::unique_lock<std::mutex> ul(m_Mtx);
			m_IsTerminationRequested = true;
			m_CV.notify_all();
		}

		for (auto& thread : m_Threads)
			thread.join();
		m_Threads.clear();
	}

	void ZThreadPool::ThreadLoop()
	{
		while (true)
		{
			std::unique_lock<std::mutex> ul(m_Mtx);

			m_CV.wait(ul,
				[this]
			{
				return m_IsTerminationRequested || !m_Tasks.empty();
			});

			if (m_Tasks.empty() == false)
			{
				m_TotalTasks++;
				Task task = std::move(m_Tasks.front());
				m_Tasks.pop();
				ul.unlock();
				task();
				ul.lock();
				m_TotalTasks--;
				m_CVWait.notify_one();
			}
			else if (m_IsTerminationRequested)
				return;
			
		}
	}

	void ZThreadPool::AddTask(uint32 taskCount, uint32 groupSize, const std::function<void(ZThreadTaskData)>& task)
	{
		if (taskCount == 0 || groupSize == 0)
			return;

		// タスク数がグループ数より小さい場合はタスク数とグループ数を同じ値に
		if (groupSize < taskCount) groupSize = taskCount;

		const uint32 groupCount = (taskCount + groupSize - 1) / groupSize;

		for (uint32 groupIndex = 0; groupIndex < groupCount; groupIndex++)
		{
			const auto& taskGroup =
				[task,taskCount, groupSize, groupIndex]()
			{
				const uint32 groupTaskOffset = groupIndex * groupSize;
				const uint32 groupTaskEnd = min(groupTaskOffset + groupSize, taskCount);
				
				ZThreadTaskData data;
				data.GroupIndex = groupIndex;
				
				for (uint32 i = groupTaskOffset; i < groupTaskEnd; i++)
				{
					data.TaskIndex = i;
					task(data);
				}
			};

			{
				std::unique_lock<std::mutex> ul(m_Mtx);
				m_Tasks.push(taskGroup);
			}
			// スレッド起動
			m_CV.notify_one();
		}

	}

	void ZThreadPool::WaitForAllTasksFinish()
	{
		std::unique_lock<std::mutex> ul(m_Mtx);
		m_CVWait.wait(ul,
			[this]
		{
			return m_Tasks.empty() && m_TotalTasks == 0;
		});
	}

}