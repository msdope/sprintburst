#pragma once

/*========================================================================*/
//	Structure
/*========================================================================*/

	//	ポストエフェクトのフラグ(使用するエフェクトだけtrueに)
struct EffectFlgs
{
	bool LightBloom		= false;
	bool DepthOfField	= false;
	bool XRay			= false;
public:
	void AllTrue()
	{
		LightBloom		= true;
		DepthOfField	= true; 
		XRay			= true;
	}
	void AllFalse()
	{
		LightBloom		= false;
		DepthOfField	= false;
		XRay			= false;
	}

	template<class Archive>
	void serialize(Archive & archive)
	{
		archive(cereal::make_nvp("LightBloom", LightBloom));
		archive(cereal::make_nvp("DoF", DepthOfField));
		archive(cereal::make_nvp("XRay", XRay));
	}

};

//	ブルーム描画時のステート
struct BloomState
{
public:
	int		Loop;
	float	Disp;
	int		MipLoop = 4;
	std::vector<float> AddPow;	//	Jsonで配列として書き出したいのでvecotr
//	float	AddPow[BlurRenderCnt];
public:
	BloomState()
	{
		AddPow.resize(BlurRenderCnt);
		for (int i = 0; i < BlurRenderCnt; i++) {
			AddPow[i] = 2.f;
		}
	}

	template<class Archive>
	void serialize(Archive & archive)
	{
		archive(cereal::make_nvp("Loop", Loop));
		archive(cereal::make_nvp("Disp", Disp));
		archive(cereal::make_nvp("MipLoop", MipLoop));

		archive(cereal::make_nvp("BlurRenderCnt", BlurRenderCnt));
		archive(cereal::make_nvp("BlurPow", AddPow));

	}
};

class PostEffect
{
public:

	/*========================================================================*/
	// Functions
	/*========================================================================*/
	PostEffect() {};
	~PostEffect() { Release(); }

	void Init();
	void Release();

	//	ポストエフェクトを実行する(実行前に必要テクスチャのセットと実行ポストエフェクトを設定する必要がある)
	void Execute();

	void SetUseEffectList(EffectFlgs flg) { m_EffectFlg = flg; }

	//	使用するテクスチャのセット
	//void SetDOFTexture(ZSPZTexture> tex)			{ m_BaseTextures["DOF"]			= tex; }
	//void SetEmissiveTexture(ZSPZTexture> tex)		{ m_BaseTextures["Emissive"]	= tex; }
	void SetBaseTexture(ZSP<ZTexture> tex)			{ m_BaseTextures["Base"]		= tex; }
	void SetDepthTexture(ZSP<ZTexture> tex)			{ m_BaseTextures["Depth"]		= tex; }
	void SetCharaDepthTexture(ZSP<ZTexture> tex)	{ m_BaseTextures["CharaDepth"]	= tex; }
	void SetReflectTexture(ZSP<ZTexture> tex)		{ m_BaseTextures["CharaReflect"]= tex; }

	//	ライトブルームの設定
	void SetBloomState(int loop, float disp)
	{
		m_BloomState.Loop = loop;
		m_BloomState.Disp = disp;
	}

	void ImGui();


	bool LoadState(const std::string& file);
	void SaveState(const std::string& file);
private:
	
	void PresentToBackBuffer(ZSP<ZTexture> tex);

	//	ベースの画像を描画
	void BackBufferToGameScene();
	void DepthOfFieldToGameScene();

	void LightBloom();
	void XRay();

	//	3D描画レンダーターゲット群を取得してくる
	void GetRendertargets();

private:
	/*========================================================================*/
	//	Variable
	/*========================================================================*/
	
	//	使用するエフェクトを示すフラグ
	EffectFlgs						m_EffectFlg;
	//	エフェクトをかける前のテクスチャ
	ZAMap<ZAString, ZSP<ZTexture>>		m_BaseTextures;

	//	レンダーターゲット
	ZRenderTargets	m_RT;
	ZRenderTargets	m_SaveRT;

	//	エミッシブ用ステート
	BloomState		m_BloomState;
	
	//	被写界深度用ステート
	DOF_Shader		m_DOF;

	//	キャラ透視用
	X_Ray			m_XRay;

	//	ゲームの完成画面(ここにポストエフェクトをかけた画面を作り、それをバックバッファに送る)
	ZSP<ZTexture>	m_CreateGameScene;
	ZVec2 m_GameSceneSize;

};