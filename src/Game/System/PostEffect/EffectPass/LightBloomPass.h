#ifndef __LIGHT_BLOOM_PASS_H__
#define __LIGHT_BLOOM_PASS_H__

class LightBloomPass : public ZPostEffectPass
{
public:
	struct BloomState
	{
	public:
		BloomState();

		template<class Archive>
		void serialize(Archive & archive)
		{
			archive(cereal::make_nvp("Loop", Loop));
			archive(cereal::make_nvp("Disp", Disp));
			archive(cereal::make_nvp("MipLoop", MipLoop));

			archive(cereal::make_nvp("BlurRenderCnt", BlurShader::BlurRenderCnt));
			archive(cereal::make_nvp("BlurPow", AddPow));
		}

	public:
		int Loop;
		float Disp;
		int MipLoop;
		std::vector<float> AddPow;//	Jsonで配列として書き出したいのでvector
	};

public:
	LightBloomPass();
	void Init();
	void Release();
	void ImGui()override;

	void LoadSettingFromJson(const json11::Json& jsonObj)override;
	void SaveSettingToJson(cereal::JSONOutputArchive& archive)override;

	ZString GetPassName()override;
	void RenderPass(ZSP<ZTexture> source);

private:
	BloomState m_BloomState;
	ZRenderTargets m_RT;
	ZSP<ZTexture> m_TexEmissive;
};


#endif