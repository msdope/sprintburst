#include "Game/Application.h"

XRayPass::XRayPass() : ZPostEffectPass()
{
}

void XRayPass::Init()
{
	m_XRay.Init("Shader/X-Ray_VS.cso", "Shader/X-Ray_PS.cso");
	m_TexXRayResult = Make_Shared(ZTexture,sysnew);
	m_TexXRayResult->CreateRT(ZDx.GetRezoW(), ZDx.GetRezoH());
}

void XRayPass::Release()
{
	m_XRay.Release();
}

void XRayPass::ImGui()
{
#if _DEBUG
	auto setting = [this]()
	{
		if (ImGui::Begin(ZPostEffects::PostEffectWindowName) == false)
		{
			ImGui::End();
			return;
		}
		
		if (ImGui::CollapsingHeader("X-RayParams"))
			ImGui::ColorEdit4("Diffuse", m_XRay.GetXColor(), ImGuiColorEditFlags_AlphaBar);

		ImGui::End();
	};

	DW_IMGUI_FUNC(setting);
#endif
}

void XRayPass::LoadSettingFromJson(const json11::Json & jsonObj)
{
	// 有効フラグ
	{
		auto item = jsonObj["Flgs"].object_items();
		m_IsEnable = item["XRay"].bool_value();
	}

#if _DEBUG
	// XRayパラメータ
	{
		auto item = jsonObj["XRayState"].object_items();
		m_XRay.GetXColor().Set(item["Color"]);
	}
#endif
}

void XRayPass::SaveSettingToJson(cereal::JSONOutputArchive & archive)
{
	archive(cereal::make_nvp("XRayState", m_XRay));
}

ZString XRayPass::GetPassName()
{
	return "X-Ray Pass";
}

void XRayPass::RenderPass(ZSP<ZTexture> source)
{
	if (m_pPostEffects == nullptr) return;
	if (m_IsEnable == false)return;

	auto texCompleted = m_pPostEffects->GetCompletedTexture();
	auto texCharaDepth = m_pPostEffects->GetSourceTexture("CharaDepth");
	if (texCharaDepth == nullptr)return;
	auto texDepth = m_pPostEffects->GetSourceTexture("Depth");
	if (texDepth == nullptr)return;
	auto texCharaReflect = m_pPostEffects->GetSourceTexture("CharaReflect");
	if (texCharaReflect == nullptr)return;

	auto info = m_TexXRayResult->GetInfo();
	ZVec2 size = ZVec2(info.Width, info.Height);
	m_XRay.Render(texCompleted, texCharaDepth, texDepth, texCharaReflect, m_TexXRayResult, size);

	// XRayの結果を完成イメージにコピー
	m_pPostEffects->PresentToCompletedTexture(m_TexXRayResult);
}
