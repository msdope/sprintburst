#ifndef __DOF_PASS_H__
#define __DOF_PASS_H__

class DOFPass : public ZPostEffectPass
{
public:
	DOFPass();

	void Init();
	void Release();

	void ImGui()override;

	void LoadSettingFromJson(const json11::Json& jsonObj)override;
	void SaveSettingToJson(cereal::JSONOutputArchive& archive)override;

	ZString GetPassName()override;
	void RenderPass(ZSP<ZTexture> sourcs);

private:
	DOF_Shader m_DOF;
	ZSP<ZTexture> m_TexDOF;

};

#endif