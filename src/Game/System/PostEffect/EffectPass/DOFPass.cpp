#include "Game/Application.h"

DOFPass::DOFPass() : ZPostEffectPass()
{
}

void DOFPass::Init()
{
	m_DOF.Init("Shader/DOF_VS.cso", "Shader/DOF_PS.cso");

	m_TexDOF = Make_Shared(ZTexture, sysnew);
	m_TexDOF->CreateRT(ZDx.GetRezoW() / 2, ZDx.GetRezoH() / 2, DXGI_FORMAT_R16G16_UNORM);
}

void DOFPass::Release()
{
	m_DOF.Release();
}

void DOFPass::ImGui()
{
	auto setting = [this]()
	{
		if(ImGui::Begin(ZPostEffects::PostEffectWindowName) == false)
		{
			ImGui::End();
			return;
		}

		if (ImGui::CollapsingHeader("DoFParams"))
		{
			ImGui::DragFloat("FarRange", &RENDERER.m_cb9_PerMaterial.m_Data.FarState.x, 0.01f, 0.f, 1.f);
			ImGui::DragFloat("FarPow", &RENDERER.m_cb9_PerMaterial.m_Data.FarState.y, 0.1f, 0.f, 100.f);
			ImGui::DragFloat("NearRange", &RENDERER.m_cb9_PerMaterial.m_Data.NearState.x, 0.01f, 0.f, 1.f);
			ImGui::DragFloat("NearPow", &RENDERER.m_cb9_PerMaterial.m_Data.NearState.y, 0.1f, 0.f, 100.f);
			ImGui::DragInt("UseFarBlur", &RENDERER.m_cb9_PerMaterial.m_Data.UseFarBlur, 1, 0, 1);
		}

		ImGui::End();
	};

	DW_IMGUI_FUNC(setting);

	auto textureDebug = [this]()
	{
		auto dockID = ImGui::GetID("PostEffectTextures_Tab");
		ImGui::SetNextWindowDockId(dockID);
		if (ImGui::Begin("DOF"))
		{
			if (m_TexDOF != nullptr)
				ImGui::Image(m_TexDOF->GetTex(), ImGui::GetWindowSize());
		}
		ImGui::End();
	};

	DW_IMGUI_FUNC(textureDebug);
}

void DOFPass::LoadSettingFromJson(const json11::Json& jsonObj)
{
	// 有効フラグ
	{
		auto item = jsonObj["Flgs"].object_items();
		m_IsEnable = item["DoF"].bool_value();
	}

	// DoFステータス
	{
		auto item = jsonObj["DoFState"].object_items();
		RENDERER.m_cb9_PerMaterial.m_Data.FarState.Set(item["Far"]);
		RENDERER.m_cb9_PerMaterial.m_Data.NearState.Set(item["Near"]);
		RENDERER.m_cb9_PerMaterial.m_Data.UseFarBlur = item["UseFar"].int_value();
	}
}

void DOFPass::SaveSettingToJson(cereal::JSONOutputArchive& archive)
{
	archive(cereal::make_nvp("DoFState", RENDERER.m_cb9_PerMaterial.m_Data));
}

ZString DOFPass::GetPassName()
{
	return "DoF Pass";
}

void DOFPass::RenderPass(ZSP<ZTexture> source)
{
	if (m_IsEnable == false) return;
	if (m_pPostEffects == nullptr) return;
	
	// 深度テクスチャ取得
	auto depth = m_pPostEffects->GetSourceTexture("Depth");
	if (depth == nullptr) return;

	// テクスチャクリア
	m_TexDOF->ClearRT(ZVec4(0));
	
	// DoFMap作成
	{
		auto texInfo = m_TexDOF->GetInfo();
		ZVec2 texSize((float)texInfo.Width, (float)texInfo.Height);
		m_DOF.GenerateDOFMap(depth, m_TexDOF, texSize);
	}

	// 被写界深度ブレンド
	{
		// Sourceの絵をぼかす
		ShMgr.m_Blur.ChangeRenderTarget(BlurShader::BlurRenderFlgs::Quarter);
		ShMgr.m_Blur.GenerateBlur(source, 2, 5.f);

		// ブレンド
		auto texCompleted = m_pPostEffects->GetCompletedTexture();
		auto texInfo = texCompleted->GetInfo();
		ZVec2 texSize((float)texInfo.Width, (float)texInfo.Height);
		m_DOF.Render(source, ShMgr.m_Blur.GetTexture(), m_TexDOF,texCompleted,texSize);

		// ブラーターゲットサイズを戻す
		ShMgr.m_Blur.ChangeRenderTarget();
	}
}
