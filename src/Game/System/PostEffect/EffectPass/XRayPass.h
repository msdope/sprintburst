#ifndef __XRAY_PASS_H__
#define __XRAY_PASS_H__

class XRayPass : public ZPostEffectPass
{
public:
	XRayPass();

	void Init();
	void Release();

	void ImGui()override;
	void LoadSettingFromJson(const json11::Json& jsonObj)override;
	void SaveSettingToJson(cereal::JSONOutputArchive& archive)override;

	ZString GetPassName()override;
	void RenderPass(ZSP<ZTexture> source);

private:
	X_Ray m_XRay;
	ZSP<ZTexture> m_TexXRayResult;
};

#endif