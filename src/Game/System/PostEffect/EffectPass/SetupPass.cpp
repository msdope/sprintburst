#include "Game/Application.h"

SetupPass::SetupPass() : ZPostEffectPass()
{
}

ZString SetupPass::GetPassName()
{
	return "SetUp Pass";
}

void SetupPass::RenderPass(ZSP<ZTexture> sourcs)
{
	if (m_pPostEffects == nullptr)return;

	ZRenderTarget_BackUpper bu;

	ZRenderTargets rt;
	rt.GetNowTop();
	rt.Depth(nullptr);
	rt.RT(0, m_pPostEffects->GetCompletedTexture()->GetRTTex());
	rt.SetToDevice();
	
	ShMgr.m_Ss.Begin(false, true);
	auto info = ZDx.GetBackBuffer()->GetInfo();
	ShMgr.m_Ss.Draw2D(sourcs->GetTex(), 0, 0, info.Width,info.Height);
	ShMgr.m_Ss.End();

	// 各種テクスチャセット
	m_pPostEffects->SetTextureResource("Depth", RENDERER.m_RenderTargets->GetTexZSP(1));
	m_pPostEffects->SetTextureResource("CharaDepth", RENDERER.m_RenderTargets->GetTexZSP(2));
	m_pPostEffects->SetTextureResource("CharaReflect", RENDERER.m_RenderTargets->GetTexZSP(3));
}
