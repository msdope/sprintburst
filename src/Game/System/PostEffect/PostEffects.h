#ifndef __POSTEFFECTS_H__
#define __POSTEFFECTS_H__

#include "PCH/pch.h"

class PostEffects : public ZPostEffects
{
public:
	PostEffects();
	
	void ImGui()override;

	void PresentToCompletedTexture(ZSP<ZTexture> source);

protected:
	void SetBlendState();
	void Present(); // 完成イメージをバックバッファへ

private:
	ZDepthStencilState	m_DSBackUp;
	ZBlendState			m_BSBackUp;
	ZDepthStencilState	m_DSState;
	ZBlendState			m_BState;

};


#endif
