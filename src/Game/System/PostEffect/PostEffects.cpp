#include "Game/Application.h"

PostEffects::PostEffects()
{
	// Z書き込み、判定無効化
	m_DSState.Set_ZEnable(false);
	m_DSState.Set_ZWriteEnable(false);
	
	// ブレンドステート
	m_BState.Set_Alpha();
}

void PostEffects::ImGui()
{
	auto setting = [this]()
	{
		if (ImGui::Begin(ZPostEffects::PostEffectWindowName))
		{
			for (auto& pass : m_PassMap)
			{
				bool enable = pass.second->IsEnable();
				ZString label = "Use " + pass.second->GetPassName();
				ImGui::Checkbox(label.c_str(), &enable);
				pass.second->SetEnable(enable);
			}
		}
		ImGui::End();
	};

	DW_IMGUI_FUNC(setting);

	auto textureDebug = [this]()
	{
		auto dockID = ImGui::GetID("PostEffectTextures_Tab");
		if (ImGui::Begin("PostEffectTextures"))
			ImGui::DockSpace(dockID);
		ImGui::End();
	};

	DW_IMGUI_FUNC(textureDebug);

	// 各種ポストエフェクトパスのImGui
	if (m_Passes.empty() == false)
	{
		for (auto& pass : m_Passes) pass->ImGui();
	}

}

void PostEffects::SetBlendState()
{
	//	レンダリングステート設定
	{
		// 現状の各種ステート記憶
		m_DSBackUp.SetAll_GetState();
		m_BSBackUp.SetAll_GetState();

		m_DSState.Set_FromDesc(m_DSBackUp.GetDesc());

		// Z書き込み、判定無効化
		m_DSState.SetState();

		// ブレンドステート
		m_BState.SetState();
	}
}

void PostEffects::PresentToCompletedTexture(ZSP<ZTexture> source)
{
	ZRenderTargets rt;
	rt.GetNowTop();
	rt.RT(0, m_TexCompleted->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	SetBlendState();

	ShMgr.m_Ss.Begin(false, true);
	auto info = source->GetInfo();
	ShMgr.m_Ss.Draw2D(source->GetTex(), 0, 0, info.Width, info.Height);
	ShMgr.m_Ss.End();
	
	//	ステート戻す
	m_BSBackUp.SetState();
	m_DSBackUp.SetState();
}

void PostEffects::Present()
{
	m_RTSave.SetToDevice();

	SetBlendState();

	ZRenderTargets rt;
	rt.GetNowTop();
	rt.RT(0, ZDx.GetBackBuffer()->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	ShMgr.m_Ss.Begin(false, true);
	ShMgr.m_Ss.Draw2D(m_TexCompleted->GetTex(),0, 0,
		(float)APP.m_Window->GetWidth(), (float)APP.m_Window->GetHeight());
	ShMgr.m_Ss.End();

	//	ステート戻す
	m_BSBackUp.SetState();
	m_DSBackUp.SetState();
}
