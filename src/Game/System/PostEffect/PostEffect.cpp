#include "Game/Application.h"


void PostEffect::Init()
{
	m_EffectFlg.AllTrue();

	//	エミッシブステート初期化
	m_BloomState.Loop = 1;
	m_BloomState.Disp = 8.f;

	//	ポストエフェクト用シェーダ読み込み
	m_DOF.Init("Shader/DOF_VS.cso", "Shader/DOF_PS.cso");
	m_XRay.Init("Shader/X-Ray_VS.cso", "Shader/X-Ray_PS.cso");
	
	//	ポストエフェクトテクスチャ作成
	m_BaseTextures["DOF"] = Make_Shared(ZTexture,appnew);
	m_BaseTextures["DOF"]->CreateRT(640, 360, DXGI_FORMAT_R16G16_UNORM);


	m_BaseTextures["Emissive"] = Make_Shared(ZTexture, appnew);
	m_BaseTextures["Emissive"]->CreateRT(640, 360, DXGI_FORMAT_R11G11B10_FLOAT);

	//	作成画面
	m_CreateGameScene = Make_Shared(ZTexture, appnew);
	m_CreateGameScene->CreateRT(ZDx.GetRezoW(), ZDx.GetRezoH());

}

void PostEffect::Release()
{
	m_DOF.Release();
	m_XRay.Release();

	if(m_CreateGameScene)m_CreateGameScene->Release();

	//	ポストエフェクトテクスチャ解放
	if (m_BaseTextures["DOF"])m_BaseTextures["DOF"]->Release();
	if (m_BaseTextures["Emissive"])m_BaseTextures["Emissive"]->Release();
}

void PostEffect::Execute()
{

	GetRendertargets();
	m_CreateGameScene->ClearRT(ZVec4(0, 0, 0, 0));

	//	現在ターゲット情報を取得
	m_RT.GetNowTop();
	m_SaveRT.GetNowAll();


#if _DEBUG
	ShMgr.m_Blur.SaveRTBegin();
#endif


	//	被写界深度:デフォルトのバックバッファに描画する
	if (m_EffectFlg.DepthOfField)
	{
		DepthOfFieldToGameScene();
	}
	else
	{
		PresentToBackBuffer(m_BaseTextures["Base"]);
	}

	//	キャラ透視:デフォルトのバックバッファを使ってゲームシーンに描画する
	if (m_EffectFlg.XRay)
	{
		XRay();
	}
	else
	{
		BackBufferToGameScene();
	}

	//	ライトブルーム
	if (m_EffectFlg.LightBloom)
	{
		LightBloom();
	}


#if _DEBUG
	ShMgr.m_Blur.SaveRTEnd(); 
#endif

	//	完成画面をバックバッファに送る
	PresentToBackBuffer(m_CreateGameScene);

}

void PostEffect::PresentToBackBuffer(ZSP<ZTexture> tex)
{
	m_SaveRT.SetToDevice();

	ShMgr.m_Ss.Begin(false, true);
	ShMgr.m_Ss.Draw2D(tex->GetTex(), 0, 0,
		(float)APP.m_Window->GetWidth(), (float)APP.m_Window->GetHeight());
	ShMgr.m_Ss.End();
}

void PostEffect::BackBufferToGameScene()
{
	m_RT.RT(0, m_CreateGameScene->GetRTTex());
	m_RT.Depth(nullptr);
	m_RT.SetToDevice();
	ShMgr.m_Ss.Begin(false, true);
	ShMgr.m_Ss.Draw2D(ZDx.GetBackBuffer()->GetTex(), 0, 0, (float)m_CreateGameScene->GetInfo().Width, (float)m_CreateGameScene->GetInfo().Height);
	ShMgr.m_Ss.End();

}

void PostEffect::LightBloom()
{
	if (!m_BaseTextures["Emissive"]) { return; }

	//	高輝度抽出
	m_BaseTextures["Emissive"]->ClearRT(ZVec4(0, 0, 0, 0));
	ShMgr.m_Blur.HIE(m_BaseTextures["Base"], m_BaseTextures["Emissive"], ZVec2((float)m_BaseTextures["Emissive"]->GetInfo().Width, (float)m_BaseTextures["Emissive"]->GetInfo().Height));

	//	縮小バッファブラー
	{
		BlurRenderFlgs test[6] = {
		//	Original,
			Harf,
			Quarter,
			Eighth,
			Sixteen,
			ThirtyOne,
			Sixty
		};			

		ShMgr.m_Blur.ChangeRenderTarget(test[0]);
		ShMgr.m_Blur.GenerateBlur(m_BaseTextures["Emissive"], m_BloomState.Loop, m_BloomState.Disp);

		for (int i = 1; i < m_BloomState.MipLoop; ++i)
		{
			/*========================================*/
			//	バックバッファに加算
			/*========================================*/
			m_RT.RT(0, m_CreateGameScene->GetRTTex());
			m_RT.Depth(nullptr);
			m_RT.SetToDevice();

			ShMgr.m_Ss.Begin(false, true);
			ShMgr.m_bsAdd.SetState();
		/*	ShMgr.m_Ss.Draw2D(ShMgr.m_Blur.GetTexture()->GetTex(), 0.f, 0.f,
				m_CreateGameScene->GetInfo().Width, m_CreateGameScene->GetInfo().Height, &ZVec4(2, 2, 2, 2));
*/
			ShMgr.m_Ss.Draw2D(ShMgr.m_Blur.GetTexture()->GetTex(), 0.f, 0.f,
				(float)m_CreateGameScene->GetInfo().Width, (float)m_CreateGameScene->GetInfo().Height, &ZVec4(m_BloomState.AddPow[i]));

			ShMgr.m_Ss.End();
			ShMgr.m_bsAlpha_AtoC.SetState();
			

			ShMgr.m_Blur.ChangeRenderTarget(test[i]);
			ShMgr.m_Blur.GenerateBlur(ShMgr.m_Blur.GetMipTex(test[i-1]), m_BloomState.Loop, m_BloomState.Disp);
		}
	}
	//	複数枚ブラー
	{
	/*========================================*/
	//	ブラー実行	
	/*========================================*/
	//	ShMgr.m_Blur.ChangeRenderTarget(Eighth);
	//	ShMgr.m_Blur.GenerateBlur(m_BaseTextures["Emissive"], m_BloomState.Loop, m_BloomState.Disp);

	//	/*========================================*/
	//	//	バックバッファに加算
	//	/*========================================*/
	////	m_SaveRT.SetToDevice();
	//	m_RT.RT(0, m_CreateGameScene->GetRTTex());
	//	m_RT.Depth(nullptr);
	//	m_RT.SetToDevice();

	//	ShMgr.m_Ss.Begin(false, true);
	//	ShMgr.m_bsAdd.SetState();
	////	ShMgr.m_Ss.Draw2D(ShMgr.m_Blur.GetTexture()->GetTex(), 0.f, 0.f, (float)APP.m_Window->GetWidth(), (float)APP.m_Window->GetHeight(), &ZVec4(2, 2, 2, 2));
	//	ShMgr.m_Ss.Draw2D(ShMgr.m_Blur.GetTexture()->GetTex(), 0.f, 0.f,
	//		m_CreateGameScene->GetInfo().Width, m_CreateGameScene->GetInfo().Height, &ZVec4(2, 2, 2, 2));

	//	ShMgr.m_Ss.End();
	//	ShMgr.m_bsAlpha_AtoC.SetState();

	//	/*-------------------------------------------------------------------------------*/
	//	ShMgr.m_Blur.ChangeRenderTarget(Harf);
	//	ShMgr.m_Blur.GenerateBlur(m_BaseTextures["Emissive"], m_BloomState.Loop, m_BloomState.Disp * 2);

	////	m_SaveRT.SetToDevice();

	//	m_RT.RT(0, m_CreateGameScene->GetRTTex());
	//	m_RT.Depth(nullptr);
	//	m_RT.SetToDevice();

	//	ShMgr.m_Ss.Begin(false, true);
	//	ShMgr.m_bsAdd.SetState();
	////	ShMgr.m_Ss.Draw2D(ShMgr.m_Blur.GetTexture()->GetTex(), 0.f, 0.f, (float)APP.m_Window->GetWidth(), (float)APP.m_Window->GetHeight(), &ZVec4(2, 2, 2, 1));
	//	ShMgr.m_Ss.Draw2D(ShMgr.m_Blur.GetTexture()->GetTex(), 0.f, 0.f,
	//		m_CreateGameScene->GetInfo().Width, m_CreateGameScene->GetInfo().Height,
	//		&ZVec4(2, 2, 2, 2));
	//	ShMgr.m_Ss.End();
	//	ShMgr.m_bsAlpha_AtoC.SetState();
	}
}

void PostEffect::DepthOfFieldToGameScene()
{
	if (!m_BaseTextures["DOF"]) { return; }

	{
		m_BaseTextures["DOF"]->ClearRT(ZVec4(0, 0, 0, 0));
		//	DoFMapの作成
		m_DOF.GenerateDOFMap(
			m_BaseTextures["Depth"],
			m_BaseTextures["DOF"],
			ZVec2((float)m_BaseTextures["DOF"]->GetInfo().Width, (float)m_BaseTextures["DOF"]->GetInfo().Height));
	}

	//	被写界深度ブレンド
	{
		//	Baseの絵をぼかす
		ShMgr.m_Blur.ChangeRenderTarget(BlurRenderFlgs::Quarter);
		ShMgr.m_Blur.GenerateBlur(m_BaseTextures["Base"], 2, 5.f);

		
		//	ブレンド
		m_DOF.Render(m_BaseTextures["Base"],
			ShMgr.m_Blur.GetTexture(),
			m_BaseTextures["DOF"],
			ZDx.GetBackBuffer(),
			ZVec2((float)ZDx.GetBackBuffer()->GetInfo().Width,
				  (float)ZDx.GetBackBuffer()->GetInfo().Height));

		//	ブラーターゲットサイズを戻す
		ShMgr.m_Blur.ChangeRenderTarget();
	}
}

void PostEffect::XRay()
{
	m_XRay.Render(
		ZDx.GetBackBuffer(),
		m_BaseTextures["CharaDepth"],
		m_BaseTextures["Depth"],
		m_BaseTextures["CharaReflect"],
		m_CreateGameScene,
		ZVec2((float)m_CreateGameScene->GetInfo().Width,
			  (float)m_CreateGameScene->GetInfo().Height));

}

void PostEffect::GetRendertargets()
{
	SetBaseTexture(RENDERER.m_RenderTargets->GetTexZSP(0));
	SetDepthTexture(RENDERER.m_RenderTargets->GetTexZSP(1));
	SetCharaDepthTexture(RENDERER.m_RenderTargets->GetTexZSP(2));
	SetReflectTexture(RENDERER.m_RenderTargets->GetTexZSP(3));
}

void PostEffect::ImGui()
{
	auto Post = [this]
	{
		std::string imGuiWndName = "PostEffectFlg";
		if (ImGui::Begin(imGuiWndName.c_str()) == false) {
			ImGui::End();
			return;
		}

		ImGui::Checkbox("Use LightBloom", &m_EffectFlg.LightBloom);
		ImGui::Checkbox("Use DoF", &m_EffectFlg.DepthOfField);
		ImGui::Checkbox("Use X-Ray", &m_EffectFlg.XRay);


		if (ImGui::CollapsingHeader("BloomParams"))
		{
			ImGui::SliderInt("BlurLoop", &m_BloomState.Loop, 1, 10);
			ImGui::DragFloat("BlurDisp", &m_BloomState.Disp, 0.1f, -15.f, 15.f);
			ImGui::SliderInt("BlurMipLoop", &m_BloomState.MipLoop, 0, 6);

			for (int i = 0; i < BlurRenderCnt; i++)
			{
				std::string name = "BlurPow" + std::to_string(i);
				ImGui::DragFloat(name.c_str(), &m_BloomState.AddPow[i], 0.1f, 0.f, 10.f);
			}
		}
		if (ImGui::CollapsingHeader("DoFParams"))
		{

			ImGui::DragFloat("FarRange", &RENDERER.m_cb9_PerMaterial.m_Data.FarState.x, 0.01f, 0.f, 1.f);
			ImGui::DragFloat("FarPow", &RENDERER.m_cb9_PerMaterial.m_Data.FarState.y, 0.1f, 0.f, 100.f);
			ImGui::DragFloat("NearRange", &RENDERER.m_cb9_PerMaterial.m_Data.NearState.x, 0.01f, 0.f, 1.f);
			ImGui::DragFloat("NearPow", &RENDERER.m_cb9_PerMaterial.m_Data.NearState.y, 0.1f, 0.f, 100.f);
			ImGui::DragInt("UseFarBlur", &RENDERER.m_cb9_PerMaterial.m_Data.UseFarBlur, 1, 0, 1);
		}

#ifdef _DEBUG
		if (ImGui::CollapsingHeader("X-RayParams"))
		{
			ImGui::ColorEdit4("Diffuse", m_XRay.GetXColor(), ImGuiColorEditFlags_AlphaBar);
		}
#endif

		ImGui::End();
	};

	DW_IMGUI_FUNC(Post);


	auto PostTexs = [this]
	{
		auto dockID = ImGui::GetID("PostEffectTextures_Tab");
		if (ImGui::Begin("PostEffectTextures") == false)
		{
			ImGui::End();
			return;
		}
		ImGui::DockSpace(dockID);
		ImGui::End();

		ImGui::SetNextWindowDockId(dockID, ImGuiCond_FirstUseEver);
		if (ImGui::Begin("DOF"))
		{
			if (m_BaseTextures["DOF"]->GetTex())
				ImGui::Image(m_BaseTextures["DOF"]->GetTex(), ImGui::GetWindowSize());
		}
		ImGui::End();

		ImGui::SetNextWindowDockId(dockID, ImGuiCond_FirstUseEver);
		if (ImGui::Begin("Emissive"))
		{
			if (m_BaseTextures["Emissive"]->GetTex())
				ImGui::Image(m_BaseTextures["Emissive"]->GetTex(), ImGui::GetWindowSize());
		}
		ImGui::End();

		ImGui::SetNextWindowDockId(dockID, ImGuiCond_FirstUseEver);
		if (ImGui::Begin("GameScene"))
		{
			if (m_CreateGameScene->GetTex())
				ImGui::Image(m_CreateGameScene->GetTex(), ImGui::GetWindowSize());
		}
		ImGui::End();

	};

	DW_IMGUI_FUNC(PostTexs);
}

bool PostEffect::LoadState(const std::string & file)
{
	// json ファイル読み込み
	std::string error;
	json11::Json json = LoadJsonFromFile(file, error);
	if (error.size() != 0) {
		return false;
	}
	//	Flgs
	{
		auto item = json["Flgs"].object_items();
		m_EffectFlg.LightBloom		= item["LightBloom"].bool_value();
		m_EffectFlg.DepthOfField	= item["DoF"].bool_value();
		m_EffectFlg.XRay			= item["XRay"].bool_value();
	}
	//	LightBloom
	{
		auto item=json["BloomState"].object_items();
		m_BloomState.Loop		= item["Loop"].int_value();
		m_BloomState.Disp		= (float)item["Disp"].number_value();
		m_BloomState.MipLoop	= item["MipLoop"].int_value();
		for (int i = 0; i < m_BloomState.MipLoop; i++) {
			m_BloomState.AddPow[i] = (float)item["BlurPow"].array_items()[i].number_value();
		}
	}
	//	DoF(ステータスとしてはこっちでいじる方が正しいのでここで読み込み)
	{
		auto item = json["DoFState"].object_items();
		RENDERER.m_cb9_PerMaterial.m_Data.FarState.Set(item["Far"]);
		RENDERER.m_cb9_PerMaterial.m_Data.NearState.Set(item["Near"]);
		RENDERER.m_cb9_PerMaterial.m_Data.UseFarBlur = item["UseFar"].int_value();

	}
	//	X-Ray
#ifdef _DEBUG
	{
		auto item = json["XRayState"].object_items();
		m_XRay.GetXColor().Set(item["Color"]);
	}
#endif
	return true;
}

void PostEffect::SaveState(const std::string & file)
{
	std::ofstream ss(file);
	{
		cereal::JSONOutputArchive o_archive(ss);
		o_archive(cereal::make_nvp("Flgs",m_EffectFlg));
		o_archive(cereal::make_nvp("BloomState",m_BloomState));
		o_archive(cereal::make_nvp("DoFState", RENDERER.m_cb9_PerMaterial.m_Data));
		o_archive(cereal::make_nvp("XRayState",m_XRay));
	}

}
