#include "Game/Application.h"

void ClosureTaskManager::Update()
{
	auto it = m_List.begin();

	while (it != m_List.end())
	{
		// クロージャ実行
		bool ret = (*it).second();
		if (!ret)
		{
			// 削除
			it = m_List.erase(it);
		}
		else
		{
			++it;
		}
	}
}
