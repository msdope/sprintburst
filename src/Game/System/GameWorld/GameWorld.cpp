#include "Game/Application.h"
#include "../CommonECSComponents/CommonComponents.h"
#include "../TestECSComponents/TestComponents.h"
#include "../TestECSSystems/TestSystems.h"

#include "../MapObject/MapObjectComponents/MapObjectComponents.h"
#include "../MapObject/MapObjectSystems/MapObjectSystems.h"

#include "../Character/CharacterComponents/CharacterComponents.h"
#include "../Character/CharacterSystems/CharacterSystems.h"

#include "../CommonECSComponents/CommonListener.h"

#include "GameWorld.h"


void GameWorld::Init()
{

	//	ポストエフェクトを作成
	m_PostEffects.Init();
	m_PostEffects.LoadSettingFromJson("data/Scene/HkScene/PostState.json");
	// EffectPass追加
	{
		auto* setupPass = appnew(SetupPass);
		auto* dofPass = appnew(DOFPass);
		auto* xRayPass = appnew(XRayPass);
		auto* lightBloomPass = appnew(LightBloomPass);
		setupPass->Init();
		dofPass->Init();
		xRayPass->Init();
		lightBloomPass->Init();
		m_PostEffects.AddEffectPass("Setup", setupPass);
		m_PostEffects.AddEffectPass("DoF", dofPass);
		m_PostEffects.AddEffectPass("XRay", xRayPass);
		m_PostEffects.AddEffectPass("LightBloom", lightBloomPass);
	}

}

void GameWorld::Release()
{
	m_NowCamera = nullptr;

	// エンティティ削除
	// ※ 削除方法について詳しくはECSEnity.h参照
	ECS.RemoveAllListener();
	ECSEntity::RemoveAllEntity(m_Entities);
	m_Entities.shrink_to_fit();
	m_PostEffects.SaveSettingToJson("data/Scene/HkScene/PostState.json");
}

void GameWorld::Update()
{
	m_FrameCnt++;
}

void GameWorld::Draw()
{
	
}

