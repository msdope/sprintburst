#ifndef __GAMEWORLD_H__
#define __GAMEWORLD_H__

#include "Game/Camera/GameCamera.h"
#include "Game/System/Character/CharacterComponents/CharacterComponents.h"
#include "Game/System/CommonECSComponents/CommonListener.h"

class GameWorld
{
public:

	// 初期設定
	void Init();
	// 解放
	void Release();

	// 更新処理
	void Update();

	// 描画処理
	void Draw();

	// 経過フレーム
	int m_FrameCnt = 0;

	// ゲームの終了フラグ
	bool m_bEndGame = false;


	// その他
	ZAVector<ZSP<ECSEntity>> m_Entities; // エンティティハンドル

	CameraComponent* m_NowCamera;


	//	ポストエフェクト用(宣言はどこかシングルトンクラス内に作った方がいいけど、テスト的にここで)
	PostEffects				m_PostEffects;

	~GameWorld() {}

#pragma region Singleton

public:
	inline static GameWorld& GetInstance()
	{
		static GameWorld instance;
		return instance;
	}

	// コピー禁止
	GameWorld(GameWorld&) = delete;
	GameWorld& operator=(GameWorld&) = delete;

private:

	GameWorld(){}
	
#pragma endregion

};


#define GW GameWorld::GetInstance()

#endif // !__GAMEWORLD_H__