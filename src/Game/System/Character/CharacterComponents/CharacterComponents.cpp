#include "Game/Application.h"
#include "CharacterComponents.h"

void CameraComponent::InitFromJson(const json11::Json & jsonObj)
{
	Enable = jsonObj["Enable"].bool_value();

	ZVec3 basePos;
	basePos.Set(jsonObj["BasePosition"]);
	
	ZVec3 localPos;
	localPos.Set(jsonObj["LocalPosition"]);
	
	ZVec3 rotate;   
	rotate.Set(jsonObj["Rotation"]);
	Cam.m_RotAngle.x = rotate.x;
	Cam.m_RotAngle.y = rotate.y;

	ZVec2 RotAngleLimit;
	RotAngleLimit.Set(jsonObj["RotAngleLimit"]);
	Cam.m_XRotAngleLimit = RotAngleLimit;

	// 初期化
	Cam.Init(basePos, localPos);

}

void PlayerComponent::InitFromJson(const json11::Json & jsonObj)
{
	ActionState = jsonObj["ActionState"].int_value();
	MoveSpeed = jsonObj["MoveSpeed"].number_value();
	Gravity = jsonObj["Gravity"].number_value();
}

void PlayerControllerComponent::InitFromJson(const json11::Json & jsonObj)
{
	Enable = jsonObj["Enable"].bool_value();
}

const int KillerComponent::CreateWallLimit = 3;

void KillerComponent::InitFromJson(const json11::Json & jsonObj)
{

}

const int SurvivorComponent::MaxCure = 10 * 60; // (秒数*fps）
const float SurvivorComponent::SkillCheckTimeLimit = 1.5f * 60; // (秒数*fps）
const int SurvivorComponent::SkillCheckInterval = 5 * 60; // (秒数*fps）
const int SurvivorComponent::SkillCheckProbability = 20;
const int SurvivorComponent::DestroyObjLimit = 3;
const float SurvivorComponent::WalkSpeed = 0.023f;
const float SurvivorComponent::RunSpeed = 0.043f;
const float SurvivorComponent::SquatWalkSpeed = 0.012f;
const float SurvivorComponent::DownWalkSpeed = 0.006f;

void SurvivorComponent::InitFromJson(const json11::Json & jsonObj)
{
	HP = jsonObj["HP"].int_value();


	DamageEffect = EFFECT.LoadEffect("data/Effect/Damages/Damages.efk");
	
	//WarpEffect = EFFECT.LoadEffect("data/Effect/Warp/Warp.efk");
	TraceEffect = EFFECT.LoadEffect("data/Effect/Hacker_Trace/Hacker_Trace.efk");
	InjuryEffect = EFFECT.LoadEffect("data/Effect/Injury/Injury.efk");
	//DestoryObjEffect = EFFECT.LoadEffect("data/Effect/Obj_Break.efk");
	TreatmentEffect = EFFECT.LoadEffect("data/Effect/Treatment/Treatment.efk");
}


