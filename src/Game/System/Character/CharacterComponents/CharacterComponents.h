#ifndef __CHARACTER_COMPONENTS_H__
#define __CHARACTER_COMPONENTS_H__

#include "Game/Camera/GameCamera.h"


DefComponent(CameraComponent)
{
	GameCamera Cam;

	bool Enable;

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// プレイヤー共通
DefComponent(PlayerComponent)
{
	
	int ActionState;	// ステート
	float MoveSpeed;	// 移動速度
	float Gravity;		// 重力
	bool IsSky;			// 空中か
	ZVec3 vMove;		// 移動（保存）


	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

DefComponent(PlayerControllerComponent)
{
	bool Enable;
	ZVec3 Axis;
	int Button;		// 今フレームのボタン入力情報

	enum Buttons
	{
		Access = 0x00000001,
		Run = 0x00000002,
		Squat = 0x00000004,
		Skill = 0x00000008,
		Action = 0x00000010,
		Access_Stay = 0x00000020,
		Action_Stay = 0x00000040,
		START = 0x00000080
	};

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// キラー用
DefComponent(KillerComponent)
{

	enum Action
	{
		WAIT,
		WALK,
		ATTACK_SINK,
		ATTACK,
		ATTACK_NOHIT,
		ATTACK_HIT,
		CREATE_WALL
	};

	ZUnorderedMap<int, int> MultiHitMap; // 多段ヒット防止用
	char Power;				// 攻撃力
	bool HitAttack = false;	// 攻撃が当たったか

	bool CreateWallMode = false;
	int CreateWallCnt;		// 生成した壁の数
	ZSP<ECSEntity> WallEntity;

	static const int CreateWallLimit;	// 生成できる壁の数

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// サバイバー用
DefComponent(SurvivorComponent)
{
	// アクションステート用
	enum Action
	{
		WAIT,
		WALK,
		RUN,
		SQUAT_WAIT,
		SQUAT_WALK,
		FALL,
		DOWN_WAIT,
		DOWN_WALK,
		ACCESS_START,
		ACCESS,
		ACCESS_END,
		CURE,
		GETUP,
		DESTORY_OBJ_START,
		DESTORY_OBJ,
		DESTORY_OBJ_END
	};

	// ダメージステート用
	enum Damage
	{
		NORMAL,
		DAMAGE,
		DOWN
	};

	Effekseer::Effect* DamageEffect;
	ZAUnorderedMap<Effekseer::Handle,ZVec3> DamageEffectPos;

	Effekseer::Effect* WarpEffect;
	Effekseer::Effect* TraceEffect;
	Effekseer::Effect* InjuryEffect;
	Effekseer::Effect* DestoryObjEffect;
	Effekseer::Effect* TreatmentEffect;

	int		HP;					// 体力
	int		CureGage = 0;		// 負傷・ダウン時の回復ゲージ
	bool	FinishCure = false;	// 治療完了
	char	DamageState;		// ダメージの状態
	int		DamageEffectTime = 0;
	
	bool	SuccessSkillCheck;	// スキルチェックが成功したかどうか
	char	SkillCheckKey;		// スキルチェックのキー
	float	SkillCheckTime;		// スキルチェックの残り時間

	bool	SquatFlag = false;	// しゃがんでいるか
	bool	AccessFlag = false;	// アクセスしているか
	int		AccessTime = 0;		// 端末にアクセスしている時間

	ZWP<ECSEntity> DestroyObj;
	bool	DestroyObjMode = false;
	int		DestroyObjCnt = 0;	// 破壊したオブジェクト数

	static const int MaxCure;	// 治療ゲージの最大値

	static const float SkillCheckTimeLimit;	// スキルチェックの制限時間
	static const int SkillCheckInterval;	// スキルチェックの間隔
	static const int SkillCheckProbability;	// スキルチェックの発生確率
	static const int DestroyObjLimit;		// 破壊できるオブジェクト数

	// 移動スピード関係
	static const float WalkSpeed;
	static const float RunSpeed;
	static const float SquatWalkSpeed;
	static const float DownWalkSpeed;
	
	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

#endif