#ifndef __CHARACTER_SYSTEMS_H__
#define __CHARACTER_SYSTEMS_H__


// プレイヤーの更新
class PlayerUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(ModelBoneControllerComponent, TransformComponent,
						PlayerComponent, CameraComponent,
						PlayerControllerComponent, ColliderComponent)

public:
	PlayerUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;
	
};

// キラーの更新
class KillerUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(ModelBoneControllerComponent, 
		AnimatorComponent,TransformComponent,
		PlayerComponent,CameraComponent,
		PlayerControllerComponent,ColliderComponent,
		KillerComponent)
public:
	KillerUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

private:
	void Action_Wait(UpdateCompParams components);
	void Action_Walk(UpdateCompParams components,float delta);
	void Action_Attack_Sink(UpdateCompParams components);
	void Action_Attack(UpdateCompParams components);
	void Action_Attack_NoHit(UpdateCompParams components);
	void Action_Attack_Hit(UpdateCompParams components);
	void Action_CreateWall(UpdateCompParams components);

	void Script_Attack(UpdateCompParams components,json11::Json& scrItem);
private:
	ClosureTaskManager m_CTM;

	ZSP<ECSEntity> m_WallEntity;
	GameModelComponent* m_WallModelComponent;
	ColliderComponent* m_WallColliderComponent;
};

// サバイバーの更新
class SurvivorUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(AnimatorComponent,ModelBoneControllerComponent,
		TransformComponent, PlayerComponent,
		CameraComponent, PlayerControllerComponent,
		ColliderComponent,SurvivorComponent)

public:
	SurvivorUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

private:
	void Action_Wait(UpdateCompParams components);
	void Action_Walk(UpdateCompParams components, float delta);
	void Action_Run(UpdateCompParams components, float delta);
	void Action_SquatWait(UpdateCompParams components);
	void Action_SquatWalk(UpdateCompParams components, float delta);
	void Action_Fall(UpdateCompParams components);
	void Action_DownWait(UpdateCompParams components);
	void Action_DownWalk(UpdateCompParams components, float delta);
	void Action_AccessStart(UpdateCompParams components);
	void Action_Access(UpdateCompParams components);
	void Action_AccessEnd(UpdateCompParams components);
	void Action_Cure(UpdateCompParams components);
	void Action_GetUp(UpdateCompParams components);
	void Action_DestoryObjStart(UpdateCompParams components);
	void Action_DestoryObj(UpdateCompParams components);
	void Action_DestoryObjEnd(UpdateCompParams components);

	void SetCollision_Hit(UpdateCompParams components, const ZGM_RigidBodyData& rb);
	void SetCollision_Cure(UpdateCompParams components, const ZGM_RigidBodyData& rb);
	void SetCollision_Access(UpdateCompParams components, const ZGM_RigidBodyData& rb);

	void DestroyObjMode(UpdateCompParams components);

private:
	ClosureTaskManager m_CTM;
};

#endif