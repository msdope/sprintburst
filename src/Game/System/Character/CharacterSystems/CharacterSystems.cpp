#include "SubSystems.h"
#include "Game/Application.h"
#include "../../GameWorld/GameWorld.h"
#include "../../CommonECSComponents/CommonComponents.h"
#include "../CharacterComponents/CharacterComponents.h"
#include "../../MapObject/MapObjectComponents/MapObjectComponents.h"
#include "CharacterSystems.h"

#include "Game/Camera/GameCamera.h"

KillerUpdateSystem::KillerUpdateSystem()
{
	Init();
	m_DebugSystemName = "KillerUpdateSystem";
}

void KillerUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{

	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto killerComp = GetCompFromUpdateParam(KillerComponent,components);

	// 多段ヒットリストの寿命処理
	for (auto it = killerComp->MultiHitMap.begin(); it != killerComp->MultiHitMap.end();)
	{
		(*it).second--;
		if ((*it).second <= 0)
			it = killerComp->MultiHitMap.erase(it);
		else
			++it;
	}

	m_CTM.Update();

	switch (playerComp->ActionState)
	{
	case KillerComponent::WAIT:
		Action_Wait(components);
		break;
	case KillerComponent::WALK:
		Action_Walk(components,delta);
		break;
	case KillerComponent::ATTACK_SINK:
		Action_Attack_Sink(components);
		break;
	case KillerComponent::ATTACK:
		Action_Attack(components);
		break;
	case KillerComponent::ATTACK_NOHIT:
		Action_Attack_NoHit(components);
		break;
	case KillerComponent::ATTACK_HIT:
		Action_Attack_Hit(components);
		break;
	default:
		break;
	}

	if (killerComp->CreateWallMode == true)
	{
		Action_CreateWall(components);
	}

	// スクリプトキー時に実行される関数
	auto scriptProc = [this,components](ZAnimeKey_Script* scr)
	{

		// 文字列をJsonとして解析
		std::string errorMsg;
		json11::Json jsonObj = json11::Json::parse(scr->Value.c_str(), errorMsg);
		if (errorMsg.size() > 0)
		{
			DW_SCROLL(2, "JsonError（%s）", errorMsg.c_str());
			return;
		}

		auto scriptArray = jsonObj["Scripts"].array_items();
		for (auto& scrItem : scriptArray)
		{
			// スクリプトの種類
			std::string scrType = scrItem["Type"].string_value();
			// 攻撃判定発生
			if (scrType == "Attack")
				Script_Attack(components,scrItem);
			
		}

	};


	animatorComp->ScriptProc = scriptProc;
}

void KillerUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);

	// モデルデータの物理演算設定のNo9を当たり判定として使う
	ZGM_PhysicsDataSet& phyData = bcComp->BoneController->GetGameModel()->GetPhysicsDataSetList()[9];

	for (auto& rb : phyData.RigidBodyDataTbl)
	{
		if (rb.RigidBodyName == "ぶつかり")
		{
			if (rb.Shape != ZBP_RigidBody::shape::Sphere)continue;	// 球以外は無視

			// ワールド行列
			ZMatrix m = rb.GetMatrix();
			m *= transComp->Transform;

			auto hitObj = Make_Shared(Collider_Sphere, appnew);

			// 基本設定
			hitObj->Init(0,
				HitGroups::_0 | HitGroups::_8, // 判定する側のフィルタ
				HitShapes::SPHERE | HitShapes::MESH | HitShapes::BOX,
				HitGroups::_0 | HitGroups::_8 // 判定される側のフィルタ
			);

			// スフィア情報
			hitObj->Set(m.GetPos(), rb.ShapeSize.x);

			// 質量
			hitObj->m_Mass = rb.Mass;

			// 自分のEntityのアドレスを仕込んでおく
			//hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

			// デバッグ用の色
			hitObj->m_Debug_Color.Set(0, 0, 1, 1);

			// 登録
			ColEng.AddAtk(hitObj);	// 判定する側
			ColEng.AddDef(hitObj);	// 判定される側

			// ヒット時に実行される
			hitObj->m_OnHitStay = [this, transComp, colliderComp](const ZSP<ColliderBase>& hitObj)
			{
				// Hitしたやつら全て
				for (auto& res : hitObj->m_HitResTbl)
				{
					for (auto& node : res.HitDataList)
					{
						//// 相手のEntity
						//auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
						//if (youEntity == nullptr)continue;

						//// 自分自身は無視
						//if (colliderComp->m_Entity == youEntity)continue;

						float F = hitObj->CalcMassRatio(node.YouHitObj->m_Mass);
						transComp->Transform.Move(node.vPush*F);
					}
				}
			};
		}
	}

	// 地面のレイ判定
	{
		auto hitObj = Make_Shared(Collider_Ray, appnew);
		
		hitObj->Init(0,
			HitGroups::_0,
			HitShapes::MESH,
			HitGroups::_0);

		ZVec3 pos = transComp->Transform.GetPos();
		hitObj->Set(pos + ZVec3(0, 0.5, 0),	// レイの開始位置
			pos + ZVec3(0, -100, 0));		// レイの終了位置

		ColEng.AddAtk(hitObj);
		hitObj->m_OnHitStay = [this, playerComp, transComp, colliderComp](const ZSP<ColliderBase>& hitObj)
		{
			float nearestDist = FLT_MAX;
			//sptr(GameObject> nearestObj;
			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					// 相手のEntity
					//auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					//if (youEntity == nullptr)continue;
					//// 自キャラは無視
					//if (colliderComp->m_Entity == youEntity)continue;
					// 近い？
					if (nearestDist > node.RayDist)
					{
						nearestDist = node.RayDist;
						//nearestObj = youGameObj;
					}
				}
			}

			// 空中時
			if (playerComp->IsSky)	// めりこんでる
			{
				if (nearestDist < 0.5f && playerComp->vMove.y <= 0)
				{
					transComp->Transform._42 += 0.5f - nearestDist; // 押し戻す
					playerComp->vMove.y = 0;
					playerComp->IsSky = false;
				}
			}
			else
			{
				if (nearestDist > 0.5f + 0.3f || playerComp->vMove.y > 0)
				{
					playerComp->IsSky = true;
				}
				else
				{
					transComp->Transform._42 += 0.5f - nearestDist;
					playerComp->vMove.y = 0;
				}
			}

		};

		// 最後に呼ばれる
		hitObj->m_OnTerminal = [this,bcComp, transComp, cameraComp](const ZSP<ColliderBase>& hitObj)
		{
			// キャラの頭の座標を使用
			auto headLocalMat = bcComp->BoneController->SearchBone("頭")->LocalMat;
			auto headMat = headLocalMat * transComp->Transform;

			ZMatrix m;
			m = cameraComp->Cam.m_BaseMat;
			m.Move(headMat.GetPos());

			cameraComp->Cam.mCam = cameraComp->Cam.m_LocalMat * m;

		};
	}


}

void KillerUpdateSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
}

void KillerUpdateSystem::Action_Wait(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto killerComp = GetCompFromUpdateParam(KillerComponent, components);


	// 壁生成
	if (playerContComp->Button &
		PlayerControllerComponent::Action &&
		killerComp->CreateWallCnt < KillerComponent::CreateWallLimit)
	{
		killerComp->CreateWallMode = !killerComp->CreateWallMode;

		if (killerComp->CreateWallMode == true &&
			killerComp->WallEntity == nullptr)
		{
			TransformComponent* wallTransComp = ECS.MakeComponent<TransformComponent>();
			wallTransComp->Transform = transComp->Transform;

			GameModelComponent* wallModelComp = ECS.MakeComponent<GameModelComponent>();

			auto model = APP.m_ResStg.LoadMesh("data/Model/killer/RedWall.xed");
			wallModelComp->Model = model;
			wallModelComp->RenderFlg = Make_Shared(Object3D_RenderFlgs, appnew); 
			wallModelComp->RenderFlg->SemiTransparent = true;


			ColliderComponent* wallColliderComp = ECS.MakeComponent<ColliderComponent>();

			// 当たり判定用モデル
			wallColliderComp->HitModel = model;
			wallColliderComp->HitObj = Make_Shared(Collider_Mesh, appnew);

			// 基本設定
			wallColliderComp->HitObj->Init(0,
				0,// 判定する側時のマスク
				0xFFFFFFFF, // 形状マスク
				0 // 判定される側時のマスク
			);

			// メッシュを追加   
			wallColliderComp->HitObj->AddMesh(wallColliderComp->HitModel);


			auto wallEntity = ECS.MakeEntity(wallTransComp, wallModelComp,wallColliderComp);

			GW.m_Entities.push_back(wallEntity);
			killerComp->WallEntity = wallEntity;
		}
		else if (killerComp->CreateWallMode == false &&
			killerComp->WallEntity != nullptr)
		{
			// 設置せずに戻った場合モデル描画オフ
			auto wallModelComp = killerComp->WallEntity->GetComponent<GameModelComponent>();
			
		}

	}

	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{
		// 移動へ
		playerComp->ActionState = KillerComponent::WALK;
		animatorComp->Animator->ChangeAnimeSmooth("Movement", 0, 10, true);
		return;
	}

	if (playerContComp->Button &
		PlayerControllerComponent::Access &&
		killerComp->CreateWallMode == false)
	{
		// 溜めへ
		playerComp->ActionState = KillerComponent::ATTACK_SINK;
		animatorComp->Animator->ChangeAnimeSmooth("Attack(Sink)", 0, 3, false);
		return;
	}


	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;

	GameCamera& cam = cameraComp->Cam;

	ZVec3 vTar;
	//vTar += cam.m_LocalMat.GetXAxis();
	vTar += cam.m_LocalMat.GetZAxis();
	vTar.y = 0;
	vTar.Normalize();

	mat.SetLookTo(vTar, ZVec3::Up);

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);


}

void KillerUpdateSystem::Action_Walk(UpdateCompParams components,float delta)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components); 
	auto killerComp = GetCompFromUpdateParam(KillerComponent, components);

	
	if (playerContComp->Button &
		PlayerControllerComponent::Action &&
		killerComp->CreateWallCnt < KillerComponent::CreateWallLimit)
	{
		killerComp->CreateWallMode = !killerComp->CreateWallMode;

		if (killerComp->CreateWallMode == true &&
			killerComp->WallEntity == nullptr)
		{
			TransformComponent* wallTransComp = ECS.MakeComponent<TransformComponent>();
			wallTransComp->Transform = transComp->Transform;

			GameModelComponent* wallModelComp = ECS.MakeComponent<GameModelComponent>();

			auto model = APP.m_ResStg.LoadMesh("data/Model/killer/RedWall.xed");
			wallModelComp->Model = model;
			wallModelComp->RenderFlg = Make_Shared(Object3D_RenderFlgs, appnew);
			wallModelComp->RenderFlg->SemiTransparent = true;

			ColliderComponent* wallColliderComp = ECS.MakeComponent<ColliderComponent>();

			// 当たり判定用モデル
			wallColliderComp->HitModel = model;
			wallColliderComp->HitObj = Make_Shared(Collider_Mesh, appnew);

			// 基本設定
			wallColliderComp->HitObj->Init(0,
				0,// 判定する側時のマスク
				0xFFFFFFFF, // 形状マスク
				0 // 判定される側時のマスク
			);

			// メッシュを追加   
			wallColliderComp->HitObj->AddMesh(wallColliderComp->HitModel);

			auto wallEntity = ECS.MakeEntity(wallTransComp, wallModelComp,wallColliderComp);

			GW.m_Entities.push_back(wallEntity);
			killerComp->WallEntity = wallEntity;
		}
		else if (killerComp->CreateWallMode == false &&
			killerComp->WallEntity != nullptr)
		{
			// 設置せずに戻った場合モデル描画オフ
			auto wallModelComp = killerComp->WallEntity->GetComponent<GameModelComponent>();

		}
	}

	if (playerContComp->Axis.x == 0 &&
		playerContComp->Axis.z == 0)
	{
		// 待機へ
		playerComp->ActionState = KillerComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		return;
	}

	if (playerContComp->Button &
		PlayerControllerComponent::Access &&
		killerComp->CreateWallMode == false)
	{
		// 溜めへ
		playerComp->ActionState = KillerComponent::ATTACK_SINK;
		animatorComp->Animator->ChangeAnimeSmooth("Attack(Sink)", 0, 3, false);
		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;

	GameCamera& cam = cameraComp->Cam;

	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();

	float speed = playerComp->MoveSpeed * (delta*APP.GetFrameRate() /1.0f);
	mat.Move(vTar * speed);

	ZVec3 vZ = cam.m_LocalMat.GetZAxis();
	vZ.y = 0;
	//vZ.Homing(vTar, 5);
	mat.SetLookTo(vZ, ZVec3::Up);

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void KillerUpdateSystem::Action_Attack_Sink(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components); 
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components); 
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	
	// アニメーション終了か攻撃ボタンを離したら
	if (animatorComp->Animator->IsAnimationend() ||
		!(playerContComp->Button & PlayerControllerComponent::Access_Stay))
	{
		auto killerComp = GetCompFromUpdateParam(KillerComponent, components);
		
		// 溜めに応じて攻撃力を変える
		if (animatorComp->Animator->IsAnimationend())
			killerComp->Power = 2;
		else
			killerComp->Power = 1;

		cameraComp->Enable = false;
		
		// 攻撃へ
		playerComp->ActionState = KillerComponent::ATTACK;
		animatorComp->Animator->ChangeAnimeSmooth("Attack", 0, 3, false);

		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;

	GameCamera& cam = cameraComp->Cam;

	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetZAxis();
	vTar.y = 0;
	vTar.Normalize();

	mat.SetLookTo(vTar, ZVec3::Up);


	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void KillerUpdateSystem::Action_Attack(UpdateCompParams components)
{	
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);

	// アニメーション終了
	if (animatorComp->Animator->IsAnimationend())
	{
		auto killerComp = GetCompFromUpdateParam(KillerComponent, components); 
		auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
		cameraComp->Enable = true;

		// 攻撃が当たったか
		if (killerComp->HitAttack)
		{
			playerComp->ActionState = KillerComponent::ATTACK_HIT;
			animatorComp->Animator->ChangeAnimeSmooth("Attack(Hit)", 0, 3, false);
			killerComp->HitAttack = false;
		}
		else
		{
			playerComp->ActionState = KillerComponent::ATTACK_NOHIT;
			animatorComp->Animator->ChangeAnimeSmooth("Attack(NoHit)", 0, 3, false);
		}

		return;
	}
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void KillerUpdateSystem::Action_Attack_NoHit(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);

	// アニメーション終了
	if (animatorComp->Animator->IsAnimationend())
	{
		// 待機へ
		playerComp->ActionState = KillerComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true); 

		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;

	GameCamera& cam = cameraComp->Cam;

	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetZAxis();
	vTar.y = 0;
	vTar.Normalize();

	mat.SetLookTo(vTar, ZVec3::Up);

	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void KillerUpdateSystem::Action_Attack_Hit(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);

	// アニメーション終了
	if (animatorComp->Animator->IsAnimationend())
	{
		// 待機へ
		playerComp->ActionState = KillerComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		
		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;

	GameCamera& cam = cameraComp->Cam;

	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetZAxis();
	vTar.y = 0;
	vTar.Normalize();

	mat.SetLookTo(vTar, ZVec3::Up);

	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void KillerUpdateSystem::Action_CreateWall(UpdateCompParams components)
{

	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto killerComp = GetCompFromUpdateParam(KillerComponent, components);

	// 壁生成
	if (playerContComp->Button & PlayerControllerComponent::Access)
	{
		
		// 当たり判定用モデル
		auto wallColliderComp = killerComp->WallEntity->GetComponent<ColliderComponent>();

		int filter = 0;
		filter |= 1 << 0;

		// 基本設定
		wallColliderComp->HitObj->Init(0,
			filter,// 判定する側時のマスク
			0xFFFFFFFF, // 形状マスク
			filter // 判定される側時のマスク
		);

		killerComp->CreateWallCnt++;
		killerComp->WallEntity = nullptr;
		killerComp->CreateWallMode = false;

		return;

	}

	auto wallTransComp = killerComp->WallEntity->GetComponent<TransformComponent>();
	wallTransComp->Transform = transComp->Transform;
	wallTransComp->Transform.Move_Local(0, 0, 1);
}

void KillerUpdateSystem::Script_Attack(UpdateCompParams components,json11::Json& scrItem)
{
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto killerComp = GetCompFromUpdateParam(KillerComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);


	struct Param
	{
		// 寿命
		int life;
		// ボーンの名前
		ZString boneName;
		// 当たり判定の半径
		float radius;
		// ヒット間隔
		int hitInterval;

	};

	Param param;
	param.life = scrItem["Time"].int_value();
	// ボーンの名前
	param.boneName = scrItem["BoneName"].string_value().c_str();
	// 当たり判定の半径
	param.radius = (float)scrItem["Radius"].number_value();
	// ヒットの間隔
	param.hitInterval = scrItem["HitInterval"].int_value();

	auto task = [this, transComp, bcComp,colliderComp,killerComp, param]()mutable
	{
		param.life--;
		if (param.life <= 0)return false;

		// ボーン
		auto bone = bcComp->BoneController->SearchBone(param.boneName);

		// ボーン行列
		ZMatrix m = bone->LocalMat * transComp->Transform;

		auto hitObj = Make_Shared(Collider_Sphere,appnew);

		// 基本設定
		hitObj->Init(0,
			HitGroups::_5, // 判定する側のフィルタ
			HitShapes::SPHERE | HitShapes::MESH,
			HitGroups::_5  // 判定される側のフィルタ
		);

		// スフィア情報
		hitObj->Set(m.GetPos(), param.radius);

		// 自分のEntityのアドレスを仕込んでおく
		hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

		// デバッグ用の色
		hitObj->m_Debug_Color.Set(1, 0, 0, 1);

		// 登録
		ColEng.AddAtk(hitObj);	// 判定される側

		// ヒット時に実行される
		hitObj->m_OnHitStay = [this,colliderComp,killerComp, param](const ZSP<ColliderBase>& hitObj)
		{
			// Hitしたやつら全て
			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					// 相手のEntity
					auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					if (youEntity == nullptr)continue;

					// 自分自身は無視
					//if (colliderComp->m_Entity == youEntity)continue;
					
					// すでにヒットしているか
					int ID = (int)youEntity;
					if (killerComp->MultiHitMap.count(ID) != 0)continue;

					//DW_SCROLL(3, "Hit");
					killerComp->MultiHitMap[ID] = param.hitInterval;

					killerComp->HitAttack = true;

					auto youSurvivorComp = youEntity->GetComponent<SurvivorComponent>();
					auto youPlayerComp = youEntity->GetComponent<PlayerComponent>();
					auto youAnimatorComp = youEntity->GetComponent<AnimatorComponent>();
					auto youTransComp = youEntity->GetComponent<TransformComponent>();

					// エフェクト（仮置き）
					EFFECT.SubmitPlayEffect(youSurvivorComp->DamageEffect,
						youTransComp->Transform.GetPos(),ZEffectManager::OnPlayEffectFunction());

					// 攻撃力分ダメージとする
					youSurvivorComp->DamageState += killerComp->Power;

					if (youSurvivorComp->DamageState >= SurvivorComponent::DOWN)
					{
						// ダウン状態ならシルエットを出す
						auto youModelComp = youEntity->GetComponent<GameModelComponent>();
						youModelComp->RenderFlg->Character = true;

						youSurvivorComp->DamageState = SurvivorComponent::DOWN;
						youPlayerComp->ActionState = SurvivorComponent::FALL;

						// しゃがみ
						if (youSurvivorComp->SquatFlag)
							youAnimatorComp->Animator->ChangeAnimeSmooth("SitDown from Down", 0, 10, false);
						else
							youAnimatorComp->Animator->ChangeAnimeSmooth("Stand from Down", 0, 10, false);
					}
				}
			}
		};

		return true;
	};



	m_CTM.AddClosure("Attack", task);
}

SurvivorUpdateSystem::SurvivorUpdateSystem()
{
	Init();
	m_DebugSystemName = "SurvivorUpdateSystem";
}

void SurvivorUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent,components);

	m_CTM.Update();

	// しゃがみ
	if (playerContComp->Button & PlayerControllerComponent::Squat)
		survivorComp->SquatFlag = true;
	else
		survivorComp->SquatFlag = false;
	
	// ダメージ
	switch (survivorComp->DamageState)
	{
	case SurvivorComponent::Damage::DAMAGE:
	{
		auto transComp = GetCompFromUpdateParam(TransformComponent, components);
		survivorComp->DamageEffectTime++;

		// エフェクト
		if (survivorComp->DamageEffectTime > 50)
		{
			survivorComp->DamageEffectTime = 0;
			auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);

			
			auto effectFunc = [this,survivorComp,transComp,colliderComp](const Effekseer::Handle handle, Effekseer::Manager* manager)mutable
			{
				ZVec3 effectPos = ZVec3(
				RAND.GetFloat(colliderComp->HitObj->GetBaseAABB().Min.x, colliderComp->HitObj->GetBaseAABB().Max.x),
				RAND.GetFloat(colliderComp->HitObj->GetBaseAABB().Min.y, colliderComp->HitObj->GetBaseAABB().Max.y),
				RAND.GetFloat(colliderComp->HitObj->GetBaseAABB().Min.z, colliderComp->HitObj->GetBaseAABB().Max.z));


				survivorComp->DamageEffectPos[handle] = effectPos;
			};

			auto stopFunc = [this,survivorComp](bool isRemovingManager)
			{
			};

			EFFECT.SubmitPlayEffect(survivorComp->InjuryEffect, effectFunc);
		}


		Effekseer::Manager* manager = EFFECT.GetManager();

		for (auto& effect : survivorComp->DamageEffectPos)
		{
			ZVec3 pos = effect.second;

			pos.Transform(transComp->Transform);

			manager->SetLocation(effect.first,
				Effekseer::Vector3D(pos.x,pos.y,pos.z));

		}

	}
		break;
	case SurvivorComponent::Damage::DOWN:
		survivorComp->HP--;
		break;
	default:
		break;
	}

	switch (playerComp->ActionState)
	{
	case SurvivorComponent::WAIT:
		Action_Wait(components);
		break;
	case SurvivorComponent::WALK:
		Action_Walk(components,delta);
		break;
	case SurvivorComponent::RUN:
		Action_Run(components,delta);
		break;
	case SurvivorComponent::SQUAT_WAIT:
		Action_SquatWait(components);
		break;
	case SurvivorComponent::SQUAT_WALK:
		Action_SquatWalk(components,delta);
		break;
	case SurvivorComponent::FALL:
		Action_Fall(components);
		break;
	case SurvivorComponent::DOWN_WAIT:
		Action_DownWait(components);
		break;
	case SurvivorComponent::DOWN_WALK:
		Action_DownWalk(components,delta);
		break;
	case SurvivorComponent::ACCESS_START:
		Action_AccessStart(components);
		break;
	case SurvivorComponent::ACCESS:
		Action_Access(components);
		break;
	case SurvivorComponent::ACCESS_END:
		Action_AccessEnd(components);
		break;
	case SurvivorComponent::CURE:
		Action_Cure(components);
		break;
	case SurvivorComponent::GETUP:
		Action_GetUp(components);
		break;
	case SurvivorComponent::DESTORY_OBJ_START:
		Action_DestoryObjStart(components);
		break;
	case SurvivorComponent::DESTORY_OBJ:
		Action_DestoryObj(components);
		break;
	case SurvivorComponent::DESTORY_OBJ_END:
		Action_DestoryObjEnd(components);
		break;
	default:
		break;
	}

	if (survivorComp->DestroyObjMode)
		DestroyObjMode(components);
}

void SurvivorUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{

	auto bcComp = GetCompFromUpdateParam(ModelBoneControllerComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);
	auto colliderComp = GetCompFromUpdateParam(ColliderComponent, components);


	// モデルデータの物理演算設定のNo9を当たり判定として使う
	ZGM_PhysicsDataSet& phyData = bcComp->BoneController->GetGameModel()->GetPhysicsDataSetList()[9];


	for (auto& rb : phyData.RigidBodyDataTbl)
	{

		if (rb.RigidBodyName == "ぶつかり下")
		{
			if (rb.Shape != ZBP_RigidBody::shape::Sphere)continue;	// 球以外は無視

			SetCollision_Hit(components, rb);
		}
		else if (rb.RigidBodyName == "治療")
		{
			if (survivorComp->AccessFlag ||
				survivorComp->DestroyObjMode)
				continue;

			SetCollision_Cure(components, rb);
		}

		// ダウンしている場合やられとアクセスは判定しない
		if (survivorComp->DamageState >= SurvivorComponent::DOWN)continue;


		if (rb.RigidBodyName == "ぶつかり上")
		{	
			if (rb.Shape != ZBP_RigidBody::shape::Sphere)continue;	// 球以外は無視

			SetCollision_Hit(components, rb);
		}
		else if (rb.RigidBodyName == "やられ")
		{
			if (rb.Shape != ZBP_RigidBody::shape::Sphere)continue;	// 球以外は無視

			// ワールド行列
			ZMatrix m = rb.GetMatrix();
			m *= transComp->Transform;

			auto hitObj = Make_Shared(Collider_Sphere, appnew);

			// 基本設定
			hitObj->Init(0,
				HitGroups::_5, // 判定する側のフィルタ
				HitShapes::SPHERE | HitShapes::MESH | HitShapes::BOX,
				HitGroups::_5  // 判定される側のフィルタ
			);

			// スフィア情報
			hitObj->Set(m.GetPos(), rb.ShapeSize.x);

			// 質量
			hitObj->m_Mass = rb.Mass;

			// 自分のEntityのアドレスを仕込んでおく
			hitObj->m_UserMap["Entity"] = colliderComp->m_Entity;

			// デバッグ用の色
			hitObj->m_Debug_Color.Set(0, 1, 0, 1);

			// 登録
			ColEng.AddDef(hitObj);	// 判定される側

		}
		else if (rb.RigidBodyName == "アクセス")
		{
			
			if (survivorComp->SkillCheckTime == 0)
			{
				bool cureFlag = playerComp->ActionState == SurvivorComponent::CURE;
				
				if (!(playerContComp->Button & PlayerControllerComponent::Access) &&
					!survivorComp->AccessFlag ||
					!(playerContComp->Button & PlayerControllerComponent::Access_Stay) || 
					cureFlag ||
					survivorComp->DestroyObjMode)
				{
					// アクセスフラグをオフ
					survivorComp->AccessFlag = false;
					continue;
				}
			}

			// アクセスフラグをオフ
			survivorComp->AccessFlag = false;
			SetCollision_Access(components, rb);

		}
	}



	// 地面のレイ判定
	{
		auto hitObj = Make_Shared(Collider_Ray, appnew);

		hitObj->Init(0,
			HitGroups::_0,
			HitShapes::MESH,
			HitGroups::_0);

		ZVec3 pos = transComp->Transform.GetPos();
		hitObj->Set(pos + ZVec3(0, 0.5, 0),	// レイの開始位置
			pos + ZVec3(0, -100, 0));		// レイの終了位置

		ColEng.AddAtk(hitObj);
		hitObj->m_OnHitStay = [this, playerComp, transComp, colliderComp](const ZSP<ColliderBase>& hitObj)
		{
			float nearestDist = FLT_MAX;

			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					// 相手のEntity
					//auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					//if (youEntity == nullptr)continue;
					//// 自キャラは無視
					//if (colliderComp->m_Entity == youEntity)continue;
					// 近い？
					if (nearestDist > node.RayDist)
					{
						nearestDist = node.RayDist;
						//nearestObj = youGameObj;
					}
				}
			}

			// 空中時
			if (playerComp->IsSky)	// めりこんでる
			{
				if (nearestDist < 0.5f && playerComp->vMove.y <= 0)
				{
					transComp->Transform._42 += 0.5f - nearestDist; // 押し戻す
					playerComp->vMove.y = 0;
					playerComp->IsSky = false;
				}
			}
			else
			{
				if (nearestDist > 0.5f + 0.3f || playerComp->vMove.y > 0)
				{
					playerComp->IsSky = true;
				}
				else
				{
					transComp->Transform._42 += 0.5f - nearestDist;
					playerComp->vMove.y = 0;
				}
			}

		};

		// 最後に呼ばれる
		hitObj->m_OnTerminal = [this, transComp, cameraComp](const ZSP<ColliderBase>& hitObj)
		{
			// キャラの座標を使用
			ZMatrix m;
			m = cameraComp->Cam.m_BaseMat;
			m.Move(transComp->Transform.GetPos());

			cameraComp->Cam.mCam = cameraComp->Cam.m_LocalMat * m;

		};
	}

	
	// カメラのレイ判定
	//{
	//	auto hitObj = Make_Shared(Collider_Ray, appnew);
	//	//auto hitObj = appnew(Collider_Ray);
	//	hitObj->Init(0,
	//		HitGroups::_0,
	//		HitShapes::MESH,
	//		HitGroups::_0);

	//	ZVec3 pos = transComp->Transform.GetPos();
	//	hitObj->Set(pos + ZVec3(0, 1, 0),	// レイの開始位置
	//		cameraComp->Cam.mCam.GetPos());		// レイの終了位置

	//	
	//	APP.m_ColEng.AddAtk(hitObj);
	//	hitObj->m_OnHitStay = [this,cameraComp, playerComp, transComp, colliderComp](const ZSP<ColliderBase>& hitObj)
	//	{

	//		float nearestDist = FLT_MAX;
	//		//sptr(GameObject> nearestObj;
	//		for (auto& res : hitObj->m_HitResTbl)
	//		{
	//			for (auto& node : res.HitDataList)
	//			{
	//				// 近い？
	//				if (nearestDist > node.RayDist)
	//					nearestDist = node.RayDist;
	//			}
	//		}

	//		if (nearestDist < 1)
	//		{
	//			ZVec3 camVec = cameraComp->Cam.mCam.GetPos() - (transComp->Transform.GetPos() + ZVec3(0, 1, 0));
	//			camVec.Normalize();

	//			ZVec3 camMove = camVec * (1 - nearestDist);

	//			cameraComp->Cam.m_LocalTransMat.Move(0,0, cameraComp->Cam.m_LocalTransMat.GetPos().z*camMove.z);
	//		}
	//	};

	//}
}

void SurvivorUpdateSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
}

void SurvivorUpdateSystem::Action_Wait(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent,components);

	if (playerContComp->Button & PlayerControllerComponent::Action)
	{
		survivorComp->DestroyObjMode = !survivorComp->DestroyObjMode;
	}

	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{
		
		if (survivorComp->SquatFlag)
		{
			// しゃがみ歩き
			playerComp->ActionState = SurvivorComponent::SQUAT_WALK;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->SquatWalkSpeed;
		}
		else if (playerContComp->Button & PlayerControllerComponent::Run)
		{
			// 走り
			playerComp->ActionState = SurvivorComponent::RUN;
			animatorComp->Animator->ChangeAnimeSmooth("Run", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->RunSpeed;
		}
		else
		{
			// 歩き
			playerComp->ActionState = SurvivorComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->WalkSpeed;
		}
		return;
	}
	else
	{
		if (survivorComp->SquatFlag)
		{
			// しゃがみ
			playerComp->ActionState = SurvivorComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
			return;
		}
	}


	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_Walk(UpdateCompParams components,float delta)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent,components);

	if (playerContComp->Button & PlayerControllerComponent::Action)
	{
		survivorComp->DestroyObjMode = !survivorComp->DestroyObjMode;
	}
	
	if (playerContComp->Axis.x == 0 &&
		playerContComp->Axis.z == 0)
	{
		if (survivorComp->SquatFlag)
		{
			// しゃがみ
			playerComp->ActionState = SurvivorComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
		}
		else
		{
			// 待機
			playerComp->ActionState = SurvivorComponent::WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
			
		}
		return;
	}
	else if (survivorComp->SquatFlag)
	{
		// しゃがみ歩き
		playerComp->ActionState = SurvivorComponent::SQUAT_WALK;
		animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
		playerComp->MoveSpeed = survivorComp->SquatWalkSpeed;
		return;
	}
	else if (playerContComp->Button & PlayerControllerComponent::Run)
	{
		// 走り
		playerComp->ActionState = SurvivorComponent::RUN;
		animatorComp->Animator->ChangeAnimeSmooth("Run", 0, 10, true);
		playerComp->MoveSpeed = survivorComp->RunSpeed;
		return;
	}


	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;
	GameCamera& cam = cameraComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();

	float speed = playerComp->MoveSpeed * (delta*APP.GetFrameRate() / 1.0f);
	mat.Move(vTar * speed);

	ZVec3 vZ = mat.GetZAxis();
	vZ.Homing(vTar, 10);
	mat.SetLookTo(vZ, ZVec3::Up);

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);

}

void SurvivorUpdateSystem::Action_Run(UpdateCompParams components,float delta)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent,components);

	if (playerContComp->Button & PlayerControllerComponent::Action)
	{
		survivorComp->DestroyObjMode = !survivorComp->DestroyObjMode;
	}

	if (playerContComp->Axis.x == 0 &&
		playerContComp->Axis.z == 0)
	{
		if (survivorComp->SquatFlag)
		{
			// しゃがみ
			playerComp->ActionState = SurvivorComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
		}
		else
		{
			playerComp->ActionState = SurvivorComponent::WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		}
		return;
	}
	else if (!(playerContComp->Button & PlayerControllerComponent::Run))
	{
		if (survivorComp->SquatFlag)
		{
			// しゃがみ歩き
			playerComp->ActionState = SurvivorComponent::SQUAT_WALK;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->SquatWalkSpeed;
		}
		else
		{
			playerComp->ActionState = SurvivorComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->WalkSpeed;
		}
		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;
	GameCamera& cam = cameraComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();

	float speed = playerComp->MoveSpeed * (delta*APP.GetFrameRate() / 1.0f);
	mat.Move(vTar * speed);

	ZVec3 vZ = mat.GetZAxis();
	vZ.Homing(vTar, 10);
	mat.SetLookTo(vZ, ZVec3::Up);

	//EFFECT.SubmitPlayEffect(survivorComp->TraceEffect,
	//	transComp->Transform.GetPos(),
	//	ZEffectManager::OnPlayEffectFunction());

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_SquatWait(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent,components);

	if (playerContComp->Button & PlayerControllerComponent::Action)
	{
		survivorComp->DestroyObjMode = !survivorComp->DestroyObjMode;
	}

	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{
		if (survivorComp->SquatFlag)
		{
			// しゃがみ歩き
			playerComp->ActionState = SurvivorComponent::SQUAT_WALK;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->SquatWalkSpeed;
		}
		else
		{
			// 歩き
			playerComp->ActionState = SurvivorComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->WalkSpeed;
		}
		return;
	}
	else
	{
		if (!survivorComp->SquatFlag)
		{
			// 待機
			playerComp->ActionState = SurvivorComponent::WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
			return;
		}
	}

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_SquatWalk(UpdateCompParams components,float delta)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent,components);
	auto transComp = GetCompFromUpdateParam(TransformComponent,components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent,components);

	if (playerContComp->Button & PlayerControllerComponent::Action)
	{
		survivorComp->DestroyObjMode = !survivorComp->DestroyObjMode;
	}

	if (!survivorComp->SquatFlag)
	{
		if (playerContComp->Axis.x == 0 &&
			playerContComp->Axis.z == 0)
		{
			// 待機
			playerComp->ActionState = SurvivorComponent::WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		}
		else
		{
			// 歩き
			playerComp->ActionState = SurvivorComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->WalkSpeed;
		}
		return;

	}
	else
	{
		if (playerContComp->Axis.x == 0 &&
			playerContComp->Axis.z == 0)
		{
			// しゃがみ
			playerComp->ActionState = SurvivorComponent::SQUAT_WAIT;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
			return;
		}
	}


	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;
	GameCamera& cam = cameraComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();
	float speed = playerComp->MoveSpeed * (delta*APP.GetFrameRate() / 1.0f);
	mat.Move(vTar * speed);

	ZVec3 vZ = mat.GetZAxis();
	vZ.Homing(vTar, 20);
	mat.SetLookTo(vZ, ZVec3::Up);

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_Fall(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

	// アニメーション終了
	if (animatorComp->Animator->IsAnimationend())
	{
		playerComp->ActionState = SurvivorComponent::DOWN_WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Crawls around Wait", 0, 25, true);
		
		return;
	}
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_DownWait(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{

		// 歩き
		playerComp->ActionState = SurvivorComponent::DOWN_WALK;
		animatorComp->Animator->ChangeAnimeSmooth("Crawls around", 0, 10, true);
		playerComp->MoveSpeed = survivorComp->DownWalkSpeed;
		
		return;
	}


	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_DownWalk(UpdateCompParams components,float delta)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);


	if (playerContComp->Axis.x == 0 &&
		playerContComp->Axis.z == 0)
	{
		// 待機
		playerComp->ActionState = SurvivorComponent::DOWN_WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Crawls around Wait", 0, 10, true);
		return;
	}

	// カメラの向いている方向に移動
	ZMatrix& mat = transComp->Transform;
	GameCamera& cam = cameraComp->Cam;
	ZVec3 vTar;
	vTar += cam.m_LocalMat.GetXAxis()*playerContComp->Axis.x;
	vTar += cam.m_LocalMat.GetZAxis()*playerContComp->Axis.z;
	vTar.y = 0;
	vTar.Normalize();
	float speed = playerComp->MoveSpeed * (delta*APP.GetFrameRate() / 1.0f);
	mat.Move(vTar * speed);

	ZVec3 vZ = mat.GetZAxis();
	vZ.Homing(vTar, 5);
	mat.SetLookTo(vZ, ZVec3::Up);

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_AccessStart(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

	if (!survivorComp->AccessFlag)
	{
		// アクセス終了
		playerComp->ActionState = SurvivorComponent::ACCESS_END;
		animatorComp->Animator->ChangeAnimeSmooth("HackEnd", 0, 10, false);
		return;
	}

	if (animatorComp->Animator->IsAnimationend())
	{
		// アクセス
		playerComp->ActionState = SurvivorComponent::ACCESS;
		animatorComp->Animator->ChangeAnimeSmooth("Hacking", 0, 10, true);

		return;
	}

	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_Access(UpdateCompParams components)
{

	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

	if (!survivorComp->AccessFlag)
	{
		// アクセス終了
		playerComp->ActionState = SurvivorComponent::ACCESS_END;
		animatorComp->Animator->ChangeAnimeSmooth("HackEnd", 0, 10, false);
			
		return;
	}

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_AccessEnd(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);

	if (animatorComp->Animator->IsAnimationend())
	{
		// 待機
		playerComp->ActionState = SurvivorComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);

		return;
	}

	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_Cure(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

	// 治療中にボタンを離すか治療完了していたら
	if (!(playerContComp->Button & PlayerControllerComponent::Access_Stay) ||
		survivorComp->FinishCure)
	{
		// しゃがみ
		playerComp->ActionState = SurvivorComponent::SQUAT_WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("SitDownWait", 0, 10, true);
		survivorComp->FinishCure = false;
		return;
	}

	// 治療中に移動、立ち上がった場合治療中断
	if (playerContComp->Axis.x != 0 ||
		playerContComp->Axis.z != 0)
	{
		if (survivorComp->SquatFlag)
		{
			// しゃがみ歩き
			playerComp->ActionState = SurvivorComponent::SQUAT_WALK;
			animatorComp->Animator->ChangeAnimeSmooth("SitDownWalk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->SquatWalkSpeed;
		}
		else if (playerContComp->Button & PlayerControllerComponent::Run)
		{
			// 走り
			playerComp->ActionState = SurvivorComponent::RUN;
			animatorComp->Animator->ChangeAnimeSmooth("Run", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->RunSpeed;
		}
		else
		{
			// 歩き
			playerComp->ActionState = SurvivorComponent::WALK;
			animatorComp->Animator->ChangeAnimeSmooth("Walk", 0, 10, true);
			playerComp->MoveSpeed = survivorComp->WalkSpeed;
		}
		return;
	}
	else if (!survivorComp->SquatFlag)
	{
		// 待機
		playerComp->ActionState = SurvivorComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		return;
	}

	// 重力
	playerComp->vMove.y -= playerComp->Gravity;
	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_GetUp(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);

	if (animatorComp->Animator->IsAnimationend())
	{
		// 待機
		playerComp->ActionState = SurvivorComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);

		return;
	}

	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_DestoryObjStart(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);


	// 消去中断
	if (!(playerContComp->Button & PlayerControllerComponent::Access_Stay))
	{
		auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

		// 待機
		playerComp->ActionState = SurvivorComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		survivorComp->DestroyObjMode = true;
		return;
	}

	if (animatorComp->Animator->IsAnimationend())
	{
		playerComp->ActionState = SurvivorComponent::DESTORY_OBJ;
		animatorComp->Animator->ChangeAnimeSmooth("Hacking", 0, 10, false);
		return;
	}


	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_DestoryObj(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	

	// 消去中断
	if (!(playerContComp->Button & PlayerControllerComponent::Access_Stay))
	{
		auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

		// 待機
		playerComp->ActionState = SurvivorComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		survivorComp->DestroyObjMode = true;
		return;
	}

	// 消去
	if (animatorComp->Animator->IsAnimationend())
	{
		auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

		if (survivorComp->DestroyObj.IsActive())
		{
			auto destroyObj = survivorComp->DestroyObj.Lock();
			auto model = destroyObj->GetComponent<GameModelComponent>();
			
			model->RenderFlg->Delete = true;
			survivorComp->DestroyObjCnt++;
			survivorComp->DestroyObjMode = false;
			survivorComp->DestroyObj.Reset();

			// 待機
			playerComp->ActionState = SurvivorComponent::DESTORY_OBJ_END;
			animatorComp->Animator->ChangeAnimeSmooth("HackEnd", 0, 10, false);
			return;
		}
	}

	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::Action_DestoryObjEnd(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	
	// 終了
	if (animatorComp->Animator->IsAnimationend())
	{
		// 待機
		playerComp->ActionState = SurvivorComponent::WAIT;
		animatorComp->Animator->ChangeAnimeSmooth("Wait", 0, 10, true);
		return;
	}

	// 力による移動
	transComp->Transform.Move(playerComp->vMove);
}

void SurvivorUpdateSystem::DestroyObjMode(UpdateCompParams components)
{
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

	if (playerContComp->Button & PlayerControllerComponent::Access)
	{
		// 破壊実行
		auto cameraComp = GetCompFromUpdateParam(CameraComponent, components);
		auto hitObj = Make_Shared(Collider_Ray, appnew);

		// 基本設定
		hitObj->Init(0,
			HitGroups::_0, // 判定する側のフィルタ
			HitShapes::MESH,
			HitGroups::_0 // 判定される側のフィルタ
		);

		ZVec3 pos = transComp->Transform.GetPos();

		// 向いている方向ベクトル
		ZVec3 front = transComp->Transform.GetZAxis();
		front.Normalize();
		front *= 100;

		ZVec3 startPos = pos + ZVec3(0, 0.5, 0);	// レイの開始位置
		ZVec3 endPos = startPos + front;			// レイの終了位置

		hitObj->Set(startPos, endPos);

		// 自分のEntityのアドレスを仕込んでおく
		//hitObj->m_UserMap["Entity"] = transComp->m_Entity;

		// デバッグ用の色
		hitObj->m_Debug_Color.Set(0, 0, 1, 1);

		// 登録
		ColEng.AddAtk(hitObj);	// 判定する側

		hitObj->m_OnHitStay = [this, playerComp, animatorComp, survivorComp](const ZSP<ColliderBase>& hitObj)
		{
			ZSP<ECSEntity> nearObj = nullptr;
			float nearDist = FLT_MAX;

			// Hitしたやつら全て
			for (auto& res : hitObj->m_HitResTbl)
			{
				for (auto& node : res.HitDataList)
				{
					auto& youObj = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
					if (youObj == nullptr) continue;

					if (nearDist > node.RayDist)
					{
						nearObj = youObj;
						nearDist = node.RayDist;
					}
				}
			}

			if (nearObj != nullptr)
			{
				survivorComp->DestroyObj = nearObj;
				survivorComp->DestroyObjMode = false;
				playerComp->ActionState = SurvivorComponent::DESTORY_OBJ_START;
				animatorComp->Animator->ChangeAnimeSmooth("HackStart", 0, 10, false);

			}
		};

	}
}

void SurvivorUpdateSystem::SetCollision_Hit(UpdateCompParams components, const ZGM_RigidBodyData & rb)
{	
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);

	// ワールド行列
	ZMatrix m = rb.GetMatrix();
	m *= transComp->Transform;

	auto hitObj = Make_Shared(Collider_Sphere, appnew);

	// 基本設定
	hitObj->Init(0,
		HitGroups::_0 | HitGroups::_8, // 判定する側のフィルタ
		HitShapes::SPHERE | HitShapes::MESH | HitShapes::BOX,
		HitGroups::_0 | HitGroups::_8 // 判定される側のフィルタ
	);

	// スフィア情報
	hitObj->Set(m.GetPos(), rb.ShapeSize.x);

	// 質量
	hitObj->m_Mass = rb.Mass;

	// 自分のEntityのアドレスを仕込んでおく
	//hitObj->m_UserMap["Entity"] = transComp->m_Entity;

	// デバッグ用の色
	hitObj->m_Debug_Color.Set(0, 0, 1, 1);

	// 登録
	ColEng.AddAtk(hitObj);	// 判定する側
	ColEng.AddDef(hitObj);	// 判定される側

	// ヒット時に実行される
	hitObj->m_OnHitStay = [this, transComp](const ZSP<ColliderBase>& hitObj)
	{
		// Hitしたやつら全て
		for (auto& res : hitObj->m_HitResTbl)
		{
			for (auto& node : res.HitDataList)
			{
				// 相手のEntity
				//auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
				//if (youEntity == nullptr)continue;

				//// 自分自身は無視
				//if (transComp->m_Entity == youEntity)continue;

				float F = hitObj->CalcMassRatio(node.YouHitObj->m_Mass);
				transComp->Transform.Move(node.vPush*F);
			}
		}
	};
}

void SurvivorUpdateSystem::SetCollision_Cure(UpdateCompParams components, const ZGM_RigidBodyData & rb)
{
	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

	// ワールド行列
	ZMatrix m = rb.GetMatrix();
	m *= transComp->Transform;

	auto hitObj = Make_Shared(Collider_Sphere, appnew);

	// 基本設定
	hitObj->Init(0,
		HitGroups::_7, // 判定する側のフィルタ
		HitShapes::SPHERE | HitShapes::MESH | HitShapes::BOX,
		HitGroups::_7  // 判定される側のフィルタ
	);

	// スフィア情報
	hitObj->Set(m.GetPos(), rb.ShapeSize.x);

	// 自分のEntityのアドレスを仕込んでおく
	hitObj->m_UserMap["Entity"] = survivorComp->m_Entity;

	// デバッグ用の色
	hitObj->m_Debug_Color.Set(0, 1, 0, 1);

	// 治療する側
	if (survivorComp->SquatFlag &&
		survivorComp->DamageState != SurvivorComponent::DOWN)
	{
		// ボタンを押した瞬間かすでに治療状態の場合
		if(playerContComp->Button & PlayerControllerComponent::Access ||
			playerComp->ActionState == SurvivorComponent::CURE)
			ColEng.AddAtk(hitObj);
	}

	// 治療される側
	if (survivorComp->DamageState != SurvivorComponent::NORMAL)
	{
		// 負傷状態かつしゃがみもしくはダウン状態であるとき
		if (survivorComp->DamageState == SurvivorComponent::DAMAGE &&
			survivorComp->SquatFlag ||
			survivorComp->DamageState == SurvivorComponent::DOWN)
			ColEng.AddDef(hitObj);
	}

	hitObj->m_OnHitEnter = [this,transComp, playerComp, animatorComp, survivorComp](const ZSP<ColliderBase>& hitObj)
	{
		// Hitしたやつら全て
		for (auto& res : hitObj->m_HitResTbl)
		{
			for (auto& node : res.HitDataList)
			{
				// 相手のEntity
				auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
				if (youEntity == nullptr)continue;

				// 自分自身は無視
				if (survivorComp->m_Entity == youEntity)continue;

				
				if (playerComp->ActionState != SurvivorComponent::CURE)
				{
					// 治療状態に
					playerComp->ActionState = SurvivorComponent::CURE;
					animatorComp->Animator->ChangeAnimeSmooth("Treatment", 0, 10, true);

					// 相手のいる方向を向く
					auto youTransComp = youEntity->GetComponent<TransformComponent>();
					ZVec3 vZ = youTransComp->Transform.GetPos() - transComp->Transform.GetPos();
					transComp->Transform.SetLookTo(vZ, ZVec3::Up);

					
					EFFECT.SubmitPlayEffect(survivorComp->TreatmentEffect,
						youTransComp->Transform.GetPos(),
						ZEffectManager::OnPlayEffectFunction());
						
				}
			}
		}
	};

	// ヒット時に実行される
	hitObj->m_OnHitStay = [this, transComp,animatorComp, playerComp, survivorComp](const ZSP<ColliderBase>& hitObj)
	{
		// Hitしたやつら全て
		for (auto& res : hitObj->m_HitResTbl)
		{
			for (auto& node : res.HitDataList)
			{
				// 相手のEntity
				auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
				if (youEntity == nullptr)continue;

				// 自分自身は無視
				if (survivorComp->m_Entity == youEntity)continue;


				auto youSurvivorComp = youEntity->GetComponent<SurvivorComponent>();
				youSurvivorComp->CureGage++;


				// 治療完了
				if (youSurvivorComp->CureGage >= SurvivorComponent::MaxCure)
				{
					survivorComp->FinishCure = true;

					// リセット
					youSurvivorComp->CureGage = 0;

					if (youSurvivorComp->DamageState == SurvivorComponent::DAMAGE)
					{
						youSurvivorComp->DamageState = SurvivorComponent::NORMAL;

					}
					else if (youSurvivorComp->DamageState == SurvivorComponent::DOWN)
					{
						youSurvivorComp->DamageState = SurvivorComponent::DAMAGE;

						// シルエット消す
						auto youModelComp = youEntity->GetComponent<GameModelComponent>();
						youModelComp->RenderFlg->Character = false;

						// 起き上がり状態に
						auto youAnimatorComp = youEntity->GetComponent<AnimatorComponent>();
						auto youPlayerComp = youEntity->GetComponent<PlayerComponent>();
						youPlayerComp->ActionState = SurvivorComponent::GETUP;
						youAnimatorComp->Animator->ChangeAnimeSmooth("Crawls around from Stand", 0, 10, false);
					}
				}
			}
		}
	};
}

void SurvivorUpdateSystem::SetCollision_Access(UpdateCompParams components, const ZGM_RigidBodyData& rb)
{

	auto playerComp = GetCompFromUpdateParam(PlayerComponent, components);
	auto transComp = GetCompFromUpdateParam(TransformComponent, components);
	auto animatorComp = GetCompFromUpdateParam(AnimatorComponent, components);
	auto survivorComp = GetCompFromUpdateParam(SurvivorComponent, components);

	auto hitObj = Make_Shared(Collider_Box, appnew);

	// 基本設定
	hitObj->Init(0,
		HitGroups::_3, // 判定する側のフィルタ
		HitShapes::MESH | HitShapes::BOX,
		HitGroups::_3  // 判定される側のフィルタ
	);

	// ボックス情報
	hitObj->Set(rb.GetMatrix().GetPos(), rb.ShapeSize, transComp->Transform);

	// 自分のEntityのアドレスを仕込んでおく
	hitObj->m_UserMap["Entity"] = playerComp->m_Entity;

	// デバッグ用の色
	hitObj->m_Debug_Color.Set(0, 1, 0, 1);

	// 登録
	ColEng.AddAtk(hitObj);	// 判定する側

	hitObj->m_OnHitEnter = [this, playerComp, animatorComp,survivorComp](const ZSP<ColliderBase>& hitObj)
	{
		if (playerComp->ActionState != SurvivorComponent::ACCESS_START &&
			playerComp->ActionState != SurvivorComponent::ACCESS)
		{
			playerComp->ActionState = SurvivorComponent::ACCESS_START;
			animatorComp->Animator->ChangeAnimeSmooth("HackStart", 0, 10, false);

		}
	};


	hitObj->m_OnHitStay = [this,transComp, playerComp, animatorComp, survivorComp](const ZSP<ColliderBase>& hitObj)
	{
		// Hitしたやつら全て
		for (auto& res : hitObj->m_HitResTbl)
		{
			for (auto& node : res.HitDataList)
			{
				// 相手のEntity
				auto youEntity = node.YouHitObj->GetUserMap<ECSEntity>("Entity");
				if (youEntity == nullptr)continue;

				// アクセスフラグオン
				survivorComp->AccessFlag = true;

				// サーバー
				auto serverComp = youEntity->GetComponent<ServerComponent>();
				if (serverComp)
				{
					serverComp->NumAccess++;
					//DW_SCROLL(3, "Serveraccess");
					break;
				}

				// セキュリティ端末
				auto securityComp = youEntity->GetComponent<SecurityComponent>();
				if (securityComp)
				{
					//DW_SCROLL(3, "Securityaccess");

					securityComp->NumAccess++;
					survivorComp->AccessTime++;

					// 端末アクセスがスキルチェックの発生間隔に達したら
					if (survivorComp->AccessTime >= SurvivorComponent::SkillCheckInterval)
					{
						// 確率でスキルチェック発生
						if (RAND.GetInt(100) < SurvivorComponent::SkillCheckProbability)
						{
							survivorComp->SkillCheckTime = SurvivorComponent::SkillCheckTimeLimit;
							survivorComp->SkillCheckKey = 'A' + RAND.GetInt(26); // キーコード（A~Z）
							
							auto securityTransComp = youEntity->GetComponent<TransformComponent>();

							// スキルチェック
							auto task = [securityTransComp,survivorComp,securityComp]()mutable
							{
								//DW_STATIC(5, "SkillCheckTime %f", survivorComp->SkillCheckTime);
								//DW_STATIC(6, "SkillCheckKey %c", survivorComp->SkillCheckKey);

								survivorComp->SkillCheckTime--;
								if (survivorComp->SkillCheckTime <= 0)
								{
									//DW_STATIC(4, "SkillCheck Failed");

									// スキルチェック失敗
									survivorComp->SuccessSkillCheck = false;

									// エフェクト（仮置き）
									auto* testEffect = EFFECT.LoadEffect("data/Effect/Error/Error.efk");
									auto effectHandle = EFFECT.SubmitPlayEffect(testEffect,
										securityTransComp->Transform.GetPos(),
										ZEffectManager::OnPlayEffectFunction());

									// 失敗したらゲージを減らす
									securityComp->Gage -= SecurityComponent::DecreaseGage;
									if (securityComp->Gage < 0)
										securityComp->Gage = 0;
									return false;
								}

								// キーチェック
								if (INPUT.KeyEnter(survivorComp->SkillCheckKey))
								{
									//DW_STATIC(4, "SkillCheck Success");

									// スキルチェック成功
									survivorComp->SuccessSkillCheck = true;
									survivorComp->SkillCheckTime = 0;

									return false;
								}

								return true;
							};

							m_CTM.AddClosure("SkillCheck", task);
							
						}

						// 端末アクセス時間リセット
						survivorComp->AccessTime = 0;
					}

					break;
				}
			}
		}
	};
}

PlayerUpdateSystem::PlayerUpdateSystem()
{
	Init();
	m_DebugSystemName = "PlayerUpdateSystem";
}

void PlayerUpdateSystem::UpdateComponents(float delta, UpdateCompParams components)
{
	auto playerComp = GetCompFromUpdateParam(PlayerComponent,components);
	auto cameraComp = GetCompFromUpdateParam(CameraComponent,components);
	auto playerContComp = GetCompFromUpdateParam(PlayerControllerComponent,components);

	if (INPUT.KeyEnter('P'))
	{
		cameraComp->Cam.m_IsFPSMode = !cameraComp->Cam.m_IsFPSMode;
	}
	if (INPUT.KeyEnter('O'))
	{
		playerContComp->Enable = !playerContComp->Enable;
		cameraComp->Enable = !cameraComp->Enable;
	}

	// カメラ更新
	if (cameraComp->Enable)
		cameraComp->Cam.Update();
	
	// 値をリセット
	playerContComp->Axis.Set(0, 0, 0);
	playerContComp->Button = 0;
	// 無効時
	if (!playerContComp->Enable)return;

	// キーボード入力データ
	if (INPUT.KeyCheck('W', ZInput::Stay))	playerContComp->Axis.z = 1;
	if (INPUT.KeyCheck('S', ZInput::Stay))	playerContComp->Axis.z = -1;
	if (INPUT.KeyCheck('A', ZInput::Stay))	playerContComp->Axis.x = -1;
	if (INPUT.KeyCheck('D', ZInput::Stay))	playerContComp->Axis.x = 1;

	// ボタン
	if (INPUT.KeyCheck(VK_LBUTTON,ZInput::Enter)) playerContComp->Button |= PlayerControllerComponent::Access;
	if (INPUT.KeyCheck(VK_LBUTTON, ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::Access_Stay;
	if (INPUT.KeyCheck(VK_SHIFT, ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::Run;
	if (INPUT.KeyCheck(VK_CONTROL, ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::Squat;
	if (INPUT.KeyCheck('E', ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::Skill;
	if (INPUT.KeyCheck(VK_RBUTTON, ZInput::Enter)) playerContComp->Button |= PlayerControllerComponent::Action;
	if (INPUT.KeyCheck(VK_RBUTTON, ZInput::Stay)) playerContComp->Button |= PlayerControllerComponent::Action_Stay;
	
	if (INPUT.KeyCheck(VK_RETURN, ZInput::Enter)) playerContComp->Button |= PlayerControllerComponent::START;

}

void PlayerUpdateSystem::LateUpdateComponents(float delta, UpdateCompParams components)
{
	
}

void PlayerUpdateSystem::DebugImGuiRender()
{
	ECSSystemBase::DebugImGuiRender();
}
