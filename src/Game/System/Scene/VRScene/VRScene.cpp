#include "MainFrame/ZMainFrame.h"
#include "../../CommonECSComponents/CommonComponents.h"
#include "../../TestECSComponents/TestComponents.h"
#include "../../TestECSSystems/TestSystems.h"
#include "VRScene.h"

void VRScene::Release()
{
	// エンティティ削除
	// ※ 削除方法について詳しくはECSEnity.h参照
	ECSEntity::RemoveAllEntity(m_Entities);
	m_Entities.shrink_to_fit();
	m_PhysicsWorld->Release();

	// システム削除
	m_UpdateSystems.Release();
	m_DrawSystems.Release();
}

void VRScene::Init()
{
	DW_SCROLL(0, "VRシーン初期化");

	std::string error;
	const std::string initFile = "data/Scene/VRScene/init.json";
	json11::Json json = LoadJsonFromFile(initFile, error);
	float VRControllerScale = 0.1f;
	if(error.size() != 0)
		DW_SCROLL(2, "json読み込みエラー(%s)", initFile.c_str());
	else
		VRControllerScale = (float)json["VRControllerScale"].number_value();
	
	m_PhysicsWorld = Make_Shared(ZPhysicsWorld,sysnew);

	m_PhysicsWorld->Init();

	// 平行サイト作成
	m_DirLight = ShMgr.m_LightMgr.GetDirLight();

	// 平行光源設定
	m_DirLight->SetData
	(
		ZVec3(1, -1, -0.5f),		// ライト方向
		ZVec4(0.8f, 0.8f, 0.8f, 1)	// 基本色
	);

	// 環境色
	ShMgr.m_LightMgr.m_AmbientLight.Set(0.2f, 0.2f, 0.2f);

	// カメラ初期化
	m_Cam.Init(0, 0, -3);
	m_Cam.m_BaseMat.SetPos(0, 1.2f, 0);
	m_VRCam.Init();
	m_VRCam.m_CamBaseMat.SetPos(0, 0, -3);
	

	//// MMDモデル描画(PMXテスト PMDは後回し)
	//{
	//	GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
	//	CharaComponent* characomp = ECS.MakeComponent<CharaComponent>();
	//	characomp->Name = "Alicia_solid";

	//	auto model = APP.m_ResStg.LoadMesh("data/Model/Alicia/MMD/Alicia_solid.pmx");
	//	modelcomp->Model = model;

	//	TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();

	//	ModelBoneControllerComponent* bonecontrollercomp = ECS.MakeComponent<ModelBoneControllerComponent>();
	//	AnimatorComponent* animatorcomp = ECS.MakeComponent<AnimatorComponent>();
	//	bonecontrollercomp->BoneController = Make_Shared(ZBoneController,appnew);
	//	animatorcomp->Animator = Make_Shared(ZAnimator,appnew);

	//	bonecontrollercomp->BoneController->SetModel(modelcomp->Model);
	//	bonecontrollercomp->BoneController->AddAllPhysicsObjToPhysicsWorld(*m_PhysicsWorld.GetPtr());

	//	ZSP<ZAnimationSet> animeSet = Make_Shared(ZAnimationSet,appnew);

	//	animeSet->LoadVMD("data/Model/Alicia/MMD Motion/2分ループステップ1.vmd", *bonecontrollercomp->BoneController, "モーション0");
	//	modelcomp->Model->GetAnimeList().push_back(animeSet);

	//	bonecontrollercomp->BoneController->InitAnimator(*animatorcomp->Animator);

	//	animatorcomp->Animator->ChangeAnime("モーション0", true);
	//	animatorcomp->Animator->EnableRootMotion(true);

	//	auto entity = ECS.MakeEntity(transcomp, modelcomp, bonecontrollercomp, animatorcomp, characomp);
	//	m_Entities.push_back(entity);
	//}


	//VRControllerモデル
	auto VRControllerModel = APP.m_ResStg.LoadMesh("data/Model/VRController/VRController.xed");

	//VRController
	// L
	{
		GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
		modelcomp->Model = VRControllerModel;

		VRControllerComponent * vrcomp = ECS.MakeComponent<VRControllerComponent>();

		vrcomp->m_VRCon.Init(CVR::L);
		TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();

		vrcomp->MScale.CreateScale(ZVec3(VRControllerScale)); // 適当にサイズ調整

		auto entity = ECS.MakeEntity(transcomp, vrcomp, modelcomp);
		m_Entities.push_back(entity);
	}

	//VRController
	// R
	{
		GameModelComponent* modelcomp = ECS.MakeComponent<GameModelComponent>();
		modelcomp->Model = VRControllerModel;

		VRControllerComponent * vrcomp = ECS.MakeComponent<VRControllerComponent>();

		vrcomp->m_VRCon.Init(CVR::R);
		TransformComponent* transcomp = ECS.MakeComponent<TransformComponent>();

		vrcomp->MScale.CreateScale(ZVec3(VRControllerScale)); // 適当にサイズ調整

		auto entity = ECS.MakeEntity(transcomp, vrcomp, modelcomp);
		m_Entities.push_back(entity);
	}

	// システム準備
	m_UpdateSystems.AddSystem(Make_Shared(MoveUpdateSystem,sysnew));
	m_UpdateSystems.AddSystem(Make_Shared(BoxSpawnerSystem,sysnew,m_Entities));
	m_UpdateSystems.AddSystem(Make_Shared(AnimationUpdateSystem,sysnew));
	m_UpdateSystems.AddSystem(Make_Shared(PhysicsSystem, sysnew,m_PhysicsWorld));
	m_UpdateSystems.AddSystem(Make_Shared(CharaDebugSystem,sysnew));
	m_UpdateSystems.AddSystem(Make_Shared(VRControllSystem,sysnew));
	m_DrawSystems.AddSystem(Make_Shared(StaticMeshDrawSystem,sysnew));
	m_DrawSystems.AddSystem(Make_Shared(SkinMeshDrawSystem,sysnew));
}

void VRScene::Update()
{
	DW_STATIC(1, "VR_Scene");

	// Spaceでテストシーンへ
	if(INPUT.KeyEnter(VK_SPACE))
	{
		APP.m_SceneMgr.ChangeScene("Test");
		return;
	}

	// Escで終了
	if (INPUT.KeyEnter(VK_ESCAPE))
	{
		APP.ExitGameLoop();
		return;
	}

	//カメラ操作
	if (cvr.GetVRSystem() != nullptr)
		m_VRCam.Update();
	else
		m_Cam.Update();

	cvr.Update();
	ECS.UpdateSystems(m_UpdateSystems, APP.m_DeltaTime);
	
	DW_STATIC(4, "Num Entites: %d", ECS.GetNumEntities());
}

void VRScene::ImGuiUpdate()
{
	//auto imguiFunc = [this]()
	//{
	//	for (auto& system : m_UpdateSystems)
	//		system->DebugImGuiRender();
	//	
	//	for (auto& system : m_DrawSystems)
	//		system->DebugImGuiRender();
	//};
	//
	//DW_IMGUI_FUNC(imguiFunc);
}

void VRScene::Draw()
{
	// 半透明モード
	ShMgr.m_bsAlpha.SetState();

	// AlphaToCoverage付き半透明(これをしないと透明部分も描画される)
	ShMgr.m_bsAlpha_AtoC.SetState();

	//描画用のラムダ式
	std::function<void()> drawModel = [&]() 
	{
		// 描画直前にシェーダのフレーム単位データを更新する
		// 主にカメラやライトのデータをシェーダ側に転送する
		{
			// カメラ設定& シェーダに書き込み
			if (cvr.GetVRSystem())m_VRCam.SetCamera();
			else m_Cam.SetCamera();
			// ライト情報をシェーダ側に書き込む
			ShMgr.m_LightMgr.Update();
		}

		// [3D]モデル描画
		ECS.UpdateSystems(m_DrawSystems, APP.m_DeltaTime, true);

		// 物理エンジンのデバッグ描画
		m_PhysicsWorld->DebugDraw();

		ShMgr.m_Ls.Flash();
		ShMgr.m_Ms.m_SMeshRenderer->Flash();
		ShMgr.m_Ms.m_InstSMeshRenderer->Flash();
		ShMgr.m_Ms.m_SkinMeshRenderer->Flash();
	};


	if (cvr.GetVRSystem())	//HMDがある場合、左目・右目・通常で描画する
	{
		CVR::RenderMode rm[] = {CVR::RM_Left,CVR::RM_Right,CVR::RM_Center};

		//VRデバイス用に左目、右目、通常画面の3回描画を行う
		for (auto& i : rm)
		{
			cvr.BeginRender(i);
			drawModel();
			cvr.EndRender();
		}

		cvr.Submit(vr::EVREye::Eye_Left);
		cvr.Submit(vr::EVREye::Eye_Right);
		cvr.WaitGetPoses();
	}
	else
		drawModel();		//HMDがない場合、通常で描画する

}
