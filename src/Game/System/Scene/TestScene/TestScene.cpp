#include "Game/Application.h"
#include "../../CommonECSComponents/CommonComponents.h"
#include "../../TestECSComponents/TestComponents.h"
#include "../../TestECSSystems/TestSystems.h"

#include "../../MapObject/MapObjectComponents/MapObjectComponents.h"
#include "../../MapObject/MapObjectSystems/MapObjectSystems.h"

#include "../../Character/CharacterComponents/CharacterComponents.h"
#include "../../Character/CharacterSystems/CharacterSystems.h"

#include "../../CommonECSComponents/CommonListener.h"

#include "../../GameWorld/GameWorld.h"

#include "TestScene.h"

void TestScene::Release()
{
	//m_NowCamera = nullptr;

	m_CullingOctreeSpace.Release();

	//// エンティティ削除
	//// ※ 削除方法について詳しくはECSEnity.h参照
	//ECS.RemoveAllListener();
	//ECSEntity::RemoveAllEntity(m_Entities);
	//m_Entities.shrink_to_fit();
	//m_PostEffects.SaveSettingToJson("data/Scene/HkScene/PostState.json");

	GW.Release();

	// システム削除
	m_UpdateSystems.Release();
	m_DrawSystems.Release();
}

void TestScene::Init()
{
	DW_SCROLL(0, "タイトルシーン初期化");
	

	// 平行サイト作成
	m_DirLight = ShMgr.m_LightMgr.GetDirLight();

	// 平行光源設定
	m_DirLight->SetData
	(
		ZVec3(1, -1, -0.5f),		// ライト方向
		ZVec4(0.2f, 0.2f, 0.2f, 1)	// 基本色
	);

	// 環境色
	ShMgr.m_LightMgr.m_AmbientLight.Set(0.0f, 0.0f, 0.0f);


	
	// カメラ初期化
	m_Cam.Init(0, 0, -3);
	m_Cam.m_BaseMat.SetPos(0, 1.2f, 0);

	// カリング用8分木初期化
	const uint splitLevel = 5;
	m_CullingOctreeSpace.Init(splitLevel, ZAABB(-50000, 50000)); // 範囲は適当

	// コリジョンワールド デフォルト値で初期化
	ColEng.Init(1);

	// MMDモデル描画(PMXテスト PMDは後回し)
	{
		ECS.RegisterClassRefrection<GameModelComponent>("GameModel");
		ECS.RegisterClassRefrection<ModelBoneControllerComponent>("ModelBoneController");
		ECS.RegisterClassRefrection<AnimatorComponent>("Animator");
		ECS.RegisterClassRefrection<TransformComponent>("Transform");
		ECS.RegisterClassRefrection<CameraComponent>("Camera");
		ECS.RegisterClassRefrection<PlayerComponent>("Player");
		ECS.RegisterClassRefrection<PlayerControllerComponent>("PlayerController");
		ECS.RegisterClassRefrection<SurvivorComponent>("Survivor");
		ECS.RegisterClassRefrection<KillerComponent>("Killer");
		ECS.RegisterClassRefrection<SecurityComponent>("Security");
		ECS.RegisterClassRefrection<ServerComponent>("Server");
		ECS.RegisterClassRefrection<ColliderComponent>("Collider");

		m_ModelBoneControllerListener = Make_Shared(ModelBoneControllerListener,appnew);
		ECS.AddListener(m_ModelBoneControllerListener.GetPtr());

		// キラー
		//ECSEntity::CreateEntityFromJsonFile("data/Json/killer.json", &m_Entities);
		ECSEntity::CreateEntityFromJsonFile("data/Json/killer.json", &GW.m_Entities);

		// サバイバー
		ECSEntity::CreateEntityFromJsonFile("data/Json/survivor.json", &GW.m_Entities);
		// セキュリティ
		ECSEntity::CreateEntityFromJsonFile("data/Json/security.json", &GW.m_Entities);
		
		// サーバー
		ECSEntity::CreateEntityFromJsonFile("data/Json/server.json", &GW.m_Entities);
		
		// マップ
		//ECSEntity::CreateEntityFromJsonFile("data/Json/testmap.json",&m_Entities);
		ECSEntity::CreateEntityFromJsonFile("data/Json/Wilderness.json", &GW.m_Entities);

	}


	GW.Init();

	//	ポストエフェクトを作成
	//m_PostEffects.Init();
	//m_PostEffects.LoadSettingFromJson("data/Scene/HkScene/PostState.json");
	//// EffectPass追加
	//{
	//	auto* setupPass = appnew(SetupPass);
	//	auto* dofPass = appnew(DOFPass);
	//	auto* xRayPass = appnew(XRayPass);
	//	auto* lightBloomPass = appnew(LightBloomPass);
	//	setupPass->Init();
	//	dofPass->Init();
	//	xRayPass->Init();
	//	lightBloomPass->Init();
	//	m_PostEffects.AddEffectPass("Setup", setupPass);
	//	m_PostEffects.AddEffectPass("DoF", dofPass);
	//	m_PostEffects.AddEffectPass("XRay", xRayPass);
	//	m_PostEffects.AddEffectPass("LightBloom", lightBloomPass);
	//}
	

	//
	m_SkyTex = APP.m_ResStg.LoadTexture("data/Texture/title_back.png");


	ShMgr.m_Blur.CreateMipTarget(ZVec2((float)APP.m_Window->GetWidth(), (float)APP.m_Window->GetHeight()));


	// システム準備

	m_UpdateSystems.AddSystem(Make_Shared(PlayerUpdateSystem, appnew));
	m_UpdateSystems.AddSystem(Make_Shared(KillerUpdateSystem, appnew));
	m_UpdateSystems.AddSystem(Make_Shared(SurvivorUpdateSystem, appnew));
	m_UpdateSystems.AddSystem(Make_Shared(ColliderSystem,appnew));


	m_UpdateSystems.AddSystem(Make_Shared(SecurityUpdateSystem,appnew));
	m_UpdateSystems.AddSystem(Make_Shared(ServerUpdateSystem,appnew));

	m_UpdateSystems.AddSystem(Make_Shared(AnimationUpdateSystem,appnew));
	m_UpdateSystems.AddSystem(Make_Shared(PhysicsSystem,appnew,APP.m_pPhysicsWorld));
	//m_UpdateSystems.AddSystem(Make_Shared(CharaDebugSystem,appnew));
	//m_DrawSystems.AddSystem(Make_Shared(StaticMeshDrawSystem,appnew));
	//m_DrawSystems.AddSystem(Make_Shared(TestDrawSystem, appnew));
	m_DrawSystems.AddSystem(Make_Shared(TestSubmitCullingSpaceSystem, appnew,m_CullingOctreeSpace));
	m_DrawSystems.AddSystem(Make_Shared(SkinMeshDrawSystem,appnew));

}

void TestScene::Update()
{
	DW_STATIC(1, "PerformanceTest_Scene");

	// EscでVRSceneへ
	if (INPUT.KeyEnter(VK_ESCAPE))
	{
	//	APP.m_SceneMgr.ChangeScene("VR");
		APP.ExitGameLoop();
		return;
	}

	// 当たり判定の準備
	ColEng.ClearList();

	//カメラ操作
	m_Cam.Update();
	ECS.UpdateSystems(m_UpdateSystems,APP.m_DeltaTime);

	ColEng.Run();
	
	DW_STATIC(3, "Num Entites: %d", ECS.GetNumEntities());
}

void TestScene::ImGuiUpdate()
{
	GW.m_PostEffects.ImGui();
	auto imGuiFunc = [this]
	{
		if (ImGui::Begin("SystemInfo") == false)
		{
			ImGui::End();
			return;
		}

		// 物理エンジン
		{
			bool isEnablePhysicsDebug = PHYSICS.IsEnableDebugDraw();
			ImGui::Checkbox("Physics Debug Draw", &isEnablePhysicsDebug);
			PHYSICS.SetDebugDrawMode(isEnablePhysicsDebug);
			ImGui::Separator();
		}

		// 各システム
		auto debugImGui = [](ZSP<ECSSystemBase> system)
		{
			ImGui::Text(system->GetSystemName().c_str());
			system->DebugImGuiRender();
			ImGui::Separator();
		};

		for (auto system : m_UpdateSystems)
		{
			if (system->GetSystemName().empty())
				continue;

			debugImGui(system);
		}

		for (auto system : m_DrawSystems)
		{
			if (system->GetSystemName().empty())
				continue;

			debugImGui(system);
		}

		ImGui::End();
	};

	DW_IMGUI_FUNC(imGuiFunc);


	RENDERER.ImGui();
	ShMgr.m_Blur.ImGui();
	ShMgr.m_LightMgr.ImGui();

}

void TestScene::Draw()
{

	//	3D描画開始
	RENDERER.Begin3DRendering();
	{
		// 半透明モード
		ShMgr.m_bsAlpha.SetState();
	
		// AlphaToCoverage付き半透明(これをしないと透明部分も描画される)
		ShMgr.m_bsAlpha_AtoC.SetState();
	
		// カメラやライトのデータをシェーダ側に転送する
		{
			for (auto& entity : GW.m_Entities)
			{
				if (entity->IsActive() == false)
					continue;
				auto camComp = entity->GetComponent<CameraComponent>();
				auto contComp = entity->GetComponent<PlayerControllerComponent>();
				if (camComp)
				{
					if (contComp->Enable)
					{
						GW.m_NowCamera = camComp;
						break;
					}
				}
			}
	
			if (GW.m_NowCamera)
			{
				INPUT.SetFPSMode(APP.m_Window->GetWindowHandle(), GW.m_NowCamera->Cam.m_IsFPSMode);
	
				GW.m_NowCamera->Cam.SetCamera();
	
			}
			else
			{
				m_Cam.SetCamera();
	
				// シャドウマップ用
				//ShMgr.SetShadowCamTargetPoint(m_Cam.mCam.GetPos());	//	現在のカメラをセット
			}
	
			// ライト情報をシェーダ側に書き込む
			ShMgr.m_LightMgr.Update();
			//	モデルシェーダの固定定数を書き込み
			ShMgr.m_Ms.SetConstBuffers();
		}
	
		// [2D]背景描画
		{
			//ShMgr.m_Ss.Begin(false, true);
			//ShMgr.m_Ss.Draw2D(m_SkyTex->GetTex(), 0, 0, 1280, 720);
			//ShMgr.m_Ss.End();
		}
	
		// [3D]モデル描画
		ECS.UpdateSystems(m_DrawSystems, APP.m_DeltaTime, true);
	
		ZVector<GameModelComponent*> drawModels;
		GW.m_NowCamera->Cam.UpdateFrustumPlanes();
		m_CullingOctreeSpace.GetCollisionList(GW.m_NowCamera->Cam.GetAABB(), drawModels);

		for (auto model : drawModels)
		{
			auto* trans = model->m_Entity->GetComponent<TransformComponent>();
			auto aabbCenter = model->Model->GetAABB_Center();
			auto aabbHalfSize = model->Model->GetAABB_HalfSize();
			ZAABB AABB(aabbCenter - aabbHalfSize, aabbCenter + aabbHalfSize);
			AABB.Transform(trans->Transform);

			if(model->RenderFlg->FrustumCheck == false) // 視錐台カリング無効 -> 判定なしで強制的に描画
				RENDERER.m_InstSModelRenderer->Submit(&trans->Transform, model);
			else if (GW.m_NowCamera->Cam.IsInsideTheFrustum(AABB))
				RENDERER.m_InstSModelRenderer->Submit(&trans->Transform,model);
		}

		// 物理エンジンのデバッグ描画
		//m_PhysicsWorld->DebugDraw();
		if (PHYSICS.IsEnableDebugDraw())
			ColEng.DebugDraw(1.0f);
		
		// シャドウマップ用
		ShMgr.SetShadowCamTargetPoint(GW.m_NowCamera->Cam.mCam.GetPos());	//	現在のカメラをセット
		//	シャドウマップ描画	
		RENDERER.DrawShadow();
	
		//	3D描画
		ShMgr.m_Ls.Flash();
		ShMgr.m_Ms.Draw();
		//	3Dエフェクト描画
	
		// ~~~~~~
	}	//	3D描画終了
	RENDERER.End3DRendering();
	
	//	ポストエフェクト
	GW.m_PostEffects.RenderPostEffects(RENDERER.m_RenderTargets->GetTexZSP(0));

	m_CullingOctreeSpace.Clear();
}
