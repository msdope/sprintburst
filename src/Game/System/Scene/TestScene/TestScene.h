#ifndef __TITLE_SCENE_H__
#define __TITLE_SCENE_H__

#include "Game/Camera/GameCamera.h"
#include "Game/System/Character/CharacterComponents/CharacterComponents.h"
#include "Game/System/CommonECSComponents/CommonListener.h"

struct DirLight;

class TestScene : public ZSceneBase
{
public:
	virtual ~TestScene()
	{
		Release();
	}

	// 初期化
	virtual void Init()override;
	// 更新
	virtual void Update()override;
	// ImGui更新
	virtual void ImGuiUpdate()override;
	// 描画
	virtual void Draw()override;

	// 解放
	void Release();

public:
	// その他
	//ZAVector<ZSP<ECSEntity>> m_Entities; // エンティティハンドル

	ZSP<ModelBoneControllerListener> m_ModelBoneControllerListener;

	// 平行光源
	ZSP<DirLight> m_DirLight;

	// カメラ
	GameCamera m_Cam;
	//CameraComponent* m_NowCamera;

	// テクスチャ
	ZSP<ZTexture> m_texBack;
	
	ZSP<ZPhysicsWorld> m_PhysicsWorld;
	
	// カリング用
	ZOctree<GameModelComponent> m_CullingOctreeSpace;


	//	ポストエフェクト用(宣言はどこかシングルトンクラス内に作った方がいいけど、テスト的にここで)
	//PostEffects				m_PostEffects;

	//---------------------------------------------------------------------------
	ZSP<ZTexture> m_SkyTex;	//	テスト
};

#endif