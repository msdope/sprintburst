#ifndef __RENDER_STATE_H__
#define __RENDER_STATE_H__

struct Object3D_RenderFlgs
{
	bool SemiTransparent	= false;	//	trueなら半透明部分が存在するオブジェクトなのでBasePassで描画しない
	bool Character			= false;	//	trueならプレイアブルキャラクターなので、X-Passを実行する
	bool BaseFrustumOut		= false;	//	trueならベースパスの視錐台の外なのでシャドウ以外は描画しない
	bool NoneShadowRender	= false;	//	trueならBasePass内でピクセルシェーダを切り替えて、シャドウマップの計算を行わない(軽量化フラグで例えば遠景のオブジェクトに使用)
	bool Delete				= false;	//　消去 
	float Alpha				= 1.0f;		//　モデル全体のアルファ値
	ZVec4 XRayCol;
	bool FrustumCheck = true;
	bool DestoryObject = true;
};

#endif