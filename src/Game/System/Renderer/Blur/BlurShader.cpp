#include "Game/Application.h"


const float BlurShader::DefaultDisp = 5.f;
const int BlurShader::DefaultLoop	= 3;

BlurShader::BlurShader()
{
	
}

bool BlurShader::Init(const ZString & vsPath, const ZString & psPath)
{

	//	Pass1
	m_VS = APP.m_ResStg.LoadVertexShader("Shader/BlurVS_1.cso");
	m_PS = APP.m_ResStg.LoadPixelShader("Shader/BlurPS_1.cso");
	//	Pass2
	m_VS2 = APP.m_ResStg.LoadVertexShader("Shader/BlurVS_2.cso");
	m_PS2 = APP.m_ResStg.LoadPixelShader("Shader/BlurPS_2.cso");


	m_HIE_VS = APP.m_ResStg.LoadVertexShader("Shader/HIE_VS.cso");
	m_HIE_PS = APP.m_ResStg.LoadPixelShader("Shader/HIE_PS.cso");


//	m_SizeFlg = Quarter;
	//	ターゲット作成
	if (!CreateRenderTarget()) { return false; }

	//	リングバッファ作成
	m_RingBuf.Create(ZVertex_Pos_UV::GetVertexTypeData(), 4);

	//	定数バッファ作成
	m_Data.Create(0);
	m_View.Create(1);

	//	ガウスの重み計算
	ComputeGaussWeights(pow(DefaultDisp, 2));


	//	デバッグ描画用
#if _DEBUG
	m_DebugSave = Make_Shared(ZTextureSet,appnew);
	m_DebugSave->CreateRTSet(640, 360,10);
#endif

	return true;
}

void BlurShader::Release()
{
	m_Data.Release();
	m_View.Release();
	if(m_VS)m_VS->Release();
	if(m_PS)m_PS->Release();
	if(m_VS2)m_VS2->Release();
	if(m_PS2)m_PS2->Release();
	if(m_HIE_VS)m_HIE_VS->Release();
	if(m_HIE_PS)m_HIE_PS->Release();
	m_RingBuf.Release();

	ReleaseRenderTarget();
	DeleteMipTarget();

#if _DEBUG
	m_DebugSave->Release(); 
#endif
}

void BlurShader::CreateMipTarget(ZVec2 size)
{

	//	すでに作成済みなら戻る
	if (m_MipFlg) { return; }

	DeleteMipTarget();

	//	一旦解放
	m_MipTargets.clear();

	float ReducionVal = 1.f;

	//	オリジナルサイズでのMipTargetは実装しないので初期値に
	for (int i = Original; i <= Sixty; i++)
	{
		m_MipTargets[i] = Make_Shared(ZTextureSet,appnew);
		//	ターゲットを作成
		m_MipTargets[i]->CreateRTSet((int)(size.x*ReducionVal), (int)(size.y*ReducionVal),2);
		//	半分にする
		ReducionVal*=0.5f;
	}
	m_MipFlg = true;
}

void BlurShader::DeleteMipTarget()
{
	m_MipTargets.clear();
	m_MipFlg = false;
}

void BlurShader::ChangeTarget()
{

	float ReducionVal = CalcReductionVal(m_SizeFlg);

	//	ターゲットのサイズ
	m_RenderSize = ZVec2(APP.m_Window->GetWidth()*ReducionVal, APP.m_Window->GetHeight()*ReducionVal);

	//	テクセルサイズセット
	SetTargetSize(m_RenderSize.x, m_RenderSize.y);

	m_BlurTex->Release();

	m_BlurTex->SetTex(0, m_MipTargets[m_SizeFlg]->GetTexZSP(0));
	m_BlurTex->SetTex(1, m_MipTargets[m_SizeFlg]->GetTexZSP(1));

}

void BlurShader::ChangeRenderTarget(BlurRenderFlgs flg)
{
	m_SizeFlg = flg;

	if (m_MipFlg)
	{
		//	作成済みの場合はポインターを切り替えるだけ
		ChangeTarget();
	}
	else
	{
		//	Mipが作成されていない時はターゲットの作成から
		ReleaseRenderTarget();
		CreateRenderTarget();
	}
}

void BlurShader::SaveRTBegin()
{
#ifdef _DEBUG
	if(!m_DebugSaveFlg)
	{
		m_DebugSave->AllClearRT();
	}
	m_DebugSaveCnt = 0;
	m_DebugSaveFlg = true;
#endif
}

void BlurShader::SaveRTEnd()
{
#ifdef _DEBUG
	m_DebugSaveFlg = false;
#endif
}

void BlurShader::GenerateBlur(ZSP<ZTexture> tex, UINT loop, float disp)
{
	
	ComputeGaussWeights(pow(disp, 2));


	ZRenderTarget_BackUpper bu;

	//	クリア
	m_BlurTex->AllClearRT();

	//	
	m_RT.GetNowTop();
	m_RT.Depth(nullptr);

	m_RT.RT(0, m_BlurTex->GetTex(1)->GetRTTex());
	m_RT.SetToDevice();


	/**
	*	ReSize
	**/

	ShMgr.m_Ss.Begin(false, true);
	ShMgr.m_Ss.Draw2D(tex->GetTex(), 0, 0, m_RenderSize.x, m_RenderSize.y);
	ShMgr.m_Ss.End();


	/**
	*	Blur
	**/
	m_Data.m_Data.Size		= m_RenderSize;
	m_Data.m_Data.Offset	= ZVec2(16.f / m_RenderSize.x, 16.f / m_RenderSize.y);
	m_Data.SetVS();
	m_Data.SetPS();
	m_Data.WriteData();


	Begin();

	for (UINT i = 0; i < loop; i++) {
		// Pass1
		m_RT.RT(0, *m_BlurTex->GetTex(0));
		m_RT.SetToDevice();
		Draw2D(m_BlurTex->GetTexZSP(1), ZVec2::zero, m_RenderSize, *m_VS, *m_PS);

		// Pass2
		m_RT.RT(0, *m_BlurTex->GetTex(1));
		m_RT.SetToDevice();
		Draw2D(m_BlurTex->GetTexZSP(0), ZVec2::zero, m_RenderSize, *m_VS2, *m_PS2);

		// ReSize

	}

	End();


#if _DEBUG
	if (m_DebugSaveFlg)
	{
		if (m_DebugSaveCnt < 10)
		{
			m_RT.Depth(nullptr);
			m_RT.RT(0, m_DebugSave->GetTex(m_DebugSaveCnt)->GetRTTex());
			m_RT.SetToDevice();

			ShMgr.m_Ss.Begin(false, true);
			ShMgr.m_Ss.Draw2D(m_BlurTex->GetTexZSP(1)->GetTex(), 0, 0, 640, 360);
			ShMgr.m_Ss.End();

			++m_DebugSaveCnt;
		}
	}
#endif

}

void BlurShader::HIE(ZSP<ZTexture> base, ZSP<ZTexture> out, const ZVec2 & size)
{

	ZRenderTarget_BackUpper bu;

	//	変換行列作成
	{
		UINT pNumViewports = 1;
		D3D11_VIEWPORT vp;
		ZDx.GetDevContext()->RSGetViewports(&pNumViewports, &vp);

		ZMatrix p2DMat;
		p2DMat.CreateOrthoLH(size.x, size.y, 0, 1);
		p2DMat.Move(-1, -1, 0);
		p2DMat.Scale(1, -1, 1);
		m_View.m_Data.view = p2DMat;
		m_View.WriteData();
	}

	ZRenderTargets rt;
	rt.GetNowTop();
	rt.RT(0, out->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	//	レンダリングステート保存
	ZDepthStencilState	DSBackUp;
	ZBlendState			BSBackUp;
	{
		// 現状の各種ステート記憶
		DSBackUp.SetAll_GetState();
		BSBackUp.SetAll_GetState();

		// Z書き込み、判定無効化
		ZDepthStencilState ds;
		ds.Set_FromDesc(DSBackUp.GetDesc());
		ds.Set_ZEnable(false);
		ds.Set_ZWriteEnable(false);
		ds.SetState();

		// ブレンドステート
		ZBlendState bs;
		bs.Set_Alpha();
		bs.SetState();
	}

	//	シェーダセット
	m_HIE_VS->SetShader();
	m_HIE_PS->SetShader();	
	m_View.SetVS();
	m_View.SetPS();

	//	テクスチャセット
	base->SetTexturePS(0);



	// 頂点作成
	const int	 numVtx = 4;
	ZVertex_Pos_UV vertices[numVtx] =
	{
	{ ZVec3(0.f, size.y,0) 	 ,	ZVec2(0,1), },
	{ ZVec3(0.f,0.f, 0)      ,	ZVec2(0,0), },
	{ ZVec3(size.x,size.y, 0),	ZVec2(1,1), },
	{ ZVec3(size.x,0.f,	0)	 ,	ZVec2(1,0), }
	};


	//	描画
	m_RingBuf.SetDrawData();
	m_RingBuf.WriteAndDraw(vertices, numVtx);



	//	テクスチャ解除
	ZDx.RemoveTexturePS(0);


	//	ステート戻す
	BSBackUp.SetState();
	DSBackUp.SetState();

}

bool BlurShader::CreateRenderTarget()
{

	float ReductionVal = CalcReductionVal(m_SizeFlg);

	//	ターゲットのサイズ
	m_RenderSize = ZVec2(APP.m_Window->GetWidth()*ReductionVal, APP.m_Window->GetHeight()*ReductionVal);

	//	テクセルサイズセット
	SetTargetSize(m_RenderSize.x, m_RenderSize.y);


	//	ブラー用の作業用テクスチャ作成
	{
		m_BlurTex = Make_Shared(ZTextureSet,appnew,2);
		auto b1 = Make_Shared(ZTexture,appnew);
		if (!b1->CreateRT((int)m_RenderSize.x, (int)m_RenderSize.y)) {
			return false;
		}
		m_BlurTex->SetTex(0, b1);

		auto b2 = Make_Shared(ZTexture, appnew);
		if (!b2->CreateRT((int)m_RenderSize.x, (int)m_RenderSize.y)) {
			return false;
		}
		m_BlurTex->SetTex(1, b2);
	}

	return true;
}

void BlurShader::ReleaseRenderTarget()
{
	if (m_BlurTex)	{ m_BlurTex->Release(); }

	m_BlurTex	= nullptr;
}

void BlurShader::ComputeGaussWeights(float disp)
{

	const int	NumWeight	= 8;
	float		total		= 0.f;

	for (int i = 0; i < NumWeight; i++) {
		float pos = 1.f + 2.f * (float)i;
		m_Data.m_Data.weights[i] = expf(-0.5f*(pos*pos) / disp);
		total += 2.f*m_Data.m_Data.weights[i];
	}

	float invTotal = 1.0f / total;
	for (int i = 0; i < NumWeight; i++) {
		m_Data.m_Data.weights[i] *= invTotal;
	}
}

void BlurShader::Begin()
{

	/**
	*	2D射影作成
	**/

	UINT pNumViewports = 1;
	D3D11_VIEWPORT vp;
	ZDx.GetDevContext()->RSGetViewports(&pNumViewports, &vp);
	m_Proj2DMat.CreateOrthoLH(vp.Width, vp.Height, 0, 1);
	m_Proj2DMat.Move(-1, -1, 0);
	m_Proj2DMat.Scale(1, -1, 1);
	m_View.m_Data.view = m_Proj2DMat;
	m_View.WriteData();

	/**
	*	レンダリングステート
	**/

	// 現状の各種ステート記憶
	m_Save.DSBackUp.SetAll_GetState();
	m_Save.BSBackUp.SetAll_GetState();

	// Z書き込み、判定無効化
	ZDepthStencilState ds;
	ds.Set_FromDesc(m_Save.DSBackUp.GetDesc());
	ds.Set_ZEnable(false);
	ds.Set_ZWriteEnable(false);
	ds.SetState();	//	セット

	// ブレンドステート
	ZBlendState bs;
	bs.Set_Alpha();
	bs.SetState();

}

void BlurShader::End()
{
	// ステート復元
	m_Save.BSBackUp.SetState();
	m_Save.DSBackUp.SetState();
}

void BlurShader::Draw2D(ZSP<ZTexture> tex, ZVec2 pos, ZVec2 size, ZVertexShader& vs, ZPixelShader& ps)
{

	// テクスチャ
	if (!tex)return;

	vs.SetShader();
	ps.SetShader();
	SetConstantBuffers();


	tex->SetTexturePS(0);

	// 頂点作成
	const int	 numVtx = 4;
	float		x2		= pos.x + size.x;
	float		y2		= pos.y + size.y;

	ZVertex_Pos_UV vertices[numVtx] =
	{
		{ ZVec3( pos.x,		y2,	0)	,	ZVec2(0,1), },
		{ ZVec3( pos.x,	 pos.y,	0)  ,	ZVec2(0,0), },
		{ ZVec3(	x2,		y2,	0)	,	ZVec2(1,1), },
		{ ZVec3(	x2,	 pos.y,	0)	,	ZVec2(1,0), }
	};

	m_RingBuf.SetDrawData();
	m_RingBuf.WriteAndDraw(vertices, numVtx);

	// セットされているテクスチャの解除(しないとダメ)
	ZDx.RemoveTexturePS(0);

}

void BlurShader::SetConstantBuffers()
{
	m_View.SetVS();
	m_View.SetPS();
}

void BlurShader::ImGui()
{
#if _DEBUG

	auto imGuiFunc = [this]
	{
		auto dockID = ImGui::GetID("Blurs_Tab");
		if (ImGui::Begin("Blurs") == false)
		{
			ImGui::End();
			return;
		}
		ImGui::DockSpace(dockID);
		ImGui::End();

		//for (int i = 0; i < m_DebugSave->GetListSize(); i++) {
		for (int i = 0; i < m_DebugSaveCnt; i++) 
		{
			std::string name = "Blur_" + std::to_string(i);
			ImGui::SetNextWindowDockId(dockID, ImGuiCond_FirstUseEver);
			if (ImGui::Begin(name.c_str()))
			{
				if (m_DebugSave->GetTex(i)->GetTex())
				{
					ImGui::Image(m_DebugSave->GetTex(i)->GetTex(), ImGui::GetWindowSize());
				}
			}
			ImGui::End();
		}
		
	};

	DW_IMGUI_FUNC(imGuiFunc);
#endif
}

float BlurShader::CalcReductionVal(BlurRenderFlgs flg) {

	switch (flg) {
	case Original:
		return 1.f;
		break;
	case Harf:
		return 0.5f;
		break;
	case Quarter:
		return 0.25f;
		break;
	case Eighth:
		return 0.125f;
		break;
	case Sixteen:
		return 0.0625;
		break;
	case ThirtyOne:
		return 0.03125;
		break;
	case Sixty:
		return 0.015625;
		break;
	default:	//	よくわからない値が来たらハーフテクスチャで
		m_SizeFlg = Harf;
		return 0.5f;
		break;
	}

	m_SizeFlg = Original;
	return 1.f;
}