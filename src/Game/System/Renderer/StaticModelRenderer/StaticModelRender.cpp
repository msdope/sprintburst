#include "MainFrame/ZMainFrame.h"

bool StaticModelRenderer::Init(const ZString& vsPath, const ZString& psPath)
{
	Release();

	bool successed = true;


	m_VS = APP.m_ResStg.LoadVertexShader(vsPath, ZVertex_Pos_UV_TBN::GetVertexTypeData());
	m_PS = APP.m_ResStg.LoadPixelShader(psPath);

	//	Z-プリパス用
	m_PsMono	= APP.m_ResStg.LoadPixelShader("Shader/Model_MonoPS.cso");

	m_VS_Shadow = APP.m_ResStg.LoadVertexShader("Shader/ShadowVS.cso", ZVertex_Pos_UV::GetVertexTypeData());
	m_PS_Shadow = APP.m_ResStg.LoadPixelShader("Shader/ShadowPS.cso");

	// コンスタントバッファー作成
	m_cb1_PerObject.Create(1);
	m_cb2_PerMaterial.Create(2);


	m_cb1_Matrix.Create(1);

	return successed;
}

void StaticModelRenderer::Release()
{
	if(m_VS)m_VS->Release();
	if(m_PS)m_PS->Release();
	if(m_PsMono)m_PsMono->Release();
	if(m_VS_Shadow)m_VS_Shadow->Release();
	if(m_PS_Shadow)m_PS_Shadow->Release();

	m_cb1_PerObject.Release();
	m_cb2_PerMaterial.Release();
	m_cb1_Matrix.Release();
}

void StaticModelRenderer::Submit(ZMatrix* mat, ZSingleModel* pModel, Object3D_RenderFlgs* flgs)
{
	m_RenderBuffer[std::make_tuple(pModel,flgs)].push_back(mat);
}

void StaticModelRenderer::Submit(ZMatrix* mat, ZGameModel* pModel, Object3D_RenderFlgs* flgs)
{
	for (auto sModel : pModel->GetModelTbl_Static())
		m_RenderBuffer[std::make_tuple(sModel.GetPtr(),flgs)].push_back(mat);
}

void StaticModelRenderer::Shadow()
{	
	if (m_RenderBuffer.size() <= 0) { return; }

	m_VS_Shadow->SetShader();
	m_PS_Shadow->SetShader();

	//
	m_cb1_Matrix.SetVS();


	for (auto& param : m_RenderBuffer)
	{
		ZSingleModel* pModel = std::get<0>((param).first);

		if (pModel == nullptr)continue;

		pModel->GetMesh()->SetDrawData();

		auto& materials = pModel->GetMaterials();
		for (UINT i = 0; i < materials.size(); i++) {
			
			if (pModel->GetMesh()->GetSubset(i)->FaceCount == 0) {
				continue;
			}

			m_cb2_PerMaterial.WriteData();

			ZMaterial& mate = materials[i];
			// テクスチャをセット
			if (mate.TexSet->GetTex(0))
				mate.TexSet->GetTex(0)->SetTexturePS(0);
			else
				ZDx.GetWhiteTex()->SetTexturePS(0);


			for (auto& mat : param.second) {
				if (mat == nullptr)
					continue;

				// 行列セット
				SetMatrix(*mat);
				m_cb1_Matrix.WriteData();
				pModel->GetMesh()->DrawSubset(i);
			}
		}

	}

}

void StaticModelRenderer::Flash()
{
	if (m_RenderBuffer.size() <= 0)
		return;

	m_VS->SetShader();
	m_PS->SetShader();

	SetContactBuffers();

	for (auto& param : m_RenderBuffer) {
		ZSingleModel* pModel = std::get<0>((param).first);
		Object3D_RenderFlgs* pRenderFlgs = std::get<1>((param).first);


		if (pModel == nullptr)
			continue;

		if (pRenderFlgs->Delete)
		{
			pRenderFlgs->Alpha -= 0.01f;
			if (pRenderFlgs->Alpha < 0)
			{
				pRenderFlgs->Delete = false;
				pRenderFlgs->Alpha = 0;
			}
		}
		SetMulColor(&ZVec4(1.0f, 1.0f, 1.0f, pRenderFlgs->Alpha));
		
		// メッシュ情報セット(頂点バッファ,インデックスバッファ,プリミティブ・トポロジーなどデバイスへセット)
		pModel->GetMesh()->SetDrawData();

		auto& materials = pModel->GetMaterials();
		for (UINT i = 0; i < materials.size(); i++)
		{
			if (pModel->GetMesh()->GetSubset(i)->FaceCount == 0)
				continue;

			ZMaterial& mate = materials[i];
			SetMaterial(mate);

			m_cb2_PerMaterial.WriteData();

			SetTextures(mate);

			for (auto& mat : param.second)
			{
				if (mat == nullptr)
					continue;

				// 行列セット
				SetMatrix(*mat);
				m_cb1_PerObject.WriteData();
				pModel->GetMesh()->DrawSubset(i);
			}
		}

	}

	m_RenderBuffer.clear();
}


void StaticModelRenderer::Z_Prepass()
{
	if (m_RenderBuffer.size() <= 0)
		return;

	m_VS->SetShader();
//	m_PS_Mono.SetShader();
	m_PsMono->SetShader();
	SetContactBuffers();

	for (auto& param : m_RenderBuffer) {
		ZSingleModel* pModel = std::get<0>((param).first);

		if (pModel == nullptr)
			continue;

		// メッシュ情報セット(頂点バッファ,インデックスバッファ,プリミティブ・トポロジーなどデバイスへセット)
		pModel->GetMesh()->SetDrawData();

		auto& materials = pModel->GetMaterials();
		for (UINT i = 0; i < materials.size(); i++) {
			if (pModel->GetMesh()->GetSubset(i)->FaceCount == 0)
				continue;

			ZMaterial& mate = materials[i];
			SetMaterial(mate);

			m_cb2_PerMaterial.WriteData();

		//	SetTextures(mate);
		// テクスチャをセット
			if (mate.TexSet->GetTex(0))
				mate.TexSet->GetTex(0)->SetTexturePS(0);
			else
				ZDx.GetWhiteTex()->SetTexturePS(0);

			for (auto& mat : param.second) {
				if (mat == nullptr)
					continue;

				// 行列セット
				SetMatrix(*mat);
				m_cb1_PerObject.WriteData();
				pModel->GetMesh()->DrawSubset(i);
			}
		}

	}

	//m_RenderBuffer.clear();
}


void StaticModelRenderer::SetContactBuffers()
{
	// オブジェクト単位のデータ
	m_cb1_PerObject.SetVS();
	m_cb1_PerObject.SetPS();

	// マテリアル単位のデータ
	m_cb2_PerMaterial.SetPS();
}

void StaticModelRenderer::SetMaterial(ZMaterial& mate) {
	// マテリアル,テクスチャセット
	// 拡散色(Diffuse)
	m_cb2_PerMaterial.m_Data.Diffuse = mate.Diffuse;
	// 反射色(Specular)
	m_cb2_PerMaterial.m_Data.Specular = mate.Specular;
	// 反射の強さ(Power)
	m_cb2_PerMaterial.m_Data.SpePower = mate.Power;
	// 発光色(Emissive)
	m_cb2_PerMaterial.m_Data.Emissive = mate.Emissive;

}

void StaticModelRenderer::SetTextures(ZMaterial& mate) {
	// テクスチャをセット
	if (mate.TexSet->GetTex(0))
		mate.TexSet->GetTex(0)->SetTexturePS(0);
	else
		ZDx.GetWhiteTex()->SetTexturePS(0);
	// エミッシブマップ
	if (mate.TexSet->GetTex(1))
		mate.TexSet->GetTex(1)->SetTexturePS(1);
	else
		ZDx.RemoveTexturePS(1);
	// トゥーンテクスチャ
	if (mate.TexSet->GetTex(2))
		mate.TexSet->GetTex(2)->SetTexturePS(2);
	else
		ZDx.GetToonTex()->SetTexturePS(2);
	// 材質加算スフィアマップ
	if (mate.TexSet->GetTex(3))
		mate.TexSet->GetTex(3)->SetTexturePS(3);
	else
		ZDx.RemoveTexturePS(3);
	// リフレクションマップ
	if (mate.TexSet->GetTex(4))
		mate.TexSet->GetTex(4)->SetTexturePS(4);
	else
		ZDx.RemoveTexturePS(4);


	// スペキュラマップ
	if (mate.TexSet->GetTex(5))
		mate.TexSet->GetTex(5)->SetTexturePS(5);
	else
		ZDx.GetWhiteTex()->SetTexturePS(5);
	// 材質乗算スフィアマップ(MMD用に拡張)
	if (mate.TexSet->GetTex(6))
		mate.TexSet->GetTex(6)->SetTexturePS(6);
	else
		ZDx.GetWhiteTex()->SetTexturePS(6);
	// 法線マップ
	if (mate.TexSet->GetTex(9))
		mate.TexSet->GetTex(9)->SetTexturePS(10);
	else
		ZDx.GetNormalTex()->SetTexturePS(10);
}

