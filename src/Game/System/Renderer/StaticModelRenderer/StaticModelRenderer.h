#ifndef __STATIC_MODEL_RENDERER_H__
#define __STATIC_MODEL_RENDERER_H__

#include "../ModelRenderer/ModelRendererBase.h"
#include "Shader/ContactBufferStructs.h"

class StaticModelRenderer : public ModelRendererBase<ZSingleModel, Object3D_RenderFlgs>
{
public:
	virtual ~StaticModelRenderer()
	{
		Release();
	}

	// 初期化
	virtual bool Init(const ZString& vsPath, const ZString& psPath) override;
	// 解放
	virtual void Release()override;

	// 描画
	virtual void Submit(ZMatrix* mat, ZSingleModel* pModel, Object3D_RenderFlgs* flgs) override;
	void Submit(ZMatrix* mat, ZGameModel* pModel, Object3D_RenderFlgs* flgs);
	virtual void Flash() override;

	void Shadow();

	void Z_Prepass();

#pragma region RenderParam

	// ライティング ON/OFF
	void SetLightEnable(bool flag)
	{
		m_cb1_PerObject.m_Data.LightEnable = flag ? 1 : 0;
	}

	void SetDistanceFog(bool flag, const ZVec3* fogColor = nullptr, float fogDensity = -1)
	{
		m_cb1_PerObject.m_Data.DistanceFogDensity = flag ? 1.0f : 0.0f;
		if (fogColor)
			m_cb1_PerObject.m_Data.DistanceFogColor = *fogColor;
		if (fogDensity > 0)
			m_cb1_PerObject.m_Data.DistanceFogDensity = fogDensity;
	}

	void SetMulColor(const ZVec4* color = &ZVec4::one)
	{
		if (color == nullptr)
			m_cb1_PerObject.m_Data.MulColor = ZVec4::one;
		else
			m_cb1_PerObject.m_Data.MulColor = *color;
	}

	void SetXColor(const ZVec4* col) {
		if (col == nullptr)
			m_cb1_PerObject.m_Data.X_Col.Set(ZVec4::zero);
		else
			m_cb1_PerObject.m_Data.X_Col = *col;
	}
#pragma endregion

private:
	void DrawXRayObject();
	void DrawStaticObject();

	// コンタクトバッファをシェーダーにセット
	virtual void SetConstantBuffers()override;

	void SetMatrix(const ZMatrix& mat)
	{
		m_cb0_Matrix.m_Data.Mat = mat;
	}

	void SetMaterial(ZMaterial& mate);

	void SetTextures(ZMaterial& mate);
private:
	ZConstantBuffer<cbPerObject_STModel> m_cb1_PerObject;
	ZConstantBuffer<cbPerMaterial> m_cb2_PerMaterial;

	ZConstantBuffer<cbPerObject_Matrix> m_cb0_Matrix;
	
	ZSP<ZVertexShader>	m_VS_Shadow;
	ZSP<ZPixelShader>	m_PS_Shadow;
	ZSP<ZPixelShader>	m_PS_Chara;
	ZSP<ZPixelShader> m_PsMono;
};

#endif