#include "Game/Application.h"
#include "../../CommonECSComponents/CommonComponents.h"

bool InstancingStaticModelRenderer::Init(const ZString & vsPath, const ZString & psPath)
{
	Release();

	bool successed;

	m_VS = APP.m_ResStg.LoadVertexShader(vsPath);
	m_PS = APP.m_ResStg.LoadPixelShader(psPath);

	//	Z-プリパス用
	m_PsMono = APP.m_ResStg.LoadPixelShader("Shader/Model_MonoPS.cso");

	m_VS_Shadow = APP.m_ResStg.LoadVertexShader("Shader/ShadowInstancingVS.cso");
	m_PS_Shadow = APP.m_ResStg.LoadPixelShader("Shader/ShadowPS.cso");
	m_PS_Chara = APP.m_ResStg.LoadPixelShader("Shader/Model_CharaPS.cso");

	// コンスタントバッファー作成
	m_cb1_PerObject.Create(1);
	m_cb2_PerMaterial.Create(2);
	
	// ワールド行列用バッファ作成
#ifdef _DEBUG
	successed &= CreateInstancingBuffer(5000); //数値は適当
#endif
#ifdef NDEBUG
	successed &= CreateInstancingBuffer(50000); // 数値は適当
#endif

	return successed;
}

bool InstancingStaticModelRenderer::Init(const ZString& vsPath, const ZString & psPath, int maxBufferSize)
{
	Release();

	bool successed;

	m_VS = APP.m_ResStg.LoadVertexShader(vsPath);
	m_PS = APP.m_ResStg.LoadPixelShader(psPath);

	//	Z-プリパス用
	m_PsMono = APP.m_ResStg.LoadPixelShader("Shader/Model_MonoPS.cso");

	m_VS_Shadow = APP.m_ResStg.LoadVertexShader("Shader/ShadowInstancingVS.cso");
	m_PS_Shadow = APP.m_ResStg.LoadPixelShader("Shader/ShadowPS.cso");
	m_PS_Chara = APP.m_ResStg.LoadPixelShader("Shader/Model_CharaPS.cso");

	// コンスタントバッファー作成
	m_cb1_PerObject.Create(1);
	m_cb2_PerMaterial.Create(2);

	// ワールド行列用バッファ作成
	successed &= CreateInstancingBuffer(maxBufferSize);

	return successed;
}

void InstancingStaticModelRenderer::Release()
{
	m_VS = nullptr;
	m_PS = nullptr;

	//	Z-プリパス用
	m_PsMono = nullptr;
	m_VS_Shadow = nullptr;
	m_PS_Shadow = nullptr;
	m_PS_Chara = nullptr;

	m_cb1_PerObject.Release();
	m_cb2_PerMaterial.Release();

	m_WorldMatrixBuffer.Release();

	SAFE_DELPTR(m_MatrixArray);
	SAFE_DELPTR(m_XRayColorArray);
	SAFE_DELPTR(m_MulColorArray);

	m_MatrixBufferSize = 0;
}

void InstancingStaticModelRenderer::Submit(ZMatrix* mat, GameModelComponent* pModel)
{
	std::lock_guard<std::mutex> lg(m_Mtx);
	// フラグによって振り分け
	auto& pRenderFlg = pModel->RenderFlg;
	if (pRenderFlg->XRayCol) // XRay対象オブジェクトか
	{
		m_XRayRenderingObjects[pModel] = mat;
		m_XRayRenderingModelComps[pModel->Model.GetPtr()].push_back(pModel);
	}
	else if (pRenderFlg->SemiTransparent) // 半透明対象オブジェクトか
	{
		m_SemiTransparentRenderingObjects[pModel] = mat;
		m_SemiTransparentRenderingModelComps[pModel->Model.GetPtr()].push_back(pModel);
	}
	else // 通常描画オブジェクトか
	{
		m_NormalRenderingObjects[pModel] = mat;
		m_NormalRenderingModelComps[pModel->Model.GetPtr()].push_back(pModel);
	}
}


void InstancingStaticModelRenderer::Flash()
{	
	FlashXRayObjects();
	FlashNormalObjects();
	FlashSemiTransparentObjects();

	//DrawXRayObject();
	//DrawStaticObject();

}

void InstancingStaticModelRenderer::FlashNormalObjects()
{
	m_VS->SetShader();
	m_PS->SetShader();

	SetConstantBuffers();
	m_cb1_PerObject.WriteData();

	_Flash(m_NormalRenderingObjects, m_NormalRenderingModelComps,
		   [&](ZMaterial& mate)
	{
		SetMaterial(mate);
		SetTextures(mate);

		m_cb2_PerMaterial.WriteData();
	});

	m_NormalRenderingModelComps.clear();
	m_NormalRenderingObjects.clear();
}

void InstancingStaticModelRenderer::FlashSemiTransparentObjects()
{
	m_VS->SetShader();
	m_PS->SetShader();

	SetConstantBuffers();
	m_cb1_PerObject.WriteData();
	
	// この処理はSystemにしたほうがよさげ
	for (auto& param : m_SemiTransparentRenderingObjects)
	{
		auto& pRenderFlg = param.first->RenderFlg;
		if (pRenderFlg->Delete)
		{
			pRenderFlg->Alpha -= 0.01f;
			if (pRenderFlg->Alpha < 0.5f) pRenderFlg->Alpha = 0.5f;
		}
	}

	_Flash(m_SemiTransparentRenderingObjects,m_SemiTransparentRenderingModelComps,
		   [&](ZMaterial& mate)
	{
		SetMaterial(mate);
		SetTextures(mate);

		m_cb2_PerMaterial.WriteData();
	});

	
	m_SemiTransparentRenderingModelComps.clear();
	m_SemiTransparentRenderingObjects.clear();
}

void InstancingStaticModelRenderer::FlashXRayObjects()
{
	m_VS->SetShader();
	m_PS_Chara->SetShader();
	
	SetConstantBuffers();
	m_cb1_PerObject.WriteData();

	_Flash(m_XRayRenderingObjects,m_XRayRenderingModelComps,
		   [&](ZMaterial& mate)
	{
		SetMaterial(mate);
		SetTextures(mate);

		m_cb2_PerMaterial.WriteData();
	});

	m_XRayRenderingObjects.clear();
	m_XRayRenderingModelComps.clear();
}

void InstancingStaticModelRenderer::Shadow()
{
	m_VS_Shadow->SetShader();
	m_PS_Shadow->SetShader();

	m_WorldMatrixBuffer.SetToSlot(sizeof(ZMatrix), 0);

	if (m_XRayRenderingObjects.empty() == false)
	{
		_Flash(m_XRayRenderingObjects,m_XRayRenderingModelComps,
			   [&](ZMaterial& mate)
		{
			// テクスチャをセット
			if (mate.TexSet->GetTex(0))
				mate.TexSet->GetTex(0)->SetTexturePS(0);
			else
				ZDx.GetWhiteTex()->SetTexturePS(0);

		},false);
	}

	if (m_NormalRenderingObjects.empty() == false)
	{
		_Flash(m_NormalRenderingObjects, m_NormalRenderingModelComps,
			   [&](ZMaterial& mate)
		{
			// テクスチャをセット
			if (mate.TexSet->GetTex(0))
				mate.TexSet->GetTex(0)->SetTexturePS(0);
			else
				ZDx.GetWhiteTex()->SetTexturePS(0);

		}, false);
	}

	if (m_SemiTransparentRenderingObjects.empty() == false)
	{
		_Flash(m_SemiTransparentRenderingObjects, m_SemiTransparentRenderingModelComps,
			   [&](ZMaterial& mate)
		{
			// テクスチャをセット
			if (mate.TexSet->GetTex(0))
				mate.TexSet->GetTex(0)->SetTexturePS(0);
			else
				ZDx.GetWhiteTex()->SetTexturePS(0);

		}, false);
	}

}

void InstancingStaticModelRenderer::Z_Prepass()
{
	
	m_VS->SetShader();
	m_PsMono->SetShader();
	m_cb2_PerMaterial.SetPS();

	m_WorldMatrixBuffer.SetToSlot(sizeof(ZMatrix), 0);

	if (m_XRayRenderingObjects.empty() == false)
	{
		_Flash(m_XRayRenderingObjects,m_XRayRenderingModelComps,
			   [&](ZMaterial& mate)
		{
			SetMaterial(mate);

			// テクスチャをセット
			if (mate.TexSet->GetTex(0))
				mate.TexSet->GetTex(0)->SetTexturePS(0);
			else
				ZDx.GetWhiteTex()->SetTexturePS(0);

			m_cb2_PerMaterial.WriteData();

		},false);
	}

	if (m_NormalRenderingObjects.empty() == false)
	{
		_Flash(m_NormalRenderingObjects, m_NormalRenderingModelComps,
			   [&](ZMaterial& mate)
		{
			SetMaterial(mate);

			// テクスチャをセット
			if (mate.TexSet->GetTex(0))
				mate.TexSet->GetTex(0)->SetTexturePS(0);
			else
				ZDx.GetWhiteTex()->SetTexturePS(0);

			m_cb2_PerMaterial.WriteData();

		}, false);
	}

	if (m_SemiTransparentRenderingObjects.empty() == false)
	{
		_Flash(m_SemiTransparentRenderingObjects, m_SemiTransparentRenderingModelComps,
			   [&](ZMaterial& mate)
		{
			SetMaterial(mate);

			// テクスチャをセット
			if (mate.TexSet->GetTex(0))
				mate.TexSet->GetTex(0)->SetTexturePS(0);
			else
				ZDx.GetWhiteTex()->SetTexturePS(0);

			m_cb2_PerMaterial.WriteData();

		}, false);
	}

}

void InstancingStaticModelRenderer::_Flash( ZUnorderedMap<GameModelComponent*, ZMatrix*>& renderingObjects,
											ZUnorderedMap<ZGameModel*, ZVector<GameModelComponent*>>& renderingModelComps,
											std::function<void(ZMaterial&)> prevDrawSubsetFunction,
											bool writeColorBuffers)
{
	// 描画するオブジェクトがないなら
	if (renderingObjects.empty() || renderingModelComps.empty())
		return;

	if (!prevDrawSubsetFunction)
		prevDrawSubsetFunction = [](ZMaterial&) {};

	for (auto& param : renderingModelComps)
	{
		auto& gmodel = param.first; // <- 描画対象(GameModel)

		// パラメータ(行列,合成色,XRayの色)をインスタンシング用バッファに格納
		auto mulColor = m_cb1_PerObject.m_Data.MulColor;
		uint numInstance = param.second.size();
		// バッファにも上限があるので一回にインスタンス描画する数の制御処理を書いたほうがいいが面倒なので省略
		for (uint i = 0; i < numInstance; i++)
		{
			auto& comp = param.second[i];
			auto& mat = renderingObjects[comp];
			m_MatrixArray[i] = (*mat);
			
			if(writeColorBuffers)
			{
				auto& pRenderFlg = comp->RenderFlg;
				mulColor.w = pRenderFlg->Alpha;
				m_MulColorArray[i] = mulColor;
				m_XRayColorArray[i] = pRenderFlg->XRayCol;
			}
		}

		m_WorldMatrixBuffer.WriteData(m_MatrixArray,sizeof(ZMatrix) * numInstance);
		if(writeColorBuffers)
		{
			m_MulColorBuffer.WriteData(m_MulColorArray, sizeof(Color) * numInstance);
			m_XRayColorBuffer.WriteData(m_XRayColorArray, sizeof(Color) * numInstance);
		}

		// GameModel内のSingleModel描画
		for (auto& smodel : gmodel->GetModelTbl_Static())
		{
			auto& mesh = smodel->GetMesh();
			mesh->SetDrawData();
			
			auto& materials = smodel->GetMaterials();
			uint numSubset = materials.size();

			// マテリアルループ
			for (uint currentSubset = 0; currentSubset < numSubset; currentSubset++)
			{
				if (mesh->GetSubset(currentSubset)->FaceCount == 0)
					continue;

				ZMaterial& mate = materials[currentSubset];

				prevDrawSubsetFunction(mate);
				
				mesh->DrawSubsetInstance(currentSubset, numInstance);
			}
		}

	} // for(auto& param : renderingModelComps)

}

void InstancingStaticModelRenderer::SetConstantBuffers()
{
	m_cb1_PerObject.SetVS();
	m_cb1_PerObject.SetPS();
	m_cb2_PerMaterial.SetPS();

	m_WorldMatrixBuffer.SetToSlot(sizeof(ZMatrix), 0);
	m_XRayColorBuffer.SetToSlot(sizeof(Color), 0);
	m_MulColorBuffer.SetToSlot(sizeof(Color), 0);

}

bool InstancingStaticModelRenderer::CreateInstancingBuffer(int maxBufferSize)
{
	// Matrix
	uint slot = 1;
	bool s = m_WorldMatrixBuffer.Create(sizeof(ZMatrix) * maxBufferSize, slot);
	assert(s);

	// XRay
	slot = 2;
	s &= m_XRayColorBuffer.Create(sizeof(Color) * maxBufferSize, slot);
	assert(s);
	
	// MulColor
	slot = 3;
	s &= m_MulColorBuffer.Create(sizeof(Color) * maxBufferSize, slot);
	assert(s);

	if(s)
	{
		m_MatrixBufferSize = maxBufferSize;
		m_MatrixArray = sysnewArray(ZMatrix, maxBufferSize);
		m_XRayColorArray = sysnewArray(Color, maxBufferSize);
		m_MulColorArray = sysnewArray(Color, maxBufferSize);
		return true;
	}

	return false;
}

void InstancingStaticModelRenderer::SetMaterial(ZMaterial& mate)
{
	// マテリアル,テクスチャセット
	// 拡散色(Diffuse)
	m_cb2_PerMaterial.m_Data.Diffuse = mate.Diffuse;
	// 反射色(Specular)
	m_cb2_PerMaterial.m_Data.Specular = mate.Specular;
	// 反射の強さ(Power)
	m_cb2_PerMaterial.m_Data.SpePower = mate.Power;
	// 発光色(Emissive)
	m_cb2_PerMaterial.m_Data.Emissive = mate.Emissive;
}

void InstancingStaticModelRenderer::SetTextures(ZMaterial& mate)
{
	// テクスチャをセット
	if (mate.TexSet->GetTex(0))
		mate.TexSet->GetTex(0)->SetTexturePS(0);
	else
		ZDx.GetWhiteTex()->SetTexturePS(0);
	// エミッシブマップ
	if (mate.TexSet->GetTex(1))
		mate.TexSet->GetTex(1)->SetTexturePS(1);
	else
		ZDx.RemoveTexturePS(1);
	// トゥーンテクスチャ
	if (mate.TexSet->GetTex(2))
		mate.TexSet->GetTex(2)->SetTexturePS(2);
	else
		ZDx.GetToonTex()->SetTexturePS(2);
	// 材質加算スフィアマップ
	if (mate.TexSet->GetTex(3))
		mate.TexSet->GetTex(3)->SetTexturePS(3);
	else
		ZDx.RemoveTexturePS(3);
	// リフレクションマップ
	if (mate.TexSet->GetTex(4))
		mate.TexSet->GetTex(4)->SetTexturePS(4);
	else
		ZDx.RemoveTexturePS(4);
	// スペキュラマップ
	if (mate.TexSet->GetTex(5))
		mate.TexSet->GetTex(5)->SetTexturePS(5);
	else
		ZDx.GetWhiteTex()->SetTexturePS(5);
	// 材質乗算スフィアマップ(MMD用に拡張)
	if (mate.TexSet->GetTex(6))
		mate.TexSet->GetTex(6)->SetTexturePS(6);
	else
		ZDx.GetWhiteTex()->SetTexturePS(6);
	// 法線マップ
	if (mate.TexSet->GetTex(9))
		mate.TexSet->GetTex(9)->SetTexturePS(10);
	else
		ZDx.GetNormalTex()->SetTexturePS(10);
}