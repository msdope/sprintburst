#ifndef __INSTANCING_STATIC_MODEL_RENDERER_H__
#define __INSTANCING_STATIC_MODEL_RENDERER_H__

#include "../ModelRenderer/ModelRendererBase.h"
#include "Shader/ContactBufferStructs.h"

struct GameModelComponent;

class InstancingStaticModelRenderer : public ModelRendererBase<GameModelComponent>
{
public:
	using Color = ZVec4;
	
	struct ModelColorInfo
	{
		ZVec4 XRayColor;
		float Alpha;
	};

public:
	InstancingStaticModelRenderer()
		: m_MatrixArray(nullptr), m_XRayColorArray(nullptr),m_MulColorArray(nullptr),m_MatrixBufferSize(0)
	{
	}

	virtual ~InstancingStaticModelRenderer()
	{
		Release();
	}

	virtual bool Init(const ZString& vsPath, const ZString& psPath)override;
	bool Init(const ZString& vsPath, const ZString& psPath, int maxBufferSize);

	// 解放
	virtual void Release()override;

	// 描画(m_RenderBufferに追加)
	virtual void Submit(ZMatrix* mat, GameModelComponent* pModel)override;
	
	// 本描画(m_RenderBufferに溜め込まれた描画情報を元に描画 ↓の3つの関数一括呼び出し)
	virtual void Flash()override;

	// 本描画(通常描画オブジェクト)
	void FlashNormalObjects();

	// 本描画(半透明描画オブジェクト)
	void FlashSemiTransparentObjects();

	// 本描画(XRay描画オブジェクト)
	void FlashXRayObjects();

	void Shadow();

	void Z_Prepass();


#pragma region 描画設定

	// ライティング ON/OFF
	void SetLightEnable(bool flag)
	{
		m_cb1_PerObject.m_Data.LightEnable = flag ? 1 : 0;
	}

	// 距離フォグ設定
	// fogDensity ... フォグの密度
	void SetDistanceFog(bool flag, const ZVec3* fogColor = nullptr, float fogDensity = -1)
	{
		m_cb1_PerObject.m_Data.DistanceFogDensity = flag ? 1.0f : 0.0f;
		if (fogColor)
			m_cb1_PerObject.m_Data.DistanceFogColor = *fogColor;
		if (fogDensity > 0)
			m_cb1_PerObject.m_Data.DistanceFogDensity = fogDensity;
	}

	void SetMulColor(const ZVec4* color = &ZVec4::one)
	{
		if (color == nullptr)
			m_cb1_PerObject.m_Data.MulColor = ZVec4::one;
		else
			m_cb1_PerObject.m_Data.MulColor = *color;
	}

#pragma endregion

private:
	// 描画処理の共通部分(バッファクリアなし)
	void _Flash(ZUnorderedMap<GameModelComponent*, ZMatrix*>& renderingObjects,
				ZUnorderedMap<ZGameModel*, ZVector<GameModelComponent*>>& renderingModelComps,
				std::function<void(ZMaterial&)> prevDrawSubsetFunction,bool writeColorBuffers = true);


	virtual void SetConstantBuffers()override;
	bool CreateInstancingBuffer(int maxBufferSize);

	void SetMaterial(ZMaterial& mate);
	void SetTextures(ZMaterial& mate);

private:
	ZConstantBuffer<cbPerObject_STModel> m_cb1_PerObject;
	ZConstantBuffer<cbPerMaterial> m_cb2_PerMaterial;
	ZInstancingBuffer m_WorldMatrixBuffer;
	ZInstancingBuffer m_XRayColorBuffer;
	ZInstancingBuffer m_MulColorBuffer;
	size_t m_MatrixBufferSize;
	std::mutex m_Mtx;
		
	ZSP<ZVertexShader> m_VS_Shadow;
	ZSP<ZPixelShader> m_PS_Shadow;
	ZSP<ZPixelShader> m_PS_Chara;
	ZSP<ZPixelShader> m_PsMono;
	
	// 描画時に使用
	ZMatrix* m_MatrixArray;
	Color* m_XRayColorArray;
	Color* m_MulColorArray;

	// 通常描画オブジェクト
	ZUnorderedMap<GameModelComponent*, ZMatrix*> m_NormalRenderingObjects;
	// XRay描画オブジェクト
	ZUnorderedMap<GameModelComponent*, ZMatrix*> m_XRayRenderingObjects;
	// 半透明描画
	ZUnorderedMap<GameModelComponent*, ZMatrix*> m_SemiTransparentRenderingObjects;

	// メッシュをキーとしたコンポーネントリストの
	ZUnorderedMap<ZGameModel*, ZVector<GameModelComponent*>> m_NormalRenderingModelComps;			// 通常描画
	ZUnorderedMap<ZGameModel*, ZVector<GameModelComponent*>> m_XRayRenderingModelComps;				// XRay描画
	ZUnorderedMap<ZGameModel*, ZVector<GameModelComponent*>> m_SemiTransparentRenderingModelComps;	// 半透明描画

};

#endif