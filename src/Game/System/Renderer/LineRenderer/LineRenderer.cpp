#include "Game/Application.h"
#include "LineRenderer.h"

LineRenderer::LineRenderer()
	: m_LineBufferSize(1000), m_VertexTypeData(ZVertex_Pos_UV_Color::GetVertexTypeData())
{
}

LineRenderer::LineRenderer(ULONG lineBufferSize)
	: m_VertexTypeData(ZVertex_Pos_UV_Color::GetVertexTypeData()), m_LineBufferSize(lineBufferSize)
{
}

bool LineRenderer::Init(const ZString & vsPath, const ZString & psPath)
{
	Release();

	bool successed = true;

	// 各種シェーダ読み込み
	m_VS = APP.m_ResStg.LoadVertexShader(vsPath);
	m_PS = APP.m_ResStg.LoadPixelShader(psPath);

	// コンスタントバッファ作成
	successed &= m_cb1_PerObject.Create(1);

	// 頂点バッファ
	m_RingVB.Create(m_VertexTypeData,m_LineBufferSize * 2);
	
	for (int i = 0; i < NumCircle_Sphere; i++)
	{
		CalcSphereLineMat[i].CreateMove(ZVec3(0));
		CalcSphereLineMat[i].CreateRotateY(i*(360.0f / 6));
	}

	CalcSphereLineMat[3].CreateMove(ZVec3(0));
	CalcSphereLineMat[3].RotateX_Local(90);
	
	return successed;
}

void LineRenderer::Release()
{
	if (m_VS)m_VS->Release();
	if (m_PS)m_PS->Release();

	m_cb1_PerObject.Release();

	m_RingVB.Release();
}

void LineRenderer::Submit(const ZVec3 & p1, const ZVec3 & p2, const ZVec4 & color, const ZMatrix* mat)
{
	m_LineBuffer[mat].emplace_back();
	Line& line = *(m_LineBuffer[mat].end()-1);
	
	line.v[0] = { p1,ZVec2(0,0),color };
	line.v[1] = { p2,ZVec2(0,0),color };

}

void LineRenderer::Submit(const ZVec3 & p1, const ZVec3 & p2, const ZVec4 & p1Color, const ZVec4 & p2Color, const ZMatrix* mat)
{
	m_LineBuffer[mat].emplace_back();
	Line& line = *(m_LineBuffer[mat].end()-1);
	
	line.v[0] = { p1,ZVec2(0,0),p1Color };
	line.v[1] = { p2,ZVec2(0,0),p2Color };
}

void LineRenderer::Submit_BoxLine1(const ZVec3 & vMin, const ZVec3 & vMax, const ZVec4 & color, const ZMatrix * mat)
{
	m_BoxLineBuffer[mat].emplace_back();
	BoxLine& boxLine = m_BoxLineBuffer[mat].back();

	boxLine.lines[ 0].v[0] = { ZVec3(vMin.x,vMin.y,vMin.z),ZVec2(0,0),color };
	boxLine.lines[ 0].v[1] = { ZVec3(vMin.x,vMin.y,vMax.z),ZVec2(0,0),color };
	boxLine.lines[ 1].v[0] = { ZVec3(vMin.x,vMin.y,vMax.z),ZVec2(0,0),color };
	boxLine.lines[ 1].v[1] = { ZVec3(vMax.x,vMin.y,vMax.z),ZVec2(0,0),color };
	boxLine.lines[ 2].v[0] = { ZVec3(vMax.x,vMin.y,vMax.z),ZVec2(0,0),color };
	boxLine.lines[ 2].v[1] = { ZVec3(vMax.x,vMin.y,vMin.z),ZVec2(0,0),color };
	boxLine.lines[ 3].v[0] = { ZVec3(vMax.x,vMin.y,vMin.z),ZVec2(0,0),color };
	boxLine.lines[ 3].v[1] = { ZVec3(vMin.x,vMin.y,vMin.z),ZVec2(0,0),color };
	
	boxLine.lines[ 4].v[0] = { ZVec3(vMin.x,vMax.y,vMin.z),ZVec2(0,0),color };
	boxLine.lines[ 4].v[1] = { ZVec3(vMin.x,vMax.y,vMax.z),ZVec2(0,0),color };
	boxLine.lines[ 5].v[0] = { ZVec3(vMin.x,vMax.y,vMax.z),ZVec2(0,0),color };
	boxLine.lines[ 5].v[1] = { ZVec3(vMax.x,vMax.y,vMax.z),ZVec2(0,0),color };
	boxLine.lines[ 6].v[0] = { ZVec3(vMax.x,vMax.y,vMax.z),ZVec2(0,0),color };
	boxLine.lines[ 6].v[1] = { ZVec3(vMax.x,vMax.y,vMin.z),ZVec2(0,0),color };
	boxLine.lines[ 7].v[0] = { ZVec3(vMax.x,vMax.y,vMin.z),ZVec2(0,0),color };
	boxLine.lines[ 7].v[1] = { ZVec3(vMin.x,vMax.y,vMin.z),ZVec2(0,0),color };
	
	boxLine.lines[ 8].v[0] = { ZVec3(vMin.x,vMin.y,vMin.z),ZVec2(0,0),color };
	boxLine.lines[ 8].v[1] = { ZVec3(vMin.x,vMax.y,vMin.z),ZVec2(0,0),color };
	
	boxLine.lines[ 9].v[0] = { ZVec3(vMin.x,vMin.y,vMax.z),ZVec2(0,0),color };
	boxLine.lines[ 9].v[1] = { ZVec3(vMin.x,vMax.y,vMax.z),ZVec2(0,0),color };
	
	boxLine.lines[10].v[0] = { ZVec3(vMax.x,vMin.y,vMin.z),ZVec2(0,0),color };
	boxLine.lines[10].v[1] = { ZVec3(vMax.x,vMax.y,vMin.z),ZVec2(0,0),color };
	
	boxLine.lines[11].v[0] = { ZVec3(vMax.x,vMin.y,vMax.z),ZVec2(0,0),color };
	boxLine.lines[11].v[1] = { ZVec3(vMax.x,vMax.y,vMax.z),ZVec2(0,0),color };

}

void LineRenderer::Submit_BoxLine2(const ZVec3 & vCenter, const ZVec3 & vHalfSize, const ZVec4 & color, const ZQuat * q)
{
	ZVec3 vMin = -vHalfSize;
	ZVec3 vMax = +vHalfSize;

	ZMatrix m;
	if(q)
		q->ToMatrix(m);

	Submit_BoxLine1(vMin, vMax, color);
	
	for (auto& line : m_BoxLineBuffer[&ZMatrix::Identity].back().lines)
	{
		for(auto& vtx : line.v)
		{
			vtx.Pos.Transform(m);
			vtx.Pos += vCenter;
		}
	}
}

void LineRenderer::Submit_BoxLine3(const ZVec3 & vLocalCenter, const ZVec3 & vHalfSize, const ZVec4 & color, const ZMatrix * mat)
{
	ZVec3 vMin = vLocalCenter - vHalfSize;
	ZVec3 vMax = vLocalCenter + vHalfSize;
	Submit_BoxLine1(vMin, vMax, color,mat);
}

void LineRenderer::Submit_CircleLine(const ZVec3 & pos, float rad, const ZVec4 & color, const ZMatrix* mat)
{
	m_CircleLineBuffer[mat].emplace_back();
	CircleLine& circle = m_CircleLineBuffer[mat].back();

	const float anglePerVertex = 360.0f / (NumVertex_Circle - 1);
	for (int i = 0; i < NumVertex_Circle; i++)
	{
		float deg = (i) * (anglePerVertex);

		float x = cos(ToRadian(deg)) * rad;
		float y = sin(ToRadian(deg)) * rad;

		circle.v[i].Pos = ZVec3(pos.x + x,pos.y + y,pos.z);
		circle.v[i].UV = ZVec2(0, 0);
		circle.v[i].Color = color;
	}

}

void LineRenderer::Submit_SphereLine(const ZVec3 & pos, float rad, const ZVec4 & color, const ZMatrix* mat)
{
	m_SphereLineBuffer[mat].emplace_back();
	SphereLine& sphere = m_SphereLineBuffer[mat].back();

	const float anglePerVertex = 360.0f / (NumVertex_Circle-1);

	for (int i = 0; i < NumVertex_Circle; i++)
	{
		float deg = (i) * (anglePerVertex);

		float x = cos(ToRadian(deg)) * rad;
		float y = sin(ToRadian(deg)) * rad;

		sphere.circleLines.v[i].Pos = ZVec3(x,y,0);
		sphere.circleLines.v[i].UV = ZVec2(0, 0);
		sphere.circleLines.v[i].Color = color;
	}
	sphere.pos = pos;
}

void LineRenderer::Flash()
{
	SetConstantBuffers();

	// 白色テクスチャをセット
	ZDx.GetWhiteTex()->SetTexturePS(0);
	
	ULONG index;

	m_RingVB.SetDrawData(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);

#pragma region Line描画
	
	for(auto& linebuf : m_LineBuffer)
	{
		index = 0;
		auto& lines = linebuf.second;
		
		m_cb1_PerObject.m_Data.Mat = linebuf.first != nullptr ? *linebuf.first : ZMatrix::Identity;
		m_cb1_PerObject.WriteData();

		while (index < lines.size())
		{
			size_t numDrawLine = min(lines.size() - index,m_LineBufferSize);
			m_RingVB.WriteAndDraw(&lines[index], numDrawLine * 2);
			index += numDrawLine;
		}
	}

#pragma endregion

#pragma region Box描画

	for (auto& boxLineBuf : m_BoxLineBuffer)
	{
		index = 0;
		auto& boxLines = boxLineBuf.second;

		if (boxLineBuf.first != nullptr)
			m_cb1_PerObject.m_Data.Mat = *boxLineBuf.first;
		else
			m_cb1_PerObject.m_Data.Mat.CreateIdentity();
	
		m_cb1_PerObject.WriteData();

		while(index < boxLines.size())
		{
			ULONG numLine = boxLines.size() * NumLine_Box;
			ULONG numDrawLine = numLine > m_LineBufferSize ?
						m_LineBufferSize / NumLine_Box : numLine;
			m_RingVB.WriteAndDraw(&boxLines[index],numDrawLine*2);
			index += numDrawLine / NumLine_Box;
		}
	}

#pragma endregion

	m_RingVB.SetDrawData(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP);

#pragma region Circle描画
	
	for(auto& circleLineBuf : m_CircleLineBuffer)
	{
		auto& circleLines = circleLineBuf.second;

		if (circleLineBuf.first)
			m_cb1_PerObject.m_Data.Mat = *circleLineBuf.first;
		else
			m_cb1_PerObject.m_Data.Mat.CreateIdentity();
		m_cb1_PerObject.WriteData();

		for (index = 0;index < circleLines.size();index++)
			m_RingVB.WriteAndDraw(&circleLines[index], NumVertex_Circle);
	}

#pragma endregion

#pragma region Sphere描画

	ZMatrix saveMat = m_cb1_PerObject.m_Data.Mat;
	for (auto& sphereLineBuf : m_SphereLineBuffer)
	{
		auto& sphereLines = sphereLineBuf.second;

		for (index = 0;index < sphereLines.size();index++)
		{
			m_RingVB.WriteVertexData(&sphereLines[index].circleLines, NumVertex_Circle);

			for(int i = 0;i< NumCircle_Sphere;i++)
			{
				m_cb1_PerObject.m_Data.Mat.CreateMove(sphereLines[index].pos);
				m_cb1_PerObject.m_Data.Mat = CalcSphereLineMat[i] * m_cb1_PerObject.m_Data.Mat;
				
				if (sphereLineBuf.first != nullptr)
					m_cb1_PerObject.m_Data.Mat *= *sphereLineBuf.first;

				m_cb1_PerObject.WriteData();
				
				m_RingVB.Draw(NumVertex_Circle);
			}

			m_cb1_PerObject.m_Data.Mat.CreateMove(sphereLines[index].pos);
			m_cb1_PerObject.m_Data.Mat = CalcSphereLineMat[3] * m_cb1_PerObject.m_Data.Mat;
			if (sphereLineBuf.first != nullptr)
				m_cb1_PerObject.m_Data.Mat *= *sphereLineBuf.first;
			m_cb1_PerObject.WriteData();
			
			m_RingVB.Draw(NumVertex_Circle);

		}

		m_cb1_PerObject.m_Data.Mat = saveMat;
		m_cb1_PerObject.WriteData();

	
	}
#pragma endregion

	m_LineBuffer.clear();
	m_BoxLineBuffer.clear();
	m_CircleLineBuffer.clear();
	m_SphereLineBuffer.clear();
}

void LineRenderer::SetConstantBuffers()
{
	m_VS->SetShader();
	m_PS->SetShader();

	// オブジェクト単位のデータ
	m_cb1_PerObject.SetVS();
	m_cb1_PerObject.SetPS();
}





