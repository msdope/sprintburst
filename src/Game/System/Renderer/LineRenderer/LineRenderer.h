#ifndef __LINE_RENDERER_H__
#define __LINE_RENDERER_H__

#include "../RendererBase.h"
#include "Shader/ContactBufferStructs.h"

class LineRenderer : public BatchRenderer<const ZVec3& , const ZVec3&,const ZVec4&,const ZMatrix*>
{
private:
	static const int NumLine_Box = 12;										// Box形状のライン数
	static const int NumVertex_Circle = 33;									// Circle形状のライン数
	static const int NumCircle_Sphere = 3;									// Sphere形状の円の数(Sphereは円の集合で表す)
	static const int NumVertex_Sphere = NumVertex_Circle * NumCircle_Sphere;	// Sphere形状のライン数

	struct Line
	{
		ZVertex_Pos_UV_Color v[2];
	};

	struct BoxLine
	{
		Line lines[NumLine_Box];
	};

	struct CircleLine
	{
		ZVertex_Pos_UV_Color v[NumVertex_Circle];
	};

	struct SphereLine
	{
		ZVec3 pos;
		CircleLine circleLines;
	};

public:
	LineRenderer();
	LineRenderer(ULONG lineBufferSize);

	virtual ~LineRenderer()
	{
		Release();
	}

	// 初期化
	virtual bool Init(const ZString& vsPath, const ZString& psPath)override;

	// 解放
	virtual void Release()override;
	
	// ライン描画
	virtual void Submit(const ZVec3& p1, const ZVec3& p2, const ZVec4& color, const ZMatrix* mat = &ZMatrix::Identity)override;

	// ライン描画(始点,終点の両方の色を指定)
	void Submit(const ZVec3& p1, const ZVec3& p2, const ZVec4& p1Color, const ZVec4& p2Color, const ZMatrix* mat = &ZMatrix::Identity);

	// Box線描画(ローカルのMin,Max,行列を指定)
	void Submit_BoxLine1(const ZVec3& vMin, const ZVec3& vMax, const ZVec4& color, const ZMatrix* mat = &ZMatrix::Identity);

	// Box線描画(ワールド座標,ハーフサイズ,回転を指定)
	void Submit_BoxLine2(const ZVec3& vCenter, const ZVec3& vHalfSize, const ZVec4& color, const ZQuat* q);

	// Box線描画(ローカル座標,ハーフサイズ,行列を指定 行列でワールドへ変換)
	void Submit_BoxLine3(const ZVec3& vLocalCenter, const ZVec3& vHalfSize, const ZVec4& color, const ZMatrix* mat = &ZMatrix::Identity);

	// 円線描画 XY平面の円 回転する場合はmatで指定
	void Submit_CircleLine(const ZVec3& pos, float rad, const ZVec4& color, const ZMatrix* mat = &ZMatrix::Identity);

	// 球線描画
	void Submit_SphereLine(const ZVec3& pos, float rad, const ZVec4& color, const ZMatrix* mat = &ZMatrix::Identity);

	// 本描画
	virtual void Flash()override;

protected:
	// コンタクトバッファをシェーダーにセット
	virtual void SetConstantBuffers()override;

private:


private:
	// コンタクトバッファ
	ZConstantBuffer<cbPerObject_Matrix> m_cb1_PerObject;

	// 動的頂点バッファ
	ZRingDynamicVB m_RingVB;

	ZSUnorderedMap<const ZMatrix*,ZSVector<Line>>			m_LineBuffer;
	ZSUnorderedMap<const ZMatrix*,ZSVector<BoxLine>>			m_BoxLineBuffer;
	ZSUnorderedMap<const ZMatrix*,ZSVector<CircleLine>>		m_CircleLineBuffer;
	ZSUnorderedMap<const ZMatrix*,ZSVector<SphereLine>>		m_SphereLineBuffer;

	const ULONG m_LineBufferSize;

	const ZVertexTypeData m_VertexTypeData;

	ZMatrix CalcSphereLineMat[NumCircle_Sphere + 1];
};


#endif
