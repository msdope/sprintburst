#ifndef __MODEL_RENDERER_BASE_H__
#define __MODEL_RENDERER_BASE_H__

#include "../RendererBase.h"

template<typename ...Args>
class ModelRendererBase : public BatchRenderer<ZMatrix*, Args*...>
{
public:
	ModelRendererBase()
	{
	}
	virtual ~ModelRendererBase()
	{
	}

	// 初期化
	virtual bool Init(const ZString& vsPath, const ZString& psPath) = 0;
	// 解放
	virtual void Release() = 0;

	// 描画(m_RenderBufferに追加)
	virtual void Submit(ZMatrix* mat, Args* ...param) = 0;
	// 本描画(m_RenderBufferに溜め込まれた描画情報を元に描画)
	virtual void Flash() = 0;

protected:
	// コンタクトバッファをシェーダーにセット
	virtual void SetConstantBuffers() = 0;

protected:
	ZAMap <std::tuple<Args* ...>, ZAVector<ZMatrix*>> m_RenderBuffer;
};


#endif