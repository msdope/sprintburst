#ifndef __INSTANCING_STATIC_MESH_RENDERER_H__
#define __INSTANCING_STATIC_MESH_RENDERER_H__

#include "ModelRendererBase.h"
#include "Shader/ContactBufferStructs.h"

class InstancingStaticMeshRenderer : public ModelRendererBase<ZSingleModel>
{
public:
	InstancingStaticMeshRenderer() : m_MatrixArray(nullptr), m_MatrixBufferSize(0)
	{
	}

	virtual ~InstancingStaticMeshRenderer()
	{
		Release();
	}

	// 初期化
	virtual bool Init(const ZString& vsPath, const ZString& psPath) override;
	bool Init(const ZString& vsPath, const ZString& psPath, int maxBufferSize);

	// 解放
	virtual void Release() override;

	// 描画(m_RenderBufferに追加)
	virtual void Submit(ZMatrix* mat, ZSingleModel* pModel) override;
	void Submit(ZMatrix* mat, ZGameModel* pModel);

	// 本描画(m_RenderBufferに溜め込まれた描画情報を元に描画)
	virtual void Flash() override;

	#pragma region 描画設定

	// ライティング ON/OFF
	void SetLightEnable(bool flag)
	{
		m_cb1_PerObject.m_Data.LightEnable = flag ? 1 : 0;
	}

	// 距離フォグ設定
	// fogDensity ... フォグの密度
	void SetDistanceFog(bool flag, const ZVec3* fogColor = nullptr, float fogDensity = -1)
	{
		m_cb1_PerObject.m_Data.DistanceFogDensity = flag ? 1.0f : 0.0f;
		if (fogColor)
			m_cb1_PerObject.m_Data.DistanceFogColor = *fogColor;
		if (fogDensity > 0)
			m_cb1_PerObject.m_Data.DistanceFogDensity = fogDensity;
	}

	void SetMulColor(const ZVec4* color = &ZVec4::one)
	{
		if (color == nullptr)
			m_cb1_PerObject.m_Data.MulColor = ZVec4::one;
		else
			m_cb1_PerObject.m_Data.MulColor = *color;
	}

	#pragma endregion

private:
	virtual void SetConstantBuffers() override;
	bool CreateWorldMatrixBuffer(int maxBufferSize);

	void SetMaterial(ZMaterial& mate);

	void SetTextures(ZMaterial& mate);

private:
	ZConstantBuffer<cbPerObject_STModel> m_cb1_PerObject;
	ZConstantBuffer<cbPerMaterial> m_cb2_PerMaterial;
	ZRingDynamicBuffer m_WorldMatrixBuffer;
	ZMatrix* m_MatrixArray; // 描画時に使用
	size_t m_MatrixBufferSize;
	std::mutex m_Mtx;
};

#endif