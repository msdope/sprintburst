#include "Game/Application.h"

bool StaticMeshRenderer::Init(const ZString& vsPath, const ZString& psPath)
{
	Release();

	bool successed = true;
	m_VS = APP.m_ResStg.LoadVertexShader(vsPath);
	m_PS = APP.m_ResStg.LoadPixelShader(psPath);

	// コンスタントバッファー作成
	m_cb0_Matrix.Create(0);
	m_cb1_PerObject.Create(1);
	m_cb2_PerMaterial.Create(2);

	return successed;
}

void StaticMeshRenderer::Release()
{
	if (m_VS)m_VS->Release();
	if (m_PS)m_PS->Release();
	m_cb0_Matrix.Release();
	m_cb1_PerObject.Release();
	m_cb2_PerMaterial.Release();
}

void StaticMeshRenderer::Submit(ZMatrix* mat,ZSingleModel* pModel)
{
	m_RenderBuffer[std::make_tuple(pModel)].push_back(mat);
}

void StaticMeshRenderer::Submit(ZMatrix* mat, ZGameModel* pModel)
{
	for(auto sModel : pModel->GetModelTbl_Static())
		m_RenderBuffer[std::make_tuple(sModel.GetPtr())].push_back(mat);
}

void StaticMeshRenderer::Flash()
{
	if (m_RenderBuffer.size() <= 0)
		return;

	m_VS->SetShader();
	m_PS->SetShader();

	SetConstantBuffers();

	for(auto& param : m_RenderBuffer)
	{
		ZSingleModel* pModel = std::get<0>((param).first);

		if (pModel == nullptr)
			continue;

		// メッシュ情報セット(頂点バッファ,インデックスバッファ,プリミティブ・トポロジーなどデバイスへセット)
		pModel->GetMesh()->SetDrawData();

		auto& materials = pModel->GetMaterials();
		for(UINT i = 0;i < materials.size();i++)
		{
			if (pModel->GetMesh()->GetSubset(i)->FaceCount == 0)
				continue;

			ZMaterial& mate = materials[i];
			SetMaterial(mate);

			m_cb2_PerMaterial.WriteData();

			SetTextures(mate);

			for(auto& mat : param.second)
			{
				if (mat == nullptr)
					continue;

				// 行列セット
				SetMatrix(*mat);
				m_cb0_Matrix.WriteData();
				pModel->GetMesh()->DrawSubset(i);
			}
		}

	}

	m_RenderBuffer.clear();
}

void StaticMeshRenderer::SetConstantBuffers()
{
	// オブジェクト単位のデータ
	m_cb0_Matrix.SetVS();
	m_cb1_PerObject.SetVS();
	m_cb1_PerObject.SetPS();

	// マテリアル単位のデータ
	m_cb2_PerMaterial.SetPS();
}

void StaticMeshRenderer::SetMaterial(ZMaterial& mate)
{
	// マテリアル,テクスチャセット
	// 拡散色(Diffuse)
	m_cb2_PerMaterial.m_Data.Diffuse = mate.Diffuse;
	// 反射色(Specular)
	m_cb2_PerMaterial.m_Data.Specular = mate.Specular;
	// 反射の強さ(Power)
	m_cb2_PerMaterial.m_Data.SpePower = mate.Power;
	// 発光色(Emissive)
	m_cb2_PerMaterial.m_Data.Emissive = mate.Emissive;

}

void StaticMeshRenderer::SetTextures(ZMaterial& mate)
{
	// テクスチャをセット
	if (mate.TexSet->GetTex(0))
		mate.TexSet->GetTex(0)->SetTexturePS(0);
	else
		ZDx.GetWhiteTex()->SetTexturePS(0);
	// エミッシブマップ
	if (mate.TexSet->GetTex(1))
		mate.TexSet->GetTex(1)->SetTexturePS(1);
	else
		ZDx.RemoveTexturePS(1);
	// トゥーンテクスチャ
	if (mate.TexSet->GetTex(2))
		mate.TexSet->GetTex(2)->SetTexturePS(2);
	else
		ZDx.GetToonTex()->SetTexturePS(2);
	// 材質加算スフィアマップ
	if (mate.TexSet->GetTex(3))
		mate.TexSet->GetTex(3)->SetTexturePS(3);
	else
		ZDx.RemoveTexturePS(3);
	// リフレクションマップ
	if (mate.TexSet->GetTex(4))
		mate.TexSet->GetTex(4)->SetTexturePS(4);
	else
		ZDx.RemoveTexturePS(4);
	// スペキュラマップ
	if (mate.TexSet->GetTex(5))
		mate.TexSet->GetTex(5)->SetTexturePS(5);
	else
		ZDx.GetWhiteTex()->SetTexturePS(5);
	// 材質乗算スフィアマップ(MMD用に拡張)
	if (mate.TexSet->GetTex(6))
		mate.TexSet->GetTex(6)->SetTexturePS(6);
	else
		ZDx.GetWhiteTex()->SetTexturePS(6);
	// 法線マップ
	if (mate.TexSet->GetTex(9))
		mate.TexSet->GetTex(9)->SetTexturePS(10);
	else
		ZDx.GetNormalTex()->SetTexturePS(10);
}

