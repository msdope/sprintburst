#include "Game/Application.h"

bool InstancingStaticMeshRenderer::Init(const ZString& vsPath, const ZString& psPath)
{
	Release();

	bool successed;

	m_VS = APP.m_ResStg.LoadVertexShader(vsPath);
	m_PS = APP.m_ResStg.LoadPixelShader(psPath);


	// コンスタントバッファー作成
	m_cb1_PerObject.Create(1);
	m_cb2_PerMaterial.Create(2);
	
	// ワールド行列用バッファ作成
#ifdef _DEBUG
	successed &= CreateWorldMatrixBuffer(500); //数値は適当
#endif
#ifdef NDEBUG
	successed &= CreateWorldMatrixBuffer(50000); // 数値は適当
#endif

	return successed;
}

bool InstancingStaticMeshRenderer::Init(const ZString& vsPath, const ZString & psPath, int maxBufferSize)
{
	Release();

	bool successed;

	// 頂点シェーダ作成

	m_VS = APP.m_ResStg.LoadVertexShader(vsPath);
	m_PS = APP.m_ResStg.LoadPixelShader(psPath);

	// コンスタントバッファー作成
	m_cb1_PerObject.Create(1);
	m_cb2_PerMaterial.Create(2);
	
	// ワールド行列用バッファ作成
	successed &= CreateWorldMatrixBuffer(maxBufferSize);

	return successed;
}

void InstancingStaticMeshRenderer::Release()
{
	m_VS = nullptr;
	m_PS = nullptr;
	m_cb1_PerObject.Release();
	m_cb2_PerMaterial.Release();
	
	m_WorldMatrixBuffer.Release();

	SAFE_DELPTR(m_MatrixArray);
	m_MatrixBufferSize = 0;
}

void InstancingStaticMeshRenderer::Submit(ZMatrix* mat, ZSingleModel* pModel)
{
	std::lock_guard<std::mutex> lg(m_Mtx);
	m_RenderBuffer[std::make_tuple(pModel)].push_back(mat);
}

void InstancingStaticMeshRenderer::Submit(ZMatrix* mat, ZGameModel* pModel)
{
	std::lock_guard<std::mutex> lg(m_Mtx);
	for (auto model : pModel->GetModelTbl_Static())
		m_RenderBuffer[std::make_tuple(model.GetPtr())].push_back(mat);
}

void InstancingStaticMeshRenderer::Flash()
{
	if (m_RenderBuffer.size() <= 0)
		return;

	m_VS->SetShader();
	m_PS->SetShader();
	
	SetConstantBuffers();

	for (auto& param : m_RenderBuffer)
	{
		ZSingleModel* pModel = std::get<0>((param).first);

		if (pModel == nullptr)
			continue;

		// メッシュ情報セット(頂点バッファ,インデックスバッファ,プリミティブ・トポロジーなどデバイスへセット)
		pModel->GetMesh()->SetDrawData();

		auto& materials = pModel->GetMaterials();
		UINT numSubset = materials.size();
		UINT numMat = param.second.size();

		for(UINT currentSubset = 0;currentSubset < numSubset;currentSubset++)
		{
			UINT cnt = 0;
			UINT oldCnt = 0;
			while(cnt < numMat)
			{
				// 行列を一括でバッファに書き込み
				{
					UINT i;
					m_WorldMatrixBuffer.ResetOffset();
					for (i = 0;i < m_MatrixBufferSize; i++)
					{
						if (cnt >= numMat)
							break;

						if (param.second[cnt] == nullptr)
							continue;
						
						// 効率化のため一度用意した配列にコピー
						m_MatrixArray[i] = *param.second[cnt];
						cnt++;
					}

					m_WorldMatrixBuffer.WriteData(m_MatrixArray, sizeof(ZMatrix) * i);
				}

				// slot1にインスタンスごとのワールド行列データをセット
				UINT offset = 0;
				UINT stride = sizeof(ZMatrix);
				ID3D11Buffer* buffer = m_WorldMatrixBuffer.GetBuffer();
				ZDx.GetDevContext()->IASetVertexBuffers(1, 1, &buffer, &stride, &offset);
				
				if (pModel->GetMesh()->GetSubset(currentSubset)->FaceCount > 0)
				{
					m_cb1_PerObject.WriteData();
					ZMaterial& mate = materials[currentSubset];
					SetMaterial(mate);
					m_cb2_PerMaterial.WriteData();

					SetTextures(mate);

					pModel->GetMesh()->DrawSubsetInstance(currentSubset, cnt - oldCnt);
				}

				oldCnt = cnt;
			}
		}

	}

	m_RenderBuffer.clear();

}

void InstancingStaticMeshRenderer::SetConstantBuffers()
{
	// オブジェクト単位のデータ
	m_cb1_PerObject.SetVS();
	m_cb1_PerObject.SetPS();

	// マテリアル単位のデータ
	m_cb2_PerMaterial.SetPS();
}

bool InstancingStaticMeshRenderer::CreateWorldMatrixBuffer(int maxBufferSize)
{
	D3D11_BIND_FLAG flag = D3D11_BIND_VERTEX_BUFFER;
	
	if (m_WorldMatrixBuffer.Create(flag, sizeof(ZMatrix) * maxBufferSize))
	{
		m_MatrixBufferSize = maxBufferSize;

		m_MatrixArray = sysnewArray(ZMatrix,maxBufferSize);

		return true;
	}

	return false;
}

void InstancingStaticMeshRenderer::SetMaterial(ZMaterial& mate)
{
	// マテリアル,テクスチャセット
	// 拡散色(Diffuse)
	m_cb2_PerMaterial.m_Data.Diffuse = mate.Diffuse;
	// 反射色(Specular)
	m_cb2_PerMaterial.m_Data.Specular = mate.Specular;
	// 反射の強さ(Power)
	m_cb2_PerMaterial.m_Data.SpePower = mate.Power;
	// 発光色(Emissive)
	m_cb2_PerMaterial.m_Data.Emissive = mate.Emissive;
}

void InstancingStaticMeshRenderer::SetTextures(ZMaterial& mate)
{
	// テクスチャをセット
	if (mate.TexSet->GetTex(0))
		mate.TexSet->GetTex(0)->SetTexturePS(0);
	else
		ZDx.GetWhiteTex()->SetTexturePS(0);
	// エミッシブマップ
	if (mate.TexSet->GetTex(1))
		mate.TexSet->GetTex(1)->SetTexturePS(1);
	else
		ZDx.RemoveTexturePS(1);
	// トゥーンテクスチャ
	if (mate.TexSet->GetTex(2))
		mate.TexSet->GetTex(2)->SetTexturePS(2);
	else
		ZDx.GetToonTex()->SetTexturePS(2);
	// 材質加算スフィアマップ
	if (mate.TexSet->GetTex(3))
		mate.TexSet->GetTex(3)->SetTexturePS(3);
	else
		ZDx.RemoveTexturePS(3);
	// リフレクションマップ
	if (mate.TexSet->GetTex(4))
		mate.TexSet->GetTex(4)->SetTexturePS(4);
	else
		ZDx.RemoveTexturePS(4);
	// スペキュラマップ
	if (mate.TexSet->GetTex(5))
		mate.TexSet->GetTex(5)->SetTexturePS(5);
	else
		ZDx.GetWhiteTex()->SetTexturePS(5);
	// 材質乗算スフィアマップ(MMD用に拡張)
	if (mate.TexSet->GetTex(6))
		mate.TexSet->GetTex(6)->SetTexturePS(6);
	else
		ZDx.GetWhiteTex()->SetTexturePS(6);
	// 法線マップ
	if (mate.TexSet->GetTex(9))
		mate.TexSet->GetTex(9)->SetTexturePS(10);
	else
		ZDx.GetNormalTex()->SetTexturePS(10);
}
