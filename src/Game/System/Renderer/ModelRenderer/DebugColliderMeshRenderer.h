#ifndef __DEBUG_COLLIDER_MESH_RENDERER_H__
#define __DEBUG_COLLIDER_MESH_RENDERER_H__

#include "../RendererBase.h"
#include "Shader/ContactBufferStructs.h"

class DebugColliderMeshRenderer : public RendererBase
{
public:
	virtual ~DebugColliderMeshRenderer()
	{
	}

	// 初期化
	virtual bool Init(const ZString& vsPath, const ZString& psPath)override;
	// 解放
	virtual void Release()override;

	// (ObjectData)ライティングON/OFF
	void SetLightEnable(bool enable)
	{
		m_cb1_PerObject.m_Data.LightEnable = enable ? 1 : 0;
	}

	// (ObjectData)ワールド行列をセット
	void SetWorldMat(const ZMatrix& mat)
	{
		m_cb0_Matrix.m_Data.Mat = mat;
	}

	void Begin();
	void End();

	// ワイヤーフレーム描画
	void DrawMeshWireframe(ZMesh& mesh, const ZMatrix& mat,const ZVec4* color);

protected:
	// コンタクトバッファをシェーダーにセット
	virtual void SetConstantBuffers()override;

private:
	ZConstantBuffer<cbPerObject_Matrix> m_cb0_Matrix;
	ZConstantBuffer<cbPerObject_STModel> m_cb1_PerObject;
	ZConstantBuffer<cbPerMaterial> m_cb2_PerMaterial;

	ZRasterizeState m_Save;
	ZRasterizeState m_WireframeState;
};

#endif