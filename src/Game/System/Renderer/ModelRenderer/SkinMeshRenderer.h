#ifndef __SKIN_MESH_RENDERER_H__
#define __SKIN_MESH_RENDERER_H__

#include "ModelRendererBase.h"
#include "Shader/ContactBufferStructs.h"

class SkinMeshRenderer : public ModelRendererBase<ZGameModel,ZBoneController, Object3D_RenderFlgs>
{
public:
	~SkinMeshRenderer()
	{
		Release();
	}

	// 初期化
	virtual bool Init(const ZString& vsPath, const ZString& psPath) override;
	// 解放
	virtual void Release() override;
	
	// 描画(m_RenderBufferに追加)
	virtual void Submit(ZMatrix* mat, ZGameModel* model,ZBoneController* bc, Object3D_RenderFlgs* flg) override;
	// 本描画(m_RenderBufferに溜め込まれた描画情報を元に描画)
	virtual void Flash() override;


	void Shadow();

	void Z_Prepass();

private:
	// コンタクトバッファをシェーダーにセット
	virtual void SetConstantBuffers() override;


	void SetMatrix(const ZMatrix& mat)
	{
		m_cb0_Matrix.m_Data.Mat = mat;
	}

	void SetXColor(const ZVec4* col)
	{
		if (col == nullptr)
			m_cb1_PerObject.m_Data.X_Col.Set(ZVec4::zero);
		else
			m_cb1_PerObject.m_Data.X_Col = *col;
	}
	void SetMaterial(ZMaterial& mate);

	void SetTextures(ZMaterial& mate);

private:
	ZConstantBuffer<cbPerObject_Matrix> m_cb0_Matrix;
	ZConstantBuffer<cbPerObject_STModel> m_cb1_PerObject;
	ZConstantBuffer<cbPerMaterial> m_cb2_PerMaterial;

	//	
	ZSP<ZVertexShader>	m_VS_Shadow;
	ZSP<ZPixelShader>	m_PS_Shadow;
	ZSP<ZPixelShader>	m_PS_Chara;

	ZSP<ZPixelShader> m_PsMono;
};

#endif

