#ifndef __SPRITE_RENDERER_H__
#define __SPRITE_RENDERER_H__

#include "../RendererBase.h"
#include "Shader/ContactBufferStructs.h"

class SpriteRenderer : public RendererBase
{
public:
	virtual ~SpriteRenderer()
	{
	}

	// 初期化
	virtual bool Init(const ZString& vsPath, const ZString& psPath)override;
	// 解放
	virtual void Release()override;

	// 2D描画開始
	void Begin(bool enableZBuffer = false, bool useLinerSampler = false);

	// 2D描画終了
	void End();

	// 2D描画
	void Draw2D(ID3D11ShaderResourceView* src, float x, float y, float w, float h, const ZVec4* color = &ZVec4(1, 1, 1, 1));
	void Draw2D(ZTexture& src, float x, float y, float w, float h, const ZVec4* color = &ZVec4(1, 1, 1, 1))
	{
		Draw2D(src.GetTex(), x, y, w, h, color);
	}

	// 2D描画(行列指定ver)
	void Draw2D(ID3D11ShaderResourceView* src, const ZMatrix& m = ZMatrix::Identity, const ZVec4* color = &ZVec4(1, 1, 1, 1));
	void Draw2D(ZTexture& src, const ZMatrix& m = ZMatrix::Identity, const ZVec4* color = &ZVec4(1, 1, 1, 1))
	{
		Draw2D(src, m, color);
	}

	// 2D線描画
	void DrawLine2D(const ZVec3& p1, const ZVec3& p2, const ZVec4* color = &ZVec4(1, 1, 1, 1));

	// スプライトへフォント描画
	//	戻り値 ... 描画したフォントのテクスチャ
	ZSP<ZFontSprite> DrawFont(int fontNo, const ZString& text, float x, float y, const ZVec4* color = &ZVec4::one, int antiAliasingFlag = 0);

	// スプライトへフォント描画(行列指定ver)
	//	戻り値 ... 描画したフォントのテクスチャ
	ZSP<ZFontSprite> DrawFont(int fontNo, const ZString& text, const ZMatrix& m, const ZVec4* color = &ZVec4::one, int antiAliasingFlag = 0);

	// フォント描画(fontSpriteを直接指定)
	void DrawFont(ZFontSprite& fontSprite, float x, float y, const ZVec4* color = &ZVec4::one, int antialiasingFlag = 0);

	// フォント描画(fontSpriteを直接指定 行列指定ver)
	void DrawFont(ZFontSprite& fontSprite, const ZMatrix& m, const ZVec4* color = &ZVec4::one, int antialiasingFlag = 0);

protected:
	// コンタクトバッファをシェーダーにセット
	virtual void SetConstantBuffers()override;

protected:
	// リング頂点バッファ
	ZRingDynamicVB m_RingVB;

	ZConstantBuffer<cbPerObject_Matrix> m_cb1_PerObject;

	// ステート記憶用
	ZDepthStencilState m_DSBackUp;
	ZBlendState m_BSBackUp;

	bool m_IsBegined;

	ZMatrix m_Proj2DMat;
};


#endif
