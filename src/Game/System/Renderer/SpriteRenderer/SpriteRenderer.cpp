#include "Game/Application.h"
#include "SpriteRenderer.h"

bool SpriteRenderer::Init(const ZString& vsPath, const ZString& psPath)
{
	Release();

	bool successed = true;

	// シェーダ読み込み
	m_VS = APP.m_ResStg.LoadVertexShader(vsPath);
	m_PS = APP.m_ResStg.LoadPixelShader(psPath);
	successed &= m_cb1_PerObject.Create(1);

	m_RingVB.Create(ZVertex_Pos_UV_Color::GetVertexTypeData(), 1000);
	m_IsBegined = false;
	return successed;
}

void SpriteRenderer::Release()
{
	m_cb1_PerObject.Release();
	if (m_VS)m_VS->Release();
	if (m_PS)m_PS->Release();
}

void SpriteRenderer::Begin(bool enableZBuffer, bool useLinerSampler)
{
	if (m_IsBegined)return;

	m_IsBegined = true;

	// 2D用射影行列作成
	UINT pNumViewports = 1;

	D3D11_VIEWPORT vp;
	ZDx.GetDevContext()->RSGetViewports(&pNumViewports, &vp);
	m_Proj2DMat.CreateOrthoLH(vp.Width, vp.Height,0,1);
	m_Proj2DMat.Move(-1, -1, 0); // 左上を基準に
	m_Proj2DMat.Scale(1, -1, 1); // 上下反転

	// 定数バッファ書き込み
	m_cb1_PerObject.m_Data.Mat = m_Proj2DMat;
	m_cb1_PerObject.WriteData();

	// 現状の各種ステート記憶
	m_DSBackUp.SetAll_GetState();
	m_BSBackUp.SetAll_GetState();

	// Z書き込み、判定無効化
	ZDepthStencilState ds;
	ds.Set_FromDesc(m_DSBackUp.GetDesc());
	ds.Set_ZEnable(enableZBuffer);
	ds.Set_ZWriteEnable(enableZBuffer);
	ds.SetState();

	// ブレンドステート
	ZBlendState bs;
	bs.Set_Alpha();
	bs.SetState();
	
	// サンプラーステート
	if (useLinerSampler)
		ShMgr.m_smp1_Linear_Clamp.SetStatePS(5);
	else
		ShMgr.m_smp3_Point_Clamp.SetStatePS(5);
}

void SpriteRenderer::End()
{
	if (!m_IsBegined)return;

	m_IsBegined = false;

	// ステート復元
	m_BSBackUp.SetState();
	m_DSBackUp.SetState();
}

void SpriteRenderer::Draw2D(ID3D11ShaderResourceView * src, float x, float y, float w, float h, const ZVec4 * color)
{
	bool isBgn = m_IsBegined;
	if (isBgn == false)
		Begin();
	
	SetConstantBuffers();

	// テクスチャ
	if (src)
		ZDx.GetDevContext()->PSSetShaderResources(0, 1, &src);
	else
		ZDx.GetWhiteTex()->SetTexturePS(0);

	// 頂点作成
	float x2 = x + w;
	float y2 = y + h;
	const int numVtx = 4;
	ZVertex_Pos_UV_Color vertices[numVtx] =
	{
		{ ZVec3(x,y2,0),ZVec2(0,1),*color },
		{ ZVec3(x,y,0),ZVec2(0,0),*color },
		{ ZVec3(x2,y2,0),ZVec2(1,1),*color },
		{ ZVec3(x2,y,0),ZVec2(1,0),*color }
	};

	m_RingVB.SetDrawData();
	m_RingVB.WriteAndDraw(vertices, numVtx);

	// セットされているテクスチャの解除
	ZDx.RemoveTexturePS(0);
	
	if (isBgn == false)
		End();
}

void SpriteRenderer::Draw2D(ID3D11ShaderResourceView * src, const ZMatrix & m, const ZVec4 * color)
{
	if (src == nullptr)
		return;

	// テクスチャ情報取得
	ComPtr<ID3D11Resource> resource;
	src->GetResource(&resource);
	ComPtr<ID3D11Texture2D> texture2D;
	if (FAILED(resource.As(&texture2D)))
		return;
	D3D11_TEXTURE2D_DESC desc;
	texture2D->GetDesc(&desc);

	bool isBgn = m_IsBegined;
	if (isBgn == false)
		Begin();

	SetConstantBuffers();

	// テクスチャ
	ZDx.GetDevContext()->PSSetShaderResources(0, 1, &src);

	// 頂点作成
	float imageW = (float)desc.Width;
	float imageH = (float)desc.Height;

	const int numVtx = 4;
	ZVertex_Pos_UV_Color vertices[numVtx] =
	{
		{ ZVec3(0,imageH,0),ZVec2(0,1),*color },
		{ ZVec3(0,0,0),ZVec2(0,0),*color },
		{ ZVec3(imageW,imageH,0),ZVec2(1,1),*color },
		{ ZVec3(imageW,0,0),ZVec2(1,0),*color }
	};
	// 変換
	for (auto& v : vertices)
		v.Pos.Transform(m);

	m_RingVB.SetDrawData();
	m_RingVB.WriteAndDraw(vertices, numVtx);

	// セットされているテクスチャの解除
	ZDx.RemoveTexturePS(0);

	if (isBgn)
		End();
}

void SpriteRenderer::DrawLine2D(const ZVec3 & p1, const ZVec3 & p2, const ZVec4 * color)
{
	bool isBgn = m_IsBegined;
	if (isBgn == false)
		Begin();

	SetConstantBuffers();

	// 定数バッファ書き込み
	m_cb1_PerObject.m_Data.Mat = m_Proj2DMat;
	m_cb1_PerObject.WriteData();

	// テクスチャ
	ZDx.GetWhiteTex()->SetTexturePS(0);

	// 線の頂点作成
	const int numVtx = 2;
	ZVertex_Pos_UV_Color vertices[numVtx] = 
	{
		{p1,ZVec2(0,0),*color},
		{p2,ZVec2(0,0),*color}
	};

	m_RingVB.SetDrawData(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	m_RingVB.WriteAndDraw(vertices, 2);

	ZDx.RemoveTexturePS(0);

	if (isBgn == false)
		End();
}

ZSP<ZFontSprite> SpriteRenderer::DrawFont(int fontNo, const ZString& text, float x, float y, const ZVec4* color, int antiAliasingFlag)
{
	ZSP<ZFontSprite> fontSprite;

	ZMatrix m;
	m.CreateMove(x, y, 0);
	DrawFont(*fontSprite,m,color,antiAliasingFlag);

	return fontSprite;
}

ZSP<ZFontSprite> SpriteRenderer::DrawFont(int fontNo, const ZString & text, const ZMatrix & m, const ZVec4 * color, int antiAliasingFlag)
{
	ZSP<ZFontSprite> fontSprite;

	DrawFont(*fontSprite, m, color, antiAliasingFlag);

	return fontSprite;
}

void SpriteRenderer::DrawFont(ZFontSprite & fontSprite, float x, float y, const ZVec4 * color, int antialiasingFlag)
{
	ZMatrix m;
	m.CreateMove(x, y, 0);
	
	DrawFont(fontSprite, m, color, antialiasingFlag);
}

void SpriteRenderer::DrawFont(ZFontSprite & fontSprite, const ZMatrix & m, const ZVec4 * color, int antialiasingFlag)
{
	if (fontSprite.GetTexList().size() <= 0)
		return;

	bool isBgn = m_IsBegined;
	if (isBgn == false)
		Begin();

	SetConstantBuffers();

	m_RingVB.SetDrawData();

	const int numVtx = 4;
	ZVertex_Pos_UV_Color vertices[numVtx];
	for (auto& v : vertices)
		v.Color = *color;

	// フォントの高さ
	float h = (float)fontSprite.GetTexList()[0]->Tex->GetInfo().Height;

	// すべての文字テクスチャを描画する
	float moveX = 0;
	ZMatrix mat = m;
	for (auto& data : fontSprite.GetTexList())
	{
		// 改行文字の場合は座標操作
		if (data->Code == '\n')
		{
			mat.Move_Local(-moveX, h, 0);
			moveX = 0;
		}
		else
		{
			data->Tex->SetTexturePS(0);
			float imageW = (float)data->Tex->GetInfo().Width;
			float imageH = (float)data->Tex->GetInfo().Height;
			vertices[0].Pos.Set(0, imageH, 0);
			vertices[0].UV.Set(0, 1);
			vertices[0].Pos.Transform(mat);

			vertices[1].Pos.Set(0, 0, 0);
			vertices[1].UV.Set(0, 0);
			vertices[1].Pos.Transform(mat);

			vertices[2].Pos.Set(imageW, imageH, 0);
			vertices[2].UV.Set(1, 1);
			vertices[2].Pos.Transform(mat);

			vertices[3].Pos.Set(imageW, 0, 0);
			vertices[3].UV.Set(1, 0);
			vertices[3].Pos.Transform(mat);
			
			// 1文字描画
			m_RingVB.WriteAndDraw(vertices,numVtx);
			
			// 次の文字のためX座標の間隔を開ける
			mat.Move_Local((float)data->Tex->GetInfo().Width, 0, 0);
			
			moveX += data->Tex->GetInfo().Width;
		}

	}

	ZDx.RemoveTexturePS(0);

	if (isBgn == false)
		End();
}

void SpriteRenderer::SetConstantBuffers()
{
	// シェーダーセット
	m_VS->SetShader();
	m_PS->SetShader();

	// 定数バッファセット
	m_cb1_PerObject.SetVS();
	m_cb1_PerObject.SetPS();
}
