#include "Game/Application.h"

bool X_Ray::Init(const ZString & vsPath, const ZString & psPath)
{
	
	m_VS = APP.m_ResStg.LoadVertexShader(vsPath);
	m_PS = APP.m_ResStg.LoadPixelShader(psPath);

	m_RingBuf.Create(ZVertex_Pos_UV::GetVertexTypeData(), 4);

	m_cbXRay.Create(0);

	return true;
}

void X_Ray::Release()
{
	m_cbXRay.Release();
	m_RingBuf.Release();
	m_VS = nullptr;
	m_PS = nullptr;
}

void X_Ray::Render(ZSP<ZTexture> base, ZSP<ZTexture> charaDepth, ZSP<ZTexture> allDepth, ZSP<ZTexture> ref, ZSP<ZTexture> out, const ZVec2& size)
{
	ZRenderTarget_BackUpper bu;

	//	変換行列作成
	{
		UINT pNumViewports = 1;
		D3D11_VIEWPORT vp;
		ZDx.GetDevContext()->RSGetViewports(&pNumViewports, &vp);

		ZMatrix p2DMat;
		p2DMat.CreateOrthoLH(size.x, size.y, 0, 1);
		p2DMat.Move(-1, -1, 0);
		p2DMat.Scale(1, -1, 1);
		m_cbXRay.m_Data.view = p2DMat;
	}

	ZRenderTargets rt;
	rt.GetNowTop();
	rt.RT(0, out->GetRTTex());
	rt.Depth(nullptr);
	rt.SetToDevice();

	//	レンダリングステート保存
	ZDepthStencilState	DSBackUp;
	ZBlendState			BSBackUp;
	{
		// 現状の各種ステート記憶
		DSBackUp.SetAll_GetState();
		BSBackUp.SetAll_GetState();

		// Z書き込み、判定無効化
		ZDepthStencilState ds;
		ds.Set_FromDesc(DSBackUp.GetDesc());
		ds.Set_ZEnable(false);
		ds.Set_ZWriteEnable(false);
		ds.SetState();

		// ブレンドステート
		ZBlendState bs;
		bs.Set_Alpha();
		bs.SetState();
	}

	//	シェーダセット
	m_VS->SetShader();
	m_PS->SetShader();
	m_cbXRay.SetVS();
	m_cbXRay.SetPS();

	m_cbXRay.WriteData();

	//	テクスチャセット
	charaDepth->SetTexturePS(0);
	allDepth->SetTexturePS(1);
	base->SetTexturePS(2);
	ref->SetTexturePS(3);


	// 頂点作成
	const int	 numVtx = 4;
	ZVertex_Pos_UV vertices[numVtx] =
	{
	{ ZVec3(0.f, size.y,0) 	 ,	ZVec2(0,1), },
	{ ZVec3(0.f,0.f, 0)      ,	ZVec2(0,0), },
	{ ZVec3(size.x,size.y, 0),	ZVec2(1,1), },
	{ ZVec3(size.x,0.f,	0)	 ,	ZVec2(1,0), }
	};


	//	描画
	m_RingBuf.SetDrawData();
	m_RingBuf.WriteAndDraw(vertices, numVtx);



	//	テクスチャ解除
	ZDx.RemoveTexturePS(0);
	ZDx.RemoveTexturePS(1);
	ZDx.RemoveTexturePS(2);
	ZDx.RemoveTexturePS(3);


	//	ステート戻す
	BSBackUp.SetState();
	DSBackUp.SetState();
}
