#ifndef __X_RAY_H__
#define __X_RAY_H__

#include "Shader/ContactBufferStructs.h"

class X_Ray :public RendererBase
{
public:
	X_Ray() = default;
	~X_Ray()
	{
		Release();
	}

	bool Init(const ZString& vsPath, const ZString& psPath)override;
	void Release()override;

	void Render(ZSP<ZTexture> base, ZSP<ZTexture> charaDepth, ZSP<ZTexture> allDepth, ZSP<ZTexture> ref, ZSP<ZTexture> out,const ZVec2& size);

	void SetXColor(const ZVec4& col) { m_cbXRay.m_Data.xCol = col; }

#if _DEBUG
	//	主にデバッグ用
	ZVec4& GetXColor() { return  m_cbXRay.m_Data.xCol; }
#endif

	template<class Archive>
	void serialize(Archive & archive)
	{
		SaveVector4(archive, m_cbXRay.m_Data.xCol, "Color");
	}

private:
	void SetConstantBuffers() {};

private:
	ZRingDynamicVB	m_RingBuf;
	ZConstantBuffer<cbXRay> m_cbXRay;

};

#endif