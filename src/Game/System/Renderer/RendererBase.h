#ifndef __RENDERER_BASE_H__
#define __RENDERER_BASE_H__

#include "Game/System/Renderer/RenderState.h"

class RendererBase
{
public:
	RendererBase()
	{
	}

	virtual ~RendererBase()
	{
	}

	// 初期化
	virtual bool Init(const ZString& vsPath, const ZString& psPath) = 0;

	// 解放
	virtual void Release() = 0;

	inline ZShaderSet& GetShaderSet()
	{
		return m_Shaders;
	}

protected:
	// コンスタントバッファをシェーダーにセット
	virtual void SetConstantBuffers() = 0;

protected:
	ZSP<ZVertexShader> m_VS;
	ZSP<ZPixelShader>  m_PS;
	ZShaderSet m_Shaders;
};

template<typename ...Args>
class BatchRenderer : public RendererBase
{
public:
	BatchRenderer()
	{
	}
	virtual ~BatchRenderer()
	{
	}

	// 初期化
	virtual bool Init(const ZString& vsPath, const ZString& psPath) = 0;

	// 解放
	virtual void Release() = 0;

	// 描画(バッファに保存)
	virtual void Submit(Args ...param) = 0;
	// 本描画(バッファに溜め込まれた描画情報を元に描画)
	virtual void Flash() = 0;

protected:
	// コンタクトバッファをシェーダーにセット
	virtual void SetConstantBuffers() = 0;

};

#endif