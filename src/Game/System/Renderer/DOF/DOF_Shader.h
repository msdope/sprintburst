#ifndef __DOF_SHADER_H__
#define __DOF_SHADER_H__

#include "Shader/ContactBufferStructs.h"

class DOF_Shader :public RendererBase
{
public:
	~DOF_Shader() { Release(); }

	bool Init(const ZString& vsPath, const ZString& psPath)override;
	void Release()override;

	//	ベースパスの画像とぼけた画像をブレンドする
	void Render(ZSP<ZTexture> base, ZSP<ZTexture> blur, ZSP<ZTexture> dof,ZSP<ZTexture> out,const ZVec2& size);

	//	深度マップからDOFテクスチャを作成する
	void GenerateDOFMap(ZSP<ZTexture> base, ZSP<ZTexture> out,const ZVec2& size);

private:
	void SetConstantBuffers() {};

private:
	ZRingDynamicVB	m_RingBuf;
	ZConstantBuffer<cbView> m_View;

	//	深度抽出用PS
	ZSP<ZPixelShader> m_ExtractionPS;
};

#endif