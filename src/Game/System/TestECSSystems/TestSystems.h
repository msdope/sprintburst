#ifndef __TEST_SYSTEMS_H__
#define __TEST_SYSTEMS_H__

class MoveUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent, MotionComponent)
public:
	MoveUpdateSystem();
	 
	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
	
private:
	void verlet(ZVec3& pos, ZVec3& velocity, const ZVec3& acceleration, float delta);

	void forestRuth(ZVec3& pos, ZVec3& velocity, const ZVec3& acceleration, float delta);

};

class AnimationUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent,ModelBoneControllerComponent,AnimatorComponent)
public:
	AnimationUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;
};

class StaticMeshDrawSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent,GameModelComponent)
public:
	StaticMeshDrawSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

private:
	bool m_IsInstancingDraw;

};

class SkinMeshDrawSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent, GameModelComponent,ModelBoneControllerComponent)
public:
	SkinMeshDrawSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;
};

class BoxSpawnerSystem : public ECSSystemBase
{
public:
	BoxSpawnerSystem(ZAVector<ZSP<ECSEntity>>& entityList);

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

private:
	ZAVector<ZSP<ECSEntity>>* m_pEntityList;
	ZSP<ZGameModel> m_BoxModel;
	size_t m_NumSpawnEnitites;

};

class PhysicsSystem : public ECSSystemBase
{
public:
	PhysicsSystem(ZPhysicsWorld* world) : m_pPhysicsWorld(world)
	{
	}

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

private:
	ZPhysicsWorld* m_pPhysicsWorld;
};

class CharaDebugSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent,CharaComponent)
public:
	CharaDebugSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

};

//VRコントローラを出すためだけのSystem
//class VRControllSystem : public ECSSystemBase
//{
//	DefUseComponentType(TransformComponent,VRControllerComponent)
//public:
//	VRControllSystem();
//
//	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
//};

class ColliderSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent, ColliderComponent)
public:
	ColliderSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;
};











/*--------------------------------------------------------------------------------------------------------------------------------------*/

//	テスト描画用
class TestDrawSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent,GameModelComponent)

public:
	TestDrawSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

private:
	bool m_IsInstancingDraw;
};


//	テスト
class TestSubmitCullingSpaceSystem : public ECSSystemBase
{
	DefUseComponentType(TransformComponent, GameModelComponent)

public:
	TestSubmitCullingSpaceSystem(ZOctree<GameModelComponent>& cullingOctreeSpace);

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

private:
	ZOctree<GameModelComponent>* m_pCullingOctreeSpace;
};

#endif