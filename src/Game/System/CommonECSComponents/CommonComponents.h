#ifndef __COMMON_COMPONENTS_H__
#define __COMMON_COMPONENTS_H__

#include "ZECS/ECSComponent.h"


// 移動量コンポーネント
DefComponent(VelocityComponent)
{
	ZVec3 Velocity;
};

// ゲーム用モデルコンポーネント
DefComponent(GameModelComponent)
{
	ZSP<ZGameModel> Model;
	ZSP<Object3D_RenderFlgs> RenderFlg;
	virtual void InitFromJson(const json11::Json& jsonObj);
};

// 単一メッシュコンポーネント
DefComponent(SingleModelComponent)
{
	ZSP<ZSingleModel> Model;
};

// 当たり判定
DefComponent(ColliderComponent)
{
	ZSP<ZGameModel> HitModel;
	ZSP<Collider_Mesh> HitObj;
	virtual void InitFromJson(const json11::Json& jsonObj);
};

// ゲーム用モデルのボーンコントローラーコンポーネント
DefComponent(ModelBoneControllerComponent)
{
	ZSP<ZBoneController> BoneController;
	virtual void InitFromJson(const json11::Json& jsonObj);
};

// アニメーターコンポーネント
DefComponent(AnimatorComponent)
{
	bool Enable;
	ZSP<ZAnimator> Animator;
	bool InitAnimeLoop;
	ZString InitAnimeName;
	std::function<void(ZAnimeKey_Script*)>ScriptProc;

	virtual void InitFromJson(const json11::Json& jsonObj);
};

// UI用コンポーネント
DefComponent(UIComponent)
{
	ZAVector<ZSP<ZTexture>> TexList;	// UI用テクスチャリスト
};

#endif