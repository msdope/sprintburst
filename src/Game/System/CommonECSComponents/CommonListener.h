#ifndef __COMMON_LISTENER_H__
#define __COMMON_LISTENER_H__

#include "CommonComponents.h"

// ゲーム用モデルのボーンコントローラーリスナー
class ModelBoneControllerListener : public ECSListener
{
public:
	ModelBoneControllerListener();

	virtual ~ModelBoneControllerListener()override {}

	virtual void OnMakeEntity(EntityHandle handle)override {}
	virtual void OnRemoveEntity(EntityHandle hadle)override {}

	virtual void OnAddComponent(EntityHandle handle, uint32 id)override;
	
	virtual void OnRemoveComponent(EntityHandle handle, uint32 id)override {}

};

// ColliderComponentのリスナー
class ColliderListener : public ECSListener
{
public:
	ColliderListener()
	{
		AddComponentID(ColliderComponent::ID);
	}

	virtual ~ColliderListener()override {}

	virtual void OnMakeEntity(EntityHandle handle)override {}
	virtual void OnRemoveEntity(EntityHandle hadle)override {}

	virtual void OnAddComponent(EntityHandle handle, uint32 id)override
	{
		auto colliderComp = ECS.GetComponent<ColliderComponent>(handle);

		colliderComp->HitObj->m_UserMap["Entity"] = colliderComp->m_Entity;
	}
	virtual void OnRemoveComponent(EntityHandle handle, uint32 id)override {}


};

#endif