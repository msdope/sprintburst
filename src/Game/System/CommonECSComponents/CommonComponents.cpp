#include "Game/Application.h"
#include "CommonComponents.h"

void GameModelComponent::InitFromJson(const json11::Json & jsonObj)
{
	// モデル読み込み
	Model = APP.m_ResStg.LoadMesh(jsonObj["ModelFileName"].string_value().c_str());
	if (Model == nullptr)
	{
		MessageBox(APP.m_Window->GetWindowHandle(), jsonObj["ModelFileName"].string_value().c_str(), "", MB_OK);
		if (MessageBox(APP.m_Window->GetWindowHandle(), "終了しますか", "", MB_YESNO) == IDYES)
		{
			exit(0);
			return;
		}
		
	}

	// レンダーフラグ
	RenderFlg = Make_Shared(Object3D_RenderFlgs, appnew);
	RenderFlg->Character = jsonObj["Character"].bool_value();
	RenderFlg->SemiTransparent = jsonObj["SemiTransparent"].bool_value();
	RenderFlg->BaseFrustumOut = jsonObj["BaseFrustumOut"].bool_value();
	RenderFlg->NoneShadowRender = jsonObj["NoneShadowRender"].bool_value();
}

void ColliderComponent::InitFromJson(const json11::Json & jsonObj)
{
	// 当たり判定用モデル
	HitModel = APP.m_ResStg.LoadMesh(jsonObj["ModelFileName"].string_value().c_str());
	
	// 当たり判定マスク
	int hitGroups = 0;
	auto ary = jsonObj["HitGroups"].array_items();
	for (auto& n : ary)
	{
		hitGroups |= 1 << n.int_value();
	}
	HitObj = Make_Shared(Collider_Mesh,appnew);

	// 基本設定
	HitObj->Init(0,
		hitGroups,
		0xFFFFFFFF, // 形状マスク
		hitGroups // 判定される側時のマスク
	);

	// ColliderComponent情報を仕込ませておく
	//HitObj->m_UserMap["ColliderComp"] = HitObj;
	// メッシュを追加   
	HitObj->AddMesh(HitModel);

}

void ModelBoneControllerComponent::InitFromJson(const json11::Json & jsonObj)
{
	BoneController = Make_Shared(ZBoneController,appnew);
}

void AnimatorComponent::InitFromJson(const json11::Json & jsonObj)
{
	Animator = Make_Shared(ZAnimator,appnew);
	Enable = jsonObj["Enable"].bool_value();

	InitAnimeLoop = jsonObj["InitAnimeLoop"].bool_value();
	InitAnimeName = jsonObj["InitAnimeName"].string_value().c_str(); 

	ScriptProc = nullptr;

}
