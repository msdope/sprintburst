#include "Game/Application.h"
#include "CommonListener.h"

ModelBoneControllerListener::ModelBoneControllerListener()
{
	AddComponentID(ModelBoneControllerComponent::ID);
	AddComponentID(AnimatorComponent::ID);
	AddComponentID(GameModelComponent::ID);

}

void ModelBoneControllerListener::OnAddComponent(EntityHandle handle, uint32 id)
{
	auto bcComp = ECS.GetComponent<ModelBoneControllerComponent>(handle);
	auto animComp = ECS.GetComponent<AnimatorComponent>(handle);
	auto modelComp = ECS.GetComponent<GameModelComponent>(handle);

	if (bcComp)
	{
		if (modelComp)
		{
			bcComp->BoneController->SetModel(modelComp->Model);
			bcComp->BoneController->AddAllPhysicsObjToPhysicsWorld(PHYSICS);
		}

		if (animComp)
		{
			bcComp->BoneController->InitAnimator(*animComp->Animator);
			animComp->Animator->ChangeAnime(animComp->InitAnimeName, animComp->InitAnimeLoop);
			animComp->Animator->EnableRootMotion(true);
		}
	}

	//if (bcComp && animComp && modelComp)
	//{
	//	bcComp->BoneController->SetModel(modelComp->Model);
	//	bcComp->BoneController->AddAllPhysicsObjToPhysicsWorld(*APP.m_PhysicsWorld.GetPtr());

	//	bcComp->BoneController->InitAnimator(*animComp->Animator);
	//	animComp->Animator->ChangeAnime(animComp->InitAnimeName, true);
	//	animComp->Animator->EnableRootMotion(true);	
	//}
}
