#include "Game/Application.h"
#include "MapObjectComponents.h"


const int SecurityComponent::MaxGage = 120 * 60; // （秒数*fps）
const int SecurityComponent::DecreaseGage = 10 * 60; // （秒数*fps）

void SecurityComponent::InitFromJson(const json11::Json & jsonObj)
{
	Enable = true;
	Gage = jsonObj["Gage"].int_value();
}

const int ServerComponent::MaxGage = 30 * 60; // （秒数*fps）

void ServerComponent::InitFromJson(const json11::Json & jsonObj)
{
	Enable = true;
	Gage = jsonObj["Gage"].int_value();
}


