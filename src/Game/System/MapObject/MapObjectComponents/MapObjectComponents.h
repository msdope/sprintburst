#ifndef __MAPOBJECT_COMPONENTS_H__
#define __MAPOBJECT_COMPONENTS_H__

// セキュリティ
DefComponent(SecurityComponent)
{
	bool Enable;
	int NumAccess;	// アクセスされている数
	int Gage;		// 現在のゲージ

	ZSP<Effekseer::Effect> NormalEffect;	// 通常時
	ZSP<Effekseer::Effect> HackEffect;		// ハッキング完了時
	ZSP<Effekseer::Effect> ErrorEffect;		// スキルチェック失敗時

	static const int MaxGage; // ゲージが溜まりきる時間
	static const int DecreaseGage; // スキルチェック失敗時の減少分

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};

// サーバー
DefComponent(ServerComponent)
{
	bool Enable;
	int NumAccess;	// アクセスされている数
	int Gage;		// 現在のゲージ

	static const int MaxGage; // ゲージが溜まりきる時間

	virtual void InitFromJson(const json11::Json& jsonObj)override;
};


#endif