#ifndef __MAPOBJECT_SYSTEMS_H__
#define __MAPOBJECT_SYSTEMS_H__


class SecurityUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(ModelBoneControllerComponent,
		AnimatorComponent,TransformComponent,
		ColliderComponent,SecurityComponent)
public:
	SecurityUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;
	
};

class ServerUpdateSystem : public ECSSystemBase
{
	DefUseComponentType(ModelBoneControllerComponent,
		TransformComponent, ColliderComponent,
		ServerComponent)
public:
	ServerUpdateSystem();

	virtual void UpdateComponents(float delta, UpdateCompParams components)override;

	virtual void LateUpdateComponents(float delta, UpdateCompParams components)override;

	virtual void DebugImGuiRender()override;

};

#endif