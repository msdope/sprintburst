#include "Game/Application.h"
#include "SubSystems.h"

HitResData::FaceHitInfo::FaceHitInfo(int meshNo, int faceIndex, const ZVec3& nearHitPos)
	: MeshNo(meshNo), FaceIndex(faceIndex), vNearHitPos(nearHitPos)
{
}

HitResData::Node::Node()
	: MyHitObj(nullptr),
	YouHitObj(nullptr)
{
}

void HitResData::Init()
{
	HitDataList.clear();
}

#pragma region ColliderBase

ColliderBase::ColliderBase()
	: m_ID(0),
	m_AtkFilter(0xFFFFFFFF),
	m_DefFilter(0xFFFFFFFF),
	m_ShapeFilter(0xFFFFFFFF),
	m_Shape(0),
	m_NowLineNo(0),
	m_HitState(0),
	m_Mass(0),
	m_Debug_Color(ZVec4(1))
{
}

ColliderBase::~ColliderBase()
{
}

void ColliderBase::DebugDraw(float alpha)const
{
}

void ColliderBase::Init(size_t id, int atkFilter, int atkShapeFilter, int defFilter)
{
	m_ID = id;
	m_AtkFilter = atkFilter;
	m_DefFilter = defFilter;
	m_ShapeFilter = atkShapeFilter;

	m_IgnoreIDs.clear();
	m_BroadPhaseChack = nullptr;

	m_NowLineNo = 0;
	m_HitState = 0;
	m_HitResTbl.clear();

	m_OnHitEnter = nullptr;
	m_OnHitStay = nullptr;
	m_OnHitExit = nullptr;

	// m_OnHitEnter_Parallel = nullptr;
	// m_OnHitStay_Parallel = nullptr;
	// m_OnHitExit_Parallel = nullptr;

	m_Mass = 0;
	m_Debug_Color = ZVec4(1);

}

float ColliderBase::CalcMassRatio(float mass)const
{
	// 自身の質量が0なら、自身は不動
	if (m_Mass == 0)
		return 0.0f;
	// 相手の質量が0なら、自身は動く
	if (mass == 0)
		return 1.0f;

	// 両方質量あり
	return mass * (1.0f / (m_Mass + mass));
}

bool ColliderBase::IsTest(const ColliderBase* obj)const
{
	// 汎用事前チェック関数
	if (m_BroadPhaseChack)
	{
		if (m_BroadPhaseChack(obj) == false)
			return false;
	}

	// 自分[攻グループ] vs 相手[防グループ]
	if ((m_AtkFilter & obj->m_DefFilter) == false)
		return false;

	// 同じオブジェクトは除外
	if (this == obj)
		return false;

	// 判定して良い形状か
	if ((m_ShapeFilter & obj->GetShape()) == false)
		return false;

	// 無視リスト判定
	for (size_t i = 0; i < m_IgnoreIDs.size(); i++)
	{
		// 一致するなら判定スキップ
		if (m_IgnoreIDs[i] == obj->m_ID)
			return false;
	}

	return true;
}

#pragma endregion

#pragma region Collider_Sphere

Collider_Sphere::Collider_Sphere()
{
	m_Shape = sShape();
}

bool Collider_Sphere::HitTest(const ColliderBase* obj, HitResData* resData_Sph, HitResData* resData2)
{
	// vs 球
	if (obj->GetShape() & HitShapes::SPHERE)
	{
		if (IsTest(obj) == false)
			return false;

		const Collider_Sphere* sph = static_cast<const Collider_Sphere*>(obj);

		// 球判定
		ZVec3 vDir = static_cast<const ZVec3&>(sph->m_Sphere.Center) - m_Sphere.Center;
		const float dist = vDir.Length();

		// 当たっていないから終了
		if (dist >= m_Sphere.Radius + sph->m_Sphere.Radius)
			return false;

		// 当たっているなら結果を登録
		vDir.Normalize();
		float f = (m_Sphere.Radius + sph->m_Sphere.Radius) - dist;
		ZVec3 vPush = (-vDir * f);

		// 自身に結果を記憶する
		if (resData_Sph)
		{
			HitResData::Node node;
			node.MyHitObj = this;
			node.YouHitObj = obj;
			node.vNearHitPos = m_Sphere.Center + vDir * (dist - sph->m_Sphere.Radius);
			node.vPush = vPush;

			resData_Sph->HitDataList.push_back(std::move(node));
		}

		// 自身に結果を記憶する
		if (resData_Sph)
		{
			HitResData::Node node;
			node.MyHitObj = obj;
			node.YouHitObj = this;
			node.vNearHitPos = m_Sphere.Center - vDir * (dist - sph->m_Sphere.Radius);
			node.vPush = -vPush;

			resData_Sph->HitDataList.push_back(std::move(node));
		}

		return true;
	}
	else if (obj->GetShape() & HitShapes::RAY)
	{
		if (IsTest(obj) == false)
			return false;
		const Collider_Ray* ray = static_cast<const Collider_Ray*>(obj);

		return HitTests::SphereToRay(*this, resData_Sph, *ray, resData2);
	}
	else if (obj->GetShape() & HitShapes::BOX)
	{
		if (IsTest(obj) == false)
			return false;

		const Collider_Box* box = static_cast<const Collider_Box*>(obj);

		return HitTests::SphereToBox(*this, resData_Sph, *box, resData2);
	}
	else if (obj->GetShape() & HitShapes::MESH)
	{
		if (IsTest(obj) == false)
			return false;

		const Collider_Mesh* mesh = static_cast<const Collider_Mesh*>(obj);

		return HitTests::SphereToMesh(*this, resData_Sph, *mesh, resData2);
	}
	else if (obj->GetShape() & HitShapes::COMPOUND)
	{
		const Collider_Compound* compound = static_cast<const Collider_Compound*>(obj);

		bool bHit = false;
		for (auto& hitObj : compound->m_Colliders)
		{
			if (IsTest(hitObj) == false)
				continue;

			bool ret = HitTest(hitObj, resData_Sph, resData2);

			if (ret)
				bHit = true;
		}

		return bHit;
	}

	return false;
}

void Collider_Sphere::DebugDraw(float alpha)const
{
	ZVec4 mulCol(1, 1, 1, alpha);

	ShMgr.m_Ls.Submit_SphereLine(m_Sphere.Center, m_Sphere.Radius, (m_Debug_Color * mulCol), &ZMatrix::Identity);

	for (auto& res : m_HitResTbl)
	{
		for (auto& node : res.HitDataList)
		{
			ShMgr.m_Ls.Submit(m_Sphere.Center, node.vNearHitPos, ZVec4(1, 1, 0, alpha));

			// メッシュとのヒット
			for (auto& faceHitInfo : node.Mesh_FaceHitTbl)
				ShMgr.m_Ls.Submit(m_Sphere.Center, faceHitInfo.vNearHitPos, ZVec4(1, 1, 0, alpha));
		}
	}

}

#pragma endregion

#pragma region Collider_Box

Collider_Box::Collider_Box()
{
	m_Shape = sShape();
}

bool Collider_Box::HitTest(const ColliderBase* obj, HitResData* resData_Box, HitResData* resData2)
{
	if (obj->GetShape() & HitShapes::SPHERE)
	{
		if (IsTest(obj) == false)
			return false;

		const Collider_Sphere* sph = static_cast<const Collider_Sphere*>(obj);

		return HitTests::SphereToBox(*sph, resData2, *this, resData_Box);
	}
	else if (obj->GetShape() & HitShapes::RAY)
	{
		if (IsTest(obj) == false)
			return false;

		const Collider_Ray* ray = static_cast<const Collider_Ray*>(obj);

		return HitTests::RayToBox(*ray, resData2, *this, resData_Box);
	}
	else if (obj->GetShape() & HitShapes::BOX)
	{
		if (IsTest(obj) == false)
			return false;

		const Collider_Box* box = static_cast<const Collider_Box*>(obj);

		if (m_Box.Intersects(box->m_Box) == false)
			return false;

		// 自身に結果を記憶する
		if (resData_Box)
		{
			HitResData::Node node;
			node.MyHitObj = this;
			node.YouHitObj = box;
			node.vNearHitPos = box->m_Box.Center;

			resData_Box->HitDataList.push_back(std::move(node));
		}

		// 相手に結果を記憶する
		if (resData2)
		{
			// 結果データ追加
			HitResData::Node node;
			node.MyHitObj = box;
			node.YouHitObj = this;
			node.vNearHitPos = m_Box.Center;

			resData2->HitDataList.push_back(std::move(node));
		}

		return true;
	}
	else if (obj->GetShape() & HitShapes::COMPOUND)
	{
		const Collider_Compound* compound = static_cast<const Collider_Compound*>(obj);

		bool bHit = false;
		for (auto& hitObj : compound->m_Colliders)
		{
			if (IsTest(hitObj) == false)
				continue;

			bool ret = HitTest(hitObj, resData_Box, resData2);

			if (ret)
				bHit = true;
		}

		return bHit;
	}

	return false;
}

void Collider_Box::DebugDraw(float alpha)const
{
	ZVec4 mulCol(1, 1, 1, alpha);

	const ZQuat& q = m_Box.Orientation;
	ShMgr.m_Ls.Submit_BoxLine2(m_Box.Center, m_Box.Extents, (m_Debug_Color * mulCol), &q);

	for (auto& res : m_HitResTbl)
	{
		for (auto& node : res.HitDataList)
			ShMgr.m_Ls.Submit(m_Box.Center, node.vNearHitPos, ZVec4(1, 1, 0, alpha));
	}

}

#pragma endregion

#pragma region Collider_Ray

Collider_Ray::Collider_Ray()
{
	m_Shape = sShape();
}

bool Collider_Ray::HitTest(const ColliderBase* obj, HitResData* resData_Ray, HitResData* resData2)
{
	// vs 球
	if (obj->GetShape() & HitShapes::SPHERE)
	{
		if (IsTest(obj) == false)
			return false;

		const Collider_Sphere* sph = static_cast<const Collider_Sphere*>(obj);

		return HitTests::SphereToRay(*sph, resData2, *this, resData_Ray);
	}
	else if (obj->GetShape() & HitShapes::BOX)
	{
		if (IsTest(obj) == false)
			return false;

		const Collider_Box* box = static_cast<const Collider_Box*>(obj);

		return HitTests::RayToBox(*this, resData_Ray, *box, resData2);
	}
	else if (obj->GetShape() & HitShapes::MESH)
	{
		if (IsTest(obj) == false)
			return false;

		const Collider_Mesh* mesh = static_cast<const Collider_Mesh*>(obj);

		return HitTests::RayToMesh(*this, resData_Ray, *mesh, resData2);
	}
	else if (obj->GetShape() & HitShapes::COMPOUND)
	{
		const Collider_Compound* compound = static_cast<const Collider_Compound*>(obj);

		bool bHit = false;
		for (auto& hitObj : compound->m_Colliders)
		{
			if (IsTest(hitObj) == false)
				return false;

			bool ret = HitTest(hitObj, resData_Ray, resData2);

			if (ret)
				bHit = true;
		}

		return bHit;
	}

	return false;
}

void Collider_Ray::DebugDraw(float alpha)const
{
	ZVec4 mulCol(1, 1, 1, alpha);

	ShMgr.m_Ls.Submit(m_vPos1, m_vPos2, (m_Debug_Color * mulCol));

	// 当たった場所に円描画
	for (auto& res : m_HitResTbl)
	{
		for (auto& node : res.HitDataList)
		{
			ZMatrix m;
			m.SetPos(m_vPos1 + m_vDir * node.RayDist);
			m.SetLookAt(m_vDir, ZVec3::Up);
			ShMgr.m_Ls.Submit_CircleLine(ZVec3::Zero, 0.1f, ZVec4(1, 1, 0, alpha), &m);

			for (auto& faceHitInfo : node.Mesh_FaceHitTbl)
			{
				if (node.YouHitObj->GetShape() != HitShapes::MESH)
					continue;

				const Collider_Mesh* mesh = node.YouHitObj->Cast<const Collider_Mesh>();
				const ZVec3 vN = mesh->m_MeshTbl[faceHitInfo.MeshNo]->GetExtFace()[faceHitInfo.FaceIndex].vN;

				ShMgr.m_Ls.Submit(node.vNearHitPos, node.vNearHitPos + vN * 0.3f, ZVec4(1, 1, 0, 1));

			}
		}
	}

}

#pragma endregion

#pragma region Collider_Mesh

Collider_Mesh::Collider_Mesh()
{
	m_Shape = sShape();
}

bool Collider_Mesh::HitTest(const ColliderBase* obj, HitResData* resData_Mesh, HitResData* resData2)
{
	if (obj->GetShape() & HitShapes::SPHERE)
	{
		if (IsTest(obj) == false)
			return false;

		const Collider_Sphere* sph = static_cast<const Collider_Sphere*>(obj);

		return HitTests::SphereToMesh(*sph, resData2, *this, resData_Mesh);
	}
	else if (obj->GetShape() & HitShapes::RAY)
	{
		if (IsTest(obj) == false)
			return false;

		const Collider_Ray* ray = static_cast<const Collider_Ray*>(obj);

		return HitTests::RayToMesh(*ray, resData2, *this, resData_Mesh);
	}
	else if (obj->GetShape() & HitShapes::COMPOUND)
	{
		const Collider_Compound* compound = static_cast<const Collider_Compound*>(obj);

		bool bHit = false;
		for (auto& hitObj : compound->m_Colliders)
		{
			if (IsTest(hitObj) == false)
				continue;

			bool ret = HitTest(hitObj, resData_Mesh, resData2);

			if (ret)
				bHit = true;
		}

		return bHit;
	}

	// ※ メッシュ同士、Boxとの判定はなし

	return false;
}

void Collider_Mesh::DebugDraw(float alpha)const
{
	ZVec4 mulCol(1, 1, 1, alpha);

	ShMgr.m_Ms.m_DebugColliderMeshRenderer->Begin();
	ShMgr.m_Ms.m_DebugColliderMeshRenderer->SetLightEnable(false);

	for (auto& mesh : m_MeshTbl)
		ShMgr.m_Ms.m_DebugColliderMeshRenderer->DrawMeshWireframe(*mesh, m_Mat, &(m_Debug_Color * mulCol));

	ShMgr.m_Ms.m_DebugColliderMeshRenderer->SetLightEnable(true);
	ShMgr.m_Ms.m_DebugColliderMeshRenderer->End();
}

#pragma endregion

#pragma region Collider_Cmmpound

Collider_Compound::Collider_Compound()
{
	m_Shape = sShape();
}

bool Collider_Compound::HitTest(const ColliderBase* obj, HitResData* resData_Comp, HitResData* resData2)
{
	bool bHit = false;
	for (auto& hitObj : m_Colliders)
	{
		if (hitObj->IsTest(obj) == false)
			continue;
		bool ret = hitObj->HitTest(obj, resData_Comp, resData2);

		if (ret)
			bHit = true;
	}

	return bHit;
}

void Collider_Compound::DebugDraw(float alpha)const
{
	for (auto& hitObj : m_Colliders)
		hitObj->DebugDraw(alpha);
}

#pragma endregion

#pragma region CollisionEngine

CollisionEngine::CollisionEngine()
	: m_IsMultiThread(true),
	  m_IsInitialized(false),
	  m_CpuCnt(0),
	  m_Octree()
{
	// CPU数取得
	SYSTEM_INFO sys;
	GetSystemInfo(&sys);
	m_CpuCnt = (int)sys.dwNumberOfProcessors;
}

CollisionEngine::~CollisionEngine()
{
	ClearList();
}

void CollisionEngine::Init(uint octreeAreaSplitLevel, const ZAABB& octreeArea)
{
	bool success = m_Octree.Init(octreeAreaSplitLevel, octreeArea);
	assert(success);
	if (success)
		m_IsInitialized = true;
}

void CollisionEngine::Run()
{
	assert(m_IsInitialized);
	if (m_IsInitialized == false)
		return;

	if (m_IsMultiThread)
	{
		for (uint lineNo = 0; lineNo < NumAtkLines; lineNo++)
		{
			auto execTask = [this](ZThreadPool::ZThreadTaskData data,uint lineNo)
			{
				// 判定時のラインNo
				m_AtkLists[lineNo][data.TaskIndex]->m_NowLineNo = lineNo;

				// atkObj vs 衝突の可能性のあるdefObj
				Test(m_AtkLists[lineNo][data.TaskIndex].GetPtr());
			};

			if (m_AtkLists[lineNo].empty())continue;

			THPOOL.AddTask(m_AtkLists[lineNo].size(), THPOOL.GetNumThreads(),
				[execTask, &lineNo] (ZThreadPool::ZThreadTaskData data)
			{
				execTask(data,lineNo);
			});
			THPOOL.WaitForAllTasksFinish();
		}

	}
	else
	{
		// シングルスレッドで実行

		// 全ラインを順番に実行
		for(size_t lineNo = 0;lineNo < NumAtkLines;lineNo++)
		{
			for (size_t ai = 0; ai < m_AtkLists[lineNo].size(); ai++)
			{
				// 判定時のラインNo
				m_AtkLists[lineNo][ai]->m_NowLineNo = lineNo;

				// atkObj vs 全m_DefList

				Test(m_AtkLists[lineNo][ai].GetPtr());
			}
		}
	}
	
	// 結果通知関数実行
	for (auto& atkList : m_AtkLists)
	{
		for (auto& atkObj : atkList)
		{
			// 1つでもヒットしていれば通知関数実行
			if (atkObj->m_HitResTbl.size() > 0)
			{
				// ヒットした最初の1回
				if (atkObj->m_HitState & HitStates::ENTER && atkObj->m_OnHitEnter)
					atkObj->m_OnHitEnter(atkObj);

				// ヒットしている間
				if (atkObj->m_HitState & HitStates::STAY && atkObj->m_OnHitStay)
					atkObj->m_OnHitStay(atkObj);
			}

			// ヒットしなくなった最初の1回
			if (atkObj->m_HitState & HitStates::EXIT && atkObj->m_OnHitExit)
				atkObj->m_OnHitExit(atkObj);

			// 最後に実行
			if (atkObj->m_OnTerminal)
				atkObj->m_OnTerminal(atkObj);
		}
	}

}

ZVector<ColliderBase*> CollisionEngine::GetCollisionObjects(ZAABB& testAabb)
{
	ZVector<ColliderBase*> collisionObjects;

	m_Octree.GetCollisionList(testAabb, collisionObjects);

	return collisionObjects;
}

void CollisionEngine::Test(ColliderBase* atkObj)
{
	assert(m_IsInitialized);
	if (m_IsInitialized == false)
		return;

	atkObj->m_HitResTbl.clear();

	ZVector<ColliderBase*> colObjlist;
	atkObj->ReCalcAABB();
	m_Octree.GetCollisionList(atkObj->GetAABB(), colObjlist);

	HitResData res;

	// atkObjと所属空間が同じdefObjと衝突判定
	for (auto colObj : colObjlist)
	{
		res.Init();
	
		// 判定実行
		if (atkObj->HitTest(colObj, &res, nullptr))
			atkObj->m_HitResTbl.push_back(res);
	}

	// フラグ操作

	// 初回HITフラグとHIT終了フラグをOFF
	atkObj->m_HitState &= ~HitStates::ENTER;
	atkObj->m_HitState &= ~HitStates::EXIT;

	// 初回HITならHITフラグ系をON
	if (atkObj->m_HitResTbl.size() > 0)
	{
		if ((atkObj->m_HitState & HitStates::STAY) == false)
		{
			atkObj->m_HitState |= HitStates::ENTER; // 初回HIT ON
			atkObj->m_HitState |= HitStates::STAY; // HIT中 ON

			/*
			// 並列OnHitEnter実行
			if (atkObj->m_OnHitEnter_Parallel)
				atkObj->m_OnHitEnter_Parallel(atkObj);
			*/
		}

		/*
		// 並列OnHitState実行
		if (atkObj->m_OnHitStay_Parallel)
			atkObj->m_OnHitStay_Parallel(atkObj);
		*/
	}
	else
	{
		if (atkObj->m_HitState & HitStates::STAY)
		{
			atkObj->m_HitState |= HitStates::EXIT; // ON
			atkObj->m_HitState &= ~HitStates::STAY; // OFF

			/*
			// 並列OnHitExit実行
			if (atkObj->m_OnHitExit_Parallel)
				atkObj->m_OnHitExit_Parallel(atkObj);
			*/
		}
	}

}

void CollisionEngine::DebugDraw(float alpha)
{
	// 判定する側全オブジェクトのDebugDraw()を実行
	for (auto& atkList : m_AtkLists)
	{
		for (auto& atkObj : atkList)
			atkObj->DebugDraw(alpha);
	}

	// 判定される側全オブジェクトのDebugDraw()を実行

	for (auto& defObj : m_DefList)
		defObj->DebugDraw(alpha);
}

#pragma endregion