#pragma region ColliderBase

inline int ColliderBase::GetShape()const
{
	return m_Shape;
}

inline void ColliderBase::AddIgnoreID(size_t id)
{
	m_IgnoreIDs.push_back(id);
}

template<typename T>
inline ZSP<T> ColliderBase::GetUserMap(const ZString& name)const
{
	auto it = m_UserMap.find(name);
	if (it == m_UserMap.end())
		return nullptr;
	ZSP<T> pt = (*it).second;
	return pt;
}

template<typename T>
inline T* ColliderBase::Cast()const
{
	if (T::sShape() == m_Shape)
		return static_cast<T*>(this);
	return nullptr;
}

#pragma endregion

#pragma region Collider_Sphere

inline const ZAABB& Collider_Sphere::GetAABB()const
{
	return m_AABB;
}

inline void Collider_Sphere::Set(const ZVec3& pos, float rad)
{
	m_Sphere.Center = pos;
	m_Sphere.Radius = rad;
	ReCalcAABB();
}

inline void Collider_Sphere::SetPos(const ZVec3& pos)
{
	m_Sphere.Center = pos;
	ReCalcAABB();
}

inline void Collider_Sphere::SetRadius(float rad)
{
	m_Sphere.Radius = rad;
	ReCalcAABB();
}

inline void Collider_Sphere::ReCalcAABB()
{
	m_AABB.Max = m_Sphere.Center + m_Sphere.Radius;
	m_AABB.Min = m_Sphere.Center - m_Sphere.Radius;
}

#pragma endregion

#pragma region Collider_Box

inline const ZAABB& Collider_Box::GetAABB()const
{
	return m_AABB;
}

inline const DirectX::BoundingOrientedBox& Collider_Box::GetBox()const
{
	return m_Box;
}

inline const ZVec3& Collider_Box::GetDir(int index)const
{
	return m_Dir[index];
}

inline void Collider_Box::Set(const ZVec3& vLocalCenterPos, const ZVec3& vHalfSize, const ZMatrix& mat)
{
	// 行列拡大成分取得
	ZVec3 scale = mat.GetScale();

	// ローカル中心座標を行列でワールド変換
	ZVec3 vCenter;
	vLocalCenterPos.Transform(vCenter, mat);

	// 回転をクォータニオンに変換
	ZQuat rotate;
	mat.ToQuaternion(rotate, true);

	m_Box.Center = vCenter;
	m_Box.Extents = vHalfSize * scale;
	m_Box.Orientation = rotate;
	m_Dir[0] = mat.GetXAxis().Normalized();
	m_Dir[1] = mat.GetYAxis().Normalized();
	m_Dir[2] = mat.GetZAxis().Normalized();
	
	ReCalcAABB();

}

inline void Collider_Box::Set_MinMax(const ZVec3& vlocalMin, const ZVec3& vlocalMax, const ZMatrix& mat)
{
	ZQuat rotate;
	ZVec3 scale = mat.GetScale();
	ZVec3 vExtents = (vlocalMax - vlocalMin) / 2;
	ZVec3 vCenter = vlocalMin + vExtents;
	vCenter.Transform(mat);
	vExtents *= scale;

	mat.ToQuaternion(rotate, true);
	m_Box.Center = vCenter;
	m_Box.Extents = vExtents;
	m_Box.Orientation = rotate;

	m_Dir[0] = mat.GetXAxis().Normalized();
	m_Dir[1] = mat.GetYAxis().Normalized();
	m_Dir[2] = mat.GetZAxis().Normalized();

	ReCalcAABB();
}

inline void Collider_Box::ReCalcAABB()
{
	// 拡縮なしの行列算出
	auto rotMat = ZQuat(m_Box.Orientation).ToMatrix();
	rotMat.SetPos(m_Box.Center);
	m_AABB.Transform(rotMat);
}

#pragma endregion

#pragma region Collider_Ray

inline const ZAABB& Collider_Ray::GetAABB()const
{
	return m_AABB;
}

inline void Collider_Ray::Set(const ZVec3& pos1, const ZVec3& pos2)
{
	m_vPos1 = pos1;
	m_vPos2 = pos2;
	m_vDir = pos2 - pos1;
	m_RayLen = m_vDir.Length();
	m_vDir.Normalize();

	ReCalcAABB();

}

inline const ZVec3& Collider_Ray::GetPos1()const
{
	return m_vPos1;
}

inline const ZVec3& Collider_Ray::GetPos2()const
{
	return m_vPos2;
}

inline const ZVec3& Collider_Ray::GetDir()const
{
	return m_vDir;
}

inline float Collider_Ray::GetRayLen()const
{
	return m_RayLen;
}

inline void Collider_Ray::ReCalcAABB()
{
	ZVec3::CreateAABBFromLineSegment(m_vPos1, m_vPos2, m_AABB.Min, m_AABB.Max);
}

#pragma endregion

#pragma region Collider_Mesh

inline const ZAABB& Collider_Mesh::GetAABB()const
{
	return m_AABB;
}

inline void Collider_Mesh::Set(const ZMatrix& mat)
{
	m_AABB = m_BaseAABB;
	m_Mat = mat;
	
	ReCalcAABB();
}

inline void Collider_Mesh::AddMesh(ZSP<ZGameModel> pModel)
{
	if (pModel == nullptr)
		return;
	for (auto& model : pModel->GetModelTbl())
		m_MeshTbl.push_back(model->GetMesh());
	
	m_BaseAABB.Max = ZVec3(FLT_MIN);
	m_BaseAABB.Min = ZVec3(FLT_MAX);

	for (auto& mesh : m_MeshTbl)
	{
		ZVec3 halfsize = mesh->GetAABB_HalfSize();
		ZVec3 center = mesh->GetAABB_Center();
		ZAABB aabb{ center - halfsize,center + halfsize };
		for(uint i = 0;i<3;i++)
		{
			if (aabb.Max[i] > m_BaseAABB.Max[i])
				m_BaseAABB.Max[i] = aabb.Max[i];
			if (aabb.Min[i] < m_BaseAABB.Min[i])
				m_BaseAABB.Min[i] = aabb.Min[i];
		}
	}

	m_AABB = m_BaseAABB;
}

inline void Collider_Mesh::ClearMesh()
{
	m_MeshTbl.clear();
}

inline void Collider_Mesh::ReCalcAABB()
{
	// 拡縮なしの行列算出
	auto rotMat = m_Mat.ToQuaternion().ToMatrix();
	rotMat.SetPos(m_Mat.GetPos());
	m_AABB.Transform(rotMat);
}

#pragma endregion

#pragma region Collider_Compound

inline const ZAABB& Collider_Compound::GetAABB()const
{
	return m_AABB;
}

inline void Collider_Compound::AddHitObj(ColliderBase* hitObj)
{
	m_Colliders.push_back(hitObj);
	ZAABB aabb = hitObj->GetAABB();
	for (uint i = 0; i < 3; i++)
	{
		if (aabb.Max[i] > m_AABB.Max[i])
			m_AABB.Max[i] = aabb.Max[i];
		if (aabb.Min[i] < m_AABB.Min[i])
			m_AABB.Min[i] = aabb.Min[i];
	}

}

inline void Collider_Compound::ClearHitObj()
{
	m_Colliders.clear();
}

#pragma endregion

#pragma region CollisionEngine

inline void CollisionEngine::AddAtk(ZSP<ColliderBase> obj, size_t lineNo)
{
	assert(m_IsInitialized);
	if (m_IsInitialized == false)
		return;

	if (obj == nullptr)
		return;
	if (lineNo >= NumAtkLines)
		lineNo = NumAtkLines - 1;

	m_AtkLists[lineNo].push_back(obj);
}

inline void CollisionEngine::AddDef(ZSP<ColliderBase> obj)
{
	assert(m_IsInitialized);
	if (m_IsInitialized == false)
		return;

	m_Octree.RegisterObject(obj->GetAABB(),obj.GetPtr());
	m_DefList.push_back(obj);
}

inline void CollisionEngine::ClearList()
{
	if (m_IsInitialized == false)
		return;

	for (auto& node : m_AtkLists)
		node.clear();
	m_DefList.clear();
	m_Octree.Clear();
}

inline void CollisionEngine::EnableMultiThread(bool flg)
{
	m_IsMultiThread = flg;
}

#pragma endregion