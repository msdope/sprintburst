﻿#include "Game/Application.h"
#pragma comment(lib, "rpcrt4.lib")
#pragma comment(lib,"enet/enet.lib")

#include "enet/enet.h"
#include "EnetWork.h"

#define _CRT_SECURE_NO_WARNINGS

int ENetWork::Init() 
{

	if (enet_initialize() != 0)
	{
		fprintf(stderr, "An error occurred while initializing ENet.\n");
		return EXIT_FAILURE;
	}
	atexit(enet_deinitialize);

#ifdef SERVER
	m_address.host = ENET_HOST_ANY;
	m_address.port = PORT_NO;
	m_host = enet_host_create(
		&m_address	/*バインドaddress*/,
		5			/*最大クライアント数*/,
		CHANNEL_SIZE/*チャンネル数(0 ～ n-1)*/,
		0			/*受信帯域幅*/,
		0			/*発信帯域幅*/);

	if (m_host == NULL)
	{
		//エラー処理
		fprintf(stderr, "An error occurred while trying to create an ENet server host.\n");
		//exit(EXIT_FAILURE);
	}


#else
	m_host = enet_host_create(
		NULL		/*クライアント作成*/,
		1			/*最大発信接続数*/,
		CHANNEL_SIZE/*チャンネル数(0 ～ n-1)*/,
		0			/*着信帯域幅*/,
		0			/*着信帯域幅*/);

	if (m_host == NULL)
	{
		//エラー処理
		fprintf(stderr, "An error occurred while trying to create an ENet client host.\n");
		//exit(EXIT_FAILURE);
	}

	//チャンネルの分だけ受信データバッファを確保
	m_Channel = std::vector<std::deque<enet_uint8 *>>(CHANNEL_SIZE, std::deque<enet_uint8*>(BUFF_SIZE, 0));

	//UUID生成・表示
	UuidCreate(&m_uuid);
	char *str;
	UuidToStringA(&m_uuid, (RPC_CSTR*)&str);
	printf("%s\n", str);

#endif
	return 0;
}

int ENetWork::Update() {
	while (enet_host_service(m_host, &m_event, 0) > 0)
	{
		switch (m_event.type)
		{
			case ENET_EVENT_TYPE_CONNECT:
			{
				/*printf("新しいクライアント %d:%u.data:%d\n",
					m_event.peer->address.host,
					m_event.peer->address.port,
					m_event.data);*/
				/* その他のクライアント情報↓ */
				//m_event.peer->data = (void *)"";
				break;
			}
			case ENET_EVENT_TYPE_RECEIVE: 
			{

				/*printf("データ長：%u データ：%d クライアント：%s チャンネルID：%u Data:%d\n",
					m_event.packet->dataLength,
					m_event.packet->data,
					m_event.peer->data,
					m_event.channelID,
					m_event.data
				);*/

#ifdef SERVER
				//UUIDを取得
				enet_uint8 * client_uuid;
				client_uuid = (enet_uint8 *)calloc(sizeof(UUID), sizeof(enet_uint8));
				memcpy(client_uuid, m_event.packet->data, sizeof(UUID));
				UUID uuid = *(UUID *)client_uuid;

				//UUIDの表示・stringに変換
				char * str;
				std::string strUUID;
				UuidToStringA(&uuid, (RPC_CSTR*)&str);
				strUUID = str;
				//printf("%s\n", strUUID.c_str());

				//[dataLength]をもとにメモリ確保
				enet_uint8 *data;
				data = (enet_uint8 *)calloc(m_event.packet->dataLength - sizeof(UUID), sizeof(enet_uint8));


				//受信データをコピー
				memcpy(data, m_event.packet->data + sizeof(UUID), m_event.packet->dataLength - sizeof(UUID));

				//初回のみ初期化（チャンネル数分確保）
				if (m_PlayerData[strUUID].empty()) {
					m_PlayerData[strUUID] = std::vector<std::deque<enet_uint8 *>>(CHANNEL_SIZE, std::deque<enet_uint8 *>());
					m_PlayerUUID[strUUID] = uuid;
				}

				//格納データがバッファサイズを超えた場合、先頭（最も古い）から削除
				if (m_PlayerData[strUUID][m_event.channelID].size() > BUFF_SIZE) {
					free(m_PlayerData[strUUID][m_event.channelID].front());
					m_PlayerData[strUUID][m_event.channelID].pop_front();
				}

				//データを挿入
				m_PlayerData[strUUID][m_event.channelID].push_back(data);

				free(client_uuid);


#else
				//[dataLength]をもとにメモリ確保
				enet_uint8 *data;
				data = (enet_uint8 *)calloc(m_event.packet->dataLength, sizeof(enet_uint8));

				//受信データをコピー
				memcpy(data, m_event.packet->data, m_event.packet->dataLength);

				//格納データがバッファサイズを超えた場合、先頭（最も古い）から削除
				if (m_Channel[m_event.channelID].size() > BUFF_SIZE) {
					m_Channel[m_event.channelID].pop_front();
				}
				//最後尾に最新データを格納
				m_Channel[m_event.channelID].push_back(data);
#endif

				/*パケットをクリーンアップ*/
				enet_packet_destroy(m_event.packet);
				break;
			}

			case ENET_EVENT_TYPE_DISCONNECT:
				printf("%s disconnected.\n", m_event.peer->data);
				/* クライアント情報をリセット */
				m_event.peer->data = NULL;
		}
	}
	return 0;
}

int ENetWork::Release()
{
#ifdef SERVER
	for (auto map = m_PlayerData.begin(); map != m_PlayerData.end();map++) {
		for (auto channel = map->second.begin(); channel != map->second.end();map++) {
			for (auto data = channel->begin(); data != channel->end();data++) {
				free(*data);
			}
			channel->erase(channel->begin(),channel->end());
			std::deque<enet_uint8 *>().swap(*channel);
		}
		map->second.erase(map->second.begin(), map->second.end());
		std::vector<std::deque<enet_uint8 *>>().swap(map->second);
	}
	m_PlayerData.erase(m_PlayerData.begin(),m_PlayerData.end());
	std::unordered_map<std::string, std::vector<std::deque<enet_uint8 *>>>().swap(m_PlayerData);

#else
	for (int i = 0; i < CHANNEL_SIZE; i++) {
		for (auto data : m_Channel[i]) {
			free(data);
		}
		m_Channel[i].clear();
		std::deque<enet_uint8 *>().swap(m_Channel[i]);
	}
	m_Channel.clear();
#endif

	//終了処理
	enet_host_destroy(m_host);


	return 0;
}

#ifndef SERVER
void ENetWork::Connect(const char * _address) {
	enet_address_set_host(&m_address, _address);
	m_address.port = PORT_NO;

	/*接続開始*/
	m_peer = enet_host_connect(m_host, &m_address, 2, 0);
	if (m_peer == NULL)
	{
		fprintf(stderr, "No available peers for initiating an ENet connection.\n");
		//exit(EXIT_FAILURE);
	}
	/*接続待機（5秒）*/
	if (enet_host_service(m_host, &m_event, 5000) > 0 &&
		m_event.type == ENET_EVENT_TYPE_CONNECT)
	{
		//接続成功
		puts("Connection succeeded.");
	}
	else
	{
		//接続失敗
		enet_peer_reset(m_peer);
		puts("Connection failed.");
		Connect(_address);
	}
}
void ENetWork::Disconnect()
{
	enet_peer_disconnect(m_peer, 0);

	while (enet_host_service(m_host, &m_event, 3000) > 0)
	{
		switch (m_event.type)
		{
		case ENET_EVENT_TYPE_RECEIVE:
			enet_packet_destroy(m_event.packet);
			break;
		case ENET_EVENT_TYPE_DISCONNECT:
			puts("Disconnection succeeded.");
			return;
		}
	}
	enet_peer_reset(m_peer);
}
#endif // !SERVER

void ENetWork::SendData(void * _data, int _size, int _channel, bool _isBroad)
{
#ifdef SERVER
	ENetPacket * packet = enet_packet_create(_data, _size, ENET_PACKET_FLAG_RELIABLE);

	if (_isBroad)
	{
		enet_host_broadcast(m_host, _channel, packet);
	}
	else
	{
		enet_peer_send(m_peer, _channel, packet);
	}



#else
	//パケットの先頭にUUIDを付与
	enet_uint8 * data = (enet_uint8 *)calloc(_size + sizeof(UUID), sizeof(enet_uint8));
	memcpy(data, &m_uuid, sizeof(UUID));
	memcpy(data + sizeof(UUID), _data, _size);

	ENetPacket * packet = enet_packet_create(data, _size + sizeof(UUID), ENET_PACKET_FLAG_RELIABLE);

	if (_isBroad)
	{
		enet_host_broadcast(m_host, _channel, packet);
	}
	else
	{
		enet_peer_send(m_peer, _channel, packet);
	}

	//解放
	free(data);

#endif
}
