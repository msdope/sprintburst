﻿#ifndef __ENETWORK_H__
#define __ENETWORK_H__

/*===============
サーバー側は
#define SERVER
を宣言する
===============*/


//#define SERVER

class ENetWork {

public:

	int Init();
	int Update();
	int Release();

	void Connect(const char *);
	void Disconnect();

	void SendData(void * _data, int _size, int _channel, bool isBroad = false);

#ifdef SERVER

	void GetUUID(std::vector<std::string> & _vectorUUID) {
		for (auto map = m_PlayerUUID.begin(); map != m_PlayerUUID.end(); map++) {
			_vectorUUID.push_back(map->first);
		}
	}
	void GetUUID(std::vector<UUID> & _vectorUUID) {
		for (auto map = m_PlayerUUID.begin(); map != m_PlayerUUID.end(); map++) {
			_vectorUUID.push_back(map->second);
		}
	}

	template <typename T>
	void GetData(std::deque<T> &_deque, std::string _uuid, int _channel) {
		for (auto map = m_PlayerData.begin(); map != m_PlayerData.end(); map++) {
			for (auto data = map->second[_channel].begin(); data != map->second[_channel].end(); data++) {
				_deque.push_back(*(T *)*data);
			}
		}
	}

#else
	template <typename T>
	void RecvData(std::deque<T> &_DequeList, int _channel) {
		_DequeList.clear();
		for (auto data : m_Channel[_channel]) {
			T tmpData = *(T *)data;
			_DequeList.push_back(tmpData);
		}
	}
#endif // SERVER



private:
	//=============定数====================
	static const int PORT_NO = 50000;	//ポート番号
	static const int BUFF_SIZE = 20;	//データバッファ数
	static const int CHANNEL_SIZE = 2;	//通信チャンネル数
	static const int PLAYER_SIZE = 5;	//接続プレイヤーサイズ

										//=============変数====================
	ENetHost * m_host;		//ホスト

	ENetAddress m_address;	//アドレス
	ENetPeer * m_peer;		//ピア
	ENetEvent m_event;		//イベント


#ifdef SERVER

							//std::map<UUID,std::vector<std::deque<enet_uint8 *>>> m_Player;
							//std::map<std::string, std::vector<std::deque<std::shared_ptr<enet_uint8>>>> m_Player;

	std::unordered_map<std::string, std::vector<std::deque<enet_uint8 *>>> m_PlayerData;
	std::unordered_map<std::string, UUID> m_PlayerUUID;
#else
	std::vector<std::deque<enet_uint8 *>> m_Channel;//受信データバッファ

	UUID m_uuid;	//UUID格納
#endif


					//=========シングルトン==============
private:
	ENetWork() {}
public:
	static ENetWork &GetInstance()
	{
		static ENetWork instance;
		return instance;
	}
};

#define ENET ENetWork::GetInstance()
#endif
