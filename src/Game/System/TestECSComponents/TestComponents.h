#ifndef __TEST_COMPONENTS_H__
#define __TEST_COMPONENTS_H__

// 箱の動作パラメータ
DefComponent(MotionComponent)
{
	ZVec3 StartPos;
	ZVec3 Velocity;
	ZVec3 Acceleration;
};

// キャラクター識別用
DefComponent(CharaComponent)
{
	ZString Name;
};

//VRコントローラーを出すためだけのコンポーネント
//DefComponent(VRControllerComponent)
//{
//	ZMatrix MScale;
//	CVRController m_VRCon;
//};

#endif