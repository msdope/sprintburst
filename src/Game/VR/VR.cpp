#include "Game/Application.h"
#include "DebugWindow/DebugWindow.h"
#include <openvr.h>
#include "VR.h"


CVR* CVR::s_pInstance = nullptr;
const float CVRCamera::m_CamMoveSpeed = 0.2f;

ZMatrix CVRController::GetMatrix()
{
	const ZMatrix* pMat = cvr.Controller_GetMatrix(m_ControllerNo);
	if (pMat)
	{
		// HMDの逆行列
		ZMatrix mHMD = cvr.GetNowHMDPoseWithEye();
		mHMD.Inverse();
		// コントローラの行列を、HMDからのローカル行列へ変換
		m_LocalMat = (*pMat) * mHMD;
	}
	// コントローラのローカル行列 * HMDの行列 * カメラのベース行列
	return m_LocalMat * cvr.GetNowHMDPoseWithEye() * cvr.m_LastCamVR.m_CamBaseMat;
}

void CVRController::TriggerHapticPulse(unsigned short power)
{
	if (m_DeviceNo < 0)return;

	cvr.GetVRSystem()->TriggerHapticPulse(m_DeviceNo, 0, power);

}


void CVRController::Update()
{
	// 
	bool bConnect = cvr.Controller_GetState(m_ControllerNo, m_NowState, &m_DeviceNo);
	if (bConnect == false)
		ZeroMemory(&m_NowState, sizeof(m_NowState));
	
	// ボタンフラグ
	for (UINT i = 0; i<vr::EVRButtonId::k_EButton_Max; i++)
	{
		// 指定のビットはクリア
		m_ButtonFlag[i] &= ~KEY_TRIGGER;
		m_ButtonFlag[i] &= ~KEY_RELEASE;

		uint64_t btn = vr::ButtonMaskFromId((vr::EVRButtonId)i);

		// VRがない時はマウス等でシミュレート
		if (cvr.IsInit() == false)
		{
			if (i == vr::EVRButtonId::k_EButton_SteamVR_Touchpad)
			{
				if (GetAsyncKeyState(VK_LBUTTON) & 0x8000) {
					m_NowState.ulButtonPressed |= btn;
				}
			}
			else if (i == vr::EVRButtonId::k_EButton_SteamVR_Trigger)
			{
				if (GetAsyncKeyState(VK_MBUTTON) & 0x8000)
					m_NowState.ulButtonPressed |= btn;
			}
			else if (i == vr::EVRButtonId::k_EButton_Grip)
			{
				if (GetAsyncKeyState('M') & 0x8000)
					m_NowState.ulButtonPressed |= btn;
			}
		}

		// フラグ
		if (m_NowState.ulButtonPressed & btn)
		{
			if (!(m_ButtonFlag[i] & KEY_BUTTON))
			{
				m_ButtonFlag[i] |= KEY_TRIGGER;
				m_ButtonFlag[i] |= KEY_BUTTON;
			}
		}
		else
		{
			if (m_ButtonFlag[i] & KEY_BUTTON)
			{
				m_ButtonFlag[i] |= KEY_RELEASE;
				m_ButtonFlag[i] &= ~KEY_BUTTON;
			}
		}
	}

}

void CVRCamera::Update()
{
	float ratio = 1.0f;
	if (INPUT.KeyStay(VK_CONTROL)) ratio *= 0.2f;
	else if (INPUT.KeyStay(VK_SHIFT)) ratio *= 3.0f;

	//=========================================================
	// カメラ操作
	//=========================================================
	// マウス右ボタン

	if (INPUT.MouseStay(ZInput::BUTTON_R))
	{
		POINT pt = INPUT.GetMouseMoveValue();
		m_CamRx += pt.y*0.5f*ratio;
		m_CamRy += pt.x*0.5f*ratio;
	}


	if (INPUT.KeyStay('W'))
	{
		ZMatrix m;
		m.CreateIdentity();
		m.RotateY(m_CamRy);
		m.Move_Local(0, 0, m_CamMoveSpeed);
		m.CreateIdentityRotate();
		m_CamBaseMat.Multiply_Local(m);
	}
	if (INPUT.KeyStay('A'))
	{
		ZMatrix m;
		m.CreateIdentity();
		m.RotateY(m_CamRy);
		m.Move_Local(-m_CamMoveSpeed, 0, 0);
		m.CreateIdentityRotate();
		m_CamBaseMat.Multiply_Local(m);
	}
	if (INPUT.KeyStay('S'))
	{
		ZMatrix m;
		m.CreateIdentity();
		m.RotateY(m_CamRy);
		m.Move_Local(0, 0, -m_CamMoveSpeed);
		m.CreateIdentityRotate();
		m_CamBaseMat.Multiply_Local(m);
	}
	if (INPUT.KeyStay('D'))
	{
		ZMatrix m;
		m.CreateIdentity();
		m.RotateY(m_CamRy);
		m.Move_Local(m_CamMoveSpeed, 0, 0);
		m.CreateIdentityRotate();
		m_CamBaseMat.Multiply_Local(m);
	}

	cvr.m_LastCamVR = *this;
}

ZMatrix CVRCamera::GetMatrix()
{
	ZMatrix m;
	m.CreateIdentity();
	m.RotateX(m_CamRx);
	m.RotateY(m_CamRy);
	m.Move(0, 0, 0);

	return m;
}

// セット
void CVRCamera::SetCamera()
{
	SetProj(cvr.GetNowProjectionMatrix());

	mCam = cvr.GetNowHMDPoseWithEye() * m_CamBaseMat;
	CameraToView();

	// シェーダー側の定数バッファに書き込む
	ShMgr.UpdateCamera(this);
}

bool CVR::Init()
{

	bool bOK = true;

	// レンダーサイズ
	uint32_t nRenderWidth = 0;
	uint32_t nRenderHeight = 0;

	//===================================================================
	// SteamVR Runtimeを読み込む
	//===================================================================
	vr::EVRInitError eError = vr::VRInitError_None;
	m_pHMD = vr::VR_Init(&eError, vr::VRApplication_Scene);
	if(eError != vr::VRInitError_None)
	{
		m_pHMD = nullptr;
		bOK = false;
	}

	if(bOK)
	{
		// 推奨されるレンダーターゲットサイズを取得
		m_pHMD->GetRecommendedRenderTargetSize(&nRenderWidth, &nRenderHeight);

		DW_SCROLL(0, "レンダーサイズ width = %d, height = %d", nRenderWidth, nRenderHeight);

		// 
		m_pRenderModels = (vr::IVRRenderModels *)vr::VR_GetGenericInterface(vr::IVRRenderModels_Version, &eError);
		if(!m_pRenderModels)
		{
			m_pHMD = nullptr;
			vr::VR_Shutdown();

			char buf[1024];
			sprintf_s(buf, ARRAYSIZE(buf), "Unable to get render model interface: %s", vr::VR_GetVRInitErrorAsEnglishDescription(eError));
			std::string temp(buf);
			std::string wtemp(temp.begin(), temp.end());
			DW_SCROLL(0, "VR_Init() 失敗:%s", wtemp.c_str());

			return false;
		}

		if(!vr::VRCompositor())
		{
			DW_SCROLL(0, "--- Compositor初期化失敗 ---");
			return false;
		}
	}

	if (m_pHMD == nullptr)
		return false;

	//===================================================================
	// クリップ範囲
	//===================================================================
	m_fNearClip = 0.01f;
	m_fFarClip = 1000;

	// 
	SetupCameras();

	// 左画面用テクスチャ
	m_RT_Left.CreateRT(nRenderWidth, nRenderHeight);
	
	// 右画面用テクスチャ
	m_RT_Right.CreateRT(nRenderWidth, nRenderHeight);
	
	// 左右RT用 深度バッファ
	m_Depth_Eye.CreateDepth(nRenderWidth, nRenderHeight);

	// センター用 RT Depth
	m_RT_Center.CreateRT(ZDx.GetRezoW(), ZDx.GetRezoH());
	m_Depth_Center.CreateDepth(ZDx.GetRezoW(), ZDx.GetRezoH());


	return true;
}

void CVR::Release()
{
	if(m_pHMD)
	{
		vr::VR_Shutdown();
		m_pHMD = nullptr;
	}

	m_RT_Left.Release();
	m_RT_Right.Release();
	m_Depth_Eye.Release();

	m_RT_Center.Release();
	m_Depth_Center.Release();

}

void CVR::Submit(vr::EVREye eye)
{
	if(m_pHMD == nullptr)return;

	if(eye == vr::Eye_Left)
	{
		// 左
		vr::Texture_t leftEyeTexture = { m_RT_Left.GetTex2D(), vr::TextureType_DirectX, vr::ColorSpace_Auto };
		vr::VRCompositor()->Submit(vr::Eye_Left, &leftEyeTexture);
	}
	else if(eye == vr::Eye_Right)
	{
		// 右
		vr::Texture_t rightEyeTexture = { m_RT_Right.GetTex2D(), vr::TextureType_DirectX, vr::ColorSpace_Auto };
		vr::VRCompositor()->Submit(vr::Eye_Right, &rightEyeTexture);
	}
}

void CVR::WaitGetPoses()
{
	if(!m_pHMD)return;

	vr::VRCompositor()->WaitGetPoses(m_rTrackedDevicePose, vr::k_unMaxTrackedDeviceCount, NULL, 0);

	m_iValidPoseCount = 0;
	m_strPoseClasses = "";
	for(int nDevice = 0; nDevice < vr::k_unMaxTrackedDeviceCount; ++nDevice)
	{
		if(m_rTrackedDevicePose[nDevice].bPoseIsValid)
		{
			m_iValidPoseCount++;
			m_rmat4DevicePose[nDevice] = ConvertSteamVRMatrixToMatrix(m_rTrackedDevicePose[nDevice].mDeviceToAbsoluteTracking);

			if(m_rDevClassChar[nDevice] == 0)
			{
				switch(m_pHMD->GetTrackedDeviceClass(nDevice))
				{
					case vr::TrackedDeviceClass_Controller:        m_rDevClassChar[nDevice] = 'C';break;
					case vr::TrackedDeviceClass_HMD:               m_rDevClassChar[nDevice] = 'H';break;
					case vr::TrackedDeviceClass_Invalid:           m_rDevClassChar[nDevice] = 'I';break;
					case vr::TrackedDeviceClass_GenericTracker:    m_rDevClassChar[nDevice] = 'G';break;
					case vr::TrackedDeviceClass_TrackingReference: m_rDevClassChar[nDevice] = 'T';break;
					default:                                       m_rDevClassChar[nDevice] = '?';break;
				}
			}
			m_strPoseClasses += m_rDevClassChar[nDevice];
		}
	}

	if(m_rTrackedDevicePose[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid)
		m_mHMDPose = m_rmat4DevicePose[vr::k_unTrackedDeviceIndex_Hmd];

}

void CVR::Update()
{
	
}

void CVR::SetupCameras()
{
	m_mat4ProjectionLeft = GetHMDMatrixProjectionEye(vr::Eye_Left);
	m_mat4ProjectionRight = GetHMDMatrixProjectionEye(vr::Eye_Right);
	// 左右の目の位置を取得
	m_mEyePosLeft = GetHMDMatrixPoseEye(vr::Eye_Left);
	m_mEyePosRight = GetHMDMatrixPoseEye(vr::Eye_Right);
}

void CVR::BeginRender(RenderMode mode)
{
	m_NowRenderMode = mode;

	// RT Depth 記憶
	m_backupRTs.GetNowAll();

	if(m_NowRenderMode == RM_Left)
	{
		m_RT_Left.ClearRT(ZVec4(0.2f, 0.2f, 0.2f, 1));
		m_Depth_Eye.ClearDepth();

		// RT,Depth変更
		ZRenderTargets rtd;
		rtd.RT(0, m_RT_Left);
		rtd.Depth(m_Depth_Eye);
		rtd.SetToDevice();
		
	}
	else if(m_NowRenderMode == RM_Right)
	{
		m_RT_Right.ClearRT(ZVec4(0.2f, 0.2f, 0.2f, 1));
		m_Depth_Eye.ClearDepth();

		// RT,Depth変更
		ZRenderTargets rtd;
		rtd.RT(0, m_RT_Right);
		rtd.Depth(m_Depth_Eye);
		rtd.SetToDevice();
	}
	else if(m_NowRenderMode == RM_Center)
	{
		/*m_RT_Center.ClearRT(ZVec4(0.2f, 0.2f, 0.2f, 1));
		m_Depth_Center.ClearDepth();

		// RT,Depth変更
		ZRenderTargets rtd;
		rtd.RT(0, m_RT_Center);
		rtd.Depth(m_Depth_Center);
		rtd.SetToDevice();*/
	}

}

void CVR::EndRender()
{
	// RT Depth 復元
	m_backupRTs.SetToDevice();
	m_backupRTs.Release();

	// モード初期化
	m_NowRenderMode = RM_None;
}

//----------------------------------------------------------
// 現在のHMDのローカル行列を取得(※右目、左目モードも含む)
// ワールド行列はCVRCameraにより変わるので、ワールド行列を取得したい場合は、
// m_LastCam.mCamを使うとよい
//  ※未接続時は、マウスモード行列を返す
//----------------------------------------------------------


//----------------------------------------------------------
// 現在のHMDのワールド行列を取得(※右目、左目モードも含む)　ここでいうワールド行列は、カメラを考慮したもの
//----------------------------------------------------------

inline ZMatrix CVR::GetNowHMDPoseWithEye_World()
{
	return GetNowHMDPoseWithEye() * m_LastCamVR.m_CamBaseMat;
}

inline ZMatrix CVR::GetNowHMDPoseWithEye()
{
	ZMatrix vrCam;

	if (m_pHMD)
	{
		// 各目の行列
		if (m_NowRenderMode == RM_Left)
			vrCam = m_mEyePosLeft;
		else if (m_NowRenderMode == RM_Right)
			vrCam = m_mEyePosRight;
		else if (m_NowRenderMode == RM_Center)
		{
		}

		// HMDの行列
		vrCam *= m_mHMDPose;
	}
	else
		return m_LastCamVR.GetMatrix();// m_Cam.GetMatrix();

	return vrCam;
}

// 

inline ZMatrix CVR::GetNowHDMPose_World()
{
	return GetNowHDMPose() * m_LastCamVR.m_CamBaseMat;
}

// 現在のHMDのローカル行列を取得

inline ZMatrix CVR::GetNowHDMPose()
{
	if (m_pHMD)
		return m_mHMDPose;
	else
		return m_LastCamVR.GetMatrix();// m_Cam.GetMatrix();
	
}


void CVR::CopyDrawEyeTexture(int RLFlag, ZTexture* srcTex)
{
	ZRenderTargets backupRTs;
	backupRTs.GetNowAll();

	if(RLFlag == 0)
	{
		ZRenderTargets rtd;
		rtd.RT(0, m_RT_Right);
		rtd.Depth(nullptr);
		rtd.SetToDevice();
		
	}
	else if(RLFlag == 1)
	{
		ZRenderTargets rtd;
		rtd.RT(0, m_RT_Left);
		rtd.Depth(nullptr);
		rtd.SetToDevice();
	}


	backupRTs.SetToDevice();
}

const ZMatrix*	CVR::Controller_GetMatrix(int controllerNo)
{
	if(m_pHMD == nullptr)return nullptr;

	std::array<ZMatrix*, 2> pMat = { nullptr, nullptr };

	int nowIdx = 0;
	// コントローラ検索
	for(vr::TrackedDeviceIndex_t unTrackedDevice = 0; unTrackedDevice < vr::k_unMaxTrackedDeviceCount; ++unTrackedDevice)
	{
		// 接続中？
		if(!m_pHMD->IsTrackedDeviceConnected(unTrackedDevice))
			continue;
		// コントローラ？
		if(m_pHMD->GetTrackedDeviceClass(unTrackedDevice) != vr::TrackedDeviceClass_Controller)
			continue;
		// 
		if(!m_rTrackedDevicePose[unTrackedDevice].bPoseIsValid)
			continue;

		pMat[nowIdx] = &m_rmat4DevicePose[unTrackedDevice];

		nowIdx++;
		if(nowIdx >= 2)break;
	}

	return pMat[controllerNo];
}

/*
bool CVR::Controller_CheckButton(int controllerNo, vr::EVRButtonId buttonID, bool isCheckPoseValid, bool isTouched, ZVec2 outAxisVal[])
{
	if(m_pHMD == nullptr)return false;

	uint64_t btn = vr::ButtonMaskFromId(buttonID);

	int nowIdx = 0;
	// コントローラ検索
	for(vr::TrackedDeviceIndex_t unTrackedDevice = 0; unTrackedDevice < vr::k_unMaxTrackedDeviceCount; ++unTrackedDevice)
	{
		// 接続中？
		if(!m_pHMD->IsTrackedDeviceConnected(unTrackedDevice))
			continue;
		// コントローラ？
		if(m_pHMD->GetTrackedDeviceClass(unTrackedDevice) != vr::TrackedDeviceClass_Controller)
			continue;
		// 
		if(isCheckPoseValid && m_rTrackedDevicePose[unTrackedDevice].bPoseIsValid == false)
			continue;

		if(controllerNo == nowIdx){
			vr::VRControllerState_t state;
			if(m_pHMD->GetControllerState(unTrackedDevice, &state)){
				if(isTouched){
					if(state.ulButtonTouched & btn){
						
						if(outAxisVal){
							for(int ai = 0; ai<vr::k_unControllerStateAxisCount;ai++){
								outAxisVal[ai].x = state.rAxis[ai].x;
								outAxisVal[ai].y = state.rAxis[ai].y;
							}
						}
						return true;
					}
				}
				else{
					if(state.ulButtonPressed & btn)return true;
				}
			}
			return false;
		}

		nowIdx++;
		if(nowIdx >= 2)break;
	}

	return false;
}
*/

bool CVR::Controller_GetState(int controllerNo, vr::VRControllerState_t& outState, int* outDeviceNo)
{
	if(m_pHMD == nullptr)return false;

	if(outDeviceNo)*outDeviceNo = -1;

	int nowIdx = 0;
	// コントローラ検索
	for(vr::TrackedDeviceIndex_t unTrackedDevice = 0; unTrackedDevice < vr::k_unMaxTrackedDeviceCount; unTrackedDevice++)
	{
		// 接続中？
		if(!m_pHMD->IsTrackedDeviceConnected(unTrackedDevice))
			continue;
		// コントローラ？
		if(m_pHMD->GetTrackedDeviceClass(unTrackedDevice) != vr::TrackedDeviceClass_Controller)
			continue;
		// 
		if(m_rTrackedDevicePose[unTrackedDevice].bPoseIsValid == false)
			continue;

		if(controllerNo == nowIdx)
		{
			if(m_pHMD->GetControllerState(unTrackedDevice, &outState, sizeof(outState)))
			{
				if(outDeviceNo)*outDeviceNo = (int)unTrackedDevice;
				return true;
			}
			else
				return false;
		}

		nowIdx++;
		if(nowIdx >= 2)break;
	}

	return false;
}




ZMatrix CVR::ConvertSteamVRMatrixToMatrix(const vr::HmdMatrix34_t &matPose)
{
	ZMatrix matrixObj;

	matrixObj._11 = matPose.m[0][0];
	matrixObj._12 = matPose.m[1][0];
	matrixObj._13 = matPose.m[2][0];
	matrixObj._14 = 0.0;
	matrixObj._21 = matPose.m[0][1];
	matrixObj._22 = matPose.m[1][1];
	matrixObj._23 = matPose.m[2][1];
	matrixObj._24 = 0.0;
	matrixObj._31 = matPose.m[0][2];
	matrixObj._32 = matPose.m[1][2];
	matrixObj._33 = matPose.m[2][2];
	matrixObj._34 = 0.0;
	matrixObj._41 = matPose.m[0][3];
	matrixObj._42 = matPose.m[1][3];
	matrixObj._43 = matPose.m[2][3];
	matrixObj._44 = 1.0f;

	// 左手座標系用にZ反転
	matrixObj.MirrorZ();

	return matrixObj;
}


ZMatrix CVR::GetHMDMatrixPoseEye(vr::Hmd_Eye nEye)
{
	if(!m_pHMD)return ZMatrix::Identity;

	vr::HmdMatrix34_t matEyeRight = m_pHMD->GetEyeToHeadTransform(nEye);
	ZMatrix matrixObj;

	matrixObj._11 = matEyeRight.m[0][0];
	matrixObj._12 = matEyeRight.m[1][0];
	matrixObj._13 = matEyeRight.m[2][0];
	matrixObj._14 = 0.0;
	matrixObj._21 = matEyeRight.m[0][1];
	matrixObj._22 = matEyeRight.m[1][1];
	matrixObj._23 = matEyeRight.m[2][1];
	matrixObj._24 = 0.0;
	matrixObj._31 = matEyeRight.m[0][2];
	matrixObj._32 = matEyeRight.m[1][2];
	matrixObj._33 = matEyeRight.m[2][2];
	matrixObj._34 = 0.0;
	matrixObj._41 = matEyeRight.m[0][3];
	matrixObj._42 = matEyeRight.m[1][3];
	matrixObj._43 = matEyeRight.m[2][3];
	matrixObj._44 = 1.0f;

	return matrixObj;
}

ZMatrix CVR::GetHMDMatrixProjectionEye(vr::Hmd_Eye nEye)
{
	if(!m_pHMD) return ZMatrix::Identity;

	vr::HmdMatrix44_t mat = m_pHMD->GetProjectionMatrix(nEye, m_fNearClip, m_fFarClip);

	ZMatrix matrixObj;

	matrixObj._11 = mat.m[0][0];
	matrixObj._12 = mat.m[1][0];
	matrixObj._13 = mat.m[2][0];
	matrixObj._14 = mat.m[3][0];

	matrixObj._21 = mat.m[0][1];
	matrixObj._22 = mat.m[1][1];
	matrixObj._23 = mat.m[2][1];
	matrixObj._24 = mat.m[3][1];
	// 左手座標系用に反転
	matrixObj._31 = -mat.m[0][2];
	matrixObj._32 = -mat.m[1][2];
	matrixObj._33 = -mat.m[2][2];
	matrixObj._34 = -mat.m[3][2];

	matrixObj._41 = mat.m[0][3];
	matrixObj._42 = mat.m[1][3];
	matrixObj._43 = mat.m[2][3];
	matrixObj._44 = mat.m[3][3];

	return matrixObj;
}
