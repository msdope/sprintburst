#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include "MainFrame/ZMainFrame.h"
#include "Shader/LightManager.h"
#include "Game/System/Renderer/RendererBase.h"
#include "Game/System/Renderer/ModelRenderer/StaticMeshRenderer.h"
#include "Game/System/Renderer/ModelRenderer/SkinMeshRenderer.h"
#include "Game/System/Renderer/ModelRenderer/DebugColliderMeshRenderer.h"
#include "Game/System/Renderer/StaticModelRenderer/StaticModelRenderer.h"
#include "Game/System/Renderer/StaticModelRenderer/InstancingStaticModelRenderer.h"

#include "Shader/ShaderManager.h"
#include "Game/System/ClosureTaskManager/ClosureTaskManager.h"
#include "Game/System/CollisionEngine/CollisionEngine.h"

#include "Game/System/PostEffect/PostEffects.h"

#include "Game/System/PostEffect/EffectPass/SetupPass.h"
#include "Game/System/PostEffect/EffectPass/DOFPass.h"
#include "Game/System/PostEffect/EffectPass/XRayPass.h"
#include "Game/System/PostEffect/EffectPass/LightBloomPass.h"

#include "Game/SubSystems.h"

class Application : public ZMainFrame
{
public:
	Application(const char* wndTitle, const ZWindowProperties& properties);

protected:
	virtual bool Init()override;
	virtual void FrameUpdate()override;
	virtual void Release()override;

public:
	/* SubSystemで追加したオブジェクトへのポインタ */
	ClosureTaskManager* m_pClosureTaskManager;
	
	ShaderManager* m_pShMgr; // シェーダーマネージャー
	CollisionEngine* m_pColEng; // コリジョンエンジン


};

#define RENDERER ShMgr.m_Ms

#endif