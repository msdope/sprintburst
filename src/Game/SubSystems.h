#ifndef __APP_SUBSYSTEMS_H__
#define __APP_SUBSYSTEMS_H__

#define CTMgr GetSubSystem<ClosureTaskManager>()
#define ShMgr GetSubSystem<ShaderManager>()
#define ColEng GetSubSystem<CollisionEngine>()

#endif