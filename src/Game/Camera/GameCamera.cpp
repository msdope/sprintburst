#include "Game/Application.h"
#include "GameCamera.h"

#define EditorCam 0

GameCamera::GameCamera()
{
	// カメラデータ初期化
	m_IsFPSMode = false;
	m_LocalMat.CreateMove(0, 10, -15);
	SetPerspectiveFovLH(60, (float)ZDx.GetRezoW() / (float)ZDx.GetRezoH(), 0.01f, 1000);
}

void GameCamera::Init(float rx, float ry, float camZoom, bool isFpsMode)
{
	m_LocalMat.CreateRotateX(rx);
	m_LocalMat.RotateY(ry);
	m_LocalMat.Move_Local(0, 0, camZoom);
	m_BaseMat.CreateMove(0, 0, 0);
	m_IsFPSMode = isFpsMode;
	UpdateFrustumPlanes();
}

void GameCamera::Init(const ZVec3 & BaseTrans, const ZVec3 & LocalTrans, bool isFpsMode)
{
	m_BaseMat.CreateMove(BaseTrans);
	m_LocalTransMat.CreateMove(LocalTrans);
	m_IsFPSMode = isFpsMode;
	UpdateFrustumPlanes();

	INPUT.SetFPSMode(APP.m_Window->GetWindowHandle(), m_IsFPSMode);
}

void GameCamera::Update()
{
	// カメラ操作	
	if (APP.m_Window->IsWindowActive() && m_IsFPSMode)
	{
		// 回転
		{
			POINT pt = INPUT.GetMouseMoveValue();

			m_RotAngle.x += pt.y*m_RotRatio;
			m_RotAngle.y += pt.x*m_RotRatio;

			// 角度制限
			if (m_RotAngle.x > m_XRotAngleLimit.x) { m_RotAngle.x = m_XRotAngleLimit.x; }
			if (m_RotAngle.x < m_XRotAngleLimit.y) { m_RotAngle.x = m_XRotAngleLimit.y; }

			// 回転行列
			ZMatrix m_LocalRot;
			ZMatrix mx, my;
			mx.CreateRotateX(m_RotAngle.x);
			my.CreateRotateY(m_RotAngle.y);
			m_LocalRot = mx * my;

			m_LocalMat = m_LocalTransMat * m_LocalRot;

		}

	}

	// 視錐台平面更新
	UpdateFrustumPlanes();

#if EditorCam
	// 移動量調整用
	float ratio = 1.0f;
	if (GetAsyncKeyState(VK_CONTROL) & 0x8000)
		ratio *= 0.2f;
	else if (GetAsyncKeyState(VK_SHIFT) & 0x8000)
		ratio *= 3.0f;

	// カメラ操作
	// FPSモードなら
	if (m_IsFPSMode) {
		// FPSモードはズーム移動は無効
		m_LocalMat.SetPos(0, 0, 0);

		// ウィンドウがアクティブなら
		if (APP.m_Window->IsWindowActive()) {
			POINT pt = INPUT.GetMouseMoveValue();

			// ホイールドラッグ
			if (INPUT.MouseStay(ZInput::BUTTON_MID)) {
				// 位置移動
				m_BaseMat.GetPos() -= mCam.GetXAxis()* (float)pt.x* 0.05f* ratio;
				m_BaseMat.GetPos() += mCam.GetYAxis()* (float)pt.y* 0.05f* ratio;
			}
			else {
				// 視点回転
				m_LocalMat.RotateAxis(m_LocalMat.GetXAxis(), pt.y*0.5f);
				m_LocalMat.RotateY(pt.x*0.5f);
			}

		}

		// ウィンドウのアクティブ状態により切り替え
		if (m_IsFPSMode) {
			if (APP.m_Window->IsWindowActive())
				INPUT.SetFPSMode(APP.m_Window->GetWindowHandle(), true);
			else
				INPUT.SetFPSMode(APP.m_Window->GetWindowHandle(), false);
		}

	}
	// 通常モード
	else {
		if (APP.m_Window->IsWindowActive()) {
			// マウス右ボタン
			if (INPUT.MouseStay(ZInput::BUTTON_R)) {
				POINT pt = INPUT.GetMouseMoveValue();

				m_LocalMat.RotateAxis(m_LocalMat.GetXAxis(), pt.y*0.5f);
				m_LocalMat.RotateY(pt.x* 0.5f);
			}

			// マウスホイール
			{
				// ローカルZ方向に移動
				m_LocalMat.Move_Local(0, 0, INPUT.GetMouseWheel()* 0.002f*ratio);

				// 距離制限
				float len = m_LocalMat.GetPos().Length(); // 距離
				float d = m_LocalMat.GetZAxis().Dot(m_LocalMat.GetPos()); // Z方向と座標の内積で原点を通り過ぎているか判定
				if (d > 0)
					len *= -1; // ズームしすぎているので距離はマイナスにする

				if (len < 0.03f)
					m_LocalMat.Move_Local(0, 0, len - 0.03f);

			}

			// ホイールドラッグ
			if (INPUT.MouseStay(ZInput::BUTTON_MID)) {
				POINT pt = INPUT.GetMouseMoveValue();
				m_BaseMat.GetPos() -= mCam.GetXAxis()* (float)pt.x*0.05f*ratio;
				m_BaseMat.GetPos() += mCam.GetYAxis()* (float)pt.y*0.05f*ratio;
			}

		}

		INPUT.SetFPSMode(APP.m_Window->GetWindowHandle(), false);

	}
#else

		// カメラ操作	
		if (APP.m_Window->IsWindowActive() && m_IsFPSMode) {
			// 回転
			{
				POINT pt = INPUT.GetMouseMoveValue();

				m_RotAngle.x += pt.y*m_RotRatio;
				m_RotAngle.y += pt.x*m_RotRatio;

				// 角度制限
				if (m_RotAngle.x > m_XRotAngleLimit.x) { m_RotAngle.x = m_XRotAngleLimit.x; }
				if (m_RotAngle.x < m_XRotAngleLimit.y) { m_RotAngle.x = m_XRotAngleLimit.y; }

				// 回転行列
				ZMatrix m_LocalRot;
				ZMatrix mx, my;
				mx.CreateRotateX(m_RotAngle.x);
				my.CreateRotateY(m_RotAngle.y);

				m_LocalRot = mx * my;

				m_LocalMat = m_LocalTransMat * m_LocalRot;
			}

			}
#endif
}

void GameCamera::SetCamera()
{
#if EditorCam
	// カメラ設定
	// 射影行列設定
	SetProj(mProj);

	// 最終的なカメラ行列を求める
	mCam = m_LocalMat * m_BaseMat;

	// カメラ行列からビュー行列を作成
	CameraToView();

	ZCamera::LastCam = *this;

	// シェーダー側の定数バッファに書き込む
	ShMgr.UpdateCamera(this);
#else
	// カメラ設定
	// 射影行列設定
	//SetProj(mProj);

	// 最終的なカメラ行列を求める
	//mCam = m_LocalMat * m_BaseMat;

	// カメラ行列からビュー行列を作成
	CameraToView();

	ZCamera::LastCam = *this;

	// シェーダー側の定数バッファに書き込む
	ShMgr.UpdateCamera(this);
#endif
}