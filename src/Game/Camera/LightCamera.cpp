#include "Game/Application.h"
#include "LightCamera.h"

LightCamera::LightCamera()
{
	SetOrthoLH(ProjW, ProjH, Near, Far);
	mCam.CreateMove(0, 0, 0);

	m_TarPos.Set(ZVec3::Zero);
	m_Direction.Set(ZVec3::Up);
	m_Distance = 50.f;
}

void LightCamera::Update()
{
	SetOrthoLH(ProjW, ProjH, Near, Far);
	m_TarPos.y = 0.f;
	mCam.CreateMove(m_TarPos + (m_Direction*m_Distance));
	mView.CreateViewMatrixPos(mCam.GetPos(), m_TarPos);
}

void LightCamera::ImGui()
{

	auto imGuiFunc = [this]
	{
		if (ImGui::Begin("DirLightCamParam") == false) {
			ImGui::End();
			return;
		}

		ImGui::DragFloat("Near", &Near, 1.f, 0.1f, 100.f);
		ImGui::DragFloat("Far", &Far, 1.f, 10.f, 1000.f);

		ImGui::DragFloat("Width", &ProjW, 10.f, 10.f, 2000.f);
		ImGui::DragFloat("Height", &ProjH, 10.f, 10.f, 2000.f);

		ImGui::DragFloat("Distance", &m_Distance, 1.f, 1.f, 500.f);
		ImGui::End();
	};

	DW_IMGUI_FUNC(imGuiFunc);
}
