Texture2D		MeshTex	: register(t0);
SamplerState	WrapSmp : register(s0);


cbuffer cbObject : register(b0)
{
    row_major float4x4 g_mW;
};

cbuffer cbBones : register(b2)
{
    row_major float4x4 g_mBones[2]	: packoffset(c0); 
    row_major float4x4 g_mBoneEnd	: packoffset(c1023);
};

struct VsDepthIn
{
    float4 pos	: POSITION; 
    float2 uv	: TEXCOORD0;
};

struct VsDepthInstIn
{
    float4 pos	: POSITION; 
    float2 uv	: TEXCOORD0;
	row_major float4x4 instanceMat : MATRIX;	// ワールド行列
};

struct VsDepthSkinIn
{
	float4 pos		: POSITION;
	float2 uv		: TEXCOORD0;
	float3 tangent	: TANGENT;
	float3 binormal : BINORMAL;
	float3 normal	: NORMAL;
	float4 blendWeight	: BLENDWEIGHT;	// ボーンウェイトx4
	uint4 blendIndex	: BLENDINDICES; // ボーン番号x4
};


struct VsDepthOut
{
    float4 pos		: SV_Position; 
    float2 uv		: TEXCOORD0;
    float2 Depth	: TEXCOORD1;  
};

typedef VsDepthOut PsDepthIn;


struct DepthOut
{
    float4 Depth : SV_Target0;
};