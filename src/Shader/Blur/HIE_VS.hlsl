#include "inc_Blur.hlsli"

VsOut main(VsIn In)
{
    VsOut Out = (VsOut) 0;
    Out.pos = mul(In.pos, g2DView);
    Out.uv = In.uv;
	return Out;
}