#include "Game/Application.h"
#include "LightManager.h"

void LightManager::Init()
{
	// シェーダ用の定数バッファ作成
	m_cb12_Light.Create(12);

	m_Cam = Make_Shared(LightCamera,appnew);

}

void LightManager::Release()
{
	m_DirLight = nullptr;
	m_cb12_Light.Release();
	m_Cam = nullptr;
}

void LightManager::Update()
{
	//-----------------------------------
	// 環境光
	//-----------------------------------
	m_cb12_Light.m_Data.AmbientLight.Set(m_AmbientLight, 0);

	//-----------------------------------
	// 平行光源
	//-----------------------------------
	m_cb12_Light.m_Data.DL.Color = m_DirLight->Diffuse * m_DirLight->Intensity;
	m_cb12_Light.m_Data.DL.Dir = m_DirLight->Direction;
	m_cb12_Light.m_Data.DL.TexelSize.x = 1.f / ShMgr.m_LightDepthTexSize.x;
	m_cb12_Light.m_Data.DL.TexelSize.y = 1.f / ShMgr.m_LightDepthTexSize.y;




	// ポイントライト
	//  無効になってるライトは削除する
	{
		auto it = m_PointLightList.begin();
		while (it != m_PointLightList.end())
		{
			if ((*it).IsActive() == false)
				it = m_PointLightList.erase(it);
			else
				++it;
		}
	}

	// 定数バッファに書き込み
	m_cb12_Light.m_Data.PL_Cnt = 0;
	for (auto& light : ShMgr.m_LightMgr.m_PointLightList)
	{
		int idx = m_cb12_Light.m_Data.PL_Cnt;
		if (idx >= ShMgr.m_LightMgr.MAX_POINTLIGHT)
			break;	// 限度数以上なら終了

		m_cb12_Light.m_Data.PL[idx].Color = light.Lock()->Diffuse* light.Lock()->Intensity;
		m_cb12_Light.m_Data.PL[idx].Pos = light.Lock()->Position;
		m_cb12_Light.m_Data.PL[idx].Radius = light.Lock()->Radius;

		m_cb12_Light.m_Data.PL_Cnt++;

	}

	// スポットライト
	//  無効になってるライトは削除する
	{
		auto it = m_SpotLightList.begin();
		while (it != m_SpotLightList.end())
		{
			if ((*it).IsActive() == false)
				it = m_SpotLightList.erase(it);
			else
				++it;
		}
	}

	// 定数バッファに書き込み
	m_cb12_Light.m_Data.SL_Cnt = 0;
	for (auto& light : ShMgr.m_LightMgr.m_SpotLightList)
	{
		int idx = m_cb12_Light.m_Data.SL_Cnt;
		
		// 限度数以上なら終了
		if (idx >= ShMgr.m_LightMgr.MAX_POINTLIGHT)
			break;

		m_cb12_Light.m_Data.SL[idx].Color = light.Lock()->Diffuse* light.Lock()->Intensity;
		m_cb12_Light.m_Data.SL[idx].Pos = light.Lock()->Position;
		m_cb12_Light.m_Data.SL[idx].Range = light.Lock()->Range;
		m_cb12_Light.m_Data.SL[idx].Dir = light.Lock()->Direction;
		m_cb12_Light.m_Data.SL[idx].Dir.Normalize();
		m_cb12_Light.m_Data.SL[idx].MinAng = ToRadian(light.Lock()->MinAngle);
		m_cb12_Light.m_Data.SL[idx].MaxAng = ToRadian(light.Lock()->MaxAngle);

		m_cb12_Light.m_Data.SL_Cnt++;
	}




	//	カメラ更新
	m_Cam->SetCameraDirection(m_DirLight->Direction);
	m_Cam->Update();
	m_cb12_Light.m_Data.sVP = m_Cam->mView * m_Cam->mProj;










	// 実際に定数バッファへ書き込み
	m_cb12_Light.WriteData();

	m_cb12_Light.SetVS();		// 頂点シェーダパイプラインへセット
	m_cb12_Light.SetPS();		// ピクセルシェーダパイプラインへセット
	m_cb12_Light.SetGS();		// ジオメトリシェーダパイプラインへセット

}

void LightManager::ImGui()
{
	auto imGuiFunc = [this]
	{
		if (ImGui::Begin("Light") == false)
		{
			ImGui::End();
			return;
		}
		ImGui::DragFloat3("DirLightVec", m_DirLight->Direction, 0.1f,-1.f, 1.f);

		ImGui::End();
	};

	DW_IMGUI_FUNC(imGuiFunc);


	GetLightCamera()->ImGui();
}
