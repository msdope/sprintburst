#ifndef __SHADER_MANAGER_H__
#define __SHADER_MANAGER_H__

#include "LightManager.h"

#include "Model/ModelRenderers.h"
#include "Game/System/Renderer/LineRenderer/LineRenderer.h"

#include "Game/System/Renderer/SpriteRenderer/SpriteRenderer.h"
#include "Game/System/Renderer/Blur/BlurShader.h"
#include "Game/System/Renderer/DOF/DOF_Shader.h"
#include "Game/System/Renderer/X-Ray/X-Ray.h"

/*------------------------------------------------------------------*/
// 定数バッファ
/*------------------------------------------------------------------*/
//  cbuffer(b0〜b8)		… 自由

//	cbuffer(b9)			…（固定）DoFで使用
//  cbuffer(b10)		… (固定)シャドウマップで使用予定
//  cbuffer(b11)		… (固定)カメラデータで使用
//  cbuffer(b12)		… (固定)ライトデータで使用
//  cbuffer(b13)		… (固定)デバッグ用で使用予定

/*------------------------------------------------------------------*/
// サンプラステート
/*------------------------------------------------------------------*/
//  s0					… Linear補間のWrapサンプラ
//  s1					… Linear補間のClampサンプラ
//  s2					… Point補間のWrapサンプラ
//  s3					… Point補間のClampサンプラ
//  s4〜9				… 固定用の予約
//  s10					… ShadowMap用 Comparisonサンプラ

//  s11〜s15			… 自由

struct ShaderManager
{
public:
	// カメラ用定数バッファ
	struct cbCamera
	{
		ZMatrix	mV;		// ビュー行列
		ZMatrix	mP;		// 射影行列
		ZMatrix	mPO;	// 射影行列
		ZVec3 CamPos;	// カメラの座標
		float tmp;		// ※パッキング規則のため
		float Near;
		float tmp2[3];
		float Far;
		float tmp3[3];
	};

	// デバッグ用定数バッファ
	struct cbDebug
	{
	public:
		cbDebug()
		{
			showCascadeDist = 0;
		}

	public:
		int showCascadeDist;
		float tmp[3];

	};

public:
	ShaderManager() : m_Ls(20000)
	{
	}

	void Init();
	void Release();

	// シェーダ(定数バッファ)のカメラのデータを更新
	void UpdateCamera(const ZCamera* camera = &ZCamera::LastCam) {
	
		if (camera == nullptr)return;
	
		// カメラ情報
		m_cb11_Camera.m_Data.mV		= camera->mView;			// ビュー行列
		m_cb11_Camera.m_Data.mP		= camera->mProj;			// 射影行列
		m_cb11_Camera.m_Data.CamPos = camera->mCam.GetPos();	// カメラ座標
		m_cb11_Camera.m_Data.Near	= camera->Near;				// カメラ距離
		m_cb11_Camera.m_Data.Far	= camera->Far;				// カメラ距離
		m_cb11_Camera.m_Data.mPO	= camera->mOrtho;			// 射影行列
	
		m_cb11_Camera.WriteData();	// 定数バッファへ書き込む
	
		m_cb11_Camera.SetVS();		// 頂点シェーダパイプラインへセット
		m_cb11_Camera.SetPS();		// ピクセルシェーダパイプラインへセット
		m_cb11_Camera.SetGS();		// ジオメトリシェーダパイプラインへセット
	}

	//	シャドウカメラの起点座標
	void SetShadowCamTargetPoint(const ZVec3& pos){
		m_LightMgr.GetLightCamera()->SetTargetPos(pos);
	}


public:
	// ライト
	LightManager	m_LightMgr;
	
	// ステート
	ZBlendState		m_bsAlpha;				// 半透明合成モード
	ZBlendState		m_bsAdd;				// 加算合成モード
	ZBlendState		m_bsNoAlpha;			// アルファ無効モード
	ZBlendState		m_bsAlpha_AtoC;			// 半透明合成モード(AlphaToCoverage付き)

	// サンプラ
	ZSamplerState	m_smp0_Linear_Wrap;		// s0 Linear補間& Wrapモード用サンプラ
	ZSamplerState	m_smp1_Linear_Clamp;	// s1 Linear補間& Clampモード用サンプラ
	ZSamplerState	m_smp2_Point_Wrap;		// s2 Point補間& Wrapモード用サンプラ
	ZSamplerState	m_smp3_Point_Clamp;		// s3 Point補間& Clampモード用サンプラ

	ZSamplerState m_smp10_Shadow;			// シャドウマップ用サンプラ
	
	// カメラ用定数バッファ
	ZConstantBuffer<cbCamera>	m_cb11_Camera;

	// デバッグ用定数バッファ
	ZConstantBuffer<cbDebug>	m_cb13_Debug;

	// シェーダクラス
	ModelRenderers	m_Ms;	// モデルレンダラー
	LineRenderer	m_Ls;	// ラインレンダラー
	SpriteRenderer	m_Ss;	//	スプライト
	BlurShader		m_Blur;	//	ブラー
	

	// その他
	ZTexture m_texCube;	// キューブマップテクスチャ(環境マッピング用)

	//	シャドウマップテクスチャ
	ZSP<ZTexture>		m_LightScene;
	ZSP<ZTexture>		m_LightDepth;
	ZVec2				m_LightDepthTexSize;


	ZUnorderedMap<ZString, ZWP<ZVertexShader>>		m_VertexShaders;
	ZUnorderedMap<ZString, ZWP<ZPixelShader>>		m_PixelShaders;

};


#endif