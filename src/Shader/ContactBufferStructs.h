#ifndef __CONTACT_BUFFER_STRUCTS_H__
#define __CONTACT_BUFFER_STRUCTS_H__

// オブジェクト単位での設定データ(行列のみ)
// インスタンシングを使用する際は行列を定数バッファにセットする必要がない
// そのため↓のオブジェクト単位の設定データとは切り離して無駄なデータ転送を避ける
struct cbPerObject_Matrix
{
	ZMatrix Mat;
};

// オブジェクト単位での設定データ(スタティックメッシュ用)
struct cbPerObject_STModel
{
public:
	cbPerObject_STModel()
	{
		MulColor = ZVec4::one;
		LightEnable = 1;
		DistanceFogEnable = 0;
		DistanceFogColor = ZVec3::One;
		DistanceFogDensity = 0.02f;
		X_Col.Set(ZVec4::zero);
	}

public:
	ZVec4 MulColor;				// 無条件で合成する色 強制的に色を変更する場合に使用
	int LightEnable;			// ライト有効/無効
	int DistanceFogEnable;		// 距離フォグ有効/無効
	float tmp[2];				// パッキング規則のためゴミを入れる
	ZVec3 DistanceFogColor;		// 距離フォグ色
	float DistanceFogDensity;	// 距離フォグ密度
	ZVec4 X_Col;
};

// マテリアル単位での設定データ
struct cbPerMaterial
{
public:
	cbPerMaterial()
	{
		Diffuse = ZVec4::one;
		Specular = ZVec4::one;
		SpePower = 0;
		Emissive = ZVec4::zero;
		ParallaxVal = 0;
	}

public:
	ZVec4 Diffuse;		// 基本色
	ZVec4 Specular;		// スペキュラ色
	float SpePower;		// スペキュラの強さ
	float tmp[3];	// パッキング規則のためゴミを入れる
	ZVec4 Emissive;		// エミッシブ
	float ParallaxVal;	// 視差度
	float tmp3[3];	// パッキング規則のためゴミを入れる
};

//	変換用行列
struct cbView
{
	ZMatrix view;
};

//	ガウス用バッファ
struct cbGauss
{
	float weights[8];
	ZVec2 Offset;
	ZVec2 Size;
	ZVec2 Texel;
	float tmp[2];
};

//	変換用行列
struct cbXRay
{
	ZMatrix view;
	ZVec4	xCol;
	//		int FresnelEnable = 0;	//	Todo:フレネル反射を入れるときは、フレネル用のステートを他にも追加すること
	cbXRay()
	{
		xCol.Set(1, 0, 0, 1);
	}
};

#endif