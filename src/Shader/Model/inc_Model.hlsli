/*=====================================================================*/
// [定数バッファ]オブジェクト単位データ
// packoffsetとは … この定数バッファ内のデータ位置を自分で指定したい場合に使用する。省略した場合は自動で決定される。
/*=====================================================================*/
cbuffer cbMatrix : register(b0)
{
	row_major float4x4	g_mW;       	// ワールド行列
};

cbuffer cbPerObject : register(b1)
{
	// 
    float4 g_MulColor; // 無条件に合成する色 強制的に色を変更したい場合に使用

	// ライト
    int g_LightEnable; // ライティング有効/無効フラグ

	// フォグ
    int g_DistanceFogEnable; // フォグ有効/無効フラグ
    float3 g_DistanceFogColor; // フォグ色
    float g_DistanceFogDensity; // フォグ減衰率

	// XRay用
    float4 g_XColor;
};

/*=====================================================================*/
// [定数バッファ]マテリアル単位データ
/*=====================================================================*/
cbuffer cbPerMaterial : register(b2)
{
    float4 g_Diffuse; // 拡散色(基本色)
    float4 g_Specular; // スペキュラ(反射)
    float g_SpePower; // スペキュラの鋭さ
    float4 g_Emissive; // エミッシブ(自己発光)

    float g_ParallaxVal; // 視差度
};

/*=====================================================================*/
// [定数バッファ]オブジェクト単位データ(スキンメッシュ用)
/*=====================================================================*/
cbuffer cbBones : register(b3)
{

//	row_major float4x4 g_mBones[1024]; // ボーン用行列配列

	// ※ボーン数により、セットされる定数バッファのサイズが変わるので、上記だと警告が出てうざい。
	// 下記のようにするとDebug時の警告を消せる。
	// 参考:http://zerogram.info/?p=860
    row_major float4x4 g_mBones[2] : packoffset(c0); // ボーン用行列配列
    row_major float4x4 g_mBoneEnd : packoffset(c1023);
};

/*=====================================================================*/
//	テクスチャ
//  最大128(t0〜t127)
/*=====================================================================*/

Texture2D MeshTex : register(t0); // メッシュの通常テクスチャ(ディフューズテクスチャ)
Texture2D EmissiveTex : register(t1); // 発光テクスチャ
Texture2D ToonTex : register(t2); // トゥーンテクスチャ(陰影用)
Texture2D MaterialSphereTex : register(t3); // 材質用加算スフィアマップ(MMDっぽい表現をするため)
Texture2D SphereParamTex : register(t4); // 映り込み(スフィア環境)マップ
Texture2D SpecularTex : register(t5); // スペキュラマップ
Texture2D MulSphereTex : register(t6); // 材質用乗算スフィアマップ(MMDモデル用)


/*=====================================================================*/

//	ノーマルテクスチャのアルファ値にハイトマップが仕込んである場合はこれでいいが、そうでない場合は別にハイトテクスチャを渡す必要がある

Texture2D NormalTex : register(t10); // 法線マップテクスチャ

Texture2D ShadowTex : register(t19); // シャドウマップ
/*=====================================================================*/

TextureCube CubeTex : register(t20); // キューブ環境マップ


/*=====================================================================*/
//	サンプラ
//  最大16(s0〜s15)
/*=====================================================================*/

SamplerState WrapSmp : register(s0); // Wrap用のサンプラ
SamplerState ClampSmp : register(s1); // Clamp用のサンプラ


SamplerComparisonState ShadowMapSmpCmp : register(s10); // ShadowMap用の比較サンプラ

/*=====================================================================*/
//	ピクセルシェーダ
/*=====================================================================*/

//ピクセルシェーダ出力データ
struct PsOut
{
    float4 Color : SV_Target0; // 通常色
};

//	キャラクター用のピクセル出力
struct PsCharaOut
{
    float4 Diffuse : SV_Target0;
    float4 CameraDepth : SV_Target1;
    float4 CharaDepth : SV_Target2;
    float4 X_RayPow : SV_Target3;
};

//	ステージオブジェクトのピクセル出力
struct PsStageOut
{
    float4 Diffuse : SV_Target0;
    float4 CameraDepth : SV_Target1;
};


float CalcSpecular_BlinnPhone(float3 lightDir, float3 normal, float3 vCam);

float CalcSpecular_Phone(float3 lightDir, float3 normal, float3 vCam);

void CalcLighting(float3 wPos, float3 normal, float3 vCam, inout float3 LightCol, inout float3 LightSpe);