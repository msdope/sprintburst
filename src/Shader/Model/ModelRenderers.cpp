#include "Game/Application.h"
#include "Game/System/CommonECSComponents/CommonComponents.h"
#include "Shader/ShaderManager.h"

bool ModelRenderers::Init()
{
	Release();

	bool successed;

	m_SMeshRenderer					= Make_Shared(StaticMeshRenderer,sysnew);
	m_SkinMeshRenderer				= Make_Shared(SkinMeshRenderer,sysnew);
	m_DebugColliderMeshRenderer		= Make_Shared(DebugColliderMeshRenderer,sysnew);
	m_StaticModelRenderer			= Make_Shared(StaticModelRenderer,sysnew);
	m_InstSModelRenderer			= Make_Shared(InstancingStaticModelRenderer, sysnew);

	successed  = m_SMeshRenderer->Init("Shader/Model_VS.cso", "Shader/Model_PS.cso");
	successed &= m_SkinMeshRenderer->Init("Shader/Model_SkinVS.cso", "Shader/Model_PS.cso");
	successed &= m_DebugColliderMeshRenderer->Init("Shader/Model_VS.cso", "Shader/Model_MonoPS.cso");
	successed &= m_StaticModelRenderer->Init("Shader/Model_VS.cso", "Shader/Model_PS.cso");
	successed &= m_InstSModelRenderer->Init("Shader/Model_InstancingVS.cso", "Shader/Model_PS.cso");

	m_RenderTargets = Make_Shared(ZTextureSet,appnew,RenderTargetCnt);
	DXGI_FORMAT	fmt[] =
	{
		DXGI_FORMAT_R16G16B16A16_FLOAT,
		DXGI_FORMAT_R32_FLOAT,
		DXGI_FORMAT_R32_FLOAT,
		DXGI_FORMAT_R16G16B16A16_FLOAT,
	};
	m_RenderTargets->CreateRTSet(APP.m_Window->GetWidth(), APP.m_Window->GetHeight(), RenderTargetCnt, fmt);

	m_cb9_PerMaterial.Create(9);
	m_cb9_PerMaterial.SetPS();
	m_cb9_PerMaterial.SetVS();
	m_cb9_PerMaterial.SetGS();
	return successed;
}

void ModelRenderers::Release()
{
	m_SMeshRenderer = nullptr;
	m_SkinMeshRenderer = nullptr;
	m_DebugColliderMeshRenderer = nullptr;
	m_StaticModelRenderer = nullptr;
	m_InstSModelRenderer = nullptr;

	m_cb9_PerMaterial.Release();

	m_RenderTargets = nullptr;
}

void ModelRenderers::Begin3DRendering()
{
	m_BackUp.GetNowAll();

	m_RenderTargets->AllClearRT(ZVec4(0,0,0,1));
	m_RenderTargets->GetTex(1)->ClearRT(ZVec4(1));

	m_RT.GetNowTop();

	for (int cnt = 0; cnt < m_RenderTargets->GetListSize(); cnt++)
		m_RT.RT(cnt, m_RenderTargets->GetTex(cnt)->GetRTTex());

	m_RT.SetToDevice();
}

void ModelRenderers::End3DRendering()
{
	m_BackUp.SetToDevice();
}

void ModelRenderers::Draw()
{
	ShMgr.m_LightScene->SetTexturePS(19);

	if (m_ZPreFlg)
	{
		m_SkinMeshRenderer->Flash();
		Z_Prepass();
	}
	else
	{
		m_SkinMeshRenderer->Flash();
	}
	
	m_StaticModelRenderer->Flash();
	m_InstSModelRenderer->Flash();

	ZDx.GetWhiteTex()->SetTexturePS(19);
}

void ModelRenderers::DrawShadow()
{
	ZRenderTarget_BackUpper b;

	ShMgr.m_LightScene->ClearRT(ZVec4(1, 1, 1, 1));
	ShMgr.m_LightDepth->ClearDepth();


	ZRenderTargets rt;
	rt.RT(0, ShMgr.m_LightScene->GetRTTex());
	rt.Depth(ShMgr.m_LightDepth->GetDepthTex());
	
	ZDx.SetViewport(ShMgr.m_LightDepthTexSize.x, ShMgr.m_LightDepthTexSize.y);	
	rt.SetToDevice();

	m_InstSModelRenderer->Shadow();
	m_StaticModelRenderer->Shadow();
	m_SkinMeshRenderer->Shadow();

}

void ModelRenderers::Z_Prepass() 
{
	m_StaticModelRenderer->Z_Prepass();
	m_InstSModelRenderer->Z_Prepass();
}

void ModelRenderers::SetConstBuffers()
{

	m_cb9_PerMaterial.WriteData();

	//...

}

void ModelRenderers::ImGui()
{
#if _DEBUG

	static const ZString title[] =
	{
		"BasePass",
		"CameraDepth",
		"CharaDepth",
		"RefPow"
	};

	auto Targets = [this]
	{
		auto dockID = ImGui::GetID("RenderTextures_Tab");
		if (ImGui::Begin("RenderTextures") == false)
		{
			ImGui::End();
			return;
		}
		ImGui::Checkbox("Use::Z-PrePass", &m_ZPreFlg);
		ImGui::DockSpace(dockID);
		ImGui::End();

		ImGui::SetNextWindowDockId(dockID, ImGuiCond_FirstUseEver);
		if (ImGui::Begin("LightDepth"))
		{
			if (ShMgr.m_LightScene->GetTex())
				ImGui::Image(ShMgr.m_LightScene->GetTex(), ImGui::GetWindowSize());
		}
		ImGui::End();

		for (int i = 0; i < m_RenderTargets->GetListSize(); i++)
		{
			ImGui::SetNextWindowDockId(dockID, ImGuiCond_FirstUseEver);
			if (ImGui::Begin(title[i].c_str()))
			{
				if (m_RenderTargets->GetTex(i)->GetTex())
					ImGui::Image(m_RenderTargets->GetTex(i)->GetTex(), ImGui::GetWindowSize());
			}
			ImGui::End();
		}

	};

	DW_IMGUI_FUNC(Targets);

#endif

}