#include "inc_Model.hlsli"
#include "../inc_CommonLayout.hlsli"
// ライト定数バッファ
#include "../inc_LightCB.hlsli"

// カメラ定数バッファ
#include "../inc_CameraCB.hlsli"




PsStageOut main(PsIn In)
{

	// このピクセルからカメラへの方向
    float3 vCam = normalize(g_CamPos - In.wPos);
	

	//============================================================
	// 法線マップ
	//============================================================
    row_major float3x3 mTBN =
    {
        normalize(In.wT), // X軸
		normalize(In.wB), // Y軸
		normalize(In.wN) // Z軸
    };

	// 視差マップ用
    row_major float3x3 mInvTBN = transpose(mTBN);
    float3 vCamLocal = mul(vCam, mInvTBN);

	In.UV += g_ParallaxVal * NormalTex.Sample(WrapSmp, In.UV).a * vCamLocal.xy;
	
    //法線マップ取得
    float4 normal = NormalTex.Sample(WrapSmp, In.UV);
    normal.xyz = normal.xyz * 2.0 - 1.0; // 0〜1から-1〜1へ変換
	float3 w_normal = normalize(mul(normal.xyz, mTBN)); // ローカルな法線を行列で変換(面の向きを考慮したことになる)

	
    float4 MateCol = 1; // 自分(材質)の色(ライトは含まない)　DiffuseとTextureと環境マップなど

	//============================================================
	// 材質の色
	//============================================================
    float4 texCol = MeshTex.Sample(WrapSmp, In.UV);
    MateCol			= g_Diffuse * texCol * In.MulColor;
    //MateCol = g_Diffuse * texCol;
	//============================================================
	// アルファテスト
    clip(MateCol.a - 0.01f);
	
	//============================================================
	// キューブ環境マップ(映り込み)
	//============================================================
	{
        float4 SphParam = SphereParamTex.Sample(WrapSmp, In.UV);
        float3 vRef = reflect(-vCam, w_normal);
        float4 envCol = CubeTex.Sample(ClampSmp, vRef);

		// 合成(ブレンド)
        MateCol.rgb = lerp(MateCol.rgb, envCol.rgb, SphParam.r);
    }
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//
	//
	// ライトの色関係(ライティング)
	//
	//
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// スペキュラマップのデータを取得
    float4 SpeMap = SpecularTex.Sample(WrapSmp, In.UV);

    float3 LightCol = 0;
    float3 LightSpe = 0;

    if (g_LightEnable)
    {
        CalcLighting(In.wPos, w_normal, vCam, LightCol, LightSpe);

		//-------------------------------------------
		// (法線マップを目立たたせる工夫　必要なければ消しても良い)
		//-------------------------------------------
		{
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			//	unityっぽい影色
            float SpePow = CalcSpecular_BlinnPhone(-vCam, w_normal, vCam);
            float3 shadowColor = lerp(float3(0.f, 0.1f, 0.3f), float3(1.0f, 1.0f, 1.0f), SpePow);
            LightSpe += 0.5 * SpeMap.r * shadowColor;
	    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        }

		//-------------------------------------------
		// 環境光
		//-------------------------------------------
        LightCol.rgb += g_AmbientLight.rgb;

    }
	// ライト無効時
    else
    {
        LightCol = 1;
        LightSpe = 0;
    }
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++




	//============================================================
    //
    // マテリアル色とライト色を合成
    //
	//============================================================

    float4 FinalCol = 0;

    FinalCol.rgb = MateCol.rgb * LightCol.rgb;
    FinalCol.a = MateCol.a; // アルファ値はそのまま使用

    FinalCol.rgb += LightSpe.rgb * g_Specular.rgb * SpeMap.r;

	//============================================================
	// 自己発光色
	//============================================================
	// エミッシブマップから取得
    float4 emiCol = EmissiveTex.Sample(WrapSmp, In.UV);
    emiCol.rgb = (g_Emissive.rgb + emiCol.rgb) * 3;
    FinalCol.rgb += (g_Emissive.rgb + emiCol.rgb);

	//============================================================
	// 距離フォグ　最終的な色に対して霧効果を付ける
	//============================================================
    if (g_DistanceFogEnable)
    {
		// フォグ 1(近い)〜0(遠い)
        float f = saturate(1.0 / exp(In.wvPos.z * g_DistanceFogDensity));
        // 適用
        FinalCol.rgb = lerp(g_DistanceFogColor.rgb, FinalCol.rgb, f);
    }






	//============================================================
	// 出力色
	//============================================================
	PsStageOut Out = (PsStageOut)0;
	// 最終的な出力色
    Out.Diffuse = FinalCol;

	//	深度A
    Out.CameraDepth = In.Depth.x / In.Depth.y;
    Out.CameraDepth.a = 1;


    return Out;
}

float CalcSpecular_BlinnPhone(float3 lightDir, float3 normal, float3 vCam)
{
    float3 vHalf = normalize(vCam - lightDir);
    float SpePow = dot(normal, vHalf); // カメラの角度差(-1〜1)
    SpePow = saturate(SpePow); // マイナスは打ち消す
    SpePow = pow(SpePow, g_SpePower) * ((g_SpePower + 2) / (2 * 3.141592));
    SpePow = min(2.5, SpePow); //	まぶしすぎるの補正
    return SpePow;
}

float CalcSpecular_Phone(float3 lightDir, float3 normal, float3 vCam)
{
    float3 vRef = normalize(vCam + normal);
    float SpePow = dot(vCam, vRef); // カメラの角度差(-1〜1)
    SpePow = saturate(SpePow); // マイナスは打ち消す
    SpePow = pow(SpePow, g_SpePower + 1);

    return SpePow;
}

void CalcLighting(float3 wPos, float3 normal, float3 vCam, inout float3 LightCol, inout float3 LightSpe)
{
    const float PI = 3.141592653589f;

	//============================================================
	// 平行光源ライティング
	//============================================================
	{
    
        float LPow = dot(-g_DL.Dir, normal);
        LPow = max(0, LPow);
        LPow = LPow * 0.5f + 0.5; // ハーフランバート
        LPow = LPow * LPow;
		
		//============================================================
		// トゥーン
		//  ライティング結果の光の強さを元にトゥーンテクスチャから、カスタマイズされた光の強さを取得
		//============================================================
        float4 hToonCol = ToonTex.Sample(ClampSmp, float2(LPow, 0.5)); // 横
        float4 vToonCol = ToonTex.Sample(ClampSmp, float2(0.5, LPow)); // 縦

        float4 ToonCol = min(hToonCol, vToonCol);
		//============================================================
		// シャドウマップでの光の遮断判定
		//============================================================

		//	シャドウ変換
        float4 LiPos = mul(float4(wPos, 1), LightVP);
        LiPos.xyz = LiPos.xyz / LiPos.w;
        float S_Pow = 1.f;
        float z = LiPos.z - 0.004;

		// テクスチャ内判定
        if (abs(LiPos.x) <= 1 && abs(LiPos.y) <= 1 && LiPos.z <= 1)
        {
			// uv計算
            float2 uv = LiPos.xy * 0.5 + 0.5;
            uv.y = 1.0f - uv.y;

            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    S_Pow +=
						ShadowTex.SampleCmpLevelZero(ShadowMapSmpCmp, float2(uv.x + i * g_DL.TexelSize.x, uv.y + j * g_DL.TexelSize.y), z).r;
                }
            }

            S_Pow /= 9; // 平均値にしてぼかす			
            S_Pow = saturate(S_Pow); // 0〜1に収める
        }
		


		//============================================================
		// ライト色を加算する
		//============================================================
        
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		//	unityっぽいシャドウ(色)
        // float3 shadowColor = float3(0.f, 0.1f, 0.3f);
        float3 shadowColor = lerp(float3(0.f, 0.1f, 0.4f), float3(1.0f, 1.0f, 1.0f), S_Pow);
        LightCol.rgb += g_DL.Color.rgb * ToonCol.rgb * shadowColor;
		//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		//============================================================
		// スペキュラ(フォン鏡面反射)
		//============================================================
		// スペキュラ計算
        float SpePow = CalcSpecular_BlinnPhone(g_DL.Dir, normal, vCam);
		// ライトスペキュラ色を加算
//        LightSpe += g_DL.Color.rgb * SpePow;
        LightSpe += g_DL.Color.rgb * SpePow * S_Pow; //	影の中ではスペキュラ控えめ
    }


	//============================================================
	// 点光源処理(ポイントライト)
	//  ※このライトはトゥーンを使用しません
	//============================================================
	// ライト数ぶん処理
    for (int pi = 0; pi < g_PL_Cnt; pi++)
    {
		// 色
		// このピクセルと点光源の距離を調べる
        float3 vDir = g_PL[pi].Pos - wPos; // 点光源への方向
        float dist = length(vDir); // 距離

		// 範囲内
        if (dist < g_PL[pi].Radius)
        {
			// 法線とライトの方向から内積で光の強さを求める
			// 点光源への方向と法線との角度の差が明るさとなる(ランバートと同じ)
	//		vDir = normalize(vDir);     //  正規化
            vDir /= dist; //  正規化
            float PLPow = dot(normal, vDir); // 法線とライトの方向との角度 (-1〜1)
            PLPow = saturate(PLPow); // 0〜1にする

			// 半径範囲による減衰を行う  ライトの座標になればなるほど明るく、半径ぎりぎりは暗い
            float attePow = 1 - saturate(dist / g_PL[pi].Radius); // 1(近い=最大の明るさ)　〜　0(遠い=完全に暗い)
            attePow *= attePow; // 光の強さは、距離の二乗に反比例する
            PLPow *= attePow; // 減衰適用

			// ライト色を加算する
            LightCol.rgb += g_PL[pi].Color.rgb * PLPow;

			// スペキュラ
			{
                float SpePow = CalcSpecular_BlinnPhone(-vDir, normal, vCam);
                SpePow *= attePow; // 範囲減衰
				// ライトスペキュラ色を加算
                LightSpe += g_PL[pi].Color.rgb * SpePow;
            }
        }
    }

	//============================================================
	// スポットライト
	//  ※このライトはトゥーンを使用しません
	//============================================================
    for (int si = 0; si < g_SL_Cnt; si++)
    {
		// このピクセルと光源の方向・距離を調べる
        float3 vDir = g_SL[si].Pos - wPos; // 光源への方向
        float dist = length(vDir); // 距離
//		vDir = normalize(vDir); // 正規化した光源への方向
        vDir /= dist; // 正規化した光源への方向

		// スポット光源の向きの角度差を求める
        float dt = acos(saturate(dot(-vDir, g_SL[si].Dir)));

        float innerAng = (g_SL[si].MinAngle * 0.5); // 内側コーン(100%光る部分)の角度の半分
        float outerAng = (g_SL[si].MaxAngle * 0.5); // 外側コーン(減衰する部分)の角度の半分

		// コーン内かどうかの角度判定
        if (dt <= outerAng)
        {
			// 法線と光の方向から内積で光の強さを求める
            float SLPow = dot(normal, vDir);
            SLPow = max(0, SLPow);

			// 距離による減衰を行う
            float attePow = 1 - saturate(dist / g_SL[si].Range); // 1(近い=最大の明るさ)　〜　0(遠い=完全に暗い)
            attePow *= attePow; // 光の強さは、距離の二乗に反比例する
            SLPow *= attePow; // 減衰適用

			// 外側コーン角度による減衰
            SLPow *= saturate((outerAng - dt) / (outerAng - innerAng));

			// ライト色を加算する
            LightCol.rgb += g_SL[si].Color.rgb * SLPow;

			// スペキュラ
			{
				
                float SpePow = CalcSpecular_BlinnPhone(-vDir, normal, vCam);
                SpePow *= attePow; // 範囲減衰

				// ライトスペキュラ色を加算
                LightSpe += g_SL[si].Color.rgb * SpePow;

            }
        }

    }

}