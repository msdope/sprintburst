#ifndef __MODEL_RENDERERS_H__
#define __MODEL_RENDERERS_H__

#include "../ContactBufferStructs.h"

using namespace EzLib;

class ModelRenderers
{
public:
	ModelRenderers()
	{
	}
	~ModelRenderers()
	{
		Release();
	}

	#pragma region 初期化・解放

	// 初期化
	bool Init();

	// 解放
	void Release();

	#pragma endregion

	
	void Begin3DRendering();
	void End3DRendering();
	void Draw();
	void DrawShadow();
	void Z_Prepass();

	void SetConstBuffers();
	void ImGui();


public:
	ZSP<StaticMeshRenderer>				m_SMeshRenderer;
	ZSP<SkinMeshRenderer>				m_SkinMeshRenderer;
	ZSP<DebugColliderMeshRenderer>		m_DebugColliderMeshRenderer;


	//	
	ZSP<StaticModelRenderer>				m_StaticModelRenderer;
	ZSP<InstancingStaticModelRenderer>		m_InstSModelRenderer;


	//	レンダーターゲットテクスチャをまとめたもの
	ZSP<ZTextureSet>	m_RenderTargets;
	static const int	RenderTargetCnt = 4;

	ZRenderTargets			m_BackUp;
	ZRenderTargets			m_RT;


	bool m_ZPreFlg = false;


	struct DoFParam
	{
	public:
		ZVec2 FarState;		//	x:距離 y:補正係数
		ZVec2 NearState;	//	x:距離 y:補正係数
		int   UseFarBlur = 1;
		float tmp[3];
		DoFParam()
		{
			FarState.Set(1.f, 50.f);
			NearState.Set(0.05f, 50.f);
		}

	public:
		template<class Archive>
		void serialize(Archive & archive)
		{
			SaveVector2(archive, FarState, "Far");
			SaveVector2(archive, NearState, "Near");
			archive(cereal::make_nvp("UseFar", UseFarBlur));
		}

	};
	ZConstantBuffer<DoFParam> m_cb9_PerMaterial;

};

#endif