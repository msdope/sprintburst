#include "inc_Model.hlsli"
#include "../inc_CommonLayout.hlsli"

// カメラ定数バッファ
#include "../inc_CameraCB.hlsli"


//===============================================
// スタティックメッシュ用頂点シェーダー
//===============================================
VsOut main(VsStandardIn In)
{
    VsOut Out = (VsOut) 0;
	
	//-------------------------------------
	// 座標変換
	//-------------------------------------
	Out.Pos			= mul(In.pos, g_mW);    // ワールド変換
	Out.wPos		= Out.Pos.xyz;			// ワールド座標を憶えておく
	Out.Pos			= mul(Out.Pos, g_mV);   // ビュー変換

  //  Out.Depth		= mul(Out.Pos, g_mPO).zw;
    Out.Depth		= mul(Out.Pos, g_mPO).zw;

	Out.wvPos		= Out.Pos.xyz;			// ビュー座標を憶えておく

	Out.Pos			= mul(Out.Pos, g_mP);   // 射影変換
	



	//-------------------------------------
	// UVはそのまま入れる
	//-------------------------------------
	Out.UV = In.uv;

	//-------------------------------------
	// ３種の法線
	//-------------------------------------
	Out.wT			= normalize(mul(In.tangent, (float3x3)g_mW));
	Out.wB			= normalize(mul(In.binormal, (float3x3)g_mW));
	Out.wN			= normalize(mul(In.normal, (float3x3)g_mW));
	
	// 加工なしの法線
	Out.DefN		= In.normal;

	// XRayColor
	Out.XRayColor = g_XColor;
	Out.MulColor = g_MulColor;

	return Out;
}
