//#include "ShaderManager.h"
#include "Game/Application.h"

void ShaderManager::Init()
{

	// シェーダー初期化
	m_Ms.Init();
	m_Ls.Init("Shader/Line_VS.cso", "Shader/Line_PS.cso");
	m_Ss.Init("Shader/Sprite_VS.cso", "Shader/Sprite_PS.cso");
	m_Blur.Init("", "");

	// よく使うブレンドステートを作成
	// 半透明合成用ステート
	m_bsAlpha.Set_Alpha(-1);
	m_bsAlpha.Create();
	// 加算合成用ステート
	m_bsAdd.Set_Add(-1);
	m_bsAdd.Create();
	// アルファ値無効モード
	m_bsNoAlpha.Set_NoAlpha(-1, true);
	m_bsNoAlpha.Create();
	// 半透明合成用ステート(AlphaToCoverage付き)
	m_bsAlpha_AtoC.Set_Alpha(-1);
	m_bsAlpha_AtoC.Set_AlphaToCoverageEnable(true);
	m_bsAlpha_AtoC.Create();

	// よく使うサンプラ作成& セット
	m_smp0_Linear_Wrap.SetAll_Standard();
	m_smp0_Linear_Wrap.Set_Wrap();
	m_smp0_Linear_Wrap.Set_Filter_Anisotropy(4);	// 高品質な異方性フィルタリングを使用(動作を重視したい場合はコメントにしてください)
	m_smp0_Linear_Wrap.Create();
	m_smp0_Linear_Wrap.SetStateVS(0);
	m_smp0_Linear_Wrap.SetStatePS(0);

	m_smp1_Linear_Clamp.SetAll_Standard();
	m_smp1_Linear_Clamp.Set_Clamp();
	m_smp1_Linear_Clamp.Set_Filter_Anisotropy(4);	// 高品質な異方性フィルタリングを使用(動作を重視したい場合はコメントにしてください)
	m_smp1_Linear_Clamp.Create();
	m_smp1_Linear_Clamp.SetStateVS(1);
	m_smp1_Linear_Clamp.SetStatePS(1);

	m_smp2_Point_Wrap.SetAll_Standard();
	m_smp2_Point_Wrap.Set_Filter_Point();
	m_smp2_Point_Wrap.Set_Wrap();
	m_smp2_Point_Wrap.Create();
	m_smp2_Point_Wrap.SetStateVS(2);
	m_smp2_Point_Wrap.SetStatePS(2);

	m_smp3_Point_Clamp.SetAll_Standard();
	m_smp3_Point_Clamp.Set_Filter_Point();
	m_smp3_Point_Clamp.Set_Clamp();
	m_smp3_Point_Clamp.Create();
	m_smp3_Point_Clamp.SetStateVS(3);
	m_smp3_Point_Clamp.SetStatePS(3);

	//	シャドウマップ用
	m_smp10_Shadow.SetAll_Standard();
	m_smp10_Shadow.Set_Border(ZVec4(1, 1, 1, 1));
	m_smp10_Shadow.Set_ComparisonMode();
	m_smp10_Shadow.Set_MipLOD(0,0);
	m_smp10_Shadow.Create();
	m_smp10_Shadow.SetStatePS(10);
	m_smp10_Shadow.SetStateVS(10);



	// ライト管理クラス
	m_LightMgr.Init();

	// カメラ定数バッファ
	m_cb11_Camera.Create(11);
	m_cb11_Camera.SetVS();		// 頂点シェーダパイプラインへセット
	m_cb11_Camera.SetPS();		// ピクセルシェーダパイプラインへセット
	m_cb11_Camera.SetGS();		// ジオメトリシェーダパイプラインへセット

								// デバッグ用定数バッファ
	m_cb13_Debug.Create(13);
	m_cb13_Debug.SetVS();
	m_cb13_Debug.SetPS();
	m_cb13_Debug.SetGS();

	// キューブマップ
	m_texCube.LoadTexture("data/Texture/CubeMap.dds");
	m_texCube.SetTexturePS(20);


	//	シャドウマップの書き込みテクスチャ作成
	m_LightDepthTexSize.Set(1024, 1024);
	m_LightScene = Make_Shared(ZTexture,sysnew);
	m_LightScene->CreateRT((int)m_LightDepthTexSize.x, (int)m_LightDepthTexSize.y, DXGI_FORMAT_R32_FLOAT);
	m_LightDepth = Make_Shared(ZTexture,sysnew);
	m_LightDepth->CreateDepth((int)m_LightDepthTexSize.x, (int)m_LightDepthTexSize.y);

}

void ShaderManager::Release()
{

	m_LightScene = nullptr;
	m_LightDepth = nullptr;

	// シェーダクラス解放
	m_Ms.Release();
	m_Ls.Release();
	m_Ss.Release();
	m_Blur.Release();
	
	// ステート解放
	m_bsAdd.Release();
	m_bsAlpha.Release();

	// サンプラ解放
	m_smp0_Linear_Wrap.Release();
	m_smp1_Linear_Clamp.Release();
	m_smp2_Point_Wrap.Release();
	m_smp3_Point_Clamp.Release();
	m_smp10_Shadow.Release();

	// カメラ用定数バッファー解放
	m_cb11_Camera.Release();
	m_LightMgr.Release();
}