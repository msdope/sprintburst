#include "inc_Effect.hlsli"
#include "../inc_CameraCB.hlsli"

// 頂点シェーダ
VS_OUT main(	float4 pos : POSITION,  // 座標
			    float2 uv : TEXCOORD0,  // UV座標
			    float4 color : COLOR0   // 頂点色
)
{
	VS_OUT Out = (VS_OUT)0;

	// 2D変換
	Out.Pos = mul(pos, g_mW);
	Out.Pos = mul(Out.Pos, g_mV);
	Out.Pos = mul(Out.Pos, g_mP);

	// UVはそのまま入れる
	Out.UV = uv;

	// 頂点色もそのまま渡す
	Out.Color = color;

	return Out;
}
